<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 18); ?>
        .bg-interna {
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center no-repeat;
            background-size: 100% 128px;
        }
    </style>

    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>


</head>

<body class="bg-interna">


<?php require_once("../includes/topo.php") ?>

<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->
<div class="row">
    <div class="col-12  padding0 localizacao-pagina text-center">
        <h4 class="open"><?php Util::imprime($banner[legenda_1]); ?></h4>
        <?php if (isset($banner[legenda_2])) :?>
        <h4 class="open"><span><?php Util::imprime($banner[legenda_2]); ?></span></h4>
        <?php endif;?>
        <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt=""
                 height="26" width="320"></amp-img>
    </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->


<div class="row bottom30 fundo_contatos">
    <!-- ======================================================================= -->
    <!-- FORMULARIO   -->
    <!-- ======================================================================= -->
    <div class="col-12">


        <?php
        //  VERIFICO SE E PARA ENVIAR O EMAIL
        if (isset($_POST[nome])) {
            $texto_mensagem = "
        Assunto: " . ($_POST[assunto]) . " <br />
        Nome: " . ($_POST[nome]) . " <br />
        Email: " . ($_POST[email]) . " <br />
        Telefone: " . ($_POST[telefone]) . " <br />
       Celular: " . ($_POST[celular]) . " <br />


        Mensagem: <br />
        " . (nl2br($_POST[mensagem])) . "
        ";

            if (Util::envia_email($config[email], ("$_POST[nome] solicitou contato pelo site"), ($texto_mensagem),
                ($_POST[nome]), $_POST[email])) {
                Util::envia_email($config[email_copia], ("$_POST[nome] solicitou contato pelo site"), ($texto_mensagem),
                    ($_POST[nome]), $_POST[email]);
                $enviado = 'sim';
                unset($_POST);
            }


        }
        ?>


        <?php if ($enviado == 'sim'): ?>
            <div class="col-12  text-center top40">
                <h3>Email enviado com sucesso.</h3>
                <a class="btn btn_verde" href="<?php echo Util::caminho_projeto() ?>/mobile/contato">
                    ENVIE OUTRO EMAIL
                </a>
            </div>
        <?php else: ?>


            <form method="post" class="p2" action-xhr="index.php" target="_top">


                <div class="ampstart-input top10 inline-block">

                    <div class="relativo">
                        <input type="text" class="input-form input100 block border-none" name="assunto"
                               placeholder="ASSUNTO" required>
                        <span class="fa fa-user-circle form-control-feedback"></span>
                    </div>

                    <div class="relativo">
                        <input type="text" class="input-form input100 block border-none" name="nome" placeholder="NOME"
                               required>
                        <span class="fa fa-user form-control-feedback"></span>
                    </div>

                    <div class="relativo">
                        <input type="email" class="input-form input100 block border-none" name="email"
                               placeholder="EMAIL" required>
                        <span class="fa fa-envelope form-control-feedback"></span>
                    </div>

                    <div class="relativo">
                        <input type="tel" class="input-form input100 block border-none" name="telefone"
                               placeholder="TELEFONE" required>
                        <span class="fa fa-phone form-control-feedback"></span>
                    </div>

                    <div class="relativo">
                        <input type="text" class="input-form input100 block border-none" name="celular"
                               placeholder="CELULAR">
                        <span class="fa fa-mobile-phone form-control-feedback"></span>
                    </div>


                    <div class="relativo">
                        <textarea name="mensagem" placeholder="MENSAGEM"
                                  class="input-form input100 campo-textarea"></textarea>
                        <span class="fa fa-pencil form-control-feedback"></span>
                    </div>

                </div>

                <div class="col-12 padding0">
                    <div class="relativo ">
                        <button type="submit"
                                value="OK"
                                class="ampstart-btn caps btn btn_verde btn_cinza_escuro col-12">ENVIAR
                            MENSAGEM
                        </button>
                    </div>
                </div>


                <div submit-success>
                    <template type="amp-mustache">
                        Email enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
                    </template>
                </div>

                <div submit-error>
                    <template type="amp-mustache">
                        Houve um erro, {{name}} por favor tente novamente.
                    </template>
                </div>

            </form>

        <?php endif; ?>
    </div>
    <!-- ======================================================================= -->
    <!-- FORMULARIO   -->
    <!-- ======================================================================= -->

</div>

<div class="row top35">
    <div class="col-12 text-center">
        <h3>NOSSA LOCALIZAÇÃO</h3>
        <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_contato.png" alt=""
                 height="24" width="217"></amp-img>

    </div>

    <div class="col-12">
        <div class="media">
            <div class="media-left media-middle">
                <i class="fa fa-home media-object right10" aria-hidden="true"></i>
            </div>
            <div class="media-body">
                <h5 class="media-heading"><?php Util::imprime($config[endereco]);?></h5>
            </div>
        </div>
    </div>

    <div class="col-6 top20">
        <div class="media">
            <div class="media-left media-middle">
                <i class="fa fa-phone media-object right10" aria-hidden="true"></i>
            </div>
            <div class="media-body">
                <h5 class="media-heading"><span><?php Util::imprime($config[ddd1]);?> <?php Util::imprime($config[telefone1]);?></span></h5>
            </div>
        </div>
    </div>

    <?php if (!empty($config[telefone2])) :?>
    <div class="col-6 top20">
        <div class="media">
            <div class="media-left media-middle">
                <i class="fa fa-whatsapp media-object right10" aria-hidden="true"></i>
            </div>
            <div class="media-body">
                <h5 class="media-heading"><span><?php Util::imprime($config[ddd2]);?> <?php Util::imprime($config[telefone2]);?></span></h5>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <?php if (!empty($config[telefone3])) :?>
        <div class="col-6 top20">
            <div class="media">
                <div class="media-left media-middle">
                    <i class="fa fa-phone media-object right10" aria-hidden="true"></i>
                </div>
                <div class="media-body">
                    <h5 class="media-heading"><span><?php Util::imprime($config[ddd3]);?> <?php Util::imprime($config[telefone3]);?></span></h5>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if (!empty($config[telefone4]) != '') :?>
        <div class="col-6 top20">
            <div class="media">
                <div class="media-left media-middle">
                    <i class="fa fa-phone media-object right10" aria-hidden="true"></i>
                </div>
                <div class="media-body">
                    <h5 class="media-heading"><span><?php Util::imprime($config[ddd4]);?> <?php Util::imprime($config[telefone4]);?></span></h5>
                </div>
            </div>
        </div>
    <?php endif; ?>

</div>


<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->
<div class="row top5 relativo">


    <div class="col-12 padding0 text-center relativo">
        <div class="col-8 col-offset-2 btn_float">
        <a class="btn btn_cinza_escuro open  " href="<?php Util::imprime($config[link_place]); ?>" target="_blank">SAIBA COMO CHEGAR</a>
        </div>
        <amp-iframe
                width="320"
                height="199"
                layout="responsive"
                sandbox="allow-scripts allow-same-origin allow-popups"
                frameborder="0"
                src="<?php Util::imprime($config[src_place]); ?>">
        </amp-iframe>
    </div>
</div>
<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->

<?php require_once("../includes/veja.php") ?>


<?php require_once("../includes/rodape_paginas.php") ?>

</body>


</html>
