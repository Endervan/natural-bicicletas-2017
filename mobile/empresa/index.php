<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>


    <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 21); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 200px center  no-repeat;
    background-size:  100% 128px;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>


</head>

<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->
  <div class="row">
      <div class="col-12  padding0 localizacao-pagina text-center">
          <h4 class="open"><?php Util::imprime($banner[legenda_1]); ?></h4>
          <?php if (isset($banner[legenda_2])) : ?>
              <h4 class="open"><span><?php Util::imprime($banner[legenda_2]); ?></span></h4>
          <?php endif; ?>
          <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt=""
                   height="26" width="320"></amp-img>
      </div>
  </div>
  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!--   EMPRESA -->
  <!--  ==============================================================  -->
  <div class="row">
    <div class="col-12 empresa_geral top10">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa",1) ?>
    </div>
    <div class="col-12 top145">
      <div ><p><?php Util::imprime($row[descricao]); ?></p></div>
    </div>
  </div>

  <!--  ==============================================================  -->
  <!--   EMPRESA -->
  <!--  ==============================================================  -->



  <div class="row top25">
    <div class="col-12">
      <a class="btn col-12 padding0 btn_escuro"
      on="tap:my-lightbox444"
      role="a"
      tabindex="0">
      LIGUE AGORA
    </a>
  </div>


</div>

  <div class="row top35">
      <div class="col-12 text-center">
          <h3>ENTRE EM CONTATO</h3>
          <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_contato.png" alt=""
                   height="24" width="217"></amp-img>

      </div>

      <div class="col-12">
          <div class="media">
              <div class="media-left media-middle">
                  <i class="fa fa-home media-object right10" aria-hidden="true"></i>
              </div>
              <div class="media-body">
                  <h5 class="media-heading"><?php Util::imprime($config[endereco]);?></h5>
              </div>
          </div>
      </div>

      <div class="col-6 top20">
          <div class="media">
              <div class="media-left media-middle">
                  <i class="fa fa-phone media-object right10" aria-hidden="true"></i>
              </div>
              <div class="media-body">
                  <h5 class="media-heading"><span><?php Util::imprime($config[ddd1]);?> <?php Util::imprime($config[telefone1]);?></span></h5>
              </div>
          </div>
      </div>

      <?php if (!empty($config[telefone2])) :?>
          <div class="col-6 top20">
              <div class="media">
                  <div class="media-left media-middle">
                      <i class="fa fa-whatsapp media-object right10" aria-hidden="true"></i>
                  </div>
                  <div class="media-body">
                      <h5 class="media-heading"><span><?php Util::imprime($config[ddd2]);?> <?php Util::imprime($config[telefone2]);?></span></h5>
                  </div>
              </div>
          </div>
      <?php endif; ?>

      <?php if (!empty($config[telefone3])) :?>
          <div class="col-6 top20">
              <div class="media">
                  <div class="media-left media-middle">
                      <i class="fa fa-phone media-object right10" aria-hidden="true"></i>
                  </div>
                  <div class="media-body">
                      <h5 class="media-heading"><span><?php Util::imprime($config[ddd3]);?> <?php Util::imprime($config[telefone3]);?></span></h5>
                  </div>
              </div>
          </div>
      <?php endif; ?>

      <?php if (!empty($config[telefone4]) != '') :?>
          <div class="col-6 top20">
              <div class="media">
                  <div class="media-left media-middle">
                      <i class="fa fa-phone media-object right10" aria-hidden="true"></i>
                  </div>
                  <div class="media-body">
                      <h5 class="media-heading"><span><?php Util::imprime($config[ddd4]);?> <?php Util::imprime($config[telefone4]);?></span></h5>
                  </div>
              </div>
          </div>
      <?php endif; ?>

  </div>


  <!-- ======================================================================= -->
  <!-- mapa   -->
  <!-- ======================================================================= -->
  <div class="row top5 relativo">


      <div class="col-12 padding0 text-center relativo">
          <div class="col-8 col-offset-2 btn_float">
              <a class="btn btn_cinza_escuro open  " href="<?php Util::imprime($config[link_place]); ?>" target="_blank">SAIBA COMO CHEGAR</a>
          </div>
          <amp-iframe
                  width="320"
                  height="199"
                  layout="responsive"
                  sandbox="allow-scripts allow-same-origin allow-popups"
                  frameborder="0"
                  src="<?php Util::imprime($config[src_place]); ?>">
          </amp-iframe>
      </div>
  </div>
  <!-- ======================================================================= -->
  <!-- mapa   -->
  <!-- ======================================================================= -->

  <?php require_once("../includes/veja.php") ?>


<?php require_once("../includes/rodape.php") ?>

</body>



</html>
