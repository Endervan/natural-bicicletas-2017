<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("./includes/head.php"); ?>

    <style amp-custom>
        <?php require_once("./css/geral.css"); ?>
        <?php require_once("./css/topo_rodape.css"); ?>
        <?php require_once("./css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",11) ?>

        .bg-interna {
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 40px center no-repeat;
            background-size: 100% 100%;
        }
    </style>


</head>


<body class="bg-interna">

<div class="row">
    <div class="col-12 text-center topo">
        <amp-img layout="responsive" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home"
                 height="60" width="320"></amp-img>
    </div>
</div>


<div class=" row font-index text-center">
    <div class="col-12 padding0 top30">
        <h4 class="text-center"><?php Util::imprime($banner[legenda_1]); ?></h4>
        <?php if (!empty($banner[legenda_2])): ?>
            <h6 class="text-center"><span><?php Util::imprime($banner[legenda_2]); ?></span></h6>
        <?php endif; ?>
        <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/barra_home.png" alt="Home" height="1"
                 width="144"></amp-img>
    </div>
</div>

<!-- ======================================================================= -->
<!--  MENU -->
<!-- ======================================================================= -->
<div class="row">

    <div class="col-4 top30">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/produtos">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_produtos_geral.png"
                     width="95"
                     height="95"
                     layout="responsive"
                     alt="AMP">
            </amp-img>
        </a>
    </div>

    <div class="col-4 top30">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/servicos">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_servicos_geral.png"
                     width="95"
                     height="95"
                     layout="responsive"
                     alt="AMP">
            </amp-img>
        </a>
    </div>

    <div class="col-4 top30">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/dicas">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_dicas_geral.png"
                     width="95"
                     height="95"
                     layout="responsive"
                     alt="AMP">
            </amp-img>
        </a>
    </div>

    <div class="col-4 top30">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/empresa">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_empresa_geral.png"
                     width="95"
                     height="95"
                     layout="responsive"
                     alt="AMP">
            </amp-img>
        </a>
    </div>

    <div class="col-4 top30">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/contato">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_contato_geral.png"
                     width="95"
                     height="95"
                     layout="responsive"
                     alt="AMP">
            </amp-img>
        </a>
    </div>

    <div class="col-4 top30">
        <a href="<?php Util::imprime($config[link_place]); ?>" target="_blank">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_onde_geral.png"
                     width="95"
                     height="95"
                     layout="responsive"
                     alt="AMP">
            </amp-img>
        </a>
    </div>


</div>
<!-- ======================================================================= -->
<!--  MENU -->
<!-- ======================================================================= -->

<div class="separador-rodape"></div>


<?php require_once("./includes/rodape.php") ?>


</body>


</html>
