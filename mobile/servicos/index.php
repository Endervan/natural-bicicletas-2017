<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo",3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>




  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14); ?>
  .bg-interna{
    background:  url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 128px;
  }
  </style>




</head>

<body class="bg-interna">


  <?php  require_once("../includes/topo.php")?>


  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->
  <div class="row">
      <div class="col-12  padding0 localizacao-pagina text-center">
          <h4 class="open"><?php Util::imprime($banner[legenda_1]); ?></h4>
          <?php if (isset($banner[legenda_2])) :?>
              <h4 class="open"><span><?php Util::imprime($banner[legenda_2]); ?></span></h4>
          <?php endif;?>
          <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt=""
                   height="26" width="320"></amp-img>
      </div>
  </div>
  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- servicos gerais -->
  <!-- ======================================================================= -->

  <div class="row">


      <?php
      $i = 0;
      $result = $obj_site->select("tb_servicos");
      if (mysql_num_rows($result) > 0) {
          while ($row = mysql_fetch_array($result)) {
              ?>

              <div class="col-6 dicas top20">
                  <div class="card">

                      <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>"
                         title="<?php Util::imprime($row[titulo]); ?>">
                          <amp-img
                                  src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>"
                                  width="150"
                                  height="100"
                                  layout="responsive"
                                  alt="<?php echo Util::imprime($row[titulo]) ?>">
                          </amp-img>
                      </a>
                      <div class="card-body bottom10">
                          <h6 class="card-text open"><?php Util::imprime($row[titulo],60); ?></h6>
                      </div>
                  </div>
              </div>

              <?php
              if ($i == 1) {
                  echo '<div class="clearfix"></div>';
                  $i = 0;
              } else {
                  $i++;
              }
          }
      }
      ?>

  </div>
  <!-- ======================================================================= -->
  <!-- servicos gerais -->
  <!-- ======================================================================= -->



  <div class="row top25">
    <div class="col-12">
      <a class="btn col-12 padding0 btn_escuro"
      on="tap:my-lightbox444"
      role="a"
      tabindex="0">
      LIGUE AGORA
    </a>
    </div>


  </div>



  </div>



  <!--  ==============================================================  -->
  <!--   VEJA TAMBEM -->
  <!--  ==============================================================  -->
  <?php require_once("../includes/veja.php") ?>
  <!--  ==============================================================  -->
  <!--   VEJA TAMBEM -->
  <!--  ==============================================================  -->




  <?php require_once("../includes/rodape_paginas.php") ?>

</body>



</html>
