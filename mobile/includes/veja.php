
<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->
<div class="row top30 bottom50">
    <div class="col-4">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/produtos">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_produtos_geral1.png"
                     width="87"
                     height="87"
                     layout="responsive"
                     alt="AMP">
            </amp-img>
        </a>
    </div>
    <div class="col-4">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/servicos">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_servicos_geral1.png"
                     width="87"
                     height="87"
                     layout="responsive"
                     alt="AMP">
            </amp-img>
        </a>
    </div>
    <div class="col-4">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_orcamento_geral.png"
                     width="87"
                     height="87"
                     layout="responsive"
                     alt="AMP">
            </amp-img>
        </a>
    </div>
</div>
<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->

