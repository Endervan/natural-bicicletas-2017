
<?php
$ids = explode("/", $_GET[get1]);

require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo",9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 128px;
  }
  </style>


  <script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>



</head>

<body class="bg-interna">

  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once("../includes/topo.php") ?>



  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->
  <div class="row">
      <div class="col-12  padding0 localizacao-pagina text-center">
          <h4 class="open"><?php Util::imprime($banner[legenda_1]); ?></h4>
          <?php if (isset($banner[legenda_2])) : ?>
              <h4 class="open"><span><?php Util::imprime($banner[legenda_2]); ?></span></h4>
          <?php endif; ?>
          <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt=""
                   height="26" width="320"></amp-img>
      </div>
  </div>
  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- PRODUTOS    -->
  <!-- ======================================================================= -->
  <div class="row">

    <?php



    //  FILTRA AS CATEGORIAS
    if (!empty( $ids[0] )) {
      $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $ids[0]);
      $complemento .= "AND id_categoriaproduto = '$id_categoria' ";

    }


    //  FILTRA AS SUBCATEGORIAS
    if (isset( $ids[1] )) {
      $id_subcategoria = $obj_site->get_id_url_amigavel("tb_subcategorias_produtos", "idsubcategoriaproduto", $ids[1]);
      $complemento .= "AND id_subcategoriaproduto = '$id_subcategoria' ";
    }

    //  FILTRA PELO TITULO
    if(isset($_POST[busca_produtos])):
      $complemento .= "AND titulo LIKE '%$_POST[busca_produtos]%'";
    endif;

    ?>

    <?php

    //  busca os produtos sem filtro
    $result = $obj_site->select("tb_produtos", $complemento);

    if(mysql_num_rows($result) == 0){
      ?>
      <div class="col-12 top10">
        <button type="button" class="btn btn_azul col-12">Nenhum produto encontrado.</button>
      </div>
      <?php
    }else{
      ?>

      <?php if (!empty( $ids[0])):?>
      <div class="col-12 top20 total-resultado-busca bottom10">
        <h3><?php echo mysql_num_rows($result) ?> PRODUTO(S) ENCONTRADO(S) . </h3>
      </div>
    <?php endif; ?>

      <?php
      require_once('../includes/lista_produtos.php');

    }
    ?>



    <div class="col-12 top40">
    	<a class="btn col-12 padding0 btn_cinza_escuro"
    	on="tap:my-lightbox444"
    	role="buttom"
    	tabindex="0">
    	LIGUE AGORA
    </a>
    </div>



  </div>
  <!-- ======================================================================= -->
  <!-- PRODUTOS    -->
  <!-- ======================================================================= -->




  <!--  ==============================================================  -->
  <!--   VEJA TAMBEM -->
  <!--  ==============================================================  -->
  <?php require_once("../includes/veja.php") ?>
  <!--  ==============================================================  -->
  <!--   VEJA TAMBEM -->
  <!--  ==============================================================  -->


  <?php require_once("../includes/rodape.php") ?>

</body>



</html>
