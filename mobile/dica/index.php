<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url = $_GET[get1];

if (!empty($url)) {
    $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_dicas", $complemento);

if (mysql_num_rows($result) == 0) {
    Util::script_location(Util::caminho_projeto() . "/mobile/dicas/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 17); ?>
        .bg-interna {
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center no-repeat;
            background-size: 100% 128px;
        }
    </style>

    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
    <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
    <script async custom-element="amp-image-lightbox"
            src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>
    <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>


</head>

<body class="bg-interna">

<?php
$voltar_para = 'dicas'; // link de volta, exemplo produtos, dicas, servicos etc
require_once("../includes/topo.php") ?>


<?php
require_once("../includes/topo.php") ?>


<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->
<div class="row">
    <div class="col-12  padding0 localizacao-pagina text-center">
        <h4 class="open"><?php Util::imprime($banner[legenda_1]); ?></h4>
        <?php if (isset($banner[legenda_2])) : ?>
            <h4 class="open"><span><?php Util::imprime($banner[legenda_2]); ?></span></h4>
        <?php endif; ?>
        <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt=""
                 height="26" width="320"></amp-img>
    </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->


<div class="row">


    <div class="col-12 top20">
        <h4><?php echo Util::imprime($dados_dentro[titulo]) ?></h4>
    </div>

    <div class="col-12 top30">
        <div><p><?php echo Util::imprime($dados_dentro[descricao]) ?></p></div>
    </div>


    <div class="col-12">

        <?php
        $result = $obj_site->select("tb_galerias_servicos", "AND id_servico = '$dados_dentro[0]' ");
        if (mysql_num_rows($result) == 0) { ?>

            <amp-carousel id="carousel-with-preview"
                          width="50"
                          height="50"
                          layout="responsive"
                          type="slides"
                          autoplay
                          delay="4000"
            >

                <amp-img
                        on="tap:lightbox2"
                        role="button"
                        tabindex="0"
                        src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($dados_dentro[imagem]) ?>"
                        layout="responsive"
                        width="50"
                        height="50"
                        alt="default">

                </amp-img>
            </amp-carousel>

            <amp-image-lightbox id="lightbox2" layout="nodisplay"></amp-image-lightbox>

        <?php } else {
            ?>
            <amp-carousel id="carousel-with-preview"
                          width="50"
                          height="50"
                          layout="responsive"
                          type="slides"
                          autoplay
                          delay="8000"
            >

                <?php
                $i = 0;

                if (mysql_num_rows($result) > 0) {
                    while ($row = mysql_fetch_array($result)) {
                        ?>

                        <amp-img
                                on="tap:lightbox1"
                                role="button"
                                tabindex="0"

                                src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
                                layout="responsive"
                                width="360"
                                height="250"
                                alt="<?php echo Util::imprime($row[titulo]) ?>">

                        </amp-img>
                        <?php
                    }
                    $i++;
                }


                ?>

            </amp-carousel>

            <amp-image-lightbox id="lightbox1" layout="nodisplay"></amp-image-lightbox>

            <?php /*
<div class="carousel-preview text-center">

<?php
$i = 0;
$result = $obj_site->select("tb_galerias_servicos", "and id_servico = $dados_dentro[idservico]");
if (mysql_num_rows($result) > 0) {
while($row = mysql_fetch_array($result)){
?>

<button class="rounded-circle left5" on="tap:carousel-with-preview.goToSlide(index=<?php echo $i++; ?>)">

<amp-img class="rounded-circle"
src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/bolinhas.png"
width="20"
height="20"
alt="<?php echo Util::imprime($row[titulo]) ?>">
</amp-img>
</button>

<?php
}
$i++;
}


?>
</div>

*/ ?>

        <?php } ?>

    </div>

    <div class="col-12">
        <a class="btn col-12 padding0 btn_escuro"
           on="tap:my-lightbox444"
           role="a"
           tabindex="0">
            LIGUE AGORA
        </a>
    </div>



</div>

<?php require_once("../includes/veja.php") ?>


<?php require_once("../includes/rodape_paginas.php") ?>

</body>


</html>
