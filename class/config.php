<?php
error_reporting(0);

/*	================================================================================================	*/
/*	DEFINE O CAMINHO COMPLETO DO PROJETO ARMAZENANDO NUMA SESSAO	*/
/*	================================================================================================	*/
$pasta_projeto = "/clientes/natural-bicicletas-2017";



/*	================================================================================================	*/
/*	HABILITA O REDIRECIONAMENTO DO MOBILE, USE SIM OU NAO	*/
/*	================================================================================================	*/
define('MOBILE', 'SIM');


/*	================================================================================================	*/
/*	CONFIGURACOES DO BANCO DE DADOS	*/
/*	================================================================================================	*/

$host_banco          = "bicicleta_prod.mysql.dbaas.com.br";
$nome_banco          = "bicicleta_prod";
$usuario_banco       = "bicicleta_prod";
$senha_banco         = "masmidia102030";

//$host_banco          = "localhost";
//$nome_banco          = "masmidia_bicicletas";
//$usuario_banco       = "root";
//$senha_banco         = "mysql";


/*	================================================================================================	*/
/*	SITEMAPS - deve colocar as páginas principais */
/*	================================================================================================	*/
$sitemaps[] = array('url' => "empresa");
$sitemaps[] = array('url' => "produtos");
$sitemaps[] = array('url' => "servicos");
$sitemaps[] = array('url' => "contato");


/*	================================================================================================	*/
/*	SITEMAPS TABELA - Informe as tabelas que deverão ser geradas os link para o sitemaps
/*	================================================================================================	*/
$sitemaps_tabela[] = array(
  'url' => "produto",
  'nome' => "tb_produtos",
);

$sitemaps_tabela[] = array(
  'url' => "servico",
  'nome' => "tb_servicos",
);





/*	================================================================================================	*/
/*	VARIAVEIS */
/*	================================================================================================	*/
define('PASTA_PROJETO', $pasta_projeto);
define('HOST_BANCO', $host_banco);
define('NOME_BANCO', $nome_banco);
define('USUARIO_BANCO', $usuario_banco);
define('SENHA_BANCO', $senha_banco);
$_SESSION['SITEMAPS'] = $sitemaps;
$_SESSION['SITEMAPS_TABELA'] = $sitemaps_tabela;



define('COPYRIGHT', "Copyright HomewebBrasil www.homewebbrasil.com.br");
define('EMAIL_GOOGLE_ANALYTICS', "");
define('SENHA_GOOGLE_ANALYTICS', "");
define('ID_GOOGLE_ANALYTICS', "");




?>
