<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 5);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
  //  SELECIONO O TIPO
  switch($_GET[tipo])
  {
    case "produto":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_produtos][$id]);
    sort($_SESSION[solicitacoes_produtos]);
    break;
    case "servico":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_servicos][$id]);
    sort($_SESSION[solicitacoes_servicos]);
    break;
    case "piscina_vinil":
    $id = $_GET[id];
    unset($_SESSION[piscina_vinil][$id]);
    sort($_SESSION[piscina_vinil]);
    break;
  }

}


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>





<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="top200"></div>


  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row  ">
      <div class="col-4 top40 mr-auto text-center produto_titulo">
        <h3><?php Util::imprime($banner[legenda_1]); ?></h3>
        <h6><?php Util::imprime($banner[legenda_2]); ?></h6>
        <img class="top5" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo_home.png" alt="">
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->




  <div class="container-fluid fundo_orcamento">
    <div class="row">


      <div class="container relativo">
        <form  class="FormContatos needs-validation " novalidate role="form" method="post" enctype="multipart/form-data">
          <div class="row ">


            <div class="col-5">

              <!-- ======================================================================= -->
              <!-- ITENS CARRINHO  -->
              <!-- ======================================================================= -->
              <?php require_once('./includes/lista_itens_orcamento.php') ?>
              <!-- ======================================================================= -->
              <!-- ITENS CARRINHO  -->
              <!-- ======================================================================= -->

              <!-- ======================================================================= -->
              <!-- telefones  -->
              <!-- ======================================================================= -->

                <div class="col-8 ml-auto tel_orcamento">
                  <div class="top50"> <button type="button" class="btn btn-outline-secondary btn_branco disabled">SE PREFERIR LIGUE </button></div>


                  <div class="row telefone_topo top25">
                    <i class="fas fa-phone fa-fw right10 left15" data-fa-transform="rotate-90" style="color:#000;"></i>
                    <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
                  </div>

                  <?php if (!empty($config[telefone2])): ?>
                    <div class="row text-center telefone_topo top15">
                      <i class="fab fa-whatsapp right10 left15" style="color:#000;"></i>
                      <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
                    </div>
                  <?php endif;

                  ?>



                  <?php if (!empty($config[telefone3])): ?>
                    <div class="row text-center telefone_topo top15">
                      <i class="fas fa-phone fa-fw right10 left15" data-fa-transform="rotate-90"  style="color:#000;"></i>
                      <?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
                    </div>
                  <?php endif; ?>

                  <?php if (!empty($config[telefone4])): ?>
                    <div class="row text-center telefone_topo top15">
                      <i class="fas fa-phone fa-fw right10 left15" data-fa-transform="rotate-90"  style="color:#000;"></i>
                      <?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>
                    </div>
                  <?php endif; ?>
                  <!-- ======================================================================= -->
                  <!-- telefones  -->
                  <!-- ======================================================================= -->

                </div>

            </div>


            <!--  ==============================================================  -->
            <!-- FORMULARIO CONTATOS-->
            <!--  ==============================================================  -->
            <div class="col-7 fundo_formulario">

              <div class="form-row">
                <div class="col">
                  <div class="form-group relativo">
                    <input type="text" name="assunto" class="form-control fundo-form" placeholder="ASSUNTO" required>
                    <span class="fas fa-user-circle form-control-feedback "></span>
                    <div class="invalid-feedback">
                      Adicione Assunto.
                    </div>

                    <div class="col-12 text-right valid-feedback">
                      <i class="fas fa-check-circle" aria-hidden></i>
                    </div>

                  </div>
                </div>

              </div>

              <div class="form-row">
                <div class="col">
                  <div class="form-group relativo">
                    <input type="text" name="nome" class="form-control fundo-form" placeholder="NOME" required>
                    <span class="fa fa-user form-control-feedback"></span>
                    <div class="invalid-feedback">
                      Por favor Insira um Nome
                    </div>
                    <div class="col-12 text-right valid-feedback">
                      <i class="fas fa-check-circle" aria-hidden></i>
                    </div>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group relativo">
                    <input type="email" name="email" class="form-control fundo-form" placeholder="E-MAIL" required>
                    <span class="fa fa-envelope form-control-feedback"></span>
                    <div class="invalid-feedback">
                      Informe Email Válido
                    </div>
                    <div class="col-12 text-right valid-feedback">
                      <i class="fas fa-check-circle" aria-hidden></i>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-row">
                <div class="col">
                  <div class="form-group relativo">
                    <input type="tel" name="telefone" class="form-control fundo-form" placeholder="TELEFONE" required>
                    <span class="fa fa-phone form-control-feedback" data-fa-transform="rotate-90"></span>
                    <div class="invalid-feedback">
                      seu número
                    </div>
                    <div class="col-12 text-right valid-feedback">
                      <i class="fas fa-check-circle" aria-hidden></i>
                    </div>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group relativo">
                    <input type="text" name="celular" class="form-control fundo-form" placeholder="CELULAR">
                    <span class="fa fas fa-mobile form-control-feedback"></span>
                  </div>
                </div>
              </div>


              <div class="form-row">
                <div class="col">
                  <div class="form-grouprelativo">
                    <textarea name="mensagem" cols="25" rows="5" class="form-control fundo-form" placeholder="MENSAGEM" required></textarea>
                    <span class="fas fa-pencil-alt form-control-feedback"></span>
                    <div class="invalid-feedback">
                      Escreva Uma Mensagem
                    </div>
                    <div class="col-12 text-right valid-feedback">
                      <i class="fas fa-check-circle" aria-hidden></i>
                    </div>
                  </div>
                </div>
              </div>

              <?php if (count($_SESSION[solicitacoes_produtos])+count($_SESSION[solicitacoes_servicos]) > 0) :?>
                <div class="col-12 mont top20 bottom40 text-right padding0 ">
                  <button type="submit" class="btn btn-lg btn_cinza_escuro btn_formulario" name="btn_contato">
                    ENVIAR MENSAGEM
                  </button>
                </div>

              <?php else: ?>
                <div class="">
                  <div class="col-12 bottom50 top10 mont text-center padding0 ">
                    <span>***OBS : PARA ENVIAR ORÇAMENTO ADICIONE UM ITEM ***</span>
                  </div>

                </div>
              <?php endif; ?>



            </div>
            <!--  ==============================================================  -->
            <!-- FORMULARIO CONTATOS-->
            <!--  ==============================================================  -->


          </div>
        </form>

      </div>


    </div>
  </div>








  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>

<?php
//  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
if(isset($_POST[nome])){



  //  CADASTRO OS PRODUTOS SOLICITADOS
  for($i=0; $i < count($_POST[qtd]); $i++){
    $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

    $produtos .= "
    <tr>
    <td><p>". $_POST[qtd][$i] ."</p></td>
    <td><p>". utf8_encode(($dados[titulo])) ."</p></td>
    </tr>
    ";
  }

  //  CADASTRO OS SERVICOS SOLICITADOS
  for($i=0; $i < count($_POST[qtd_servico]); $i++){
    $dados = $obj_site->select_unico("tb_servicos", "idservico", $_POST[idservico][$i]);
    $produtos .= "
    <tr>
    <td><p>". $_POST[qtd_servico][$i] ."</p></td>
    <td><p>". utf8_encode(($dados[titulo])) ."</p></td>
    </tr>
    ";
  }




  //  ENVIANDO A MENSAGEM PARA O CLIENTE
  echo $texto_mensagem = "
  O seguinte cliente fez uma solicitação pelo site. <br />

  Assunto: ".($_POST[assunto])." <br />
  Nome: ".($_POST[nome])." <br />
  Email: ".($_POST[email])." <br />
  Telefone: ".($_POST[telefone])." <br />
  Celular: ".($_POST[celular])." <br />


  Mensagem: <br />
  ". nl2br($_POST[mensagem]) ." <br />

  <br />
  <h2> Produtos selecionados:</h2> <br />

  <table width='100%' border='0' cellpadding='5' cellspacing='5'>
  <tr>
  <td><h4>QTD</h4></td>
  <td><h4>PRODUTO</h4></td>
  </tr>
  $produtos
  </table>

  ";




  if (Util::envia_email($config[email_orcamento], ("$_POST[nome] solicitou contato pelo site"), ($texto_mensagem), ($_POST[nome]),($_POST[email]))) {
    Util::envia_email($config[email_copia], ("$_POST[nome] solicitou um orçamento"), ($texto_mensagem), ($_POST[nome]),($_POST[email]));
    unset($_SESSION[solicitacoes_produtos]);
    unset($_SESSION[solicitacoes_servicos]);
    unset($_SESSION[piscinas_vinil]);
    Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");
  }else{
    Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
  }

}



?>
