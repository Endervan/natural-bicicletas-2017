<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="top200"></div>



  <!--  ==============================================================  -->
  <!--  DESCRICAO -->
  <!--  ==============================================================  -->
  <div class="container">

    <div class="row  ">
      <div class="col-4 top20 text-center produto_titulo">
        <h3><?php Util::imprime($banner[legenda_1]); ?></h3>
        <h6><?php Util::imprime($banner[legenda_2]); ?></h6>
        <img class="top5" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo_home.png" alt="">
      </div>
    </div>

    <div class="row">


      <div class="col-5  ml-auto order-12 descricao_geral">
        <div >
          <h2><?php Util::imprime($dados_dentro[titulo]); ?></h2>
        </div>

        <div class="text-right top10">
          <a class="btn btn-outline-secondary  btn_cinza_escuro" title="SOLICITAR UM ORÇAMENTO"
          href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'" role="button">
          SOLICITE ORÇAMENTO
        </a>
      </div>


      <div class="top25">
        <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
      </div>

      <div class="top40">

        <div class="col-12 p-0">
          <a class="btn btn-outline-secondary  btn_cinza_escuro pl-4 pr-4 " title="SOLICITAR UM ORÇAMENTO"
          href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'" role="button">
          SOLICITE ORÇAMENTO
        </a>
      </div>

    <div class="col-12 top20 p-0">
      <a  id="nav-link" class="btn btn-outline-secondary btn_branco" tabindex="0" data-html="true"  role="button" data-toggle="popover" data-placement="left" data-trigger="focus" title="NOSSOS CONTATOS"
      data-content='

      <div class="col-12 telefone_popover">
        <i class="fas fa-phone fa-fw right10" data-fa-transform="rotate-90"></i>
        <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
      </div>

      <?php if (!empty($config[telefone2])): ?>
        <div class="col-12 telefone_popover">
          <i class="fab fa-whatsapp right10"></i>
          <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
        </div>
      <?php endif;

      ?>




      <?php if (!empty($config[telefone3])): ?>
      <div class="col-12 telefone_popover">
        <i class="fas fa-phone fa-fw right10" data-fa-transform="rotate-90"></i>
      <?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
      </div>
      <?php endif; ?>

      <?php if (!empty($config[telefone4])): ?>
      <div class="col-12 telefone_popover">
        <i class="fas fa-phone fa-fw right10" data-fa-transform="rotate-90"></i>
      <?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>
      </div>
      <?php endif; ?>

    </div>

    '>
    FALE CONOSCO LIGUE AGORA
    </a>



    </div>
    </div>


  </div>


  <div class="col-6 padding0 top25 bottom120 order-1">


    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/slider_produto_servico_dentro.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->


  </div>

</div>
</div>






<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>




<?php require_once('./includes/js_css.php') ?>



<script type="text/javascript">
$(window).load(function() {


  $('#carousel_item').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: true,
    slideshow: false,
    itemWidth: 110,
    itemMargin: 0,
    asNavFor: '#slider_item'
  });

  $('#slider_item').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: true,
    slideshow: false,
    sync: "#carousel_item"
  });


});



$(window).load(function() {
  $('#slider_carousel').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 352,
    itemMargin: 0,
    controlsContainer: $(".custom-controls-container"),
    customDirectionNav: $(".custom-navigation a")
  });
});




$('#nav-link').popover({
  trigger: 'focus'
})

</script>
