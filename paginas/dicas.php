<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="top200"></div>


  <!-- ======================================================================= -->
  <!--DICAS   -->
  <!-- ======================================================================= -->
  <div class="container-fluid fundo_dicas top25">
    <div class="row">
      <div class="container">

        <div class="row  top20">
          <div class="col-4 text-center produto_titulo">
            <h3><?php Util::imprime($banner[legenda_1]); ?></h3>
            <h6><?php Util::imprime($banner[legenda_2]); ?></h6>
            <img class="top5" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo_home.png" alt="">
          </div>
        </div>

        <div class="row dicas ">




              <?php
              $i = 0;
              $result = $obj_site->select("tb_dicas");
              if (mysql_num_rows($result) > 0) {
                while($row = mysql_fetch_array($result)){
                  ?>

                  <div class="col-4 top20">
                    <div class="card p-3">

                      <a class="" href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 282, 199, array("class"=>"card-img-top", "alt"=>"$row[titulo]")) ?>
                      </a>
                      <div class="card-body bottom10 text-center">
                        <h6 class="card-text"><?php Util::imprime($row[titulo]); ?></h6>
                      </div>
                    </div>
                  </div>

                  <?php
                  if ($i == 1) {
                    echo '<div class="clearfix"></div>';
                    $i = 0;
                  }else{
                    $i++;
                  }
                }
              }
              ?>

            </div>
          </div>
        </div>


      </div>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!--DICAS   -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
<script type="text/javascript">



$(window).load(function() {
  $('#slider_carousel').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 352,
    itemMargin: 0,
    controlsContainer: $(".custom-controls-container"),
    customDirectionNav: $(".custom-navigation a")
  });
});





</script>
