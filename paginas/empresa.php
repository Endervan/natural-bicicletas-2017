<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>

<body>


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- TITULO PAGINAS   -->
  <!-- ======================================================================= -->
      <div class="container relativo top200">

        <div class="row  top20">
            <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
            <div class="col-4 text-center produto_titulo">
            <h3><?php Util::imprime($banner[legenda_1]); ?></h3>
            <h6><?php Util::imprime($banner[legenda_2]); ?></h6>
            <img class="top5" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo_home.png" alt="">
          </div>
        </div>

      </div>
  <!-- ======================================================================= -->
  <!-- TITULO PAGINAS   -->
  <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!-- empresa   -->
  <!-- ======================================================================= -->

  <style>
      <?php $pagina = $obj_site->select_unico("tb_empresa", "idempresa",5) ?>
      .bg_empresa_home{
          background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($pagina[imagem]); ?>) top center  no-repeat;
          min-height:474px;
      }
  </style>

  <div class="container-fluid bg_empresa_home bg_cinza_emp ">
      <div class="row">

          <div class="container relativo">
              <div class="row">
                  <div class="col-7 top60">
                      <div class="bottom20 scrol_empresa ml-auto">
                          <p><?php Util::imprime($pagina[descricao]); ?></p>
                      </div>
                  </div>
              </div>

              <!--  ==============================================================  -->
              <!-- background -->
              <!--  ==============================================================  -->


          </div>
      </div>
  </div>
  <!-- ======================================================================= -->
  <!-- empresa   -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">


$(window).load(function() {
  $('#slider_carousel_portifolio').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 240,
    itemMargin: 0,
  });
});


$(window).load(function() {
  $('#slider_carousel').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 352,
    itemMargin: 0,
    controlsContainer: $(".custom-controls-container"),
    customDirectionNav: $(".custom-navigation a")
  });
});





$(window).load(function() {
  $('#slider1').flexslider({
    animation: "slide",
    controlNav: false,/*tira bolinhas*/
    animationLoop: true,
    itemWidth: 190,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});



</script>
