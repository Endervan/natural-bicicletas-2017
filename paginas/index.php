<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>


</head>
<body>


<div class="topo-site">
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/topo.php') ?>
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
</div>

<!-- ======================================================================= -->
<!-- SLIDER    -->
<!-- ======================================================================= -->
<div class="container-fluid relativo">
    <div class="row">
        <div id="container_banner">
            <div id="content_slider">
                <div id="content-slider-1" class="contentSlider rsDefault">

                    <?php
                    $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");
                    if (mysql_num_rows($result) > 0) {
                        while ($row = mysql_fetch_array($result)) {
                            ?>
                            <!-- ITEM -->
                            <div>
                                <?php
                                if ($row[url] == '') {
                                    ?>
                                    <img class="rsImg"
                                         src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>"
                                         data-rsw="1920" data-rsh="996" alt=""/>
                                    <?php
                                } else {
                                    ?>
                                    <a href="<?php Util::imprime($row[url]) ?>">
                                        <img class="rsImg"
                                             src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>"
                                             data-rsw="1920" data-rsh="996" alt=""/>
                                    </a>
                                    <?php
                                }
                                ?>

                            </div>
                            <!-- FIM DO ITEM -->
                            <?php
                        }
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- ======================================================================= -->
<!-- SLIDER    -->
<!-- ======================================================================= -->


<div class="container bg_barra_home">
    <div class="row bottom50">

        <!--  ==============================================================  -->
        <!--  CONHEÇA NOSSOS PRODUTOS -->
        <!--  ==============================================================  -->
        <div class="col-4   text-right">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1); ?>
            <div class="fundo_texto_produtos  p-3">
                <h4><?php Util::imprime($row[titulo]); ?></h4>
                <h4><span><?php Util::imprime($row[legenda]); ?></span></h4>


                <div class="top10">
                    <p><?php Util::imprime($row[descricao], 128); ?></p>
                </div>
                <div class="icon"></div>
            </div>
            <div class="col-12 btn_saiba_mais_home">
                <a class="btn btn-outline-secondary btn_cinza" href="<?php echo Util::caminho_projeto() ?>/produtos"
                   title="SAIBA MAIS">
                    SAIBA MAIS
                </a>
            </div>
        </div>
        <!--  ==============================================================  -->
        <!--  CONHEÇA NOSSOS PRODUTOS -->
        <!--  ==============================================================  -->


        <!--  ==============================================================  -->
        <!--  CONHEÇA NOSSOS SERVIÇOS -->
        <!--  ==============================================================  -->
        <div class="col-4   text-right">
            <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 2); ?>
            <div class="fundo_texto_produtos  p-3">
                <h4><span><?php Util::imprime($row1[legenda]); ?></span></h4>
                <h4><?php Util::imprime($row1[titulo]); ?></h4>

                <div class="top10">
                    <p><?php Util::imprime($row1[descricao], 128); ?></p>
                </div>
                <div class="icon fundo_texto_servicos"></div>
            </div>
            <div class="col-12 btn_saiba_mais_home">
                <a class="btn btn-outline-secondary btn_cinza" href="<?php echo Util::caminho_projeto() ?>/produtos"
                   title="SAIBA MAIS">
                    SAIBA MAIS
                </a>
            </div>
        </div>
        <!--  ==============================================================  -->
        <!--  CONHEÇA NOSSOS SERVIÇOS -->
        <!--  ==============================================================  -->


        <!--  ==============================================================  -->
        <!--  FALE CONOSCO -->
        <!--  ==============================================================  -->
        <div class="col-4   text-right">
            <?php $row2 = $obj_site->select_unico("tb_empresa", "idempresa", 3); ?>
            <div class="fundo_texto_produtos  p-3">
                <h4><span><?php Util::imprime($row2[legenda]); ?></span></h4>
                <h4><?php Util::imprime($row2[titulo]); ?></h4>

                <div class="top10">
                    <p><?php Util::imprime($row2[descricao], 128); ?></p>
                </div>
                <div class="icon fundo_texto_contatos"></div>
            </div>
            <div class=" col-12 btn_saiba_mais_home">
                <a class="btn btn-outline-secondary btn_cinza" href="<?php echo Util::caminho_projeto() ?>/produtos"
                   title="SAIBA MAIS">
                    SAIBA MAIS
                </a>
            </div>
        </div>
        <!--  ==============================================================  -->
        <!--  FALE CONOSCO -->
        <!--  ==============================================================  -->

    </div>
</div>


<div class="container">


    <!-- ======================================================================= -->
    <!-- LISTA PRODUTOS E SERVIÇOS   -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/lista_produtos_servicos.php') ?>
    <!-- ======================================================================= -->
    <!-- LISTA PRODUTOS E SERVIÇOS    -->
    <!-- ======================================================================= -->
</div>


<!-- ======================================================================= -->
<!-- EMPRESA   -->
<!-- ======================================================================= -->


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
    .bg_empresa_home{
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
        min-height:352px;
    }
</style>


<div class="container-fluid bg_empresa_home top60 bottom40">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-4 top50 ml-auto text-center produto_titulo">
                    <?php $row3 = $obj_site->select_unico("tb_empresa", "idempresa", 5); ?>
                    <h3><?php Util::imprime($row3[titulo]); ?></h3>
                    <h6><?php Util::imprime($row3[legenda]); ?></h6>
                    <img class="top5" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo_home.png" alt="">
                </div>
            </div>
            <div class="row">
                <div class="col-7 ml-auto text-right top25">
                    <p><?php Util::imprime($row3[descricao], 450); ?></p>
                    <a class="btn btn-outline-secondary btn_cinza_escuro top15"
                       href="<?php echo Util::caminho_projeto() ?>/produtos" title="SAIBA MAIS">
                        SAIBA MAIS
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ======================================================================= -->
<!-- EMPRESA   -->
<!-- ======================================================================= -->


<div class="container relativo top90">
    <div class="row justify-content-center">
        <div class="col p-0">
            <!-- ======================================================================= -->
            <!-- mapa   -->
            <!-- ======================================================================= -->
            <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="390" frameborder="0"
                    style="border:0" allowfullscreen></iframe>
            <!-- ======================================================================= -->
            <!-- mapa   -->
            <!-- ======================================================================= -->
        </div>

        <div class="col-4 bloco_endereco pt-4 pb30">
            <div class="col-12 text-center desc_servicos_geral p-2">
                <h2 class=" Yanone">UNIDADE</h2>
                <img class="text-center " src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo_servicos.png"
                     alt="">
            </div>

            <div class="col-12 p-0">
                <div class="media">
                    <i class="fas fa-home  align-self-center mr-3" aria-hidden></i>
                    <div class="media-body">
                        <h6 class="mt-0"><?php Util::imprime($config[endereco]); ?></h6>
                    </div>
                </div>
            </div>

            <div class="row top15">

                <div class="col-6">
                    <div class="media">
                        <i class="fas fa-phone  align-self-center mr-3" aria-hidden data-fa-transform="rotate-90"></i>
                        <div class="media-body">
                            <h6 class="mt-0"><?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?></h6>
                        </div>
                    </div>
                </div>

                <?php if (!empty($config[telefone2])): ?>
                    <div class="col-6">
                        <div class="media">
                            <i class="fab fa-whatsapp  align-self-center mr-3" aria-hidden></i>
                            <div class="media-body">
                                <h6 class="mt-0"><?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?></h6>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

            </div>

            <div class="btn_fluatuante">
                <a class="btn btn-outline-secondary btn_cinza_escuro top15"
                   href="<?php Util::imprime($config[src_place]); ?>" target="_blank" title="SAIBA COMO CHEGAR">
                    SAIBA COMO CHEGAR
                </a>
            </div>
        </div>


    </div>
</div>


<!-- ======================================================================= -->
<!--DICAS   -->
<!-- ======================================================================= -->
<div class="container-fluid bg_cinza_claro top25">
    <div class="row">
        <div class="container">

            <div class="row bottom30 top20">
                <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 6); ?>
                <div class="col-12 text-center produto_titulo">
                    <h3><?php Util::imprime($banner[legenda_1]); ?></h3>
                    <h6><?php Util::imprime($banner[legenda_2]); ?></h6>
                    <img class="top5" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo_home.png" alt="">
                </div>
            </div>

            <div class="row dicas justify-content-center">


                <?php
                $i = 0;
                $result = $obj_site->select("tb_dicas", "ORDER BY RAND() LIMIT 3");
                if (mysql_num_rows($result) > 0) {
                    while ($row = mysql_fetch_array($result)) {
                        ?>

                        <div class="col-3">
                            <div class="card p-3">

                                <a class=""
                                   href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>"
                                   title="<?php Util::imprime($row[titulo]); ?>">
                                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 282, 199,
                                        array("class" => "card-img-top", "alt" => "$row[titulo]")) ?>
                                </a>
                                <div class="card-body">
                                    <h6 class="card-text"><?php Util::imprime($row[titulo]); ?></h6>
                                </div>
                            </div>
                        </div>

                        <?php
                        if ($i == 1) {
                            echo '<div class="clearfix"></div>';
                            $i = 0;
                        } else {
                            $i++;
                        }
                    }
                }
                ?>

                <div class="col-9 text-right bottom40">
                    <a class="btn btn-outline-secondary btn_branco top15"
                       href="<?php echo Util::caminho_projeto() ?>/dicas" title="CONFIRA TODAS AS DICAS">
                        CONFIRA TODAS AS DICAS
                    </a>
                </div>

            </div>


        </div>
    </div>
</div>

<!-- ======================================================================= -->
<!--DICAS   -->
<!-- ======================================================================= -->


<!-- rodape   =====================================================-->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">


<script>
    jQuery(document).ready(function ($) {
        // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
        // it's recommended to disable them when using autoHeight module
        $('#content-slider-1').royalSlider({
            autoHeight: true,
            arrowsNav: true,
            arrowsNavAutoHide: false,
            keyboardNavEnabled: true,
            controlNavigationSpacing: 0,
            controlNavigation: 'tabs',
            autoScaleSlider: false,
            arrowsNavAutohide: true,
            arrowsNavHideOnTouch: true,
            imageScaleMode: 'none',
            globalCaption: true,
            imageAlignCenter: false,
            fadeinLoadedSlide: true,
            loop: false,
            loopRewind: true,
            numImagesToPreload: 6,
            keyboardNavEnabled: true,
            usePreloader: false,
            autoPlay: {
                // autoplay options go gere
                enabled: true,
                pauseOnHover: true,
                delay: 9000
            }

        });
    });
</script>


<?php require_once('./includes/js_css.php') ?>
