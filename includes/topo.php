
<div class="container-fluid  bg_topo">
  <div class="row">


    <div class="container ">
      <div class="row">
        <?php
        if(empty($voltar_para)){
          $link_topo = Util::caminho_projeto()."/";
        }else{
          $link_topo = Util::caminho_projeto()."/".$voltar_para;
        }
        ?>

        <div class="col-3 text-right top15">
          <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="início" class="">
          </a>
        </div>




        <div class="col-9 ">


          <div class="col-12 top10 text-right">
            <div class="row justify-content-end">



              <!-- ======================================================================= -->
              <!-- telefones  -->
              <!-- ======================================================================= -->
              <div class="col-6 telefone_topo">
                <i class="fas fa-phone fa-fw right10" data-fa-transform="rotate-90"></i>
                  ATENDIMENTO
                <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
              </div>

              <?php if (!empty($config[telefone2])): ?>
                <div class="col-3 text-center telefone_topo">
                  <i class="fab fa-whatsapp right10"></i>
                  <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
                </div>
              <?php endif;

              ?>



              <?php /*
              <?php if (!empty($config[telefone3])): ?>
              <div class="col-3 text-center telefone_topo">
              <i class="fab fa-whatsapp right10"></i>
              <?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
              </div>
              <?php endif; ?>

              <?php if (!empty($config[telefone4])): ?>
              <div class="col-3 text-center telefone_topo">
              <i class="fab fa-whatsapp right10"></i>
              <?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>
              </div>
              <?php endif; ?>
              <!-- ======================================================================= -->
              <!-- telefones  -->
              <!-- ======================================================================= -->
              */ ?>
            </div>


          </div>

          <div class="col-12 menu_topo p-0 text-center">

            <!--  ==============================================================  -->
            <!-- MENU-->
            <!--  ==============================================================  -->
            <nav class="navbar justify-content-end navbar-expand-lg  navbar-light">

              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav col">
                  <li class="nav-item <?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>">
                    <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/">HOME <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item <?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>">
                    <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/empresa">EMPRESA</a>
                  </li>
                  <li class="nav-item <?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>">
                    <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a>
                  </li>

                  <li class="nav-item <?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>">
                    <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS</a>
                  </li>

                  <li class="nav-item <?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>">
                    <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a>
                  </li>



                  <li class="nav-item <?php if(Url::getURL( 0 ) == "contato"){ echo "active"; } ?>">
                    <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/contato">CONTATO</a>
                  </li>

                  <!--  ==============================================================  -->
                  <!--CARRINHO-->
                  <!--  ==============================================================  -->
                  <li class="nav-item dropdown text-center">
                    <a class="nav-link1" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      ORÇAMENTO <i class="fas fa-shopping-cart" aria-hidden="true"></i>
                      <br>
                      <i class="fas fa-chevron-down"></i>
                    </a>
                    <div class="dropdown-menu topo-meu-orcamento tabela_carrinho" aria-labelledby="navbarDropdownMenuLink">
                      <table class="table  table-condensed">

                        <?php if(count($_SESSION[solicitacoes_servicos])+count($_SESSION[solicitacoes_produtos]) == 0): ?>

                          <tr class="col-12">
                            <div class="text-center top10 bottom10">
                              <h5>ORÇAMENTO VAZIO</h5>
                              <a href="<?php echo Util::caminho_projeto() ?>/produtos"
                                class="btn btn-outline-success btn_azul pr-2" >
                                ADICIONE PRODUTO
                              </a>
                            </div>
                          </tr>



                        <?php else: ?>


                          <?php
                          if(count($_SESSION[solicitacoes_produtos]) > 0){
                            for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
                              $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                              ?>
                              <tr class="middle">
                                <div class="row lista-itens-carrinho">
                                  <div class="col-2 top5">
                                    <?php if(!empty($row[imagem])): ?>
                                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                                    <?php endif; ?>
                                  </div>
                                  <div class="col-8 top5">
                                    <h6><?php Util::imprime($row[titulo]) ?></h6>
                                  </div>
                                  <div class="col-1">
                                    <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                                  </div>
                                  <div class="col-12"><div class="borda_carrinho top5">  </div> </div>
                                </div>
                              </tr>
                              <?php
                            }
                          }
                          ?>

                          <!--  ==============================================================  -->
                          <!--sessao adicionar servicos-->
                          <!--  ==============================================================  -->
                          <?php
                          if(count($_SESSION[solicitacoes_servicos]) > 0)
                          {
                            for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                            {
                              $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                              ?>
                              <div class="row lista-itens-carrinho">
                                <div class="col-2 top5">
                                  <?php if(!empty($row[imagem])): ?>
                                    <!-- <img class="carrinho_servcos" src="<?php //echo Util::caminho_projeto() ?>/uploads/<?php //Util::imprime($row[imagem]); ?>" alt="" /> -->
                                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                                  <?php endif; ?>
                                </div>
                                <div class="col-8 top5">
                                  <h1><?php Util::imprime($row[titulo]) ?></h1>
                                </div>
                                <div class="col-1">
                                  <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                                </div>
                                <div class="col-12"><div class="borda_carrinho top5">  </div> </div>

                              </div>
                              <?php
                            }
                          }
                          ?>

                          <div class="col-12 text-right top10 bottom20">
                            <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ENVIAR ORÇAMENTO" class="btn btn-outline-success pr-2" >
                              ENVIAR ORÇAMENTO
                            </a>
                          </div>

                        <?php   endif; ?>

                      </table>

                    </div>
                  </li>
                  <!--  ==============================================================  -->
                  <!--CARRINHO-->
                  <!--  ==============================================================  -->
                </ul>
              </div>
            </nav>

            <!--  ==============================================================  -->
            <!-- MENU-->
            <!--  ==============================================================  -->

          </div>
            <?php if (Url::getURL(0) != ""):?>

                <?php

                if (Url::getURL(0) == 'empresa'):
                    $paginas = 1;
                elseif (Url::getURL(0) == 'produtos' OR Url::getURL(0) == 'produto'):
                    $paginas = 2;
                elseif (Url::getURL(0) == 'servicos' OR Url::getURL(0) == 'servico'):
                    $paginas = 4;
                elseif (Url::getURL(0) == 'dicas' OR Url::getURL(0) == 'dica'):
                    $paginas = 6;
                elseif (Url::getURL(0) == 'contato'):
                    $paginas = 8;
                elseif (Url::getURL(0) == 'orcamento'):
                    $paginas = 9;
                elseif (Url::getURL(0) == 'trabalhe-conosco'):
                    $paginas = 10;
                endif;

                ?>


                <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",$paginas) ?>
                <div class="col-12 border_bottom top25">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb ">
                        <li class="breadcrumb-item "><a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php Util::imprime($banner[legenda_1]); ?></li>
                    </ol>
                </nav>
            </div>
            <?php  endif;?>

        </div>

      </div>

    </div>



  </div>

</div>
</div>


</div>
