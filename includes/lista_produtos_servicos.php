<?php if (Url::getURL(0) == "" or Url::getURL(0) == "produtos") : ?>


  <?php if (Url::getURL(0) == "produtos") {
    $fundo = 'fundo_contato';
  }
  ?>
  <div class="row top20">
    <div class="col-4 top20 text-center produto_titulo">
      <?php $row3 = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 2); ?>
      <h3><?php Util::imprime($row3[legenda_1]); ?></h3>
      <h6><?php Util::imprime($row3[legenda_2]); ?></h6>
      <img class="top5" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo_home.png" alt="">


    </div>
    <?php if (Url::getURL(0) == "") : ?>

      <div class="col-4 top15 text-right ml-auto">
        <a class="btn btn-outline-secondary btn_cinza_escuro"
        href="<?php echo Util::caminho_projeto() ?>/produtos" title="SAIBA MAIS">
        VER TODOS PRODUTOS


      </a>
    </div>
  <?php endif; ?>
</div>


<div class="row">


  <?php
  $i = 0;

  if (Url::getURL(0) == "") :
    $result1 = $obj_site->select("tb_produtos", "and exibir_home = 'SIM'");
    else :
      $result1 = $obj_site->select("tb_produtos");
    endif;

    if (mysql_num_rows($result1) > 0) {

      while ($row1 = mysql_fetch_array($result1)) {

        ?>

        <div class="col-4 carroucel_produtos top25">
          <div class="card relativo">
            <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row1[url_amigavel]); ?>"
              title="<?php Util::imprime($row1[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row1[imagem]", 360, 270,
              array("class" => "w-100", "alt" => "$row1[titulo]")) ?>
            </a>


            <div class="produto-hover">
              <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row1[url_amigavel]); ?>"
                title="Saiba Mais">
                <div class="col-12 text-center card-body bg_produtos_home">
                  <h2><?php Util::imprime($row1[titulo], 40); ?></h2>
                </div>
              </a>
            </div>

          </div>

        </div>


        <?php
      }

    }
    ?>

  </div>

<?php endif; ?>




<?php if (Url::getURL(0) == "") : ?>

  <!-- ======================================================================= -->
  <!--SERVIÇOS   -->
  <!-- ======================================================================= -->
  <div class="row bottom50 top30">
    <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 4); ?>
    <div class="col-12 text-center produto_titulo">
      <h3><?php Util::imprime($banner[legenda_1]); ?></h3>
      <h6><?php Util::imprime($banner[legenda_2]); ?></h6>
      <img class="top5" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo_home.png" alt="">
    </div>


  </div>


  <div class="row">
    <?php

    if (Url::getURL(0) == "") :
      $result = $obj_site->select("tb_servicos", "and exibir_home = 'SIM' limit 3");
      else :
        $result = $obj_site->select("tb_servicos");
      endif;

      if (mysql_num_rows($result) > 0) {
        $i = 0;
        while ($row = mysql_fetch_array($result)) {

          ?>

          <div class="col-3 p-0">
            <div class="lista-produto portfolio">
              <div class="grid">
                <figure class="effect-julia">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 204, 138,
                  array("class" => "", "alt" => "$row[titulo]")) ?>
                  <figcaption>
                    <div class="hover">
                      <p class="col-12 top50 text-center">
                        <a class="btn"
                        href="<?php echo Util::caminho_projeto() ?>/servico/<?php echo Util::imprime($row[url_amigavel]) ?>"
                        title="SAIBA MAIS">
                        <i class="fas fa-plus-circle fa-3x"></i>
                      </a>
                    </p>
                  </div>
                </figcaption>
              </figure>

            </div>
            <div class="col-12 text-center desc_servicos_geral p-2">
              <h5 class="text-left Yanone"><span><?php echo Util::imprime($row[titulo], 41) ?></span></h5>
            </div>
          </div>

          <img class="col-12 p-0   text-center "
          src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo_servicos.png" alt="">


        </div>


        <?php

        if ($i == 1) {
          echo '<div class="clearfix"></div>';
          $i = 0;
        } else {
          $i++;
        }

      }
    }
    ?>

    <div class="col-3 fundo_ver_todos top120 text-right">
      <a class="btn btn-outline-secondary btn_cinza_escuro "
      href="<?php echo Util::caminho_projeto() ?>/servicos/"
      title="VER TODOS">
      VER TODOS
    </a>
  </div>

</div>
<!-- ======================================================================= -->
<!--SERVIÇOS   -->
<!-- ======================================================================= -->



<?php endif; ?>
