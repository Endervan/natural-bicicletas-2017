
<?php if (Url::getURL( 0 ) == "dicas"  or Url::getURL( 0 ) == "dica" or Url::getURL( 0 ) == "servicos"  or Url::getURL( 0 ) == "servico" or Url::getURL( 0 ) == "produto") :?>
  <div class="container">

    <div class="row top20">
      <div class="col-4 text-center produto_titulo">
        <?php $row3 = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 2);?>
        <h3><?php Util::imprime($row3[legenda_1]); ?></h3>
        <h6><?php Util::imprime($row3[legenda_2]); ?></h6>
        <img class="top5" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo_home.png" alt="">


      </div>

      <div class="col-2 ml-auto text-right top15"  id="slider_carousel1">
        <div class="custom-navigation">
          <a href="#" class="flex-prev"><i class="far fa-arrow-alt-circle-left fa-2x"></i></a>
          <!-- <div class="custom-controls-container"></div> -->
          <a href="#" class="flex-next"><i class="far fa-arrow-alt-circle-right fa-2x"></i></a>
        </div>
      </div>


      <div class="col-2 p-0 top15 text-right">
        <a class="btn btn-outline-secondary btn_cinza_escuro" href="<?php echo Util::caminho_projeto() ?>/produtos" title="SAIBA MAIS">
          VER TODOS PRODUTOS
        </a>
      </div>
    </div>


    <div class="row justify-content-md-center relativo">

      <div class="col-11 lista_produtos_home bottom20">
        <div id="slider_carousel" class="col-12 p-0 flexslider" >
          <ul class="slides">

            <?php
            $i=0;

            if (Url::getURL( 0 ) == "") :
              $result1 = $obj_site->select("tb_produtos","and exibir_home = 'SIM'");
              else :
                $result1 = $obj_site->select("tb_produtos");
              endif;

              if (mysql_num_rows($result1) > 0) {

                while ($row1 = mysql_fetch_array($result1)) {

                  ?>

                  <li class="col-4">
                    <div class=" carroucel_produtos top25">
                      <div class="card relativo">
                        <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row1[url_amigavel]); ?>" title="<?php Util::imprime($row1[titulo]); ?>">
                          <?php $obj_site->redimensiona_imagem("../uploads/$row1[imagem]", 360, 277, array("class"=>"w-100", "alt"=>"$row1[titulo]")) ?>
                        </a>


                        <div class="produto-hover">
                          <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row1[url_amigavel]); ?>" title="Saiba Mais">
                            <div class="col-12 text-center card-body bg_produtos_home">
                              <h2 ><?php Util::imprime($row1[titulo],40); ?></h2>
                            </div>
                          </a>
                        </div>

                      </div>

                    </div>


                  </li>

                  <?php
                }

              }
              ?>

            </ul>


          </div>




        </div>

      </div>
    </div>


  <?php endif; ?>



  <?php if (Url::getURL( 0 ) == "empresa"  or Url::getURL( 0 ) == "contato" or Url::getURL( 0 ) == "trabalhe-conosco"  or Url::getURL( 0 ) == "orcamento" ) :?>

    <div class="container relativo top3">
      <div class="row justify-content-center">
        <div class="col p-0">
          <!-- ======================================================================= -->
          <!-- mapa   -->
          <!-- ======================================================================= -->
          <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="390" frameborder="0" style="border:0" allowfullscreen></iframe>
          <!-- ======================================================================= -->
          <!-- mapa   -->
          <!-- ======================================================================= -->
        </div>

        <div class="col-4 bloco_endereco bloco_empresa pt-4 pb30">
          <div class="col-12 text-center desc_servicos_geral p-2">
            <h2 class=" Yanone">UNIDADE</h2>
            <img class="text-center "src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo_servicos.png" alt="">
          </div>

          <div class="col-12 p-0">
            <div class="media">
              <i class="fas fa-home  align-self-center mr-3" aria-hidden></i>
              <div class="media-body">
                <h6 class="mt-0"><?php Util::imprime($config[endereco]); ?></h6>
              </div>
            </div>
          </div>

          <div class="row top15">

            <div class="col-6">
              <div class="media">
                <i class="fas fa-phone  align-self-center mr-3" aria-hidden data-fa-transform="rotate-90"></i>
                <div class="media-body">
                  <h6 class="mt-0"><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></h6>
                </div>
              </div>
            </div>

            <?php if (!empty($config[telefone2])): ?>
              <div class="col-6">
                <div class="media">
                  <i class="fab fa-whatsapp  align-self-center mr-3" aria-hidden></i>
                  <div class="media-body">
                    <h6 class="mt-0"><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h6>
                  </div>
                </div>
              </div>
            <?php endif; ?>

          </div>

          <div class="btn_fluatuante">
            <a class="btn btn-outline-secondary btn_cinza_escuro top15" href="<?php Util::imprime($config[src_place]); ?>" target="_blank" title="SAIBA COMO CHEGAR">
              SAIBA COMO CHEGAR
            </a>
          </div>
        </div>



      </div>
    </div>

  <?php endif; ?>




<div class="container-fluid rodape">
	<div class="row pb20">

		<a href="#" class="scrollup">scrollup</a>


		<div class="container">
			<div class="row top15">


				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->
				<div class="col-1">
					<?php /*
					<a href="<?php echo Util::caminho_projeto() ?>/"  title="início">
					<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="início" />
					</a>
					*/ ?>
				</div>
				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->


				<div class="col-9">

					<!-- MENU   ================================================================ -->
					<!-- ======================================================================= -->
					<ul class="nav d-flex justify-content-start">

						<li class="nav-item ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?> "
								href="<?php echo Util::caminho_projeto() ?>/">HOME
							</a>
						</li>

						<li class="nav-item ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/empresa">EMPRESA
							</a>
						</li>

						<li class="nav-item ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "produtos" OR Url::getURL( 0 ) == "produto"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS
							</a>
						</li>


						<li class="nav-item ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/servicos">SERVICOS
							</a>
						</li>

						<li class="nav-item ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS
							</a>
						</li>


						<li class="nav-item ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "contato"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/contato">CONTATO
							</a>
						</li>

						<li class="nav-item ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "orcamento"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/orcamento">ORÇAMENTO
							</a>
						</li>





					</ul>

					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->



					<div class="col-12 top15 p-0">

						<div class="row telefone_rodape top5">
							<!-- ======================================================================= -->
							<!-- telefones  -->
							<!-- ======================================================================= -->
							<div class=" col-3 relativo">
								<h5><span>ATENDIMENTO</span></h5>
								<?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
								<?php if (!empty($config[telefone3])): ?>
									<div class="top5">
										<?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
									</div>
								<?php endif; ?>

							</div>


							<?php if (!empty($config[telefone2])): ?>
								<div class="col-3 relativo">
									<h5><span>WHATSAPP</span></h5>
									<?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>

									<?php if (!empty($config[telefone4])): ?>
										<div class="top5">
											<?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>
										</div>
									<?php endif; ?>

								</div>
							<?php endif;?>

							<!-- ======================================================================= -->
							<!-- telefones  -->
							<!-- ======================================================================= -->

							<?php if (!empty($config[endereco])): ?>
								<div class="col-6 endereco_topo media">
									<div class="media-left">
										<i class="fas fa-map-marker-alt right10" style="color: #fff;"></i>
									</div>
									<div class="media-body">
										<p class="media-heading"><?php Util::imprime($config[endereco]); ?></p>
									</div>
								</div>
							<?php endif;?>


						</div>



					</div>

				</div>



				<div class="col-2 text-center redes">
					<div class="row">

						<!-- ======================================================================= -->
						<!-- redes sociais    -->
						<!-- ======================================================================= -->
						<div class="col-8 top15 text-center redes">
							<?php if ($config[google_plus] != "") { ?>
								<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
									<i class="fab fa-google-plus-g"></i>
								</a>
							<?php } ?>

							<?php if ($config[facebook] != "") { ?>
								<a href="<?php Util::imprime($config[facebook]); ?>" title="facebook" target="_blank" >
									<i class="fab fa-facebook-square"></i>
								</a>
							<?php } ?>

							<?php if ($config[instagram] != "") { ?>
								<a href="<?php Util::imprime($config[instagram]); ?>" title="instagram" target="_blank" >
									<i class="fab fa-instagram"></i>
								</a>
							<?php } ?>
						</div>
						<!-- ======================================================================= -->
						<!-- redes sociais    -->
						<!-- ======================================================================= -->


						<div class="col-4 p-0">
							<a href="http://www.homewebbrasil.com.br" target="_blank">
								<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
							</a>
						</div>



					</div>

				</div>


			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row rodape_verde ">
		<div class="col-12 text-center top10 bottom10">
			<h6><span>TODOS OS DIREITOS RESERVADOS </span></h6>
		</div>
	</div>
</div>
