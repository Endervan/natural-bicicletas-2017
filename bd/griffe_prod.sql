-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: 186.202.152.197
-- Generation Time: 10-Maio-2017 às 12:53
-- Versão do servidor: 5.6.35-80.0-log
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `griffe_prod`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_atuacoes`
--

CREATE TABLE `tb_atuacoes` (
  `idatuacao` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_atuacoes`
--

INSERT INTO `tb_atuacoes` (`idatuacao`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`, `cargo`, `telefone`, `email`, `facebook`) VALUES
(158, 'Inovação', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'inovacao', NULL, NULL, NULL, NULL, '', '', '', NULL),
(157, 'Qualidade', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'qualidade', NULL, NULL, NULL, NULL, '', '', '', NULL),
(156, 'Ética', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'etica', NULL, NULL, NULL, NULL, '', '', '', NULL),
(155, 'Busca por resultados', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'busca-por-resultados', NULL, NULL, NULL, NULL, '', '', '', NULL),
(154, 'Compromisso com os clientes', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'compromisso-com-os-clientes', NULL, NULL, NULL, NULL, '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_avaliacoes_produtos`
--

CREATE TABLE `tb_avaliacoes_produtos` (
  `idavaliacaoproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `nota` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners`
--

CREATE TABLE `tb_banners` (
  `idbanner` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem_clientes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_btn_orcamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `imagem_clientes`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`, `legenda`, `url_btn_orcamento`) VALUES
(1, 'Index 01', '1404201708591392283977.jpg', NULL, 'NAO', NULL, '1', 'index-01', '/proddutos', NULL, NULL),
(2, 'Index 02', '1005201703207641104198.jpg', NULL, 'SIM', NULL, '1', 'index-02', '/produtos', NULL, NULL),
(3, 'Index 03', '1005201703221511734467.jpg', NULL, 'NAO', NULL, '1', 'index-03', '', NULL, NULL),
(4, 'Index 04', '1404201709001395741731.jpg', NULL, 'SIM', NULL, '1', 'index-04', '', NULL, NULL),
(5, 'Mobile-1', '1704201711391170358218.jpg', NULL, 'SIM', NULL, '2', 'mobile1', '', NULL, NULL),
(6, 'Mobile-2', '1704201711391188760037.jpg', NULL, 'SIM', NULL, '2', 'mobile2', '', NULL, NULL),
(7, 'Mobile-3', '1704201711391134576395.jpg', NULL, 'SIM', NULL, '2', 'mobile3', '', NULL, NULL),
(8, 'Mobile-4', '1704201711391184289590.jpg', NULL, 'SIM', NULL, '2', 'mobile4', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners_internas`
--

CREATE TABLE `tb_banners_internas` (
  `idbannerinterna` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `legenda` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_banners_internas`
--

INSERT INTO `tb_banners_internas` (`idbannerinterna`, `titulo`, `imagem`, `ordem`, `url_amigavel`, `ativo`, `legenda`) VALUES
(1, 'Empresa', '1404201709021241310597.jpg', NULL, 'empresa', 'SIM', NULL),
(3, 'Promoções', '1404201709101126757675.jpg', NULL, 'promocoes', 'SIM', NULL),
(5, 'Trabalhe Conosco', '1404201709111158844536.jpg', NULL, 'trabalhe-conosco', 'SIM', NULL),
(6, 'Orçamento', '1404201709161323785781.jpg', NULL, 'orcamento', 'SIM', NULL),
(7, 'Servicos', '1404201709151182017873.jpg', NULL, 'servicos', 'SIM', NULL),
(8, 'Serviços Dentro', '1404201709151120243595.jpg', NULL, 'servicos-dentro', 'SIM', NULL),
(9, 'Produtos', '1404201709161317645759.jpg', NULL, 'produtos', 'SIM', NULL),
(10, 'Produto Dentro', '1904201708341308084477.jpg', NULL, 'produto-dentro', 'SIM', NULL),
(11, 'Loja', '1604201702551333420051.jpg', NULL, 'loja', 'SIM', NULL),
(12, 'Mobile Servicos', '1804201711321269738990.jpg', NULL, 'mobile-servicos', 'SIM', NULL),
(13, 'Mobile Servicos Dentro', '1904201712581235553756.jpg', NULL, 'mobile-servicos-dentro', 'SIM', NULL),
(14, 'Mobile Trabalhe Conosco', '1904201711071137696585.jpg', NULL, 'mobile-trabalhe-conosco', 'SIM', NULL),
(15, 'Mobile Lojas', '1904201711321331686612.jpg', NULL, 'mobile-lojas', 'SIM', NULL),
(16, 'Mobile Produtos', '2104201707051350050239.jpg', NULL, 'mobile-produtos', 'SIM', NULL),
(17, 'Mobile Produto Dentro', '2104201708061376534607.jpg', NULL, 'mobile-produto-dentro', 'SIM', NULL),
(18, 'Mobile Orçamento', '1904201703221353901996.jpg', NULL, 'mobile-orcamento', 'SIM', NULL),
(19, 'Mobile Promoções', '1904201702441346886132.jpg', NULL, 'mobile-promocoes', 'SIM', NULL),
(20, 'Mobile Empresa', '2104201701301324570390.jpg', NULL, 'mobile-empresa', 'SIM', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_mensagens`
--

CREATE TABLE `tb_categorias_mensagens` (
  `idcategoriamensagem` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcategoriaproduto_pai` int(11) DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_categorias_mensagens`
--

INSERT INTO `tb_categorias_mensagens` (`idcategoriamensagem`, `titulo`, `idcategoriaproduto_pai`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(1, 'Mensagens de aniversário', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'mensagens-de-aniversario', 'Mensagens de aniversário', 'Mensagens de aniversário', 'Mensagens de aniversário'),
(2, 'Mensagens de casamento', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'mensagens-de-casamento', 'Mensagens de casamento', 'Mensagens de casamento', 'Mensagens de casamento'),
(3, 'Mensagens dia das mães', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'mensagens-dia-das-maes', 'Mensagens dia das mães', 'Mensagens dia das mães', 'Mensagens dia das mães'),
(4, 'Mensagens de amor', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'mensagens-de-amor', 'Mensagens de amor', 'Mensagens de amor', 'Mensagens de amor');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_produtos`
--

CREATE TABLE `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcategoriaproduto_pai` int(11) DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `idcategoriaproduto_pai`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(97, 'Primacy 3 - 94W', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'primacy-3--94w', '', '', ''),
(98, 'PIRELLI', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'pirelli', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_unidades`
--

CREATE TABLE `tb_categorias_unidades` (
  `idcategoriaunidade` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcategoriaunidade_pai` int(11) DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_categorias_unidades`
--

INSERT INTO `tb_categorias_unidades` (`idcategoriaunidade`, `titulo`, `idcategoriaunidade_pai`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(103, 'GOIÂNIA', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'goiÂnia', '', '', ''),
(105, 'BRASÍLIA', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'brasÍlia', '', '', ''),
(104, 'ANÁPOLIS', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'anÁpolis', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_clientes`
--

CREATE TABLE `tb_clientes` (
  `idcliente` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_clientes`
--

INSERT INTO `tb_clientes` (`idcliente`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `url`) VALUES
(40, 'VIA ENGENHARIA S.A', NULL, '2301201712493305767977..jpg', 'SIM', NULL, 'via-engenharia-sa', '', '', '', ''),
(41, 'BRASAL ADMINISTRAÇÃO E PARTICIPAÇÕES', NULL, '2301201712508758238206..jpg', 'SIM', NULL, 'brasal-administracao-e-participacoes', '', '', '', ''),
(42, 'EMPLAVI EMPREENDIMENTOS IMOBILIARIOS', NULL, '2301201712516345713362..jpg', 'SIM', NULL, 'emplavi-empreendimentos-imobiliarios', '', '', '', ''),
(43, 'ANTARES ENGENHARIA', NULL, '2301201712524572469099..jpg', 'SIM', NULL, 'antares-engenharia', '', '', '', ''),
(44, 'BROOKFIELD EMPREENDIMENTOS S.A', NULL, '2301201712532904414695..jpg', 'SIM', NULL, 'brookfield-empreendimentos-sa', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_dicas`
--

CREATE TABLE `tb_comentarios_dicas` (
  `idcomentariodica` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_dica` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_produtos`
--

CREATE TABLE `tb_comentarios_produtos` (
  `idcomentarioproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_configuracoes`
--

CREATE TABLE `tb_configuracoes` (
  `idconfiguracao` int(10) UNSIGNED NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `google_plus` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL,
  `ddd1` varchar(10) NOT NULL,
  `ddd2` varchar(10) NOT NULL,
  `ddd3` varchar(10) NOT NULL,
  `ddd4` varchar(10) NOT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `link_place` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `google_plus`, `telefone3`, `telefone4`, `ddd1`, `ddd2`, `ddd3`, `ddd4`, `facebook`, `twitter`, `link_place`, `instagram`) VALUES
(1, '', '', '', 'SIM', 0, '', 'Rua C69 N.99 Quadra C22 Lote 32 - Jardim Goiás - Goiânia - Goiás', '3515-1111', '9646-7175', 'marciomas@gmail.com', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3821.378911958309!2d-49.236650785571996!3d-16.707935288489164!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef1abc70916d7%3A0x116e733ee7fce10a!2sGriffe+Pneus+Michellin+Jardim+Goi%C3%A1s+-+Flam', NULL, NULL, 'junior@homewebbrasil.com.br', 'https://plus.google.com/103116009734588753907?hl=pt_BR', '', '', '(62)', '(62)', '', '', '', '', 'https://www.google.com.br/maps/place/Griffe+Pneus+Michellin+Jardim+Goi%C3%A1s+-+Flamboyant/@-16.7079353,-49.2366508,17z/data=!3m1!4b1!4m5!3m4!1s0x935ef1abc70916d7:0x116e733ee7fce10a!8m2!3d-16.7079353!4d-49.2344621', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_depoimentos`
--

CREATE TABLE `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_depoimentos`
--

INSERT INTO `tb_depoimentos` (`iddepoimento`, `titulo`, `descricao`, `ativo`, `ordem`, `url_amigavel`, `imagem`) VALUES
(1, 'João Paulo', '<p>\r\n	Jo&atilde;o Paulo Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'joao-paulo', '1703201603181368911340..jpg'),
(2, 'Ana Júlia', '<p>\r\n	Ana J&uacute;lia&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'ana-julia', '0803201607301356333056..jpg'),
(3, 'Tatiana Alves', '<p>\r\n	Tatiana&nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'tatiana-alves', '0803201607301343163616.jpeg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_dicas`
--

CREATE TABLE `tb_dicas` (
  `iddica` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(1, 'Vantagens do Alumínio', '<p>\r\n	As esquadrias de alum&iacute;nio s&atilde;o esteticamente bonitas, possibilitam ampla variedade de cores e tons em pintura eletrost&aacute;tica a p&oacute; ou anodiza&ccedil;&atilde;o, refor&ccedil;ando ainda mais sua resist&ecirc;ncia e propiciando uma apar&ecirc;ncia mais uniforme e agrad&aacute;vel de acordo com a cor a ser utilizada em seu projeto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tecnicamente, o alum&iacute;nio &eacute; de extrema versatilidade, adequando-se aos mais variados tipos de projetos e de dimens&otilde;es de v&atilde;os. O alum&iacute;nio apresenta uma natural resist&ecirc;ncia &agrave; corros&atilde;o, o que garante longa vida &uacute;til para as esquadrias sem grandes esfor&ccedil;os e preocupa&ccedil;&atilde;o em sua manuten&ccedil;&atilde;o. Permite a fabrica&ccedil;&atilde;o de portas, janelas, esquadrias em v&aacute;rias tipologias. Recebem vidros simples, temperados, duplos insulados e laminados mesmo os de espessuras maiores. &Eacute;, tamb&eacute;m, o material que melhor aceita todos os componentes (acess&oacute;rios) e elementos de veda&ccedil;&atilde;o (escovas de veda&ccedil;&atilde;o, borrachas de EPDM, silicone).</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quando bem projetados, constru&iacute;dos e instalados, os caixilhos de alum&iacute;nio apresentam elevado desempenho quanto &agrave; estanqueidade ao vento e &agrave; chuva. Por tudo isso, os caixilhos de alum&iacute;nio s&atilde;o os preferidos da arquitetura e da constru&ccedil;&atilde;o civil nos edif&iacute;cios residenciais, comerciais e institucionais, tanto de &aacute;reas urbanas como industriais e litor&acirc;neas. O alum&iacute;nio &eacute; um material leve, estrutural e de baixa manuten&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Rajas Esquadrias de Alum&iacute;nio trabalha com diversas linhas de perfis do mercado.</p>', '1603201712511372404173..jpg', 'SIM', NULL, 'vantagens-do-aluminio', '', '', '', NULL),
(2, 'Qual alumínio escolher para as esquadrias da sua casa?', '<p>\r\n	Qual alum&iacute;nio escolher para as esquadrias da sua casa?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Na hora de comprar as janelas e portas de alum&iacute;nio e vidro, surge aquela d&uacute;vida: Qual cor escolher?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Pois bem, a cor de alum&iacute;nio mais usada hoje &eacute; a branca. Cl&aacute;ssica e discreta, acaba compondo mais com os ambientes pintados em cores claras (em geral a maioria das casas tem a cor geral interna branca ou algum tom suave).</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Mas outro alum&iacute;nio, vem roubando a cena por onde passa, destinado a casas contempor&acirc;neas que tendem para a mistura de cores s&oacute;brias como o cinza e elementos amadeirados. Trata-se do alum&iacute;nio preto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fonte:&nbsp;http://www.casanovah.com.br</p>', '1603201712511372404173..jpg', 'SIM', NULL, 'qual-aluminio-escolher-para-as-esquadrias-da-sua-casa', '', '', '', NULL),
(3, 'Porque Esquadrias de Alumínio?', '<p>\r\n	Devo usar esquadrias de aluminio ou esquadrias de PVC? A maioria das pessoas que constroem ja fizeram essa pergunta.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Atualmente encontramos no mercado os seguintes tipos de esquadrias:&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Esquadria de Alum&iacute;nio: As portas e janelas de alum&iacute;nio s&atilde;o amplamente utilizadas por estarem h&aacute; mais tempo no mercado e apresentarem algumas vantagens como custo beneficio, durabilidade e versatilidade. A aus&ecirc;ncia de manuten&ccedil;&atilde;o &eacute; outra qualidade das esquadrias de alum&iacute;nio. Tambem podemos destacar a baixa condutividade do aluminio, que assim como a esquadria de PVC auxilia no consumo de energia quando pensamos em ar condicionado.</p>', '1603201712511372404173..jpg', 'SIM', NULL, 'porque-esquadrias-de-aluminio', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_empresa`
--

CREATE TABLE `tb_empresa` (
  `idempresa` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'CONHEÇA A GRIFFE PNEUS', '<p>\r\n	A Rede Griffe Pneus iniciou suas opera&ccedil;&otilde;es em 2003 na cidade de Goi&acirc;nia e &eacute; especialista em pneus para ve&iacute;culos de passeio e caminhonetes. Pautada pelo profissionalismo e primando pelo excelente atendimento a seus clientes, tem como miss&atilde;o ser a solu&ccedil;&atilde;o em pneus e servi&ccedil;os correlatos como: Alinhamento, balanceamento, servi&ccedil;os de suspens&atilde;o e freios.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A rede vem se expandindo e conta hoje com 11 unidades, 5 revendas de pneus em Goi&acirc;nia, 01 revenda em An&aacute;polis e 5 revendas de pneus em Bras&iacute;lia, tendo sido premiada pela Michelin nos quesitos de organiza&ccedil;&atilde;o, efici&ecirc;ncia operacional e melhores pr&aacute;ticas comerciais.</p>', 'SIM', 0, '', '', '', 'conheÇa-a-griffe-pneus', NULL, NULL, NULL),
(2, 'A GRIFFE PNEUS', '<p>\r\n	<strong>A GRIFFE PNEUS -&nbsp;</strong><strong>A QUALIDADE QUE VOC&Ecirc; MERECE!</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Rede Griffe Pneus iniciou suas opera&ccedil;&otilde;es em 2003 na cidade de Goi&acirc;nia e &eacute; especialista em pneus para ve&iacute;culos de passeio e caminhonetes. Pautada pelo profissionalismo e primando pelo excelente atendimento a seus clientes, tem como miss&atilde;o ser a solu&ccedil;&atilde;o em pneus e servi&ccedil;os correlatos como: Alinhamento, balanceamento, servi&ccedil;os de suspens&atilde;o e freios.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A rede vem se expandindo e conta hoje com 11 unidades, 5 revendas de pneus em Goi&acirc;nia, 01 revenda em An&aacute;polis e 5 revendas de pneus em Bras&iacute;lia, tendo sido premiada pela Michelin nos quesitos de organiza&ccedil;&atilde;o, efici&ecirc;ncia operacional e melhores pr&aacute;ticas comerciais.</p>', 'SIM', 0, '', '', '', 'a-griffe-pneus', NULL, NULL, NULL),
(3, 'MISSÃO', '<div>\r\n	Ser a solu&ccedil;&atilde;o em pneus e servi&ccedil;os de suspens&atilde;o e freios, primando pela seguran&ccedil;a, transpar&ecirc;ncia e honestidade, utilizando conhecimento t&eacute;cnico e experi&ecirc;ncia a favor de nossos clientes.</div>', 'SIM', 0, '', '', '', 'missÃo', NULL, NULL, NULL),
(4, 'VALORES', '<p>\r\n	Honestidade</p>\r\n<p>\r\n	Espirito de Equipe</p>\r\n<p>\r\n	Respeito aos clientes e aos Fatos</p>\r\n<p>\r\n	Transpar&ecirc;ncia</p>\r\n<div>\r\n	&nbsp;</div>', 'SIM', 0, '', '', '', 'valores', NULL, NULL, NULL),
(5, 'DESCRIÇÃO LOJA', '<p>\r\n	Venha agora mesmo fazer uma visita, nossa equipe est&aacute; sempre preparada para realizar o seu atendimento.</p>', 'SIM', 0, '', '', '', 'descricao-loja', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_equipamentos`
--

CREATE TABLE `tb_equipamentos` (
  `idequipamento` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaequipamento` int(10) UNSIGNED NOT NULL,
  `id_subcategoriaequipamento` int(11) DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modelo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao_video` longtext COLLATE utf8_unicode_ci,
  `codigo_produto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exibir_home` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modelo_produto` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_equipamentos`
--

INSERT INTO `tb_equipamentos` (`idequipamento`, `titulo`, `imagem`, `descricao`, `id_categoriaequipamento`, `id_subcategoriaequipamento`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `modelo`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `codigo_produto`, `exibir_home`, `modelo_produto`) VALUES
(1, 'ESQUADRIAS DE ALUMÍNIO', '1603201707151347515644.jpg', '<p>\r\n	As esquadrias de alum&iacute;nio s&atilde;o esteticamente bonitas, possibilitam ampla variedade de cores e tons em pintura eletrost&aacute;tica a p&oacute; ou anodiza&ccedil;&atilde;o, refor&ccedil;ando ainda mais sua resist&ecirc;ncia e propiciando uma apar&ecirc;ncia mais uniforme e agrad&aacute;vel de acordo com a cor a ser utilizada em seu projeto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tecnicamente, o alum&iacute;nio &eacute; de extrema versatilidade, adequando-se aos mais variados tipos de projetos e de dimens&otilde;es de v&atilde;os. O alum&iacute;nio apresenta uma natural resist&ecirc;ncia &agrave; corros&atilde;o, o que garante longa vida &uacute;til para as esquadrias sem grandes esfor&ccedil;os e preocupa&ccedil;&atilde;o em sua manuten&ccedil;&atilde;o. Permite a fabrica&ccedil;&atilde;o de portas, janelas, esquadrias em v&aacute;rias tipologias. Recebem vidros simples, temperados, duplos insulados e laminados mesmo os de espessuras maiores. &Eacute;, tamb&eacute;m, o material que melhor aceita todos os componentes (acess&oacute;rios) e elementos de veda&ccedil;&atilde;o (escovas de veda&ccedil;&atilde;o, borrachas de EPDM, silicone).</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quando bem projetados, constru&iacute;dos e instalados, os caixilhos de alum&iacute;nio apresentam elevado desempenho quanto &agrave; estanqueidade ao vento e &agrave; chuva. Por tudo isso, os caixilhos de alum&iacute;nio s&atilde;o os preferidos da arquitetura e da constru&ccedil;&atilde;o civil nos edif&iacute;cios residenciais, comerciais e institucionais, tanto de &aacute;reas urbanas como industriais e litor&acirc;neas. O alum&iacute;nio &eacute; um material leve, estrutural e de baixa manuten&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Rajas Esquadrias de Alum&iacute;nio trabalha com diversas linhas de perfis do mercado.</p>', 94, NULL, '', '', '', 'SIM', 0, 'esquadrias-de-aluminio', '99999', NULL, NULL, NULL, NULL, NULL, 'SIM', ''),
(32, 'JANELAS DE ALUMÍNIO', '1603201707151347515644.jpg', '<p>\r\n	Nossas janelas de alum&iacute;nio s&atilde;o ideais para dar um toque requintado ao ambiente. Os melhores produtos com alta qualidade, ideiais para varanda, sacadas e salas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Entre em contato conosco. Solicite or&ccedil;amento.</p>', 94, NULL, '', '', '', 'SIM', 0, 'janelas-de-aluminio', NULL, NULL, NULL, NULL, NULL, NULL, 'SIM', ''),
(33, 'PORTAS DE ALUMÍNIO', '1603201707151347515644.jpg', '<p>\r\n	Disponibilizamos uma linha de portas de alum&iacute;nio que atendem todos os tipos de ambientes, dos mais simples aos mais requintados.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	S&atilde;o confeccionados em alum&iacute;nio com os melhores e mais variados tipos de acabamento; toda linha possui alta resist&ecirc;ncia a intemp&eacute;ries. Oferecemos diversos modelos de ma&ccedil;anetas e puxadores.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Entre em contato conosco. Solicite or&ccedil;amento.</p>', 94, NULL, '', '', '', 'SIM', 0, 'portas-de-aluminio', NULL, NULL, NULL, NULL, NULL, NULL, 'SIM', ''),
(34, 'FACHADA STRUCTURAL GLAZING', '1603201707151347515644.jpg', '<p>\r\n	Com a aplica&ccedil;&atilde;o da tecnologia Structural Glazing nas obras comerciais e residenciais, o empreendimento ganha seu valor no mercado imobili&aacute;rio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O avan&ccedil;o das modalidades e estilos das esquadrias de alum&iacute;nio, arquitetos utilizam de potentes ferramentas para criar ambientes altamente atrativos com muito mais intensidade de luz e um sentimento maior de espa&ccedil;o f&iacute;sico.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Structural Glazing proporciona a intera&ccedil;&atilde;o entre o funcional e o belo, onde sua constru&ccedil;&atilde;o oferecer&aacute; muito mais conforto e praticidade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Entre em contato com a Rajas Esquadrias de Alum&iacute;nio, que elaboraremos um planejamento perfeito &agrave; sua obra para implementa&ccedil;&atilde;o do Structural Glazing</p>', 96, NULL, '', '', '', 'SIM', 0, 'fachada-structural-glazing', NULL, NULL, NULL, NULL, NULL, NULL, 'SIM', NULL),
(35, 'REVESTIMENTO ACM', '1603201707151347515644.jpg', '<p>\r\n	Aluminium Composite Material. Laminado de duas chapas de alum&iacute;nio, sob tens&atilde;o controlada com um n&uacute;cleo de polietileno de baixa.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Redu&ccedil;&atilde;o de custos com manuten&ccedil;&atilde;o: ACM protege a parede externa da polui&ccedil;&atilde;o e do clima.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Mant&eacute;m a modernidade da fachada por muito tempo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A face externa do ACM bloqueia as radia&ccedil;&otilde;es solares. A c&acirc;mara de ar e o isolamento t&eacute;rmico reduzem a transmiss&atilde;o de calor por convec&ccedil;&atilde;o (frio ou calor, custo reduzido de ar condicionado).</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Sistema de fachadas ventilada, protege as paredes estruturais e n&atilde;o estruturais das trocas bruscas de temperatura, reduzindo dilata&ccedil;&otilde;es t&eacute;rmicas e impedindo riscos de fissura&ccedil;&otilde;es.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Possui reten&ccedil;&atilde;o de ru&iacute;dos e/ou vibra&ccedil;&otilde;es.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Proporciona redu&ccedil;&atilde;o de cargas aplicadas na estrutura da obra, racionalizando as se&ccedil;&otilde;es de vigas, pilares e funda&ccedil;&otilde;es.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Sua estrutura de fixa&ccedil;&atilde;o tamb&eacute;m &eacute; leve, diminuindo a sobrecarga nas estruturas de apoio.</p>', 96, NULL, '', '', '', 'SIM', 0, 'revestimento-acm', NULL, NULL, NULL, NULL, NULL, NULL, 'SIM', NULL),
(36, 'BRISES DE ALUMÍNIO', '1603201707151347515644.jpg', '<p>\r\n	Painel composto por perfil direitos e esquerdos formando m&oacute;dulos vari&aacute;veis, que se unem a outro painel id&ecirc;ntico mediante conectores de policarbonatos, obtendo-se um elemento compacto.</p>\r\n<p>\r\n	Brises s&atilde;o particularmente &uacute;til para fachadas quando se necessita diminuir a a&ccedil;&atilde;o da luz direta, ou quando se requer privacidade sem perder a luminosidade, ventila&ccedil;&atilde;o e vis&atilde;o do exterior.</p>\r\n<p>\r\n	Instala-se mediante uma ancoragem que se fixa diretamente na estrutura da obra e com um suporte de ajuste telesc&oacute;pico que se une ao Brise.</p>', 95, NULL, '', '', '', 'SIM', 0, 'brises-de-aluminio', NULL, NULL, NULL, NULL, NULL, NULL, 'SIM', NULL),
(37, 'PORTÕES DE ALUMÍNIO', '1603201707151347515644.jpg', '<p>\r\n	Com mais de 24 anos de atua&ccedil;&atilde;o no mercado de esquadrias em alum&iacute;nio fabricamos port&otilde;es de imensa qualidade e beleza no modelos e padr&otilde;es desejados pelo cliente.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Trabalhamos com port&otilde;es em alum&iacute;nio anodizado nas cores : fosco, preto e bronze e com pintura eletrost&aacute;tica nas cores : branco, verde, azul, vermelho, bege... entre tantas outras.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Entre em contato conosco. Solicite or&ccedil;amento.</p>', 94, NULL, '', '', '', 'SIM', 0, 'portoes-de-aluminio', NULL, NULL, NULL, NULL, NULL, NULL, 'SIM', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_equipes`
--

CREATE TABLE `tb_equipes` (
  `idequipe` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_especificacoes`
--

CREATE TABLE `tb_especificacoes` (
  `idespecificacao` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `title_google` longtext,
  `keywords_google` longtext,
  `description_google` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_especificacoes`
--

INSERT INTO `tb_especificacoes` (`idespecificacao`, `titulo`, `imagem`, `url_amigavel`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`) VALUES
(1, 'Porta Fixa Por Dentro do Vão', '0803201611341367322959..jpg', 'porta-fixa-por-dentro-do-vao', 'SIM', NULL, NULL, NULL, NULL),
(2, 'Porta Fixa Por Trás do Vão', '0803201611341157385324..jpg', 'porta-fixa-por-tras-do-vao', 'SIM', NULL, NULL, NULL, NULL),
(3, 'Formas de Fixação das Guias', '0803201611341304077829..jpg', 'formas-de-fixacao-das-guias', 'SIM', NULL, NULL, NULL, NULL),
(4, 'Vista Lateral do Rolo da Porta', '0803201611351168393570..jpg', 'vista-lateral-do-rolo-da-porta', 'SIM', NULL, NULL, NULL, NULL),
(5, 'Portinhola Lateral', '0803201611351116878064..jpg', 'portinhola-lateral', 'SIM', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_facebook`
--

CREATE TABLE `tb_facebook` (
  `idface` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_facebook`
--

INSERT INTO `tb_facebook` (`idface`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(36, 'face 1 Lorem ipsum dolor sit amet', '<p>\r\n	teste 01 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'face-1-lorem-ipsum-dolor-sit-amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', NULL),
(37, 'face 2 Lorem ipsum dolor sit amet', '<p>\r\n	teste 2 - Constru&ccedil;&atilde;o de piscina &eacute; coisa s&eacute;ria e cara. Economize na escolha dos materiais, mas n&atilde;o na m&atilde;o de obra.</p>\r\n<p>\r\n	2 - Defina qual o tamanho da piscina que ir&aacute; escolher. Se pretende reunir a fam&iacute;lia e muitos amigos evite piscinas pequenas.<br />\r\n	&nbsp;<br />\r\n	3 - Se voc&ecirc; tem ou pretende ter crian&ccedil;as e animais, escolha uma piscina que n&atilde;o seja muito funda e se preocupe com a constru&ccedil;&atilde;o de barreiras e coberturas, por quest&otilde;es de seguran&ccedil;a. As medidas mais usadas s&atilde;o de at&eacute; 1,30m ~ 1,40 na parte mais funda e 0,40m ~ 0,50m na parte mais rasa.</p>\r\n<p>\r\n	4 - Escolha um local com boa incid&ecirc;ncia de sol. Ningu&eacute;m quer usar piscina que fica na sombra!</p>\r\n<p>\r\n	5 - Evite a constru&ccedil;&atilde;o da piscina em locais com muitas &aacute;rvores, al&eacute;m de fazerem sombra, as folhas podem tornar a limpeza e manuten&ccedil;&atilde;o da piscina um tormento.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-2-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(38, 'face 3 Lorem ipsum dolor sit amet', '<p>\r\n	teste 03&nbsp; Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-3-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(39, 'face 4 Lorem ipsum dolor sit amet', '<p>\r\n	teste 04 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-4-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(40, 'face 5 Lorem ipsum dolor sit amet', '<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; color: rgb(0, 0, 0); text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	teste 5 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.<br />\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-5-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(41, 'face 6 Lorem ipsum dolor sit amet', '<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	teste 06 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-6-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(42, 'face 7 Lorem ipsum dolor sit amet', '<p>\r\n	<span style="color: rgb(0, 0, 0); font-family: Lato, sans-serif; font-size: 20px; line-height: 28.5714px; text-align: justify; background-color: rgb(243, 221, 146);">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.&nbsp;</span></p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dicas-mobile-lorem-ipsum-dolor-sit-amet-consetetur', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_fretes`
--

CREATE TABLE `tb_fretes` (
  `idfrete` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `uf` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `valor` double NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(145) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_fretes`
--

INSERT INTO `tb_fretes` (`idfrete`, `titulo`, `uf`, `valor`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Taguatinga Sul', 'DF', 10.5, '', 0, 'taguatinga-sul'),
(2, 'Samambaia', '', 7.98, 'SIM', 0, 'samambaia'),
(3, 'Recanto das Emas', '', 15, 'SIM', 0, 'recanto-das-emas'),
(4, 'Samambaia Norte', '', 14.89, 'SIM', 0, 'samambaia-norte'),
(5, 'Asa Sul', '', 32.5, 'SIM', 0, 'asa-sul'),
(6, 'Asa Norte', '', 10.09, 'SIM', 0, 'asa-norte');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias`
--

CREATE TABLE `tb_galerias` (
  `idgaleria` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_galeria` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_galerias`
--

INSERT INTO `tb_galerias` (`idgaleria`, `titulo`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_galeria`) VALUES
(36, 'BARES E RESTAURANTES', '1110201603492601083566.png', 'SIM', NULL, 'bares-e-restaurantes', NULL),
(37, 'LIMPEZA EM GERAL', '2409201612231228099970.png', 'SIM', NULL, 'limpeza-em-geral', NULL),
(38, 'ESCRITÓRIO E COMÉRCIO', '1110201603472583030532.png', 'SIM', NULL, 'escritorio-e-comercio', NULL),
(43, 'INDÚSTRIA E CONSTRUÇÃO CIVIL', '1110201603491738726090.png', 'SIM', NULL, 'industria-e-construcao-civil', NULL),
(44, 'ÁREA DE SAÚDE', '1110201603483164367804.png', 'SIM', NULL, 'area-de-saude', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_equipamentos`
--

CREATE TABLE `tb_galerias_equipamentos` (
  `id_galeriaequipamento` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_equipamento` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_equipamentos`
--

INSERT INTO `tb_galerias_equipamentos` (`id_galeriaequipamento`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_equipamento`) VALUES
(318, '1903201712231131483576.jpg', 'SIM', NULL, NULL, 1),
(319, '1903201712231272247915.jpg', 'SIM', NULL, NULL, 1),
(320, '1903201712461238500602.jpg', 'SIM', NULL, NULL, 1),
(321, '1903201712461268543830.jpg', 'SIM', NULL, NULL, 1),
(322, '1903201712461156664053.jpg', 'SIM', NULL, NULL, 1),
(323, '1903201712471288961912.jpg', 'SIM', NULL, NULL, 1),
(324, '1903201712471224158678.jpg', 'SIM', NULL, NULL, 1),
(325, '1903201712471218730278.jpg', 'SIM', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_geral`
--

CREATE TABLE `tb_galerias_geral` (
  `id_galeriageral` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_galeria` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_geral`
--

INSERT INTO `tb_galerias_geral` (`id_galeriageral`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_galeria`) VALUES
(126, '1110201606129370584777.jpg', 'SIM', NULL, NULL, 36),
(127, '1110201606128491200018.jpg', 'SIM', NULL, NULL, 36),
(128, '1110201606123935418981.jpg', 'SIM', NULL, NULL, 36),
(129, '1110201606125431446035.jpg', 'SIM', NULL, NULL, 36),
(130, '1110201606121990333273.jpg', 'SIM', NULL, NULL, 36),
(131, '1110201606129567316079.jpg', 'SIM', NULL, NULL, 36),
(132, '1110201606123052659552.jpg', 'SIM', NULL, NULL, 36),
(133, '1110201606128704088876.jpg', 'SIM', NULL, NULL, 36),
(134, '1110201606121395695730.jpg', 'SIM', NULL, NULL, 36),
(135, '1110201606129286724370.jpg', 'SIM', NULL, NULL, 36),
(136, '1110201606122500877558.jpg', 'SIM', NULL, NULL, 36),
(137, '1110201606127282586809.jpg', 'SIM', NULL, NULL, 36),
(138, '1110201606167336113651.jpg', 'SIM', NULL, NULL, 37),
(139, '1110201606167126144290.jpg', 'SIM', NULL, NULL, 37),
(140, '1110201606166641264855.jpg', 'SIM', NULL, NULL, 37),
(141, '1110201606166856153934.jpg', 'SIM', NULL, NULL, 37),
(142, '1110201606198216783127.jpg', 'SIM', NULL, NULL, 38),
(143, '1110201606194614951705.jpg', 'SIM', NULL, NULL, 38),
(144, '1110201606194581049274.jpg', 'SIM', NULL, NULL, 38),
(145, '1110201606196961685398.jpg', 'SIM', NULL, NULL, 38),
(146, '1110201606192095600320.jpg', 'SIM', NULL, NULL, 38),
(147, '1110201606236245474128.jpg', 'SIM', NULL, NULL, 43),
(148, '1110201606238815510364.jpg', 'SIM', NULL, NULL, 43),
(149, '1110201606237030607693.jpg', 'SIM', NULL, NULL, 43),
(150, '1110201606236153364814.jpg', 'SIM', NULL, NULL, 43),
(151, '1110201606235868531324.jpg', 'SIM', NULL, NULL, 43),
(152, '1110201606278714095320.jpg', 'SIM', NULL, NULL, 44),
(153, '1110201606278558400455.jpg', 'SIM', NULL, NULL, 44),
(154, '1110201606276515941147.jpg', 'SIM', NULL, NULL, 44),
(155, '1110201606272921856492.jpg', 'SIM', NULL, NULL, 44),
(156, '1110201606278393876192.jpg', 'SIM', NULL, NULL, 44),
(157, '1110201606279452905394.jpg', 'SIM', NULL, NULL, 44),
(158, '1110201606279434882745.jpg', 'SIM', NULL, NULL, 44);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_portifolios`
--

CREATE TABLE `tb_galerias_portifolios` (
  `idgaleriaportifolio` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_portifolio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_galerias_portifolios`
--

INSERT INTO `tb_galerias_portifolios` (`idgaleriaportifolio`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_portifolio`) VALUES
(1, '1503201609571300280060.jpg', 'SIM', NULL, NULL, 1),
(2, '1503201609571194123464.jpg', 'SIM', NULL, NULL, 1),
(3, '1503201609571223466219.jpg', 'SIM', NULL, NULL, 1),
(4, '1503201609571319150261.jpg', 'SIM', NULL, NULL, 1),
(5, '1503201609571312788443.jpg', 'SIM', NULL, NULL, 1),
(6, '1503201609571185453289.jpg', 'SIM', NULL, NULL, 2),
(7, '1503201609571385251299.jpg', 'SIM', NULL, NULL, 2),
(8, '1503201609571398241846.jpg', 'SIM', NULL, NULL, 2),
(9, '1503201609571372148996.jpg', 'SIM', NULL, NULL, 2),
(10, '1503201609571203846190.jpg', 'SIM', NULL, NULL, 2),
(11, '1503201609571209439705.jpg', 'SIM', NULL, NULL, 3),
(12, '1503201609571247186947.jpg', 'SIM', NULL, NULL, 3),
(13, '1503201609571183328677.jpg', 'SIM', NULL, NULL, 3),
(14, '1503201609571245061526.jpg', 'SIM', NULL, NULL, 3),
(15, '1503201609571132779946.jpg', 'SIM', NULL, NULL, 3),
(16, '1503201609571208483876.jpg', 'SIM', NULL, NULL, 4),
(17, '1503201609571274489300.jpg', 'SIM', NULL, NULL, 4),
(18, '1503201609571406945852.jpg', 'SIM', NULL, NULL, 4),
(19, '1503201609571220302542.jpg', 'SIM', NULL, NULL, 4),
(20, '1503201609571348685064.jpg', 'SIM', NULL, NULL, 4),
(21, '1503201609571281798209.jpg', 'SIM', NULL, NULL, 5),
(22, '1503201609571119695620.jpg', 'SIM', NULL, NULL, 5),
(23, '1503201609571342930547.jpg', 'SIM', NULL, NULL, 5),
(24, '1503201609571333131668.jpg', 'SIM', NULL, NULL, 5),
(25, '1503201609571184904665.jpg', 'SIM', NULL, NULL, 5),
(26, '1603201602001119086460.jpg', 'SIM', NULL, NULL, 6),
(27, '1603201602001399143623.jpg', 'SIM', NULL, NULL, 6),
(28, '1603201602001370562965.jpg', 'SIM', NULL, NULL, 6),
(29, '1603201602001360716700.jpg', 'SIM', NULL, NULL, 6),
(30, '1603201602001161033394.jpg', 'SIM', NULL, NULL, 6),
(31, '1603201602001294477762.jpg', 'SIM', NULL, NULL, 7),
(32, '1603201602001391245593.jpg', 'SIM', NULL, NULL, 7),
(33, '1603201602001270831865.jpg', 'SIM', NULL, NULL, 7),
(34, '1603201602001379540967.jpg', 'SIM', NULL, NULL, 7),
(35, '1603201602001260348087.jpg', 'SIM', NULL, NULL, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_produtos`
--

CREATE TABLE `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(108, '0312201603261334857869.jpg', 'SIM', NULL, NULL, 10),
(109, '0312201603271162207891.jpg', 'SIM', NULL, NULL, 10),
(110, '0312201603261334857869.jpg', 'SIM', NULL, NULL, 11),
(111, '0312201603271162207891.jpg', 'SIM', NULL, NULL, 11),
(112, '0312201603261334857869.jpg', 'SIM', NULL, NULL, 12),
(114, '0312201603261334857869.jpg', 'SIM', NULL, NULL, 13),
(115, '0312201603271162207891.jpg', 'SIM', NULL, NULL, 13),
(116, '0312201603261334857869.jpg', 'SIM', NULL, NULL, 14),
(117, '0312201603271162207891.jpg', 'SIM', NULL, NULL, 14),
(118, '0312201603261334857869.jpg', 'SIM', NULL, NULL, 15),
(119, '0312201603271162207891.jpg', 'SIM', NULL, NULL, 15),
(120, '0312201603261334857869.jpg', 'SIM', NULL, NULL, 16),
(166, '0812201602177905542188.jpg', 'SIM', NULL, NULL, 12),
(181, '1412201601291279244695.jpg', 'SIM', NULL, NULL, 18),
(183, '1412201601327354653543.jpg', 'SIM', NULL, NULL, 18),
(184, '1412201601345188640529.jpg', 'SIM', NULL, NULL, 18),
(185, '1412201601363512874473.jpg', 'SIM', NULL, NULL, 18),
(187, '1412201601388367633182.jpg', 'SIM', NULL, NULL, 18),
(189, '1412201601481164258441.jpg', 'SIM', NULL, NULL, 18),
(192, '1412201601589201568464.jpg', 'SIM', NULL, NULL, 19),
(193, '1412201601594736345501.jpg', 'SIM', NULL, NULL, 19),
(194, '1412201602006736607038.jpg', 'SIM', NULL, NULL, 19),
(195, '1412201602029113734579.jpg', 'SIM', NULL, NULL, 19),
(198, '1412201602106244296224.jpg', 'SIM', NULL, NULL, 19),
(199, '1412201602112216267788.jpg', 'SIM', NULL, NULL, 19),
(200, '1412201602166483627487.jpg', 'SIM', NULL, NULL, 20),
(202, '1412201602194784770707.jpg', 'SIM', NULL, NULL, 20),
(203, '1412201602214638742945.jpg', 'SIM', NULL, NULL, 20),
(204, '1412201602248486520395.jpg', 'SIM', NULL, NULL, 21),
(206, '1412201602287809317372.jpg', 'SIM', NULL, NULL, 21),
(207, '1412201602299915680819.jpg', 'SIM', NULL, NULL, 21),
(211, '1412201602423616565503.jpg', 'SIM', NULL, NULL, 22),
(213, '1412201602451737208846.jpg', 'SIM', NULL, NULL, 22),
(214, '1412201602469263180535.jpg', 'SIM', NULL, NULL, 22),
(215, '1412201602474642230624.jpg', 'SIM', NULL, NULL, 22),
(216, '1412201602501444182437.jpg', 'SIM', NULL, NULL, 23),
(217, '1412201602519573430021.jpg', 'SIM', NULL, NULL, 23),
(225, '1412201603282334212673.jpg', 'SIM', NULL, NULL, 24),
(226, '1412201603304683658132.jpg', 'SIM', NULL, NULL, 24),
(227, '1412201603328185320037.jpg', 'SIM', NULL, NULL, 24),
(228, '1412201612446978789385.jpg', 'SIM', NULL, NULL, 25),
(231, '1412201612517431387553.jpg', 'SIM', NULL, NULL, 25),
(233, '1412201603351615363218.jpg', 'SIM', NULL, NULL, 25),
(235, '1412201603395601586360.jpg', 'SIM', NULL, NULL, 26),
(236, '1412201603405369773771.jpg', 'SIM', NULL, NULL, 26),
(237, '1412201603443020228540.jpg', 'SIM', NULL, NULL, 26),
(239, '1412201603494721755857.jpg', 'SIM', NULL, NULL, 27),
(244, '1412201604073751124424.jpg', 'SIM', NULL, NULL, 27),
(245, '1412201604095576241633.jpg', 'SIM', NULL, NULL, 27),
(246, '1412201604153967896621.jpg', 'SIM', NULL, NULL, 28),
(247, '1412201604181340972164.jpg', 'SIM', NULL, NULL, 28),
(249, '1412201604218874671016.jpg', 'SIM', NULL, NULL, 28),
(253, '1412201604297713506134.jpg', 'SIM', NULL, NULL, 29),
(254, '1412201604311125238380.jpg', 'SIM', NULL, NULL, 29),
(255, '1412201604327155315321.jpg', 'SIM', NULL, NULL, 29),
(256, '1412201604357947775936.jpg', 'SIM', NULL, NULL, 30),
(257, '1412201604381193267115.jpg', 'SIM', NULL, NULL, 30),
(258, '1412201604405962807006.jpg', 'SIM', NULL, NULL, 30),
(260, '1412201604478759739055.jpg', 'SIM', NULL, NULL, 17),
(261, '1412201604483432678062.jpg', 'SIM', NULL, NULL, 17),
(262, '1412201604489491395296.jpg', 'SIM', NULL, NULL, 17),
(263, '1412201604516023883281.jpg', 'SIM', NULL, NULL, 9),
(264, '1412201604536255972302.jpg', 'SIM', NULL, NULL, 9),
(265, '3012201611231302957148.jpg', 'SIM', NULL, NULL, 32),
(266, '3012201611231189471659.jpg', 'SIM', NULL, NULL, 32),
(267, '3012201611231370962682.jpg', 'SIM', NULL, NULL, 32),
(268, '3012201611231212298309.jpg', 'SIM', NULL, NULL, 32),
(272, '2301201701107526184688.jpg', 'SIM', NULL, NULL, 35),
(273, '2301201701116685521879.jpg', 'SIM', NULL, NULL, 35),
(276, '2301201701149408080329.jpg', 'SIM', NULL, NULL, 34),
(277, '2301201701163007234848.jpg', 'SIM', NULL, NULL, 35),
(278, '2301201701183397554966.jpg', 'SIM', NULL, NULL, 35),
(279, '2702201712476967165859.jpg', 'SIM', NULL, NULL, 36),
(280, '2702201712481604955105.jpg', 'SIM', NULL, NULL, 36),
(281, '2702201712482199689066.jpg', 'SIM', NULL, NULL, 36),
(282, '2702201712481198707036.jpg', 'SIM', NULL, NULL, 36),
(285, '2702201712483928570999.jpg', 'SIM', NULL, NULL, 36),
(286, '2702201712488637281031.jpg', 'SIM', NULL, NULL, 36),
(288, '2702201712486804403079.jpg', 'SIM', NULL, NULL, 36),
(290, '2702201712483652329022.jpg', 'SIM', NULL, NULL, 36),
(291, '2702201712535032785501.jpg', 'SIM', NULL, NULL, 34),
(292, '2702201712588663168454.jpg', 'SIM', NULL, NULL, 34),
(293, '2702201712584727947480.jpg', 'SIM', NULL, NULL, 34),
(294, '2702201712581999009164.jpg', 'SIM', NULL, NULL, 34),
(295, '2702201712587569577209.jpg', 'SIM', NULL, NULL, 34),
(296, '2702201712585999135693.jpg', 'SIM', NULL, NULL, 34),
(299, '2702201701153015845426.jpg', 'SIM', NULL, NULL, 37),
(300, '2702201701154559683642.jpg', 'SIM', NULL, NULL, 37),
(301, '2702201701157999357171.jpg', 'SIM', NULL, NULL, 37),
(304, '2702201701154017251332.jpg', 'SIM', NULL, NULL, 37),
(306, '2702201701168064343494.jpg', 'SIM', NULL, NULL, 37),
(307, '2702201701169626195000.jpg', 'SIM', NULL, NULL, 37),
(308, '2702201701162691123150.jpg', 'SIM', NULL, NULL, 37),
(309, '2702201701169389909443.jpg', 'SIM', NULL, NULL, 37),
(310, '2702201701163100121795.jpg', 'SIM', NULL, NULL, 37),
(312, '2702201701165598417934.jpg', 'SIM', NULL, NULL, 37),
(314, '2702201701173811437491.jpg', 'SIM', NULL, NULL, 37),
(317, '1903201712211207654518.jpg', 'SIM', NULL, NULL, 1),
(318, '1903201712211405020189.jpg', 'SIM', NULL, NULL, 1),
(319, '1903201712221276389920.jpg', 'SIM', NULL, NULL, 1),
(326, '0605201701436864600040.jpg', 'SIM', NULL, NULL, 41),
(327, '0605201701437746144375.jpg', 'SIM', NULL, NULL, 41),
(328, '0605201701447675149647.jpg', 'SIM', NULL, NULL, 41),
(329, '0605201701442461816271.jpg', 'SIM', NULL, NULL, 41),
(330, '0605201701445805274815.jpg', 'SIM', NULL, NULL, 41),
(331, '0605201701442477557944.jpg', 'SIM', NULL, NULL, 41),
(332, '0605201702085948229523.jpg', 'SIM', NULL, NULL, 38),
(333, '0605201702082486660207.jpg', 'SIM', NULL, NULL, 38),
(334, '0605201702088171065126.jpg', 'SIM', NULL, NULL, 38),
(335, '0605201702084732109676.jpg', 'SIM', NULL, NULL, 38),
(336, '0605201702081612989339.jpg', 'SIM', NULL, NULL, 38),
(337, '0605201702089770436377.jpg', 'SIM', NULL, NULL, 38),
(338, '0605201702194837739430.jpg', 'SIM', NULL, NULL, 39),
(339, '0605201702194150886978.jpg', 'SIM', NULL, NULL, 39),
(340, '0605201702195610654171.jpg', 'SIM', NULL, NULL, 39),
(341, '0605201702196153630067.jpg', 'SIM', NULL, NULL, 39),
(342, '0605201702193188417719.jpg', 'SIM', NULL, NULL, 39),
(343, '0605201702199999114101.jpg', 'SIM', NULL, NULL, 39),
(344, '0605201702286467354194.jpg', 'SIM', NULL, NULL, 42),
(345, '0605201702285745938999.jpg', 'SIM', NULL, NULL, 42),
(346, '0605201702288459647591.jpg', 'SIM', NULL, NULL, 42),
(347, '0605201702282256753523.jpg', 'SIM', NULL, NULL, 42),
(348, '0605201702286873867751.jpg', 'SIM', NULL, NULL, 42),
(349, '0605201702287811354928.jpg', 'SIM', NULL, NULL, 42),
(350, '0605201702304900928330.jpg', 'SIM', NULL, NULL, 43),
(351, '0605201702302323155730.jpg', 'SIM', NULL, NULL, 43),
(352, '0605201702302718101955.jpg', 'SIM', NULL, NULL, 43),
(353, '0605201702308794460408.jpg', 'SIM', NULL, NULL, 43),
(354, '0605201702307733482192.jpg', 'SIM', NULL, NULL, 43),
(355, '0605201702304063476038.jpg', 'SIM', NULL, NULL, 43),
(356, '0605201702341991304320.jpg', 'SIM', NULL, NULL, 44),
(357, '0605201702341455887113.jpg', 'SIM', NULL, NULL, 44),
(358, '0605201702343342762283.jpg', 'SIM', NULL, NULL, 44),
(359, '0605201702346187645006.jpg', 'SIM', NULL, NULL, 44),
(360, '0605201702348067686760.jpg', 'SIM', NULL, NULL, 44),
(361, '0605201702349345312801.jpg', 'SIM', NULL, NULL, 44),
(362, '0605201702365051481185.jpg', 'SIM', NULL, NULL, 45),
(363, '0605201702361252899107.jpg', 'SIM', NULL, NULL, 45),
(364, '0605201702366517826033.jpg', 'SIM', NULL, NULL, 45),
(365, '0605201702365199263575.jpg', 'SIM', NULL, NULL, 45),
(366, '0605201702364015527413.jpg', 'SIM', NULL, NULL, 45),
(367, '0605201702361232166927.jpg', 'SIM', NULL, NULL, 45),
(368, '0605201702386897310920.jpg', 'SIM', NULL, NULL, 46),
(369, '0605201702385523302596.jpg', 'SIM', NULL, NULL, 46),
(370, '0605201702382352632813.jpg', 'SIM', NULL, NULL, 46),
(371, '0605201702388185278061.jpg', 'SIM', NULL, NULL, 46),
(372, '0605201702386258668510.jpg', 'SIM', NULL, NULL, 46),
(373, '0605201702381366222960.jpg', 'SIM', NULL, NULL, 46),
(374, '0605201702414981331092.jpg', 'SIM', NULL, NULL, 48),
(375, '0605201702411643758457.jpg', 'SIM', NULL, NULL, 48),
(376, '0605201702417010818061.jpg', 'SIM', NULL, NULL, 48),
(377, '0605201702418204929741.jpg', 'SIM', NULL, NULL, 48),
(378, '0605201702416046499303.jpg', 'SIM', NULL, NULL, 48),
(379, '0605201702415020934782.jpg', 'SIM', NULL, NULL, 48),
(380, '0605201702421199832041.jpg', 'SIM', NULL, NULL, 47),
(381, '0605201702427733874925.jpg', 'SIM', NULL, NULL, 47),
(382, '0605201702429908849514.jpg', 'SIM', NULL, NULL, 47),
(383, '0605201702429133332106.jpg', 'SIM', NULL, NULL, 47),
(384, '0605201702426765978141.jpg', 'SIM', NULL, NULL, 47),
(385, '0605201702422812092771.jpg', 'SIM', NULL, NULL, 47),
(386, '0605201702442640641178.jpg', 'SIM', NULL, NULL, 49),
(387, '0605201702445455128194.jpg', 'SIM', NULL, NULL, 49),
(388, '0605201702447491856838.jpg', 'SIM', NULL, NULL, 49),
(389, '0605201702442308393753.jpg', 'SIM', NULL, NULL, 49),
(390, '0605201702443297757108.jpg', 'SIM', NULL, NULL, 49),
(391, '0605201702449829293158.jpg', 'SIM', NULL, NULL, 49),
(392, '0605201702495286162023.jpg', 'SIM', NULL, NULL, 50),
(393, '0605201702494020317449.jpg', 'SIM', NULL, NULL, 50),
(394, '0605201702495654063646.jpg', 'SIM', NULL, NULL, 50),
(395, '0605201702497076220632.jpg', 'SIM', NULL, NULL, 50),
(396, '0605201702491850122159.jpg', 'SIM', NULL, NULL, 50),
(397, '0605201702499664111936.jpg', 'SIM', NULL, NULL, 50),
(398, '0605201702517131888692.jpg', 'SIM', NULL, NULL, 51),
(399, '0605201702512568153871.jpg', 'SIM', NULL, NULL, 51),
(400, '0605201702511786173900.jpg', 'SIM', NULL, NULL, 51),
(401, '0605201702518541019625.jpg', 'SIM', NULL, NULL, 51),
(402, '0605201702519826292710.jpg', 'SIM', NULL, NULL, 51),
(403, '0605201702513358631382.jpg', 'SIM', NULL, NULL, 51),
(404, '0605201702535872173541.jpg', 'SIM', NULL, NULL, 52),
(405, '0605201702532645104833.jpg', 'SIM', NULL, NULL, 52),
(406, '0605201702539208013088.jpg', 'SIM', NULL, NULL, 52),
(407, '0605201702535602395025.jpg', 'SIM', NULL, NULL, 52),
(408, '0605201702531607277012.jpg', 'SIM', NULL, NULL, 52),
(409, '0605201702538258396544.jpg', 'SIM', NULL, NULL, 52),
(410, '0805201707534292290111.jpg', 'SIM', NULL, NULL, 233),
(411, '0805201707554428607001.jpg', 'SIM', NULL, NULL, 233);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_servicos`
--

CREATE TABLE `tb_galerias_servicos` (
  `id_galeriaservico` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_servico` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_uniformes`
--

CREATE TABLE `tb_galerias_uniformes` (
  `id_galeriauniformes` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_uniforme` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_uniformes`
--

INSERT INTO `tb_galerias_uniformes` (`id_galeriauniformes`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_uniforme`) VALUES
(111, '2809201612081274866319.jpg', 'SIM', NULL, NULL, 1),
(112, '2809201612081336658844.jpg', 'SIM', NULL, NULL, 1),
(113, '2809201612081284847276.jpg', 'SIM', NULL, NULL, 1),
(114, '2809201612081126000436.jpg', 'SIM', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galeria_empresa`
--

CREATE TABLE `tb_galeria_empresa` (
  `idgaleriaempresa` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_galeria_empresa`
--

INSERT INTO `tb_galeria_empresa` (`idgaleriaempresa`, `titulo`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_empresa`) VALUES
(2, 'EMPRESA GALERIA 02', '2710201602115670501920.jpg', 'SIM', NULL, 'empresa-galeria-02', NULL),
(21, 'EMPRESA GALERIA 03', '2710201602114947246942.jpg', 'SIM', NULL, 'empresa-galeria-03', NULL),
(22, 'EMPRESA GALERIA 04', '2710201602115671943347.jpg', 'SIM', NULL, 'empresa-galeria-04', NULL),
(23, 'IMAGEM 4', '2710201603506180535070.jpg', 'SIM', NULL, 'imagem-4', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins`
--

CREATE TABLE `tb_logins` (
  `idlogin` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `acesso_tags` varchar(3) DEFAULT 'NAO',
  `url_amigavel` varchar(255) DEFAULT NULL,
  `super_admin` varchar(3) NOT NULL DEFAULT 'NAO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `titulo`, `senha`, `ativo`, `id_grupologin`, `email`, `acesso_tags`, `url_amigavel`, `super_admin`) VALUES
(1, 'Homeweb', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br', 'SIM', NULL, 'NAO'),
(2, 'Marcio André', '202cb962ac59075b964b07152d234b70', 'SIM', 0, 'marciomas@gmail.com', 'NAO', 'marcio-andre', 'SIM'),
(3, 'Amanda', 'b362cb319b2e525dc715702edf416f10', 'SIM', 0, 'homewebbrasil@gmail.com', 'SIM', NULL, 'SIM');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logs_logins`
--

CREATE TABLE `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:03', 1),
(2, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:11', 1),
(3, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:12', 1),
(4, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:38', 1),
(5, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:19:57', 1),
(6, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:15:44', 1),
(7, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:16:58', 1),
(8, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:20:30', 1),
(9, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:21:15', 1),
(10, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:14', 1),
(11, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:27', 1),
(12, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:12', 1),
(13, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:34', 1),
(14, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:34:29', 1),
(15, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:42:14', 1),
(16, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:45:13', 1),
(17, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:06:59', 1),
(18, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:07:22', 1),
(19, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:21:44', 1),
(20, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:22:01', 1),
(21, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'1\'', '2016-03-02', '22:23:41', 1),
(22, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:24:30', 1),
(23, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:24:52', 1),
(24, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:24:56', 1),
(25, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:25:06', 1),
(26, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'1\'', '2016-03-02', '22:27:22', 1),
(27, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:27:25', 1),
(28, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:27:28', 1),
(29, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:27:31', 1),
(30, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'1\'', '2016-03-02', '22:28:19', 1),
(31, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:28:22', 1),
(32, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:28:37', 1),
(33, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:28:39', 1),
(34, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:28:42', 1),
(35, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:29:26', 1),
(36, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:29:31', 1),
(37, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:29:57', 1),
(38, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:30:01', 1),
(39, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:07', 1),
(40, 'DESATIVOU O LOGIN 3', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'3\'', '2016-03-02', '22:32:13', 1),
(41, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'3\'', '2016-03-02', '22:32:13', 1),
(42, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:41', 1),
(43, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'4\'', '2016-03-02', '22:32:46', 1),
(44, 'ATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'4\'', '2016-03-02', '22:32:49', 1),
(45, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'4\'', '2016-03-02', '22:32:51', 1),
(46, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'4\'', '2016-03-02', '22:32:54', 1),
(47, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:33:10', 1),
(48, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'5\'', '2016-03-02', '22:34:16', 1),
(49, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:34:35', 1),
(50, 'DESATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'6\'', '2016-03-02', '22:38:39', 1),
(51, 'ATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'6\'', '2016-03-02', '22:38:44', 1),
(52, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'6\'', '2016-03-02', '22:38:47', 1),
(53, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:41:49', 1),
(54, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:40:19', 0),
(55, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:03', 0),
(56, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:35', 0),
(57, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:01', 0),
(58, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:21', 0),
(59, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:34', 0),
(60, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:50', 0),
(61, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:43:06', 0),
(62, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:56:12', 0),
(63, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:57:39', 0),
(64, 'DESATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'43\'', '2016-03-07', '21:58:16', 0),
(65, 'ATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'43\'', '2016-03-07', '21:58:18', 0),
(66, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:03', 0),
(67, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:09', 0),
(68, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:34', 0),
(69, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:44', 0),
(70, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:56', 0),
(71, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:09:35', 0),
(72, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:10:27', 0),
(73, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:12:58', 0),
(74, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:14:20', 0),
(75, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:15:08', 0),
(76, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:27:15', 0),
(77, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:29:53', 0),
(78, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:30:18', 0),
(79, 'DESATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'44\'', '2016-03-08', '00:43:43', 0),
(80, 'ATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'44\'', '2016-03-08', '00:43:48', 0),
(81, 'DESATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'46\'', '2016-03-08', '00:43:53', 0),
(82, 'DESATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'45\'', '2016-03-08', '00:43:56', 0),
(83, 'ATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'46\'', '2016-03-08', '00:43:59', 0),
(84, 'ATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'45\'', '2016-03-08', '00:44:02', 0),
(85, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:32', 0),
(86, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:43', 0),
(87, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:56', 0),
(88, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:55:31', 0),
(89, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:56:16', 0),
(90, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '20:50:57', 0),
(91, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:12', 0),
(92, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:42', 0),
(93, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:58', 0),
(94, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:14', 0),
(95, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:44', 0),
(96, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:06', 0),
(97, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:55', 0),
(98, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:20', 0),
(99, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:35', 0),
(100, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:56', 0),
(101, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:17', 0),
(102, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:39', 0),
(103, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-09', '13:51:08', 0),
(104, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:47:37', 3),
(105, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:04', 3),
(106, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:53', 3),
(107, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:49:40', 3),
(108, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'3\'', '2016-03-11', '11:49:47', 3),
(109, 'DESATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = \'NAO\' WHERE idlogin = \'2\'', '2016-03-11', '11:49:51', 3),
(110, 'ATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = \'SIM\' WHERE idlogin = \'2\'', '2016-03-11', '11:49:53', 3),
(111, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:51:20', 3),
(112, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:35:06', 3),
(113, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:36:19', 3),
(114, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'4\'', '2016-03-11', '12:36:27', 3),
(115, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: contato1@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'5\'', '2016-03-11', '12:36:30', 3),
(116, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: contato2@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'6\'', '2016-03-11', '12:36:32', 3),
(117, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'\', email = \'2\', id_grupologin = \'2\' WHERE idlogin = \'2\'', '2016-03-11', '12:37:48', 3),
(118, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'\', email = \'2\', id_grupologin = \'\' WHERE idlogin = \'2\'', '2016-03-11', '12:38:15', 3),
(119, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'\', email = \'2\' WHERE idlogin = \'2\'', '2016-03-11', '12:38:42', 3),
(120, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'2\', email = \'2\' WHERE idlogin = \'2\'', '2016-03-11', '12:39:26', 3),
(121, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:22:49', 3),
(122, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:23:39', 3),
(123, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:24:16', 3),
(124, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:10', 3),
(125, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:22', 3),
(126, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:02', 3),
(127, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:36', 3),
(128, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:46:24', 3),
(129, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:47:24', 3),
(130, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:57:19', 3),
(131, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '14:32:53', 3),
(132, 'CADASTRO DO CLIENTE ', '', '2016-03-14', '21:25:38', 0),
(133, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-14', '21:41:54', 0),
(134, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:22', 0),
(135, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:53', 0),
(136, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:46:22', 0),
(137, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:15', 0),
(138, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:46', 0),
(139, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = \'4\'', '2016-03-15', '21:50:04', 0),
(140, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = \'5\'', '2016-03-15', '21:50:07', 0),
(141, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:50:37', 0),
(142, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:51:02', 0),
(143, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-15', '22:40:48', 0),
(144, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:02', 1),
(145, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:28', 1),
(146, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-17', '15:18:52', 1),
(147, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:35:37', 1),
(148, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:37:17', 1),
(149, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '10:24:37', 1),
(150, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:07:25', 1),
(151, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:28:43', 1),
(152, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:28:59', 1),
(153, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:29:32', 1),
(154, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:29:40', 1),
(155, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '20:43:11', 1),
(156, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '21:54:11', 1),
(157, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '22:07:44', 1),
(158, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '22:54:16', 1),
(159, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:01:55', 1),
(160, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:03:06', 1),
(161, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_lojas WHERE idloja = \'1\'', '2016-03-28', '23:04:52', 1),
(162, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:05:18', 1),
(163, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:05:40', 1),
(164, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:12:34', 1),
(165, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:15:26', 1),
(166, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:26:58', 1),
(167, 'EXCLUSÃO DO LOGIN 70, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'70\'', '2016-03-29', '00:36:19', 1),
(168, 'EXCLUSÃO DO LOGIN 73, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'73\'', '2016-03-29', '00:36:30', 1),
(169, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:45:43', 1),
(170, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:48:41', 1),
(171, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:49:15', 1),
(172, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:35:27', 1),
(173, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:35:49', 1),
(174, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:36:09', 1),
(175, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:36:22', 1),
(176, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:36:51', 1),
(177, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:37:26', 1),
(178, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:38:26', 1),
(179, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '02:32:40', 1),
(180, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '02:37:17', 1),
(181, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '03:57:35', 1),
(182, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:32:44', 1),
(183, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:33:28', 1),
(184, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:33:57', 1),
(185, 'CADASTRO DO CLIENTE ', '', '2016-03-31', '15:18:07', 1),
(186, 'CADASTRO DO CLIENTE ', '', '2016-03-31', '15:21:45', 1),
(187, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '15:21:55', 1),
(188, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '20:07:11', 1),
(189, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '20:19:04', 1),
(190, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '21:38:40', 1),
(191, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '23:46:08', 1),
(192, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:00:34', 1),
(193, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:29:59', 1),
(194, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:43:01', 1),
(195, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:57:06', 1),
(196, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:03:21', 1),
(197, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:04:07', 1),
(198, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:04:52', 1),
(199, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:13:23', 1),
(200, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:34:47', 1),
(201, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:39:11', 1),
(202, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:39:40', 1),
(203, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:40:19', 1),
(204, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '02:42:58', 1),
(205, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:06:04', 1),
(206, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:09:21', 1),
(207, 'EXCLUSÃO DO LOGIN 71, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'71\'', '2016-04-18', '15:20:51', 1),
(208, 'EXCLUSÃO DO LOGIN 72, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'72\'', '2016-04-18', '15:20:54', 1),
(209, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:21:57', 1),
(210, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:22:18', 1),
(211, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:22:40', 1),
(212, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:01', 1),
(213, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:38', 1),
(214, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:57', 1),
(215, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:24:16', 1),
(216, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:29:14', 1),
(217, 'DESATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'81\'', '2016-04-18', '15:30:48', 1),
(218, 'ATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = \'SIM\' WHERE idcategoriaproduto = \'81\'', '2016-04-18', '15:31:42', 1),
(219, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:45:42', 1),
(220, 'DESATIVOU O LOGIN 46', 'UPDATE tb_noticias SET ativo = \'NAO\' WHERE idnoticia = \'46\'', '2016-04-18', '16:46:04', 1),
(221, 'DESATIVOU O LOGIN 45', 'UPDATE tb_noticias SET ativo = \'NAO\' WHERE idnoticia = \'45\'', '2016-04-18', '16:46:08', 1),
(222, 'EXCLUSÃO DO LOGIN 45, NOME: , Email: ', 'DELETE FROM tb_noticias WHERE idnoticia = \'45\'', '2016-04-18', '16:46:13', 1),
(223, 'ATIVOU O LOGIN 46', 'UPDATE tb_noticias SET ativo = \'SIM\' WHERE idnoticia = \'46\'', '2016-04-18', '16:46:19', 1),
(224, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:55:46', 1),
(225, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:56:52', 1),
(226, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '22:48:24', 1),
(227, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '14:44:06', 1),
(228, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:41:17', 1),
(229, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:41:59', 1),
(230, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:42:09', 1),
(231, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:08', 1),
(232, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:39', 1),
(233, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:57', 1),
(234, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:46:13', 1),
(235, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '18:43:21', 1),
(236, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '18:43:51', 1),
(237, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:16:20', 1),
(238, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:16:53', 1),
(239, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:17:12', 1),
(240, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:11', 1),
(241, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:25', 1),
(242, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:46', 1),
(243, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:19:00', 1),
(244, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:19:12', 1),
(245, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:25:07', 1),
(246, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:49:42', 1),
(247, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:12', 1),
(248, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:35', 1),
(249, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:50', 1),
(250, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '21:59:44', 1),
(251, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:11:59', 1),
(252, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:28', 1),
(253, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:40', 1),
(254, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:50', 1),
(255, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:14:01', 1),
(256, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '22:43:10', 1),
(257, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:41:09', 1),
(258, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:42:55', 1),
(259, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:44:25', 1),
(260, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:48:05', 1),
(261, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:51:24', 1),
(262, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:51:33', 1),
(263, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:58:45', 1),
(264, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:59:34', 1),
(265, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '22:03:37', 1),
(266, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '22:49:25', 1),
(267, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '14:10:59', 1),
(268, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '14:11:05', 1),
(269, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:54:02', 1),
(270, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:58:40', 1),
(271, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:58:52', 1),
(272, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:59:03', 1),
(273, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '19:53:05', 1),
(274, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:53:46', 1),
(275, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:10', 1),
(276, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:26', 1),
(277, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:56', 1),
(278, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:55:07', 1),
(279, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:13:54', 1),
(280, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:45:51', 1),
(281, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:01', 1),
(282, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:07', 1),
(283, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:27', 1),
(284, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:49:47', 1),
(285, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:50:07', 1),
(286, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '23:39:50', 1),
(287, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '23:44:25', 1),
(288, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '13:35:07', 1),
(289, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '14:05:24', 1),
(290, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '14:27:09', 1),
(291, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-26', '23:39:01', 1),
(292, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-26', '23:40:17', 1),
(293, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '12:33:58', 1),
(294, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '13:15:17', 1),
(295, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '13:15:40', 1),
(296, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '14:42:16', 1),
(297, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '14:51:54', 1),
(298, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:27:30', 1),
(299, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:29:38', 1),
(300, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:49:14', 1),
(301, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-28', '11:01:16', 1),
(302, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-28', '11:01:35', 1),
(303, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-29', '12:43:56', 1),
(304, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '13:25:46', 1),
(305, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '13:37:42', 1),
(306, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:39:40', 1),
(307, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:42:57', 1),
(308, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:45:07', 1),
(309, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '18:39:34', 1),
(310, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-22', '14:55:09', 1),
(311, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:27:44', 1),
(312, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:29:49', 1),
(313, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:30:56', 1),
(314, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:31:20', 1),
(315, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:31:40', 1),
(316, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:32:04', 1),
(317, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:32:24', 1),
(318, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:33:16', 1),
(319, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:35:02', 1),
(320, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:40:12', 1),
(321, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '21:06:08', 1),
(322, 'DESATIVOU O LOGIN 80', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'80\'', '2016-06-01', '18:16:06', 1),
(323, 'DESATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'81\'', '2016-06-01', '18:16:08', 1),
(324, 'DESATIVOU O LOGIN 82', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'82\'', '2016-06-01', '18:16:11', 1),
(325, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:25:35', 1),
(326, 'EXCLUSÃO DO LOGIN 80, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'80\'', '2016-06-01', '18:34:17', 1),
(327, 'EXCLUSÃO DO LOGIN 81, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'81\'', '2016-06-01', '18:34:20', 1),
(328, 'EXCLUSÃO DO LOGIN 82, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'82\'', '2016-06-01', '18:34:42', 1),
(329, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:24', 1),
(330, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:38', 1),
(331, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:59', 1),
(332, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_subcategorias_produtos WHERE idsubcategoriaproduto = \'2\'', '2016-06-01', '18:36:05', 1),
(333, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:37:28', 1),
(334, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:38:26', 1),
(335, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:39:04', 1),
(336, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:39:12', 1),
(337, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '18:40:24', 1),
(338, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '18:40:53', 1),
(339, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:41:46', 1),
(340, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:42:02', 1),
(341, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:42:15', 1),
(342, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:43:23', 1),
(343, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '00:22:38', 1),
(344, 'DESATIVOU O LOGIN 5', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'5\'', '2016-06-02', '04:21:29', 1),
(345, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:00:19', 1),
(346, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:00:56', 1),
(347, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:01:45', 1),
(348, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:04:27', 1),
(349, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:04:40', 1),
(350, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'40\'', '2016-06-02', '11:05:11', 1),
(351, 'EXCLUSÃO DO LOGIN 42, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'42\'', '2016-06-02', '11:16:00', 1),
(352, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'43\'', '2016-06-02', '11:16:13', 1),
(353, 'EXCLUSÃO DO LOGIN 44, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'44\'', '2016-06-02', '11:16:21', 1),
(354, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:17:51', 1),
(355, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'1\'', '2016-06-02', '11:20:18', 1),
(356, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'2\'', '2016-06-02', '11:20:35', 1),
(357, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'7\'', '2016-06-02', '11:20:42', 1),
(358, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'8\'', '2016-06-02', '11:20:48', 1),
(359, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'9\'', '2016-06-02', '11:20:56', 1),
(360, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'10\'', '2016-06-02', '11:21:03', 1),
(361, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'11\'', '2016-06-02', '11:21:10', 1),
(362, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'12\'', '2016-06-02', '11:21:19', 1),
(363, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'13\'', '2016-06-02', '11:21:25', 1),
(364, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'14\'', '2016-06-02', '11:22:05', 1),
(365, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:32:03', 1),
(366, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:36:38', 1),
(367, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:47:10', 1),
(368, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:48:02', 1),
(369, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:22:04', 1),
(370, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:22:40', 1),
(371, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:23:02', 1),
(372, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:23:38', 1),
(373, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:24:38', 1),
(374, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:25:28', 1),
(375, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:26:59', 1),
(376, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '12:29:29', 1),
(377, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:30:38', 1),
(378, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:35:28', 1),
(379, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:01:58', 1),
(380, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:02:16', 1),
(381, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:02:32', 1),
(382, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:10:09', 1),
(383, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:10:35', 1),
(384, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:11:16', 1),
(385, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:11:30', 1),
(386, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:14:46', 1),
(387, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:16:27', 1),
(388, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:19:05', 1),
(389, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:20:43', 1),
(390, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:24:29', 1),
(391, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:29:41', 1),
(392, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:32:10', 1),
(393, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:35:32', 1),
(394, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:37:39', 1),
(395, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:40:09', 1),
(396, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:40:47', 1),
(397, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:05:48', 1),
(398, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:06:15', 1),
(399, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:08:11', 1),
(400, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:10:28', 1),
(401, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:13:04', 1),
(402, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:15:17', 1),
(403, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:16:46', 1),
(404, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:18:26', 1),
(405, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:20:23', 1),
(406, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:22:18', 1),
(407, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:24:26', 1),
(408, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:26:55', 1),
(409, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:29:36', 1),
(410, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:31:48', 1),
(411, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:36:17', 1),
(412, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:38:39', 1),
(413, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:43:09', 1),
(414, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:45:32', 1),
(415, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:48:55', 1),
(416, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:50:40', 1),
(417, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:52:46', 1),
(418, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '14:54:32', 1),
(419, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:56:52', 1),
(420, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:58:59', 1),
(421, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:00:49', 1),
(422, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:02:12', 1),
(423, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:05:12', 1),
(424, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:10:55', 1),
(425, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:16:33', 1),
(426, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:27:47', 1),
(427, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:30:01', 1),
(428, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:32:16', 1),
(429, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:34:35', 1),
(430, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:36:36', 1),
(431, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:40:38', 1),
(432, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:42:53', 1),
(433, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:44:40', 1),
(434, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:45:38', 1),
(435, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:46:04', 1),
(436, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:46:21', 1),
(437, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:48:43', 1),
(438, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:51:06', 1),
(439, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:54:10', 1),
(440, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:58:40', 1),
(441, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:01:16', 1),
(442, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '16:02:07', 1),
(443, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:02:33', 1),
(444, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:05:07', 1),
(445, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:07:05', 1),
(446, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:10:04', 1),
(447, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:11:23', 1),
(448, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:13:06', 1),
(449, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:15:27', 1),
(450, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:16:24', 1),
(451, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:17:59', 1),
(452, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:20:23', 1),
(453, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:22:13', 1),
(454, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:22:51', 1),
(455, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:24:49', 1),
(456, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:26:00', 1),
(457, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:51:47', 1),
(458, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:54:07', 1),
(459, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:56:39', 1),
(460, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:59:54', 1),
(461, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:02:10', 1),
(462, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:03:59', 1),
(463, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:05:58', 1),
(464, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:08:33', 1),
(465, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:10:38', 1),
(466, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:12:15', 1),
(467, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:14:15', 1),
(468, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:16:56', 1),
(469, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:20:40', 1),
(470, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:28:31', 1),
(471, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'41\'', '2016-06-02', '17:28:55', 1),
(472, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:31:32', 1),
(473, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:32:55', 1),
(474, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:34:01', 1),
(475, 'EXCLUSÃO DO LOGIN 46, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'46\'', '2016-06-02', '17:34:38', 1),
(476, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:38:07', 1),
(477, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:41:36', 1),
(478, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:44:20', 1),
(479, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:45:10', 1),
(480, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:48:45', 1),
(481, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:00:57', 1),
(482, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:03:36', 1),
(483, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:05:28', 1),
(484, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:08:49', 1),
(485, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:08:57', 1),
(486, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:09:09', 1),
(487, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:09:16', 1),
(488, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:11:08', 1),
(489, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-03', '14:35:23', 1),
(490, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-03', '14:48:13', 1),
(491, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:22:16', 1),
(492, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:22:59', 1),
(493, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:27:37', 1),
(494, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:28:23', 1),
(495, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:33:30', 1),
(496, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:01:20', 1),
(497, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:01:27', 1),
(498, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:04:09', 1),
(499, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '13:12:53', 1),
(500, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:08:56', 1),
(501, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:10:32', 1),
(502, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:19:38', 1),
(503, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'5\'', '2016-06-07', '14:20:12', 1),
(504, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'3\'', '2016-06-07', '14:20:29', 1),
(505, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'4\'', '2016-06-07', '14:20:31', 1),
(506, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:25:43', 1),
(507, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '14:26:57', 1),
(508, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '14:31:49', 1),
(509, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:58:26', 1),
(510, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:01:49', 1),
(511, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-06-07', '15:02:31', 1),
(512, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:02:44', 1),
(513, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:06:22', 1),
(514, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-06-07', '15:06:29', 1),
(515, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-06-07', '15:07:17', 1),
(516, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:08:05', 1),
(517, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'2\'', '2016-06-07', '19:19:12', 1),
(518, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '19:20:53', 1),
(519, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-06-07', '16:48:33', 1),
(520, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '16:57:35', 1),
(521, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '17:11:13', 1),
(522, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '17:11:59', 1),
(523, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '14:05:10', 3),
(524, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '14:08:08', 3),
(525, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '12:30:55', 1),
(526, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '15:38:33', 1),
(527, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '18:39:40', 3),
(528, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '20:42:20', 3),
(529, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:40:49', 1),
(530, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:41:59', 1),
(531, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:43:56', 1),
(532, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:47:20', 1),
(533, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:07:52', 1),
(534, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:09:32', 1),
(535, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:10:41', 1),
(536, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:14:05', 1),
(537, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:14:39', 1),
(538, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:15:18', 1),
(539, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:16:56', 1),
(540, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:17:48', 1),
(541, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '17:54:16', 1),
(542, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '17:55:13', 1),
(543, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:37:40', 1),
(544, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:43:05', 1),
(545, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:46:24', 1),
(546, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '09:11:11', 1),
(547, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '11:34:48', 1),
(548, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '12:03:24', 1),
(549, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '19:02:56', 0),
(550, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:39:29', 0),
(551, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '19:40:31', 0),
(552, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:05:34', 0),
(553, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:13:26', 0),
(554, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:15:05', 0),
(555, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '21:31:04', 0),
(556, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:12:42', 1),
(557, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:13:23', 1),
(558, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:13:54', 1),
(559, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:14:21', 1),
(560, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:09:27', 0),
(561, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:10:20', 0),
(562, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:15:20', 0),
(563, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '01:11:29', 0),
(564, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '09:35:50', 1),
(565, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:55:00', 0),
(566, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '09:55:01', 1),
(567, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:59:56', 0),
(568, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '10:49:36', 1),
(569, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '10:50:07', 1),
(570, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '11:03:55', 1),
(571, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:52:10', 1),
(572, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '13:19:01', 1),
(573, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '17:00:30', 1),
(574, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '17:41:42', 1),
(575, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '09:30:13', 1),
(576, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '09:32:16', 1),
(577, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-06', '17:07:15', 1),
(578, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:50:58', 1),
(579, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:10', 1),
(580, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:22', 1),
(581, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:30', 1),
(582, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '13:56:40', 1),
(583, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '14:02:37', 1),
(584, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '17:20:46', 1),
(585, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-13', '21:33:07', 1),
(586, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '10:53:24', 1),
(587, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:37:57', 1),
(588, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:42:35', 1),
(589, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:46:29', 1),
(590, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:51:04', 1),
(591, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:24:55', 1),
(592, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:27:05', 1),
(593, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:28:29', 1),
(594, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:02', 1),
(595, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:29', 1),
(596, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:39', 1),
(597, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:48', 1),
(598, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:40:05', 1),
(599, 'CADASTRO DO CLIENTE ', '', '2016-07-14', '15:47:41', 1),
(600, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:53:52', 1),
(601, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:57:42', 1),
(602, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:57:59', 1),
(603, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:13', 1),
(604, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:26', 1),
(605, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:41', 1),
(606, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:53', 1),
(607, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:59:08', 1),
(608, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '16:32:46', 1),
(609, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '18:56:30', 1),
(610, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:12', 1),
(611, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:31', 1),
(612, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:48', 1),
(613, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:00', 1),
(614, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:22', 1),
(615, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:46', 1),
(616, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:15:06', 1),
(617, 'CADASTRO DO CLIENTE ', '', '2016-07-14', '19:17:31', 1),
(618, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:30:31', 1),
(619, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '08:52:29', 1),
(620, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '11:58:06', 1),
(621, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '19:46:29', 1),
(622, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '19:47:35', 1),
(623, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'14\'', '2016-07-15', '19:59:25', 1),
(624, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'13\'', '2016-07-15', '19:59:32', 1),
(625, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'15\'', '2016-07-15', '19:59:36', 1),
(626, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'16\'', '2016-07-15', '19:59:42', 1),
(627, 'EXCLUSÃO DO LOGIN 18, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'18\'', '2016-07-15', '19:59:45', 1),
(628, 'EXCLUSÃO DO LOGIN 17, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'17\'', '2016-07-15', '19:59:49', 1),
(629, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '20:14:55', 1),
(630, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '11:00:46', 1),
(631, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:28:19', 1),
(632, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:37:56', 1),
(633, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:38:07', 1),
(634, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:48:05', 1),
(635, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:48:53', 1),
(636, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:11', 1),
(637, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:27', 1),
(638, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:45', 1),
(639, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:06', 1),
(640, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:29', 1),
(641, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:58', 1),
(642, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:02:46', 1),
(643, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:04:37', 1),
(644, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:12:13', 1),
(645, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:15:36', 1),
(646, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:06:46', 1),
(647, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:49:01', 1),
(648, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:54:14', 1),
(649, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:55:10', 1),
(650, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:00:07', 1),
(651, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:08:04', 1),
(652, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:15:21', 1),
(653, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:16:03', 1),
(654, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:19:17', 1),
(655, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:22:55', 1),
(656, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:25:29', 1),
(657, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-20', '09:35:26', 1),
(658, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-20', '18:33:00', 1),
(659, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '08:47:49', 1),
(660, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:01:41', 1),
(661, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:30:24', 1),
(662, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:46:31', 1),
(663, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '10:41:53', 1),
(664, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '10:42:27', 1),
(665, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '12:01:08', 1),
(666, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '12:59:31', 1),
(667, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '13:07:58', 1),
(668, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '19:12:55', 1),
(669, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_facebook WHERE idface = \'43\'', '2016-07-21', '19:23:48', 1),
(670, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:05', 1),
(671, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:28', 1),
(672, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:48', 1),
(673, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:08', 1),
(674, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:26', 1),
(675, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:41', 1),
(676, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:28', 1),
(677, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:36', 1),
(678, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:46', 1),
(679, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:57', 1),
(680, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:34:45', 1),
(681, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:57:51', 1),
(682, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:59:26', 1),
(683, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:01:05', 1),
(684, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:04:38', 1),
(685, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:04:55', 1),
(686, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:09', 1),
(687, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:26', 1),
(688, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:45', 1),
(689, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:06:02', 1),
(690, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '18:07:45', 1),
(691, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:29:36', 1),
(692, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:32:50', 1),
(693, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:33:06', 1),
(694, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:34:14', 1);
INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(695, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:34:39', 1),
(696, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-29', '08:57:37', 1),
(697, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '12:23:41', 1),
(698, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '15:32:54', 1),
(699, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '17:54:41', 1),
(700, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '17:55:28', 1),
(701, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '18:42:16', 1),
(702, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '19:42:53', 1),
(703, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '20:04:57', 1),
(704, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '20:12:02', 1),
(705, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-31', '14:29:17', 1),
(706, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:28', 1),
(707, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:42', 1),
(708, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:56', 1),
(709, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:52:43', 1),
(710, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '10:15:31', 1),
(711, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '10:21:54', 1),
(712, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '12:04:07', 1),
(713, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '15:58:27', 1),
(714, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '16:02:46', 1),
(715, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '17:35:49', 1),
(716, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:56:30', 1),
(717, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:01', 1),
(718, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:12', 1),
(719, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:23', 1),
(720, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '16:28:16', 1),
(721, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '17:20:26', 1),
(722, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '18:37:39', 1),
(723, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:25:58', 1),
(724, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:26:08', 1),
(725, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:58:36', 1),
(726, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:58:47', 1),
(727, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '09:09:03', 1),
(728, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '10:12:40', 1),
(729, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '16:44:59', 1),
(730, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '16:52:14', 1),
(731, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '17:12:56', 1),
(732, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '17:47:19', 1),
(733, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-04', '20:15:30', 1),
(734, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:27:29', 1),
(735, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:27:38', 1),
(736, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: teste1@gmail.com', 'DELETE FROM tb_equipes WHERE idequipe = \'1\'', '2016-08-25', '11:30:04', 1),
(737, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'2\'', '2016-08-25', '11:30:06', 1),
(738, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'3\'', '2016-08-25', '11:30:08', 1),
(739, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'4\'', '2016-08-25', '11:30:09', 1),
(740, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'5\'', '2016-08-25', '11:30:11', 1),
(741, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'6\'', '2016-08-25', '11:30:13', 1),
(742, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'7\'', '2016-08-25', '11:30:15', 1),
(743, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'8\'', '2016-08-25', '11:30:19', 1),
(744, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'9\'', '2016-08-25', '11:30:21', 1),
(745, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'10\'', '2016-08-25', '11:30:23', 1),
(746, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'11\'', '2016-08-25', '11:30:25', 1),
(747, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = \'12\'', '2016-08-25', '11:30:27', 1),
(748, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:33:12', 1),
(749, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:33:41', 1),
(750, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:33:55', 1),
(751, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:02', 1),
(752, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:09', 1),
(753, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:15', 1),
(754, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:22', 1),
(755, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:47', 1),
(756, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:56', 1),
(757, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:35:05', 1),
(758, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:35:37', 1),
(759, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:36:08', 1),
(760, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:36:30', 1),
(761, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'1\'', '2016-08-25', '11:36:53', 1),
(762, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'2\'', '2016-08-25', '11:36:54', 1),
(763, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'3\'', '2016-08-25', '11:36:56', 1),
(764, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'4\'', '2016-08-25', '11:36:58', 1),
(765, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'5\'', '2016-08-25', '11:37:01', 1),
(766, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'6\'', '2016-08-25', '11:37:03', 1),
(767, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'7\'', '2016-08-25', '11:37:05', 1),
(768, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'8\'', '2016-08-25', '11:37:07', 1),
(769, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'9\'', '2016-08-25', '11:37:09', 1),
(770, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'10\'', '2016-08-25', '11:37:10', 1),
(771, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'11\'', '2016-08-25', '11:37:12', 1),
(772, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'12\'', '2016-08-25', '11:37:13', 1),
(773, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'13\'', '2016-08-25', '11:37:15', 1),
(774, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'14\'', '2016-08-25', '11:37:17', 1),
(775, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'15\'', '2016-08-25', '11:37:19', 1),
(776, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'16\'', '2016-08-25', '11:37:21', 1),
(777, 'EXCLUSÃO DO LOGIN 17, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'17\'', '2016-08-25', '11:37:22', 1),
(778, 'EXCLUSÃO DO LOGIN 18, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'18\'', '2016-08-25', '11:37:24', 1),
(779, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-08-25', '11:38:15', 1),
(780, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'3\'', '2016-08-25', '11:38:17', 1),
(781, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'4\'', '2016-08-25', '11:38:19', 1),
(782, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:39:33', 1),
(783, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:41:06', 1),
(784, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:48:51', 1),
(785, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:49:46', 1),
(786, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:51:10', 1),
(787, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:53:18', 1),
(788, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:53:50', 1),
(789, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '12:02:47', 1),
(790, 'EXCLUSÃO DO LOGIN 39, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'39\'', '2016-08-25', '12:02:52', 1),
(791, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'40\'', '2016-08-25', '12:02:55', 1),
(792, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'41\'', '2016-08-25', '12:02:57', 1),
(793, 'EXCLUSÃO DO LOGIN 42, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'42\'', '2016-08-25', '12:02:59', 1),
(794, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'43\'', '2016-08-25', '12:03:01', 1),
(795, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '12:03:41', 1),
(796, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:14:52', 1),
(797, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:15:10', 1),
(798, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'1\'', '2016-08-25', '14:21:41', 1),
(799, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'2\'', '2016-08-25', '14:21:45', 1),
(800, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'3\'', '2016-08-25', '14:21:48', 1),
(801, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'4\'', '2016-08-25', '14:21:50', 1),
(802, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'5\'', '2016-08-25', '14:21:52', 1),
(803, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'6\'', '2016-08-25', '14:21:54', 1),
(804, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'7\'', '2016-08-25', '14:21:56', 1),
(805, 'CADASTRO DO CLIENTE ', '', '2016-08-25', '14:22:57', 1),
(806, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'8\'', '2016-08-25', '14:23:51', 1),
(807, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:43:32', 1),
(808, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:43:55', 1),
(809, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:44:17', 1),
(810, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:44:30', 1),
(811, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'8\'', '2016-08-25', '14:45:02', 1),
(812, 'EXCLUSÃO DO LOGIN 50, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'50\'', '2016-08-25', '14:45:07', 1),
(813, 'EXCLUSÃO DO LOGIN 51, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'51\'', '2016-08-25', '14:45:11', 1),
(814, 'EXCLUSÃO DO LOGIN 52, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'52\'', '2016-08-25', '14:45:15', 1),
(815, 'EXCLUSÃO DO LOGIN 53, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'53\'', '2016-08-25', '14:45:18', 1),
(816, 'EXCLUSÃO DO LOGIN 54, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'54\'', '2016-08-25', '14:45:20', 1),
(817, 'EXCLUSÃO DO LOGIN 55, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'55\'', '2016-08-25', '14:45:25', 1),
(818, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:49:01', 1),
(819, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:49:21', 1),
(820, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:49:47', 1),
(821, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:52:26', 1),
(822, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:55:43', 1),
(823, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:57:09', 1),
(824, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:59:15', 1),
(825, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:00:19', 1),
(826, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:00:59', 1),
(827, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:10:30', 1),
(828, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:12:32', 1),
(829, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:19:14', 1),
(830, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:19:41', 1),
(831, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:20:23', 1),
(832, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:21:01', 1),
(833, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:41:28', 1),
(834, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:32:47', 1),
(835, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:32:58', 1),
(836, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:09', 1),
(837, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:17', 1),
(838, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:24', 1),
(839, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:32', 1),
(840, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:40', 1),
(841, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:49', 1),
(842, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:56', 1),
(843, 'EXCLUSÃO DO LOGIN 151, NOME: , Email: ', 'DELETE FROM tb_atuacoes WHERE idatuacao = \'151\'', '2016-08-26', '15:34:14', 1),
(844, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:36:16', 1),
(845, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:42:46', 1),
(846, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:05', 1),
(847, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:22', 1),
(848, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:33', 1),
(849, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:46', 1),
(850, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:44:16', 1),
(851, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:44:52', 1),
(852, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:45:04', 1),
(853, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:46:30', 1),
(854, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '18:51:26', 0),
(855, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-08-27', '18:51:30', 0),
(856, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:16:59', 0),
(857, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:18:46', 0),
(858, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:21:34', 0),
(859, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:24:24', 0),
(860, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'3\'', '2016-08-27', '19:24:28', 0),
(861, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:33:13', 0),
(862, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'4\'', '2016-08-27', '19:33:40', 0),
(863, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-08-27', '19:41:11', 0),
(864, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'3\'', '2016-08-27', '19:41:14', 0),
(865, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:42:48', 0),
(866, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:43:20', 0),
(867, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-08-27', '19:43:24', 0),
(868, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'4\'', '2016-08-27', '19:43:27', 0),
(869, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:47:39', 0),
(870, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'3\'', '2016-08-27', '19:47:43', 0),
(871, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:50:56', 0),
(872, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:54:50', 0),
(873, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'4\'', '2016-08-27', '19:54:56', 0),
(874, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'3\'', '2016-08-27', '19:54:58', 0),
(875, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-08-27', '19:55:00', 0),
(876, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:56:22', 0),
(877, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:19:06', 0),
(878, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-08-27', '20:19:10', 0),
(879, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:20:55', 0),
(880, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:22:38', 0),
(881, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'3\'', '2016-08-27', '20:22:41', 0),
(882, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:29:14', 0),
(883, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:32:24', 0),
(884, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-08-27', '20:32:43', 0),
(885, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:34:14', 0),
(886, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:47:12', 0),
(887, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-08-27', '20:47:22', 0),
(888, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:49:08', 0),
(889, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:53:49', 0),
(890, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:59:22', 0),
(891, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '21:01:55', 0),
(892, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '21:05:47', 0),
(893, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-28', '03:27:16', 0),
(894, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-28', '03:46:33', 0),
(895, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-29', '11:46:28', 1),
(896, 'EXCLUSÃO DO LOGIN 77, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'77\'', '2016-08-29', '11:46:37', 1),
(897, 'EXCLUSÃO DO LOGIN 78, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'78\'', '2016-08-29', '11:46:39', 1),
(898, 'EXCLUSÃO DO LOGIN 79, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'79\'', '2016-08-29', '11:46:41', 1),
(899, 'CADASTRO DO CLIENTE ', '', '2016-08-29', '11:49:11', 1),
(900, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '16:39:22', 13),
(901, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:47:03', 13),
(902, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:48:25', 13),
(903, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:48:41', 13),
(904, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:48:52', 13),
(905, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:26:42', 1),
(906, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:28:49', 1),
(907, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:35:08', 1),
(908, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:57:02', 1),
(909, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:27:02', 1),
(910, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:28:27', 1),
(911, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:31:51', 1),
(912, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:35:11', 1),
(913, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:38:10', 1),
(914, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:53:56', 1),
(915, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:01:02', 1),
(916, 'CADASTRO DO CLIENTE ', '', '2016-09-13', '19:01:40', 1),
(917, 'CADASTRO DO CLIENTE ', '', '2016-09-13', '19:03:58', 1),
(918, 'CADASTRO DO CLIENTE ', '', '2016-09-13', '19:04:19', 1),
(919, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:05:43', 1),
(920, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:09:40', 1),
(921, 'EXCLUSÃO DO LOGIN 74, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'74\'', '2016-09-13', '19:09:48', 1),
(922, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:11:26', 1),
(923, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:11:47', 1),
(924, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:15:07', 1),
(925, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:57:44', 1),
(926, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '20:01:10', 1),
(927, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:23:45', 1),
(928, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:24:20', 1),
(929, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:27:15', 1),
(930, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:33:52', 1),
(931, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '10:39:26', 1),
(932, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '11:34:16', 1),
(933, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '11:46:08', 1),
(934, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '11:52:01', 1),
(935, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:07:45', 1),
(936, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:20:04', 1),
(937, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:20:42', 1),
(938, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:20:58', 1),
(939, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '15:11:57', 1),
(940, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '15:29:28', 1),
(941, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '15:29:48', 1),
(942, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '15:30:16', 1),
(943, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '15:31:19', 1),
(944, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '17:12:33', 1),
(945, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '19:50:15', 1),
(946, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '08:37:22', 1),
(947, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '09:01:49', 1),
(948, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '09:38:31', 1),
(949, 'EXCLUSÃO DO LOGIN 37, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'37\'', '2016-09-16', '09:38:38', 1),
(950, 'EXCLUSÃO DO LOGIN 38, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'38\'', '2016-09-16', '09:38:41', 1),
(951, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '09:41:49', 1),
(952, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:06:26', 1),
(953, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:06:41', 1),
(954, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:07:02', 1),
(955, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:54:08', 1),
(956, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '12:02:52', 1),
(957, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '12:03:05', 1),
(958, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '12:33:32', 1),
(959, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '15:53:42', 1),
(960, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '19:20:41', 1),
(961, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '10:48:09', 1),
(962, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '11:49:25', 1),
(963, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '11:52:15', 1),
(964, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '13:26:08', 1),
(965, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '13:26:47', 1),
(966, 'ATIVOU O LOGIN 1', 'UPDATE tb_unidades SET ativo = \'SIM\' WHERE idunidade = \'1\'', '2016-09-17', '14:48:21', 1),
(967, 'DESATIVOU O LOGIN 2', 'UPDATE tb_unidades SET ativo = \'NAO\' WHERE idunidade = \'2\'', '2016-09-17', '14:48:29', 1),
(968, 'DESATIVOU O LOGIN 1', 'UPDATE tb_unidades SET ativo = \'NAO\' WHERE idunidade = \'1\'', '2016-09-17', '14:48:34', 1),
(969, 'CADASTRO DO CLIENTE ', '', '2016-09-17', '14:50:07', 1),
(970, 'ATIVOU O LOGIN 2', 'UPDATE tb_unidades SET ativo = \'SIM\' WHERE idunidade = \'2\'', '2016-09-17', '14:51:18', 1),
(971, 'ATIVOU O LOGIN 1', 'UPDATE tb_unidades SET ativo = \'SIM\' WHERE idunidade = \'1\'', '2016-09-17', '14:51:21', 1),
(972, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '17:27:48', 1),
(973, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '17:28:34', 1),
(974, 'CADASTRO DO CLIENTE ', '', '2016-09-17', '17:30:06', 1),
(975, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:10:13', 1),
(976, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:10:49', 1),
(977, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:11:03', 1),
(978, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:14:08', 1),
(979, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:54:11', 1),
(980, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:54:30', 1),
(981, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:54:48', 1),
(982, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:55:08', 1),
(983, 'CADASTRO DO CLIENTE ', '', '2016-09-18', '11:09:45', 1),
(984, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:43:17', 1),
(985, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:44:24', 1),
(986, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:50:51', 1),
(987, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:50:58', 1),
(988, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:51:07', 1),
(989, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:51:13', 1),
(990, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '15:06:09', 1),
(991, 'EXCLUSÃO DO LOGIN 85, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'85\'', '2016-09-19', '11:05:44', 1),
(992, 'EXCLUSÃO DO LOGIN 76, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'76\'', '2016-09-19', '11:05:46', 1),
(993, 'EXCLUSÃO DO LOGIN 83, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'83\'', '2016-09-19', '11:05:49', 1),
(994, 'EXCLUSÃO DO LOGIN 84, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'84\'', '2016-09-19', '11:05:52', 1),
(995, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:07:45', 1),
(996, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:13:50', 1),
(997, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:14:46', 1),
(998, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:15:35', 1),
(999, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:17:05', 1),
(1000, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:21:57', 1),
(1001, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:03', 1),
(1002, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:09', 1),
(1003, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:15', 1),
(1004, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:20', 1),
(1005, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:27:28', 1),
(1006, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:28:25', 1),
(1007, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:06:56', 1),
(1008, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:07:31', 1),
(1009, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:10:36', 1),
(1010, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:12:26', 1),
(1011, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:12:53', 1),
(1012, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '13:55:28', 1),
(1013, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:36:48', 1),
(1014, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:40:08', 1),
(1015, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:40:15', 1),
(1016, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:42:01', 1),
(1017, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:42:07', 1),
(1018, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:44:41', 1),
(1019, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '12:23:17', 1),
(1020, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '12:23:45', 1),
(1021, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:01:25', 1),
(1022, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:01:35', 1),
(1023, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:01:52', 1),
(1024, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:02:04', 1),
(1025, 'CADASTRO DO CLIENTE ', '', '2016-09-21', '14:58:35', 1),
(1026, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '15:45:26', 1),
(1027, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '18:06:57', 1),
(1028, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '18:07:12', 1),
(1029, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '18:51:52', 1),
(1030, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '20:17:45', 1),
(1031, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '08:46:52', 1),
(1032, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '09:19:31', 1),
(1033, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '10:09:37', 1),
(1034, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '11:36:44', 1),
(1035, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:48:23', 1),
(1036, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:48:40', 1),
(1037, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:49:04', 1),
(1038, 'EXCLUSÃO DO LOGIN 39, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = \'39\'', '2016-09-23', '12:49:16', 1),
(1039, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:49:42', 1),
(1040, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '13:07:13', 1),
(1041, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '13:20:03', 1),
(1042, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = \'40\'', '2016-09-23', '14:07:52', 1),
(1043, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = \'41\'', '2016-09-23', '14:07:56', 1),
(1044, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '16:32:57', 1),
(1045, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '12:21:42', 1),
(1046, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '12:22:23', 1),
(1047, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '12:23:20', 1),
(1048, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '19:59:32', 3),
(1049, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:05:57', 3),
(1050, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:06:32', 3),
(1051, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:07:04', 3),
(1052, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:07:20', 3),
(1053, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:07:39', 3),
(1054, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:08:11', 3),
(1055, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:08:38', 3),
(1056, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:08:55', 3),
(1057, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:15:17', 3),
(1058, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:15:33', 3),
(1059, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_unidades WHERE idunidade = \'3\'', '2016-09-26', '20:15:38', 3),
(1060, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:17:17', 3),
(1061, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-07', '22:06:13', 3),
(1062, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-07', '22:06:47', 3),
(1063, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-07', '22:07:05', 3),
(1064, 'CADASTRO DO CLIENTE ', '', '2016-10-07', '22:07:34', 3),
(1065, 'CADASTRO DO CLIENTE ', '', '2016-10-07', '22:07:47', 3),
(1066, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-07', '22:09:08', 3),
(1067, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-07', '22:09:27', 3),
(1068, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-08', '12:00:31', 3),
(1069, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-08', '12:03:43', 3),
(1070, 'DESATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'1\'', '2016-10-09', '15:02:38', 3),
(1071, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:08:48', 3),
(1072, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:09:23', 3),
(1073, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:09:50', 3),
(1074, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:10:02', 3),
(1075, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:10:18', 3),
(1076, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:10:51', 3),
(1077, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:11:03', 3),
(1078, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:11:24', 3),
(1079, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:11:53', 3),
(1080, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:12:07', 3),
(1081, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:12:49', 3),
(1082, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:13:01', 3),
(1083, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:13:23', 3),
(1084, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:13:38', 3),
(1085, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:14:04', 3),
(1086, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:14:28', 3),
(1087, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:14:40', 3),
(1088, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:14:47', 3),
(1089, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:15:14', 3),
(1090, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:15:22', 3),
(1091, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:16:23', 3),
(1092, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:16:30', 3),
(1093, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:22:21', 3),
(1094, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:24:52', 3),
(1095, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:27:33', 3),
(1096, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:31:31', 3),
(1097, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:35:00', 3),
(1098, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:37:05', 3),
(1099, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:37:34', 3),
(1100, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:39:37', 3),
(1101, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:41:29', 3),
(1102, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:43:11', 3),
(1103, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:44:09', 3),
(1104, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:47:04', 3),
(1105, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:49:03', 3),
(1106, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:50:09', 3),
(1107, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:50:43', 3),
(1108, 'EXCLUSÃO DO LOGIN 19, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'19\'', '2016-10-11', '13:05:30', 3),
(1109, 'EXCLUSÃO DO LOGIN 20, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'20\'', '2016-10-11', '13:05:32', 3),
(1110, 'EXCLUSÃO DO LOGIN 21, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'21\'', '2016-10-11', '13:05:34', 3),
(1111, 'EXCLUSÃO DO LOGIN 22, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = \'22\'', '2016-10-11', '13:05:36', 3),
(1112, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:44:02', 3),
(1113, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:45:48', 3),
(1114, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:48:25', 3),
(1115, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '14:48:50', 3),
(1116, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:51:01', 3),
(1117, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:53:55', 3),
(1118, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:56:29', 3),
(1119, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:58:04', 3),
(1120, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:59:16', 3),
(1121, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '14:59:45', 3),
(1122, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:01:09', 3),
(1123, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:01:33', 3),
(1124, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:02:41', 3),
(1125, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:03:01', 3),
(1126, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:04:15', 3),
(1127, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:04:33', 3),
(1128, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:04:55', 3),
(1129, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:07:10', 3),
(1130, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:09:03', 3),
(1131, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:10:47', 3),
(1132, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:12:13', 3),
(1133, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:13:38', 3),
(1134, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:14:55', 3),
(1135, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:15:15', 3),
(1136, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:16:36', 3),
(1137, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:17:59', 3),
(1138, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:22:48', 3),
(1139, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:25:17', 3),
(1140, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:26:00', 3),
(1141, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:28:06', 3),
(1142, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:28:55', 3),
(1143, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:46:06', 3),
(1144, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:47:39', 3),
(1145, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:48:02', 3),
(1146, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:49:01', 3),
(1147, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:49:37', 3),
(1148, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '18:32:33', 3),
(1149, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '19:20:42', 3),
(1150, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '19:27:20', 3),
(1151, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '19:29:11', 3),
(1152, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '19:30:38', 3),
(1153, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:25:13', 3),
(1154, 'ATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'1\'', '2016-10-11', '22:25:17', 3),
(1155, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:30:29', 3),
(1156, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:31:54', 3),
(1157, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:40:25', 3),
(1158, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:50:00', 3),
(1159, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'3\'', '2016-10-11', '22:50:08', 3),
(1160, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'4\'', '2016-10-11', '22:50:10', 3),
(1161, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:51:37', 3),
(1162, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:54:18', 3),
(1163, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '23:04:12', 3),
(1164, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'3\'', '2016-10-11', '23:04:16', 3),
(1165, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '23:09:40', 3),
(1166, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:22:21', 3),
(1167, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:23:46', 3),
(1168, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:24:49', 3),
(1169, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:25:46', 3),
(1170, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:26:22', 3),
(1171, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:27:03', 3),
(1172, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:27:55', 3),
(1173, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:28:45', 3),
(1174, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:29:31', 3),
(1175, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:45:46', 1),
(1176, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:47:20', 1),
(1177, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:47:46', 1),
(1178, 'DESATIVOU O LOGIN 31', 'UPDATE tb_clientes SET ativo = \'NAO\' WHERE idcliente = \'31\'', '2016-10-25', '16:48:23', 1),
(1179, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:49:14', 1),
(1180, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:49:39', 1),
(1181, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:50:12', 1),
(1182, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:50:36', 1),
(1183, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:50:59', 1),
(1184, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '16:55:28', 1),
(1185, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '16:58:06', 1),
(1186, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:11:41', 1),
(1187, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:33:20', 1),
(1188, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:47:05', 1),
(1189, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:47:14', 1),
(1190, 'DESATIVOU O LOGIN 11', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'11\'', '2016-10-25', '17:47:19', 1),
(1191, 'DESATIVOU O LOGIN 12', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'12\'', '2016-10-25', '17:47:21', 1),
(1192, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:56:16', 1),
(1193, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:57:49', 1),
(1194, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '18:01:26', 1),
(1195, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '18:14:28', 1),
(1196, 'ATIVOU O LOGIN 11', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'11\'', '2016-10-25', '18:14:32', 1),
(1197, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '18:26:53', 1),
(1198, 'ATIVOU O LOGIN 12', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'12\'', '2016-10-25', '18:26:57', 1),
(1199, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-26', '18:03:00', 1),
(1200, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-26', '18:33:00', 1),
(1201, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'4\'', '2016-10-26', '18:33:05', 1),
(1202, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-26', '18:33:55', 1),
(1203, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-26', '18:40:01', 1),
(1204, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '14:10:45', 1),
(1205, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '14:11:03', 1),
(1206, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '14:11:10', 1),
(1207, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '14:11:18', 1),
(1208, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:29:57', 1),
(1209, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:30:44', 1),
(1210, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:32:35', 1),
(1211, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:34:08', 1),
(1212, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:35:40', 1),
(1213, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:36:34', 1),
(1214, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:47:29', 1),
(1215, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:49:00', 1),
(1216, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_galeria_empresa WHERE idgaleriaempresa = \'1\'', '2016-10-27', '15:49:32', 1),
(1217, 'CADASTRO DO CLIENTE ', '', '2016-10-27', '15:49:57', 1),
(1218, 'EXCLUSÃO DO LOGIN 45, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = \'45\'', '2016-10-27', '15:50:31', 1),
(1219, 'CADASTRO DO CLIENTE ', '', '2016-10-27', '15:50:45', 1),
(1220, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-24', '22:58:20', 1),
(1221, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-24', '22:58:37', 1),
(1222, 'CADASTRO DO CLIENTE ', '', '2016-11-24', '23:08:08', 1),
(1223, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-24', '23:08:51', 1),
(1224, 'CADASTRO DO CLIENTE ', '', '2016-11-24', '23:17:24', 1),
(1225, 'CADASTRO DO CLIENTE ', '', '2016-11-24', '23:25:59', 1),
(1226, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-24', '23:28:19', 1),
(1227, 'CADASTRO DO CLIENTE ', '', '2016-11-24', '23:32:55', 1),
(1228, 'CADASTRO DO CLIENTE ', '', '2016-11-24', '23:38:41', 1),
(1229, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-26', '03:07:31', 1),
(1230, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '10:54:44', 1),
(1231, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '10:55:21', 1),
(1232, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '11:02:08', 1),
(1233, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '11:02:21', 1),
(1234, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '11:05:57', 1),
(1235, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '11:06:35', 1),
(1236, 'CADASTRO DO CLIENTE ', '', '2016-12-03', '15:26:55', 3),
(1237, 'CADASTRO DO CLIENTE ', '', '2016-12-03', '15:27:52', 3),
(1238, 'CADASTRO DO CLIENTE ', '', '2016-12-03', '15:28:19', 3),
(1239, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:10:04', 3),
(1240, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:11:06', 3),
(1241, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:12:36', 3),
(1242, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:12:56', 3),
(1243, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:13:17', 3),
(1244, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:13:28', 3),
(1245, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:13:40', 3),
(1246, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:13:55', 3),
(1247, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:30:08', 3),
(1248, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:30:51', 3),
(1249, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:31:18', 3),
(1250, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:31:35', 3),
(1251, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:31:49', 3),
(1252, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:32:06', 3),
(1253, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:32:29', 3),
(1254, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:32:50', 3),
(1255, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:33:29', 3),
(1256, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:34:11', 3),
(1257, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:52:09', 3),
(1258, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:53:50', 3),
(1259, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:54:26', 3),
(1260, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '13:17:32', 3),
(1261, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '13:17:49', 3),
(1262, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '13:18:59', 3),
(1263, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '09:55:39', 3),
(1264, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '09:55:58', 3),
(1265, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '09:56:08', 3),
(1266, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '09:56:17', 3),
(1267, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '11:15:39', 3),
(1268, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '11:33:15', 3),
(1269, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '12:09:39', 3),
(1270, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '13:14:03', 3),
(1271, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '14:12:12', 3),
(1272, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '15:56:06', 3),
(1273, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '16:42:24', 3),
(1274, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '17:53:31', 3),
(1275, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '18:02:16', 3),
(1276, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '18:31:59', 3),
(1277, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '18:52:22', 3),
(1278, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '19:41:09', 3),
(1279, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-06', '00:13:19', 3),
(1280, 'CADASTRO DO CLIENTE ', '', '2016-12-06', '12:39:49', 3),
(1281, 'CADASTRO DO CLIENTE ', '', '2016-12-06', '12:39:59', 3),
(1282, 'CADASTRO DO CLIENTE ', '', '2016-12-06', '12:40:06', 3),
(1283, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-06', '23:41:05', 3),
(1284, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-06', '23:46:57', 3),
(1285, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-06', '23:47:25', 3),
(1286, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-06', '23:47:42', 3),
(1287, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:42:08', 3),
(1288, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:43:18', 3),
(1289, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:44:05', 3),
(1290, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:03:32', 3),
(1291, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:37:32', 3),
(1292, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:45:20', 1),
(1293, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:49:00', 1),
(1294, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:50:57', 1),
(1295, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:53:12', 1),
(1296, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:53:45', 1),
(1297, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:03:39', 1),
(1298, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:10:36', 1),
(1299, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:11:11', 1),
(1300, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:21:25', 1),
(1301, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:22:51', 1),
(1302, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:23:15', 1),
(1303, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:29:34', 1),
(1304, 'EXCLUSÃO DO LOGIN 45, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'45\'', '2016-12-07', '11:30:02', 1),
(1305, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:31:13', 1),
(1306, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:57:52', 1),
(1307, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '12:06:49', 1),
(1308, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '12:08:52', 1),
(1309, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '12:11:00', 1),
(1310, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '12:13:02', 1),
(1311, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '12:32:42', 1),
(1312, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '12:33:58', 1),
(1313, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '12:47:31', 1),
(1314, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '12:47:51', 1),
(1315, 'DESATIVOU O LOGIN 6', 'UPDATE tb_servicos SET ativo = \'NAO\' WHERE idservico = \'6\'', '2016-12-07', '12:48:16', 1),
(1316, 'DESATIVOU O LOGIN 7', 'UPDATE tb_servicos SET ativo = \'NAO\' WHERE idservico = \'7\'', '2016-12-07', '12:48:18', 1),
(1317, 'CADASTRO DO CLIENTE ', '', '2016-12-08', '02:03:38', 1),
(1318, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'10\'', '2016-12-08', '02:16:57', 1),
(1319, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'11\'', '2016-12-08', '02:16:59', 1),
(1320, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'12\'', '2016-12-08', '02:17:44', 1),
(1321, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'13\'', '2016-12-08', '02:17:46', 1),
(1322, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'14\'', '2016-12-08', '02:17:49', 1),
(1323, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'15\'', '2016-12-08', '02:17:51', 1),
(1324, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'16\'', '2016-12-08', '02:17:53', 1),
(1325, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-08', '02:20:00', 1),
(1326, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-12-08', '02:20:24', 1),
(1327, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-08', '02:37:18', 1),
(1328, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-08', '16:03:00', 1),
(1329, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-08', '21:52:47', 1),
(1330, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '14:45:32', 1),
(1331, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '14:47:43', 1),
(1332, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '14:50:04', 1),
(1333, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '14:51:54', 1),
(1334, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '14:53:37', 1),
(1335, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '14:55:04', 1),
(1336, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '14:58:15', 1),
(1337, 'DESATIVOU O LOGIN 90', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'90\'', '2016-12-09', '14:58:52', 1),
(1338, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '15:00:29', 1),
(1339, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '15:01:58', 1),
(1340, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '15:05:34', 1),
(1341, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '15:05:58', 1),
(1342, 'ATIVOU O LOGIN 90', 'UPDATE tb_categorias_produtos SET ativo = \'SIM\' WHERE idcategoriaproduto = \'90\'', '2016-12-09', '15:06:02', 1),
(1343, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '15:07:55', 1),
(1344, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '22:45:36', 1),
(1345, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '22:48:13', 1),
(1346, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '22:50:38', 1),
(1347, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '22:52:20', 1),
(1348, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '22:53:48', 1),
(1349, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '22:55:56', 1),
(1350, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '22:57:44', 1),
(1351, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:00:34', 1),
(1352, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:01:12', 1),
(1353, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '23:06:32', 1),
(1354, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'7\'', '2016-12-09', '23:07:03', 1),
(1355, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'6\'', '2016-12-09', '23:07:07', 1),
(1356, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:07:47', 1),
(1357, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:08:15', 1),
(1358, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:08:49', 1),
(1359, 'CADASTRO DO CLIENTE ', '', '2016-12-09', '23:22:13', 1),
(1360, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:23:53', 1),
(1361, 'DESATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'46\'', '2016-12-09', '23:24:08', 1);
INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1362, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:24:39', 1),
(1363, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:24:57', 1),
(1364, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:26:39', 1),
(1365, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '23:27:04', 1),
(1366, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-12', '14:27:43', 1),
(1367, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-12', '14:29:06', 1),
(1368, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-12', '14:29:15', 1),
(1369, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-12', '14:29:23', 1),
(1370, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-12', '14:29:32', 1),
(1371, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-12', '14:30:36', 1),
(1372, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-12', '14:32:12', 1),
(1373, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-12', '19:55:52', 3),
(1374, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-13', '16:44:42', 1),
(1375, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:25:45', 1),
(1376, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:27:35', 1),
(1377, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:27:56', 1),
(1378, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:28:25', 1),
(1379, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:28:48', 1),
(1380, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:29:16', 1),
(1381, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:29:40', 1),
(1382, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:29:58', 1),
(1383, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:30:14', 1),
(1384, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:30:29', 1),
(1385, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:30:45', 1),
(1386, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:31:59', 1),
(1387, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:33:59', 1),
(1388, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '12:34:26', 1),
(1389, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-14', '15:29:57', 1),
(1390, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:38:27', 1),
(1391, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:39:03', 1),
(1392, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:41:23', 1),
(1393, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:43:06', 1),
(1394, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:44:08', 1),
(1395, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:45:59', 1),
(1396, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:47:12', 1),
(1397, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:47:47', 1),
(1398, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:50:12', 1),
(1399, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:52:24', 1),
(1400, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:54:17', 1),
(1401, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:55:32', 1),
(1402, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:56:43', 1),
(1403, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:58:28', 1),
(1404, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '02:59:13', 1),
(1405, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:02:02', 1),
(1406, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:02:15', 1),
(1407, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:02:23', 1),
(1408, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:02:29', 1),
(1409, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:02:50', 1),
(1410, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:03:04', 1),
(1411, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:03:10', 1),
(1412, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:03:25', 1),
(1413, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:03:33', 1),
(1414, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:03:47', 1),
(1415, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:03:55', 1),
(1416, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:04:23', 1),
(1417, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:04:32', 1),
(1418, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:05:45', 1),
(1419, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:12:16', 1),
(1420, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-12-15', '03:12:20', 1),
(1421, 'CADASTRO DO CLIENTE ', '', '2016-12-15', '03:20:08', 1),
(1422, 'DESATIVOU O LOGIN 7', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'7\'', '2016-12-15', '03:21:00', 1),
(1423, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:23:29', 1),
(1424, 'ATIVOU O LOGIN 7', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'7\'', '2016-12-15', '03:23:32', 1),
(1425, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:24:50', 1),
(1426, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:27:11', 1),
(1427, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-12-15', '03:27:47', 1),
(1428, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:30:33', 1),
(1429, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:35:57', 1),
(1430, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:36:57', 1),
(1431, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:41:28', 1),
(1432, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:41:51', 1),
(1433, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:51:19', 1),
(1434, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:53:11', 1),
(1435, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:53:22', 1),
(1436, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:56:17', 1),
(1437, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:56:28', 1),
(1438, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:57:59', 1),
(1439, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:59:37', 1),
(1440, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '03:59:57', 1),
(1441, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:00:36', 1),
(1442, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:00:52', 1),
(1443, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:01:11', 1),
(1444, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:01:40', 1),
(1445, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:05:19', 1),
(1446, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:05:39', 1),
(1447, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:07:28', 1),
(1448, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:09:19', 1),
(1449, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:28:00', 1),
(1450, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-15', '04:30:40', 1),
(1451, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-12-29', '11:28:30', 1),
(1452, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '11:29:00', 1),
(1453, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '11:29:10', 1),
(1454, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '11:29:29', 1),
(1455, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '11:41:13', 1),
(1456, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '14:26:51', 1),
(1457, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '14:28:42', 1),
(1458, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '15:01:53', 1),
(1459, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '15:02:16', 1),
(1460, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '15:07:10', 1),
(1461, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '15:12:38', 1),
(1462, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '15:14:15', 1),
(1463, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '16:04:02', 1),
(1464, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '16:04:46', 1),
(1465, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '16:25:47', 1),
(1466, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '16:46:32', 1),
(1467, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '16:47:02', 1),
(1468, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '16:48:45', 1),
(1469, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '16:48:57', 1),
(1470, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '16:49:08', 1),
(1471, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '16:49:20', 1),
(1472, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '17:55:55', 1),
(1473, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '17:57:33', 1),
(1474, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'2\'', '2016-12-29', '18:14:10', 1),
(1475, 'EXCLUSÃO DO LOGIN 46, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'46\'', '2016-12-29', '18:14:12', 1),
(1476, 'EXCLUSÃO DO LOGIN 44, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = \'44\'', '2016-12-29', '18:14:16', 1),
(1477, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '18:38:14', 1),
(1478, 'CADASTRO DO CLIENTE ', '', '2016-12-29', '18:38:45', 1),
(1479, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '19:52:19', 1),
(1480, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-29', '20:00:32', 1),
(1481, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-30', '21:38:15', 1),
(1482, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-30', '21:38:31', 1),
(1483, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-30', '21:49:29', 1),
(1484, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-30', '21:49:41', 1),
(1485, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-30', '21:52:47', 1),
(1486, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-30', '21:53:02', 1),
(1487, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-30', '22:09:11', 1),
(1488, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-30', '23:20:53', 1),
(1489, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-31', '01:19:13', 1),
(1490, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'7\'', '2016-12-31', '01:48:46', 1),
(1491, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-31', '03:04:50', 1),
(1492, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-31', '03:25:01', 1),
(1493, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-31', '12:49:31', 1),
(1494, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-31', '18:06:07', 1),
(1495, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-31', '18:40:52', 1),
(1496, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '12:14:39', 1),
(1497, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '12:14:48', 1),
(1498, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '12:14:56', 1),
(1499, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '18:33:18', 1),
(1500, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '18:35:52', 1),
(1501, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '18:55:07', 1),
(1502, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '19:23:25', 1),
(1503, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '20:09:59', 1),
(1504, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-01', '20:11:33', 1),
(1505, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-02', '23:25:43', 1),
(1506, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-03', '00:18:23', 1),
(1507, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-03', '00:36:41', 1),
(1508, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '18:58:16', 3),
(1509, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '18:59:01', 3),
(1510, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:00:01', 3),
(1511, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:00:29', 3),
(1512, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:07:19', 3),
(1513, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:09:33', 3),
(1514, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:09:59', 3),
(1515, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:11:11', 3),
(1516, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:17:38', 3),
(1517, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:18:02', 3),
(1518, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:18:26', 3),
(1519, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-18', '19:18:59', 3),
(1520, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:49:23', 1),
(1521, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:50:35', 1),
(1522, 'CADASTRO DO CLIENTE ', '', '2017-01-23', '12:51:45', 1),
(1523, 'CADASTRO DO CLIENTE ', '', '2017-01-23', '12:52:38', 1),
(1524, 'CADASTRO DO CLIENTE ', '', '2017-01-23', '12:53:24', 1),
(1525, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:55:15', 1),
(1526, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:56:55', 1),
(1527, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:57:10', 1),
(1528, 'CADASTRO DO CLIENTE ', '', '2017-01-23', '12:57:51', 1),
(1529, 'CADASTRO DO CLIENTE ', '', '2017-01-23', '12:58:11', 1),
(1530, 'CADASTRO DO CLIENTE ', '', '2017-01-23', '12:58:30', 1),
(1531, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:58:50', 1),
(1532, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:58:58', 1),
(1533, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:59:08', 1),
(1534, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:59:32', 1),
(1535, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:59:38', 1),
(1536, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:59:44', 1),
(1537, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:59:50', 1),
(1538, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '12:59:54', 1),
(1539, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:01:16', 1),
(1540, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:02:27', 1),
(1541, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:03:04', 1),
(1542, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:03:46', 1),
(1543, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:04:32', 1),
(1544, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:04:59', 1),
(1545, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:05:30', 1),
(1546, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:06:09', 1),
(1547, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:06:34', 1),
(1548, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:19:17', 1),
(1549, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:20:46', 1),
(1550, 'ALTERAÇÃO DO CLIENTE ', '', '2017-01-23', '13:23:44', 1),
(1551, 'CADASTRO DO CLIENTE ', '', '2017-02-27', '13:07:57', 1),
(1552, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:32:27', 1),
(1553, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:33:26', 1),
(1554, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:36:11', 1),
(1555, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:38:59', 1),
(1556, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:39:28', 1),
(1557, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:42:33', 1),
(1558, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:43:54', 1),
(1559, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:44:29', 1),
(1560, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:47:06', 1),
(1561, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:47:32', 1),
(1562, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:50:16', 1),
(1563, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:50:44', 1),
(1564, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:51:56', 1),
(1565, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:52:48', 1),
(1566, 'CADASTRO DO CLIENTE ', '', '2017-02-27', '13:54:27', 1),
(1567, 'DESATIVOU O LOGIN 7', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'7\'', '2017-02-27', '13:55:31', 1),
(1568, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '13:58:40', 1),
(1569, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '14:06:14', 1),
(1570, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '14:08:30', 1),
(1571, 'CADASTRO DO CLIENTE ', '', '2017-02-27', '14:12:52', 1),
(1572, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '14:13:15', 1),
(1573, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '14:13:51', 1),
(1574, 'ATIVOU O LOGIN 7', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'7\'', '2017-02-27', '14:14:39', 1),
(1575, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '14:16:05', 1),
(1576, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '14:17:15', 1),
(1577, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '14:56:54', 1),
(1578, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '15:14:57', 1),
(1579, 'ALTERAÇÃO DO CLIENTE ', '', '2017-02-27', '16:52:22', 1),
(1580, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-12', '09:05:14', 1),
(1581, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-15', '19:07:35', 1),
(1582, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-15', '18:36:54', 1),
(1583, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '12:52:07', 1),
(1584, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '13:04:00', 1),
(1585, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '13:59:02', 1),
(1586, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '14:05:17', 1),
(1587, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '16:42:38', 1),
(1588, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '16:54:18', 1),
(1589, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '16:55:51', 1),
(1590, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '16:57:15', 1),
(1591, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '16:59:24', 1),
(1592, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '18:39:14', 1),
(1593, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-16', '19:15:16', 1),
(1594, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-19', '11:28:05', 1),
(1595, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-19', '11:34:43', 1),
(1596, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-19', '11:35:54', 1),
(1597, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-19', '23:16:31', 1),
(1598, 'CADASTRO DO CLIENTE ', '', '2017-03-19', '23:26:58', 1),
(1599, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_tipos_servicos WHERE idtiposervico = \'1\'', '2017-03-19', '23:27:36', 1),
(1600, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_tipos_servicos WHERE idtiposervico = \'2\'', '2017-03-19', '23:27:40', 1),
(1601, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_tipos_servicos WHERE idtiposervico = \'3\'', '2017-03-19', '23:27:45', 1),
(1602, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_tipos_servicos WHERE idtiposervico = \'8\'', '2017-03-19', '23:27:52', 1),
(1603, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_tipos_servicos WHERE idtiposervico = \'9\'', '2017-03-19', '23:27:56', 1),
(1604, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_tipos_servicos WHERE idtiposervico = \'10\'', '2017-03-19', '23:28:01', 1),
(1605, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-19', '23:35:11', 1),
(1606, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-19', '23:36:33', 1),
(1607, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-19', '23:39:47', 1),
(1608, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-19', '23:57:24', 1),
(1609, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-20', '00:10:50', 1),
(1610, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-20', '17:56:33', 1),
(1611, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-20', '18:08:57', 1),
(1612, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-20', '18:25:38', 1),
(1613, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-20', '18:25:52', 1),
(1614, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-20', '21:23:10', 1),
(1615, 'CADASTRO DO CLIENTE ', '', '2017-03-20', '21:44:06', 1),
(1616, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '10:34:38', 1),
(1617, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '11:17:29', 1),
(1618, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '11:17:48', 1),
(1619, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '11:18:14', 1),
(1620, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '11:18:15', 1),
(1621, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '11:18:16', 1),
(1622, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'7\'', '2017-03-21', '11:18:39', 1),
(1623, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'7\'', '2017-03-21', '11:18:41', 1),
(1624, 'CADASTRO DO CLIENTE ', '', '2017-03-21', '11:19:39', 1),
(1625, 'CADASTRO DO CLIENTE ', '', '2017-03-21', '11:19:50', 1),
(1626, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'9\'', '2017-03-21', '11:21:53', 1),
(1627, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'9\'', '2017-03-21', '11:21:55', 1),
(1628, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'9\'', '2017-03-21', '11:21:57', 1),
(1629, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'9\'', '2017-03-21', '11:21:57', 1),
(1630, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'9\'', '2017-03-21', '11:21:58', 1),
(1631, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '11:22:22', 1),
(1632, 'CADASTRO DO CLIENTE ', '', '2017-03-21', '12:35:53', 1),
(1633, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '13:00:12', 1),
(1634, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '13:00:34', 1),
(1635, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-21', '13:01:21', 1),
(1636, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-23', '10:01:25', 1),
(1637, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-23', '10:01:45', 1),
(1638, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-23', '10:02:02', 1),
(1639, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-24', '02:54:51', 1),
(1640, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-24', '21:28:06', 1),
(1641, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-24', '21:43:17', 1),
(1642, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-24', '22:00:34', 1),
(1643, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-24', '23:29:19', 1),
(1644, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-24', '23:35:40', 1),
(1645, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-24', '23:56:52', 1),
(1646, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-25', '10:56:26', 1),
(1647, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-25', '13:26:02', 1),
(1648, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '08:59:43', 1),
(1649, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '08:59:53', 1),
(1650, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:00:01', 1),
(1651, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:00:11', 1),
(1652, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:00:33', 1),
(1653, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:02:27', 1),
(1654, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:10:56', 1),
(1655, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:11:15', 1),
(1656, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:15:27', 1),
(1657, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:15:50', 1),
(1658, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:16:03', 1),
(1659, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:16:13', 1),
(1660, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:23:28', 1),
(1661, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:25:07', 1),
(1662, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:40:01', 1),
(1663, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:40:18', 1),
(1664, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:40:41', 1),
(1665, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '09:47:59', 1),
(1666, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '11:16:46', 1),
(1667, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '11:47:43', 1),
(1668, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '11:48:04', 1),
(1669, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '11:48:47', 1),
(1670, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '11:53:24', 1),
(1671, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '11:55:26', 1),
(1672, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '11:56:06', 1),
(1673, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '11:57:28', 1),
(1674, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '11:57:44', 1),
(1675, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '11:58:07', 1),
(1676, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '11:59:23', 1),
(1677, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '11:59:36', 1),
(1678, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '12:00:05', 1),
(1679, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '12:01:40', 1),
(1680, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '12:02:09', 1),
(1681, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '16:20:43', 1),
(1682, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '16:24:19', 1),
(1683, 'EXCLUSÃO DO LOGIN 102, NOME: , Email: ', 'DELETE FROM tb_categorias_unidades WHERE idcategoriaunidade = \'102\'', '2017-04-14', '16:39:41', 1),
(1684, 'EXCLUSÃO DO LOGIN 101, NOME: , Email: ', 'DELETE FROM tb_categorias_unidades WHERE idcategoriaunidade = \'101\'', '2017-04-14', '16:40:19', 1),
(1685, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '16:40:49', 1),
(1686, 'CADASTRO DO CLIENTE ', '', '2017-04-14', '16:53:12', 1),
(1687, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_unidades WHERE idunidade = \'1\'', '2017-04-14', '17:08:34', 1),
(1688, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '17:08:52', 1),
(1689, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '17:09:58', 1),
(1690, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-14', '18:28:47', 1),
(1691, 'CADASTRO DO CLIENTE ', '', '2017-04-16', '10:44:44', 1),
(1692, 'CADASTRO DO CLIENTE ', '', '2017-04-16', '10:58:34', 1),
(1693, 'CADASTRO DO CLIENTE ', '', '2017-04-16', '11:08:39', 1),
(1694, 'CADASTRO DO CLIENTE ', '', '2017-04-16', '11:09:42', 1),
(1695, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-16', '14:55:51', 1),
(1696, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-16', '17:02:15', 1),
(1697, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-17', '11:39:17', 0),
(1698, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-17', '11:39:28', 0),
(1699, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-17', '11:39:44', 0),
(1700, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-17', '11:39:53', 0),
(1701, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-17', '13:58:40', 0),
(1702, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-17', '14:07:15', 0),
(1703, 'DESATIVOU O LOGIN 2', 'UPDATE tb_unidades SET ativo = \'NAO\' WHERE idunidade = \'2\'', '2017-04-17', '20:19:36', 0),
(1704, 'ATIVOU O LOGIN 2', 'UPDATE tb_unidades SET ativo = \'SIM\' WHERE idunidade = \'2\'', '2017-04-17', '20:20:04', 0),
(1705, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-17', '23:47:18', 1),
(1706, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-17', '23:49:21', 1),
(1707, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-18', '23:32:24', 1),
(1708, 'CADASTRO DO CLIENTE ', '', '2017-04-19', '00:36:30', 1),
(1709, 'CADASTRO DO CLIENTE ', '', '2017-04-19', '00:37:08', 1),
(1710, 'CADASTRO DO CLIENTE ', '', '2017-04-19', '00:38:23', 1),
(1711, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-19', '00:58:25', 1),
(1712, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-19', '11:07:11', 1),
(1713, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-19', '11:32:36', 1),
(1714, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-19', '14:29:56', 1),
(1715, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-19', '14:44:56', 1),
(1716, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-19', '15:22:59', 1),
(1717, 'CADASTRO DO CLIENTE ', '', '2017-04-19', '20:08:53', 1),
(1718, 'CADASTRO DO CLIENTE ', '', '2017-04-19', '20:09:27', 1),
(1719, 'CADASTRO DO CLIENTE ', '', '2017-04-19', '20:23:40', 1),
(1720, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-19', '20:34:36', 1),
(1721, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '16:44:49', 1),
(1722, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '16:45:47', 1),
(1723, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:01:09', 0),
(1724, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '20:01:23', 0),
(1725, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '20:01:36', 0),
(1726, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:03:10', 0),
(1727, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:03:24', 0),
(1728, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:04:00', 0),
(1729, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:04:14', 0),
(1730, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:04:30', 0),
(1731, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:05:09', 0),
(1732, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:05:21', 0),
(1733, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '20:05:29', 0),
(1734, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '18:31:18', 1),
(1735, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '18:36:47', 1),
(1736, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '18:38:50', 1),
(1737, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '21:40:13', 0),
(1738, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '21:40:23', 0),
(1739, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:40:57', 0),
(1740, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:41:10', 0),
(1741, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:41:19', 0),
(1742, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:41:28', 0),
(1743, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:42:29', 0),
(1744, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:42:41', 0),
(1745, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:43:01', 0),
(1746, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:43:32', 0),
(1747, 'CADASTRO DO CLIENTE ', '', '2017-04-20', '21:43:51', 0),
(1748, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'40\'', '2017-04-20', '21:47:29', 0),
(1749, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '18:50:47', 1),
(1750, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '19:14:12', 1),
(1751, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '19:24:42', 1),
(1752, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '19:24:53', 1),
(1753, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '19:25:12', 1),
(1754, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_promocoes WHERE idpromocao = \'3\'', '2017-04-20', '19:36:27', 1),
(1755, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '19:39:42', 1),
(1756, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '19:39:58', 1),
(1757, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-20', '19:40:14', 1),
(1758, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-21', '01:24:58', 1),
(1759, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-21', '01:25:11', 1),
(1760, 'CADASTRO DO CLIENTE ', '', '2017-04-21', '01:26:11', 1),
(1761, 'CADASTRO DO CLIENTE ', '', '2017-04-21', '01:27:37', 1),
(1762, 'EXCLUSÃO DO LOGIN 47, NOME: , Email: ', 'DELETE FROM tb_promocoes WHERE idpromocao = \'47\'', '2017-04-21', '01:30:18', 1),
(1763, 'CADASTRO DO CLIENTE ', '', '2017-04-21', '01:31:05', 1),
(1764, 'EXCLUSÃO DO LOGIN 48, NOME: , Email: ', 'DELETE FROM tb_promocoes WHERE idpromocao = \'48\'', '2017-04-21', '01:31:54', 1),
(1765, 'CADASTRO DO CLIENTE ', '', '2017-04-21', '12:53:41', 0),
(1766, 'CADASTRO DO CLIENTE ', '', '2017-04-21', '12:53:53', 0),
(1767, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-21', '13:30:32', 1),
(1768, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-21', '19:05:26', 1),
(1769, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-21', '20:06:12', 1),
(1770, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-23', '12:26:31', 1),
(1771, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-23', '12:27:05', 1),
(1772, 'CADASTRO DO CLIENTE ', '', '2017-04-23', '12:32:24', 1),
(1773, 'CADASTRO DO CLIENTE ', '', '2017-04-23', '12:32:59', 1),
(1774, 'CADASTRO DO CLIENTE ', '', '2017-04-23', '12:33:47', 1),
(1775, 'CADASTRO DO CLIENTE ', '', '2017-04-23', '12:34:10', 1),
(1776, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:21:25', 1),
(1777, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:23:18', 1),
(1778, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:26:28', 1),
(1779, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:29:07', 1),
(1780, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:30:55', 1),
(1781, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:32:33', 1),
(1782, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:33:54', 1),
(1783, 'DESATIVOU O LOGIN 103', 'UPDATE tb_categorias_unidades SET ativo = \'NAO\' WHERE idcategoriaunidade = \'103\'', '2017-05-03', '14:34:18', 1),
(1784, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:34:43', 1),
(1785, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:34:52', 1),
(1786, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:35:01', 1),
(1787, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:35:36', 1),
(1788, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:35:50', 1),
(1789, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:35:57', 1),
(1790, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:36:03', 1),
(1791, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:36:08', 1),
(1792, 'ATIVOU O LOGIN 103', 'UPDATE tb_categorias_unidades SET ativo = \'SIM\' WHERE idcategoriaunidade = \'103\'', '2017-05-03', '14:36:51', 1),
(1793, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:37:46', 1),
(1794, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:38:29', 1),
(1795, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:38:42', 1),
(1796, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:39:23', 1),
(1797, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:39:41', 1),
(1798, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:40:20', 1),
(1799, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:40:33', 1),
(1800, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:41:19', 1),
(1801, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:41:47', 1),
(1802, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:42:20', 1),
(1803, 'CADASTRO DO CLIENTE ', '', '2017-05-03', '14:44:42', 1),
(1804, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:45:15', 1),
(1805, 'CADASTRO DO CLIENTE ', '', '2017-05-03', '14:50:45', 1),
(1806, 'CADASTRO DO CLIENTE ', '', '2017-05-03', '14:52:52', 1),
(1807, 'CADASTRO DO CLIENTE ', '', '2017-05-03', '14:54:15', 1),
(1808, 'CADASTRO DO CLIENTE ', '', '2017-05-03', '14:56:08', 1),
(1809, 'CADASTRO DO CLIENTE ', '', '2017-05-03', '14:57:55', 1),
(1810, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:58:53', 1),
(1811, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:59:03', 1),
(1812, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:59:09', 1),
(1813, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:59:31', 1),
(1814, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:59:36', 1),
(1815, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:59:41', 1),
(1816, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:59:45', 1),
(1817, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:59:51', 1),
(1818, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '14:59:56', 1),
(1819, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:01:23', 1),
(1820, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:02:15', 1),
(1821, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:02:38', 1),
(1822, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:03:03', 1),
(1823, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:03:22', 1),
(1824, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:04:05', 1),
(1825, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:04:41', 1),
(1826, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:07:31', 1),
(1827, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:08:08', 1),
(1828, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:08:36', 1),
(1829, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:09:20', 1),
(1830, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:10:28', 1),
(1831, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:11:50', 1),
(1832, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:12:54', 1),
(1833, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:13:23', 1),
(1834, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:13:47', 1),
(1835, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:14:07', 1),
(1836, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:15:57', 1),
(1837, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-03', '15:17:02', 1),
(1838, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-05', '18:13:46', 1),
(1839, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2017-05-07', '14:55:58', 1),
(1840, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-08', '22:59:28', 1),
(1841, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-08', '23:00:42', 1),
(1842, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:10:39', 1),
(1843, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:20:04', 1),
(1844, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:20:31', 1),
(1845, 'CADASTRO DO CLIENTE ', '', '2017-05-09', '16:26:46', 1),
(1846, 'DESATIVOU O LOGIN 19', 'UPDATE tb_tipos_servicos SET ativo = \'NAO\' WHERE idtiposervico = \'19\'', '2017-05-09', '16:27:21', 1),
(1847, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:28:14', 1),
(1848, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:28:39', 1),
(1849, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:31:54', 1),
(1850, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:32:24', 1),
(1851, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:34:31', 1),
(1852, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:44:59', 1),
(1853, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:45:32', 1),
(1854, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:46:13', 1),
(1855, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:46:41', 1),
(1856, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:47:53', 1),
(1857, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:52:33', 1),
(1858, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:53:26', 1),
(1859, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-09', '16:53:58', 1),
(1860, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '02:00:19', 1),
(1861, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2017-05-10', '02:00:24', 1),
(1862, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '02:08:13', 1),
(1863, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '02:17:42', 1),
(1864, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '02:28:47', 1),
(1865, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '03:20:28', 1),
(1866, 'ALTERAÇÃO DO CLIENTE ', '', '2017-05-10', '03:22:50', 1),
(1867, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'3\'', '2017-05-10', '03:24:02', 1),
(1868, 'DESATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'1\'', '2017-05-10', '03:24:03', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_lojas`
--

CREATE TABLE `tb_lojas` (
  `idloja` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `link_maps` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_lojas`
--

INSERT INTO `tb_lojas` (`idloja`, `titulo`, `telefone`, `endereco`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `description_google`, `keywords_google`, `link_maps`) VALUES
(2, 'BRASÍLIA', '(61)3456-0987', '2ª Avenida, Bloco 241, Loja 1 - Núcleo Bandeirante, Brasília - DF', 'SIM', NULL, 'brasilia', '', '', '', 'http://www.google.com'),
(3, 'GOIÂNIA', '(61)3456-0922', '2ª Avenida, Bloco 241, Loja 1 - Goiânia-GO', 'SIM', NULL, 'goiania', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_marcas_veiculos`
--

CREATE TABLE `tb_marcas_veiculos` (
  `idmarcaveiculo` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `ordem` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) NOT NULL,
  `id_tipoveiculo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_marcas_veiculos`
--

INSERT INTO `tb_marcas_veiculos` (`idmarcaveiculo`, `titulo`, `ordem`, `ativo`, `url_amigavel`, `id_tipoveiculo`) VALUES
(123, 'Ford', 0, 'SIM', 'ford', 11),
(124, 'Audio', 0, 'SIM', 'audio', 11),
(125, 'BMW', 0, 'SIM', 'bmw', 11),
(126, 'Yamaha', 0, 'SIM', 'yamaha', 12),
(127, 'Suzuki', 0, 'SIM', 'suzuki', 12),
(128, 'Kawasaki', 0, 'SIM', 'kawasaki', 12),
(129, 'Jeep', 0, 'SIM', 'jeep', 13),
(130, 'Toyota', 0, 'SIM', 'toyota', 13),
(131, 'Caterpillar', 0, 'SIM', 'caterpillar', 15),
(132, 'Komatsu', 0, 'SIM', 'komatsu', 15),
(133, 'Case', 0, 'SIM', 'case', 15),
(134, 'Ford', 0, 'SIM', 'ford', 13);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_modelos_veiculos`
--

CREATE TABLE `tb_modelos_veiculos` (
  `idmodeloveiculo` int(11) NOT NULL,
  `titulo` varchar(255) CHARACTER SET latin1 NOT NULL,
  `ordem` int(11) NOT NULL,
  `ativo` varchar(3) CHARACTER SET latin1 NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) CHARACTER SET latin1 NOT NULL,
  `id_marcaveiculo` int(11) NOT NULL,
  `id_tipoveiculo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_modelos_veiculos`
--

INSERT INTO `tb_modelos_veiculos` (`idmodeloveiculo`, `titulo`, `ordem`, `ativo`, `url_amigavel`, `id_marcaveiculo`, `id_tipoveiculo`) VALUES
(1, 'Focus', 0, 'SIM', 'focus', 123, 11),
(2, 'A3', 0, 'SIM', 'a3', 124, 11),
(3, 'EcoSport', 0, 'SIM', 'ecosport', 123, 11),
(4, 'Fusion', 0, 'SIM', 'fusion', 123, 11),
(5, 'A1', 0, 'SIM', 'a1', 125, 11),
(6, 'A3', 0, 'SIM', 'a3', 125, 11),
(7, 'NEO 125', 0, 'SIM', 'neo-125', 126, 12),
(8, 'Fazer 250', 0, 'SIM', 'fazer-250', 126, 12),
(9, 'Hilux', 0, 'SIM', 'hilux', 130, 13),
(10, 'Grand Cherokee', 0, 'SIM', 'grand-cherokee', 129, 13),
(11, 'Renegade', 0, 'SIM', 'renegade', 129, 13),
(12, 'F250', 0, 'SIM', 'f250', 134, 13);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_noticias`
--

CREATE TABLE `tb_noticias` (
  `idnoticia` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_noticias`
--

INSERT INTO `tb_noticias` (`idnoticia`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(49, 'SELO QUALISOL Programa de Qualificação de Fornecedores de Sistemas de Aquecimento Solar', '<p style="text-align: justify;">\r\n	O QUALISOL BRASIL &eacute; o Programa de Qualifica&ccedil;&atilde;o de Fornecedores de Sistemas de Aquecimento Solar, que engloba fabricantes, revendas e instaladoras. Fruto de um conv&ecirc;nio entre a ABRAVA, o INMETRO e o PROCEL/Eletrobras, o programa tem como objetivo garantir ao consumidor qualidade dos fornecedores de sistemas de aquecimento solar, de modo a permitir:&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A amplia&ccedil;&atilde;o do conhecimento de fornecedores em rela&ccedil;&atilde;o ao aquecimento solar;</p>\r\n<p style="text-align: justify;">\r\n	A amplia&ccedil;&atilde;o da base de mercado do aquecimento solar e suas diversas aplica&ccedil;&otilde;es;</p>\r\n<p style="text-align: justify;">\r\n	O aumento da qualidade das instala&ccedil;&otilde;es e conseq&uuml;ente satisfa&ccedil;&atilde;o do consumidor final;</p>\r\n<p style="text-align: justify;">\r\n	Uma melhor e mais duradoura reputa&ccedil;&atilde;o e confian&ccedil;a em sistemas de aquecimento solar nas suas diversas aplica&ccedil;&otilde;es;</p>\r\n<p style="text-align: justify;">\r\n	Um crescente interesse e habilidade dos fornecedores na prospec&ccedil;&atilde;o de novos clientes e est&iacute;mulo ao surgimento de novos empreendedores.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Assim como em diversos pa&iacute;ses do mundo, no Brasil, as revendas e instaladores representam uma posi&ccedil;&atilde;o estrat&eacute;gica com rela&ccedil;&atilde;o &agrave; difus&atilde;o do aquecimento solar. Na maioria das vezes est&atilde;o em contato direto com o consumidor no momento de decis&atilde;o de compra e instala&ccedil;&atilde;o e algumas vezes tamb&eacute;m planejam e entregam os equipamentos e s&atilde;o respons&aacute;veis diretos por garantir uma instala&ccedil;&atilde;o qualificada com funcionamento, durabilidade e est&eacute;tica assegurados e comprovados. Al&eacute;m das revendas e instaladoras, as empresas fabricantes de equipamentos solares tamb&eacute;m realizam instala&ccedil;&otilde;es e contratos diretos com consumidores finais e assumem a responsabilidade por todo o processo desde a venda at&eacute; a instala&ccedil;&atilde;o e p&oacute;s-venda.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Portanto, a qualidade dos sistemas de aquecimento solar depende diretamente de:</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	bons produtos, etiquetados;</p>\r\n<p style="text-align: justify;">\r\n	bons projetistas e revendas, qualificadas;</p>\r\n<p style="text-align: justify;">\r\n	bons instaladores, qualificados;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '0206201605386174124988..jpg', 'SIM', NULL, 'selo-qualisol-programa-de-qualificacao-de-fornecedores-de-sistemas-de-aquecimento-solar', '', '', '', NULL),
(50, 'ENERGIA RENOVÁVEL NO BRASIL - NÃO BASTA SOMENTE GERAR, TEMOS QUE SABER USÁ-LA', '<p>\r\n	A gera&ccedil;&atilde;o de energia renov&aacute;vel mundial apresentar&aacute; forte crescimento nos pr&oacute;ximos anos, com expectativa de crescimento de 12,7% no per&iacute;odo entre 2010 a 2013 segundo a International Energy Agency - IEA. As raz&otilde;es principais dessa previs&atilde;o s&atilde;o as metas de redu&ccedil;&atilde;o de emiss&otilde;es de CO2 e mudan&ccedil;as clim&aacute;ticas; melhorias tecnol&oacute;gicas favorecendo novas alternativas; aumento na demanda de energia; ambiente regulat&oacute;rio mais favor&aacute;vel; e incentivos governamentais.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Enquanto os benef&iacute;cios de desenvolver-se uma agenda s&oacute;lida para consolida&ccedil;&atilde;o da energia renov&aacute;vel no Brasil s&atilde;o evidentes, &eacute; preciso atentar-se para os riscos associados &agrave; segrega&ccedil;&atilde;o do tema de energias renov&aacute;veis do panorama geral da agenda energ&eacute;tica no Brasil, atualmente levada pela ANP (Ag&ecirc;ncia Nacional do Petr&oacute;leo, G&aacute;s Natural e Biocombust&iacute;veis) e ANEEL (Ag&ecirc;ncia Nacional de Energia El&eacute;trica. &Eacute; preciso existir um incentivo maior &agrave;s pol&iacute;ticas p&uacute;blicas que tornem economicamente vi&aacute;veis melhorias de projetos n&atilde;o s&oacute; voltadas para reduzir os gastos com energia el&eacute;trica, aumentando a efici&ecirc;ncia energ&eacute;tica dos processos, como tamb&eacute;m que possibilitem o uso sustent&aacute;vel dos recursos naturais, diz Ricardo Antonio do Esp&iacute;rito Santo Gomes, consultor em efici&ecirc;ncia energ&eacute;tica do CTE.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para Gomes &eacute; preciso pensar mais na maneira como usamos a energia, n&atilde;o somente na maneira como a geramos: N&atilde;o adianta gerarmos muita energia de forma eficiente se aqui na ponta usamos chuveiro el&eacute;trico, ao inv&eacute;s de aquecedor solar t&eacute;rmico por exemplo. Portanto, a gera&ccedil;&atilde;o distribu&iacute;da &eacute; a maneira mais eficiente de se garantir um crescimento sustent&aacute;vel para a sociedade, diminuindo perdas em transmiss&atilde;o, diminuindo o impacto ambiental de grandes centrais geradoras de energia e produzindo, localmente, as utilidades de que o cliente final precisa, como energia el&eacute;trica, vapor, &aacute;gua quente e &aacute;gua gelada, garantindo que um combust&iacute;vel se transforme ao m&aacute;ximo poss&iacute;vel, em outras formas de energia, causando menor impacto poss&iacute;vel e garantindo o n&iacute;vel de conforto exigido.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ele acrescenta que na pauta de pesquisas e desenvolvimento est&atilde;o os sistemas que possuem o atrativo de manterem altas taxas de efici&ecirc;ncia energ&eacute;tica, baixa emiss&atilde;o de poluentes e de CO2 e redu&ccedil;&atilde;o de custos de transmiss&atilde;o.</p>\r\n<div>\r\n	&nbsp;</div>', '0206201605489606782930..jpg', 'SIM', NULL, 'energia-renovavel-no-brasil--nao-basta-somente-gerar-temos-que-saber-usala', '', '', '', NULL),
(48, 'BANHO AQUECIDO ATRAVÉS DE AQUECIMENTO SOLAR', '<p>\r\n	Apesar de abundante, a energia solar como energia t&eacute;rmica &eacute; pouco aproveitada no Brasil, com isso o banho quente brasileiro continua, em 67% dos lares brasileiros, a utilizar o chuveiro el&eacute;trico que consome 8% da eletricidade produzida ao inv&eacute;s do aquecedor solar, segundo dados do Instituto Vitae Civillis &quot;Um Banho de Sol para o Brasil&quot;, de D&eacute;lcio Rodrigues e Roberto Matajs.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Esses n&uacute;meros contrastam com a capacidade que o territ&oacute;rio brasileiro tem de produzir energia solar: O potencial de gera&ccedil;&atilde;o &eacute; equivalente a 15 trilh&otilde;es de MW/h, correspondente a 50 mil vezes o consumo nacional de eletricidade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O projeto Cidade Solares, parceria entre o Vitae Civilis e a DASOL/ABRAVA*, foi criado para discutir, propor e acompanhar a tramita&ccedil;&atilde;o e entrada em vigor de leis que incentivam ou obrigam o uso de sistemas solares de aquecimento de &aacute;gua nas cidades e estados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A id&eacute;ia &eacute; debater e difundir conhecimento sobre o assunto, al&eacute;m de analisar as possibilidades de implanta&ccedil;&atilde;o de projetos nas cidades. Ao ganhar o t&iacute;tulo de &quot;Solar&quot;, uma cidade servir&aacute; de exemplo para outras, com a difus&atilde;o do tema e das tecnologias na pr&aacute;tica. No site, o projeto lista v&aacute;rias iniciativas de cidades que implantaram com sucesso sistemas solares, como Freiburg- na Alemanha, Graz- na &Aacute;ustria, Portland- nos EUA e Oxford- na Inglaterra.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* DASOL/ ABRAVA: Departamento Nacional de Aquecimento Solar da Associa&ccedil;&atilde;o Brasileira de Refrigera&ccedil;&atilde;o, Ar-condicionado, Ventila&ccedil;&atilde;o e Aquecimento.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fonte: Revista Super Interessante - Editora Abril</p>', '0206201605411524312164..jpg', 'SIM', NULL, 'banho-aquecido-atraves-de-aquecimento-solar', '', '', '', NULL),
(47, 'COMO FUNCIONA UM SISTEMA DE AQUECIMENTO SOLAR DE ÁGUA? VOCÊ SABE?', '<p>\r\n	A mesma energia solar que ilumina e aquece o planeta pode ser usada para esquentar a &aacute;gua dos nossos banhos, acenderem l&acirc;mpadas ou energizar as tomadas de casa. O sol &eacute; uma fonte inesgot&aacute;vel de energia e, quando falamos em sustentabilidade, em economia de recursos e de &aacute;gua, em economia de energia e redu&ccedil;&atilde;o da emiss&atilde;o de g&aacute;s carb&ocirc;nico na atmosfera, nada mais natural do que pensarmos numa maneira mais eficiente de utiliza&ccedil;&atilde;o da energia solar. Esta energia &eacute; totalmente limpa e, principalmente no Brasil, onde temos uma enorme incid&ecirc;ncia solar, os sistemas para o aproveitamento da energia do sol s&atilde;o muito eficientes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para ser utilizada, a energia solar deve ser transformada e, para isto, h&aacute; duas maneiras principais de realizar essa transforma&ccedil;&atilde;o: Os pain&eacute;is fotovoltaicos e os aquecedores solares.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os primeiros s&atilde;o respons&aacute;veis pela transforma&ccedil;&atilde;o da energia solar em energia el&eacute;trica. Com esses pain&eacute;is podemos utilizar o sol para acender as l&acirc;mpadas das nossas casas ou para ligar uma televis&atilde;o. A segunda forma &eacute; com o uso de aquecedores solares, que utiliza a energia solar para aquecer a &aacute;gua que ser&aacute; direcionada aos chuveiros ou piscinas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Como funciona um sistema de aquecimento solar?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Basicamente este sistema &eacute; composto por dois elementos: Os coletores solares (pain&eacute;is de capta&ccedil;&atilde;o, que vemos freq&uuml;entemente nos telhados das casas) e o reservat&oacute;rio de &aacute;gua quente, tamb&eacute;m chamado de boiler.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os coletores s&atilde;o formados por uma placa de vidro que isola do ambiente externo aletas de cobre ou alum&iacute;nio pintadas com tintas especiais na cor escura para que absorvam o m&aacute;ximo da radia&ccedil;&atilde;o. Ao absorver a radia&ccedil;&atilde;o, estas aletas deixam o calor passar para tubos em forma de serpentina geralmente feitos de cobre. Dentro desses tubos passa &aacute;gua, que &eacute; aquecida antes de ser levada para o reservat&oacute;rio de &aacute;gua quente. Estas placas coletoras podem ser dispostas sobre telhados e lajes e a quantidade de placas instaladas varia conforme o tamanho do reservat&oacute;rio, o n&iacute;vel de insola&ccedil;&atilde;o da regi&atilde;o e as condi&ccedil;&otilde;es de instala&ccedil;&atilde;o. No hemisf&eacute;rio sul, normalmente as placas ficam inclinadas para a dire&ccedil;&atilde;o norte para receber a maior quantidade poss&iacute;vel de radia&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os reservat&oacute;rios s&atilde;o cilindros de alum&iacute;nio, inox e polipropileno com isolantes t&eacute;rmicos que mant&eacute;m pelo maior tempo poss&iacute;vel a &aacute;gua aquecida. Uma caixa de &aacute;gua fria abastece o sistema para que o boiler fique sempre cheio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os reservat&oacute;rios devem ser instalados o mais pr&oacute;ximo poss&iacute;vel das placas coletoras (para evitar perda de efici&ecirc;ncia do sistema), de prefer&ecirc;ncia devem estar sob o telhado (para evitar a perda de calor para a atmosfera) e em n&iacute;vel um pouco elevado. Dessa forma, consegue-se o efeito chamado de termossif&atilde;o, ou seja, conforme a &aacute;gua dos coletores vai esquentando, ela torna-se menos densa e vai sendo empurrada pela &aacute;gua fria. Assim ela sobe e chega naturalmente ao boiler, sem a necessidade de bombeamento. Em casos espec&iacute;ficos, em que o reservat&oacute;rio n&atilde;o possa ser instalado acima das placas coletoras, podem-se utilizar bombas para promover a circula&ccedil;&atilde;o da &aacute;gua.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	E nos dias nublados, chuvosos ou &agrave; noite?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Apesar de poderem ser instalados de forma independente, os aquecedores solares normalmente trabalham com um sistema auxiliar de aquecimento da &aacute;gua. Este sistema pode ser el&eacute;trico ou a g&aacute;s, e j&aacute; vem de f&aacute;brica. Quando houver uma seq&uuml;&ecirc;ncia de dias nublados ou chuvosos em que a energia gerada pelo aquecedor solar n&atilde;o seja suficiente para esquentar toda a &aacute;gua necess&aacute;ria para o consumo di&aacute;rio, um aquecedor el&eacute;trico ou a g&aacute;s &eacute; acionado, gerando &aacute;gua quente para as pias e chuveiros. O mesmo fato acontece &agrave; noite quando n&atilde;o houver mais &aacute;gua aquecida no reservat&oacute;rio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Num pa&iacute;s tropical como o nosso, na grande maioria dos dias a &aacute;gua ser&aacute; aquecida com a energia solar e, portanto, os sistemas auxiliares ficar&atilde;o desligados a maior parte do tempo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Vale a pena economicamente?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O custo do sistema &eacute; compensado pela economia mensal na conta de energia el&eacute;trica. O Aquecedor Solar Heliotek reduz at&eacute; 80% dos custos de energia com aquecimento e em poucos anos o sistema se paga, gerando lucro ao longo de sua vida &uacute;til? 20 anos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os aquecedores solares podem ser instalados em novas obras ou durante reformas. Ao construir ou reformar, estude esta op&ccedil;&atilde;o, que &eacute; boa para o planeta e pode ser &oacute;tima para a sua conta de energia.</p>', '0206201605446692289010..jpg', 'SIM', NULL, 'como-funciona-um-sistema-de-aquecimento-solar-de-agua-voce-sabe', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_parceiros`
--

CREATE TABLE `tb_parceiros` (
  `idparceiro` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_parceiros`
--

INSERT INTO `tb_parceiros` (`idparceiro`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `url`) VALUES
(1, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', '', '', '', 'http://www.uol.com.br'),
(2, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(3, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(4, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(5, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(6, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(7, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(8, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(9, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(10, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(11, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(12, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(13, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(14, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(15, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(16, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(17, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(18, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_portfolios`
--

CREATE TABLE `tb_portfolios` (
  `idportfolio` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `video_youtube` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_portfolios`
--

INSERT INTO `tb_portfolios` (`idportfolio`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `video_youtube`) VALUES
(1, 'Portifólio 1 com titulo grande de duas linhas', '1503201609451124075050..jpg', '<p>\r\n	Port&atilde;o autom&aacute;tico basculante articulado&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lider Portão automático basculante articulado', 'keywords Portão automático basculante articulado', 'description Portão automático basculante articulado', 'SIM', NULL, 'portifolio-1-com-titulo-grande-de-duas-linhas', 'https://www.youtube.com/embed/BbagKCkzrWs'),
(2, 'Portão basculante', '1503201609451146973223..jpg', '<p>\r\n	Port&atilde;o basculante&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portão basculante', 'Portão basculante', 'Portão basculante', 'SIM', NULL, 'portao-basculante', NULL),
(3, 'Fabricas portas aço', '1503201609461282883881..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', NULL, 'fabricas-portas-aco', NULL),
(4, 'Portões de madeira', '1503201609501367441206..jpg', '<p>\r\n	Port&otilde;es de madeira&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portões de madeira', 'Portões de madeira', 'Portões de madeira', 'SIM', NULL, 'portoes-de-madeira', NULL),
(5, 'Portões automáticos', '1503201609511206108237..jpg', '<p>\r\n	Port&otilde;es autom&aacute;ticos&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portões automáticos', 'Portões automáticos', 'Portões automáticos', 'SIM', NULL, 'portoes-automaticos', NULL),
(6, 'Portfólio 1', '1603201602001115206967..jpg', '<p>\r\n	Portf&oacute;lio 1</p>', 'Portfólio 1', 'Portfólio 1', 'Portfólio 1', 'SIM', NULL, 'portfolio-1', NULL),
(7, 'Portfólio 2', '1603201602001357146508..jpg', '<p>\r\n	Portf&oacute;lio 2</p>', 'Portfólio 2', 'Portfólio 2', 'Portfólio 2', 'SIM', NULL, 'portfolio-2', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

CREATE TABLE `tb_produtos` (
  `idproduto` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) UNSIGNED NOT NULL,
  `id_subcategoriaproduto` int(11) NOT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modelo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao_video` longtext COLLATE utf8_unicode_ci,
  `codigo_produto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exibir_home` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modelo_produto` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aro` int(100) DEFAULT NULL,
  `medida` int(100) DEFAULT NULL,
  `indice` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `carga` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `largura` int(100) DEFAULT NULL,
  `perfil` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `garantia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_tipoveiculo` int(11) NOT NULL,
  `id_marcaveiculo` int(11) NOT NULL,
  `id_modeloveiculo` int(11) NOT NULL,
  `altura` int(11) NOT NULL,
  `indice_carga` int(11) NOT NULL,
  `indice_velocidade` int(11) NOT NULL,
  `imagem_flayer` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `id_categoriaproduto`, `id_subcategoriaproduto`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `modelo`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `codigo_produto`, `exibir_home`, `modelo_produto`, `aro`, `medida`, `indice`, `carga`, `largura`, `perfil`, `garantia`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `altura`, `indice_carga`, `indice_velocidade`, `imagem_flayer`) VALUES
(38, 'PNEU MICHELIN 165/70 R 13 79T ENERGY XM2', '0605201702055112035208..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong> em estoque</p>', 97, 0, '', '', '', 'SIM', 0, 'pneu-michelin-16570-r-13-79t-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 13, 205, 'V (Até 240 km/h)', '96 (710 kg por pneu)', 165, 'teste perfil', '5 ANOS', 11, 0, 0, 70, 79, 190, '0605201702054235714231..jpg'),
(39, 'PNEU MICHELIN 175/70 R 13 82T ENERGY XM2', '0605201702197240633211..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 98, 0, '', '', '', 'SIM', 0, 'pneu-michelin-17570-r-13-82t-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 13, 175, 'V (Até 220 km/h)', '92 (790 kg por pneu)', 175, 'Fórmula Energy', '5 ANOS', 11, 0, 0, 70, 82, 190, '0605201702198208783177..jpg'),
(41, 'PNEU MICHELIN 185/70 R 13 86T ENERGY XM2', '0605201702246525118321..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade: </strong>em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-18570-r-13-86t-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 13, NULL, NULL, NULL, 185, NULL, '5 ANOS', 11, 0, 0, 70, 86, 190, '0605201702241377029029..jpg'),
(42, 'PNEU MICHELIN 165/70 R 14 81T ENERGY XM2', '0605201702273471260157..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-16570-r-14-81t-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 165, NULL, '5 ANOS', 11, 0, 0, 70, 81, 190, '0605201702277447726825..jpg'),
(43, 'PNEU MICHELIN 165/65 R 14 79T ENERGY XM2', '0605201702292500721083..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-16565-r-14-79t-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 165, NULL, '5 ANOS', 11, 0, 0, 65, 79, 190, '0605201702293259450329..jpg'),
(44, 'PNEU MICHELIN 175/80 R 14 88H ENERGY XM2', '0605201702339108840458..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-17580-r-14-88h-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 175, NULL, '5 ANOS', 11, 0, 0, 80, 88, 210, '0605201702331297741581..jpg'),
(45, 'PNEU MICHELIN 175/70 R 14 88T ENERGY XM2', '0605201702353722688737..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-17570-r-14-88t-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 175, NULL, '5 ANOS', 11, 0, 0, 70, 88, 190, '0605201702356130405105..jpg'),
(46, 'PNEU MICHELIN 175/65 R 14 82T ENERGY XM2', '0605201702373744328742..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-17565-r-14-82t-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 175, NULL, '5 ANOS', 11, 0, 0, 65, 82, 190, '0605201702378196271976..jpg'),
(47, 'PNEU MICHELIN 175/65 R 14 86T ENERGY XM2', '0605201702402278019351..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-17565-r-14-86t-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 175, NULL, '5 ANOS', 11, 0, 0, 65, 86, 190, '0605201702401587241064..jpg'),
(48, 'PNEU MICHELIN 185/70 R 14 88T ENERGY XM2 T', '0605201702412558720364..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-18570-r-14-88t-energy-xm2-t', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 185, NULL, '5 ANOS', 11, 0, 0, 70, 88, 190, '0605201702416462010091..jpg'),
(49, 'PNEU MICHELIN 185/70 R 14 88H ENERGY XM2 H', '0605201702445049924316..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-18570-r-14-88h-energy-xm2-h', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'NAO', NULL, 14, NULL, NULL, NULL, 185, NULL, '5 ANOS', 11, 0, 0, 70, 88, 210, '0605201702447115491777..jpg'),
(50, 'PNEU MICHELIN 185/65 R 14 86T ENERGY XM2 T', '0605201702482019755012..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-18565-r-14-86t-energy-xm2-t', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 185, NULL, '5 ANOS', 11, 0, 0, 65, 86, 190, '0605201702481797462680..jpg'),
(51, 'PNEU MICHELIN 185/65 R 14 86H ENERGY XM2 H', '0605201702507904078885..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-18565-r-14-86h-energy-xm2-h', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 185, NULL, '5 ANOS', 11, 0, 0, 65, 86, 210, '0605201702503722066709..jpg'),
(52, 'PNEU MICHELIN 185/60 R 14 82H ENERGY XM2 H', '0605201702535474547011..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-18560-r-14-82h-energy-xm2-h', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 185, NULL, '5 ANOS', 11, 0, 0, 60, 82, 210, '0605201702537600082606..jpg'),
(53, 'PNEU MICHELIN 195/70 R 14 91H ENERGY XM2', '0605201702564625275457..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-19570-r-14-91h-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 70, 91, 210, '0605201702566481719501..jpg'),
(54, 'PNEU MICHELIN 195/65 R 14 89H ENERGY SAVER', '0605201702571847168496..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-19565-r-14-89h-energy-saver', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 65, 89, 210, '0605201702575401887123..jpg'),
(55, 'PNEU MICHELIN 195/60 R 14 86H ENERGY XM2', '0605201703006410428434..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-19560-r-14-86h-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'NAO', NULL, 14, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 60, 86, 210, '0605201703008697481304..jpg'),
(56, 'PNEU MICHELIN 175/65 R 15 84H ENERGY XM2', '0605201703026582005156..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-17565-r-15-84h-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 175, NULL, '5 ANOS', 11, 0, 0, 65, 84, 210, '0605201703023729672336..jpg'),
(57, 'PNEU MICHELIN 175/65 R 15 84H ENERGY SAVER', '0605201703038264091263..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-17565-r-15-84h-energy-saver', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 175, NULL, '5 ANOS', 11, 0, 0, 65, 84, 210, '0605201703039558578220..jpg'),
(58, 'PNEU MICHELIN 185/65 R 15 88T ENERGY XM2 T', '0605201703042437892125..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-18565-r-15-88t-energy-xm2-t', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 185, NULL, '5 ANOS', 11, 0, 0, 65, 88, 190, '0605201703042813515435..jpg'),
(59, 'PNEU MICHELIN 185/65 R 15 88H ENERGY XM2 H', '0605201703066612214868..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-18565-r-15-88h-energy-xm2-h', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 185, NULL, '5 ANOS', 11, 0, 0, 65, 88, 210, '0605201703068478003491..jpg'),
(60, 'PNEU MICHELIN 185/60 R 15 88H ENERGY XM2', '0605201703072731081263..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-18560-r-15-88h-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 185, NULL, '5 ANOS', 11, 0, 0, 60, 88, 210, '0605201703071682436346..jpg'),
(61, 'PNEU MICHELIN 195/65 R 15 91H ENERGY XM2 H', '0605201703083676236503..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-19565-r-15-91h-energy-xm2-h', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 65, 91, 210, '0605201703081441975612..jpg'),
(62, 'PNEU MICHELIN 195/60 R 15 88H ENERGY XM2 H', '0605201703092560954069..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-19560-r-15-88h-energy-xm2-h', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 60, 88, 210, '0605201703098621063385..jpg'),
(63, 'PNEU MICHELIN 195/55 R 15 85V ENERGY XM2', '0605201703115324520385..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-19555-r-15-85v-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 55, 85, 240, '0605201703113801844372..jpg'),
(64, 'PNEU MICHELIN 205/65 R 15 94H ENERGY XM2', '0605201703129572624477..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20565-r-15-94h-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 65, 94, 210, '0605201703127965315969..jpg'),
(65, 'PNEU MICHELIN 205/60 R 15 91H ENERGY XM2 H', '0605201703132435471630..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20560-r-15-91h-energy-xm2-h', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 60, 91, 210, '0605201703135917832172..jpg'),
(66, 'PNEU MICHELIN 205/60 R 15 91V ENERGY XM2 V', '0605201703145353093325..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20560-r-15-91v-energy-xm2-v', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 60, 91, 240, '0605201703143045647363..jpg'),
(67, 'PNEU MICHELIN 185/55 R 16 83V ENERGY XM2', '0605201703156730302029..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-18555-r-16-83v-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 185, NULL, '5 ANOS', 11, 0, 0, 55, 83, 240, '0605201703156182615160..jpg'),
(68, 'PNEU MICHELIN 195/60 R 16 89H ENERGY XM2', '0605201703177663707505..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-19560-r-16-89h-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 60, 89, 210, '0605201703172188861701..jpg'),
(69, 'PNEU MICHELIN 195/55 R 16 87H ENERGY XM2', '0605201703199204137494..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-19555-r-16-87h-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 55, 87, 210, '0605201703192456274334..jpg'),
(70, 'PNEU MICHELIN 195/55 R 16 87V ENERGY XM2', '0605201703206846922358..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-19555-r-16-87v-energy-xm2', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 55, 87, 240, '0605201703209795464533..jpg'),
(71, 'PNEU MICHELIN 205/55 R 16 91V ENERGY XM2 V', '0605201703227308561499..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20555-r-16-91v-energy-xm2-v', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 55, 91, 240, '0605201703227298000641..jpg'),
(72, 'PNEU MICHELIN 205/55 R 16 91V ENERGY SAVER', '0605201703239872549085..jpg', '<p>\r\n	<strong>PNEU MICHELIN ENERGY XM 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O pneu MICHELIN EnergyTM XM2 deixou os principais concorrentes para tr&aacute;s!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a:</strong></p>\r\n<p>\r\n	Mais resistente a choques, diminuindo a possibilidade de danos acidentais no pneu.</p>\r\n<p>\r\n	Freia at&eacute; 4 metros antes(2) em piso molhado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais durabilidade:</strong></p>\r\n<p>\r\n	20% mais dur&aacute;vel(3), oferecendo uma excelente rela&ccedil;&atilde;o custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade</strong>: em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20555-r-16-91v-energy-saver', NULL, NULL, NULL, 'https://www.youtube.com/embed/YZ6dQ-AsAow', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 55, 91, 240, '0605201703232766192124..jpg'),
(73, 'PNEU MICHELIN 195/65 R 15 91H PILOT PRIMACY 3', '0605201707129154154323..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong> em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-19565-r-15-91h-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 65, 91, 210, '0605201707169947967943..jpg'),
(74, 'PNEU MICHELIN 205/60 R 16 96V PILOT PRIMACY 3', '0605201707192577684525..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20560-r-16-96v-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 60, 96, 240, '0605201707195368852412..jpg'),
(75, 'PNEU MICHELIN 205/55 R 16 94V PILOT PRIMACY 3', '0605201707202784436046..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20555-r-16-94v-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 55, 94, 240, '0605201707204189638528..jpg'),
(76, 'PNEU MICHELIN 205/55 R 16 91W PILOT PRIMACY 3', '0605201707213227829655..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20555-r-16-91w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 55, 91, 270, '0605201707218030964119..jpg'),
(77, 'PNEU MICHELIN 205/55 R 16 91W PILOT PRIMACY 3', '0605201707238021282214..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20555-r-16-91w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 55, 91, 270, '0605201707234931121919..jpg'),
(78, 'PNEU MICHELIN 215/60 R 16 99V PILOT PRIMACY 3', '0605201707246047065994..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21560-r-16-99v-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 60, 99, 240, '0605201707247079962049..jpg'),
(79, 'PNEU MICHELIN 215/55 R 16 93V PILOT PRIMACY 3', '0605201707251272516805..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21555-r-16-93v-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 55, 93, 240, '0605201707262229752730..jpg'),
(80, 'PNEU MICHELIN 215/55 R 16 97W PILOT PRIMACY 3', '0605201707297878212030..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21555-r-16-97w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 55, 97, 270, '0605201707299569021794..jpg'),
(81, 'PNEU MICHELIN 225/60 R 16 102V PILOT PRIMACY 3', '0605201707304715982683..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22560-r-16-102v-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 60, 102, 240, '0605201707306215875219..jpg'),
(82, 'PNEU MICHELIN 225/55 R 16 95W PILOT PRIMACY 3', '0605201707317627773313..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22555-r-16-95w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 55, 95, 270, '0605201707317923415944..jpg'),
(83, 'PNEU MICHELIN 205/55 R 17 95V PILOT PRIMACY 3', '0605201707336979591544..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20555-r-17-95v-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 55, 95, 240, '0605201707334303759187..jpg'),
(84, 'PNEU MICHELIN 205/55 R 17 91W PILOT PRIMACY 3', '0605201707347136400163..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20555-r-17-91w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 55, 91, 270, '0605201707347448242964..jpg'),
(85, 'PNEU MICHELIN 205/55 R 17 91W PILOT PRIMACY 3', '0605201707353423999154..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20555-r-17-91w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 55, 91, 270, '0605201707359426791378..jpg'),
(86, 'PNEU MICHELIN 205/50 R 17 93V PILOT PRIMACY 3', '0605201707362581814920..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20550-r-17-93v-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 50, 93, 240, '0605201707365378310303..jpg'),
(87, 'PNEU MICHELIN 205/45 R 17 88W PILOT PRIMACY 3', '0605201707406188844094..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20545-r-17-88w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 45, 88, 270, '0605201707404114599037..jpg'),
(88, 'PNEU MICHELIN 205/45 R 17 88W PILOT PRIMACY 3', '0605201707414181161899..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20545-r-17-88w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 45, 88, 270, '0605201707413675826692..jpg'),
(89, 'PNEU MICHELIN 215/55 R 17 94V PILOT PRIMACY 3', '0605201707427850158827..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21555-r-17-94v-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 55, 94, 240, '0605201707426555427118..jpg'),
(90, 'PNEU MICHELIN 215/50 R 17 91V PILOT PRIMACY 3', '0605201707432067042605..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21550-r-17-91v-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 50, 91, 240, '0605201707432477655613..jpg');
INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `id_categoriaproduto`, `id_subcategoriaproduto`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `modelo`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `codigo_produto`, `exibir_home`, `modelo_produto`, `aro`, `medida`, `indice`, `carga`, `largura`, `perfil`, `garantia`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `altura`, `indice_carga`, `indice_velocidade`, `imagem_flayer`) VALUES
(91, 'PNEU MICHELIN 215/50 R 17 95W PILOT PRIMACY 3', '0605201707447555260339..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21550-r-17-95w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 50, 95, 270, '0605201707443829164099..jpg'),
(92, 'PNEU MICHELIN 225/60 R 17 99V PILOT PRIMACY 3', '0605201707464051144532..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22560-r-17-99v-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 60, 99, 240, '0605201707464691032423..jpg'),
(93, 'PNEU MICHELIN 225/55 R 17 101W PILOT PRIMACY 3', '0605201707478615579472..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22555-r-17-101w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 55, 101, 270, '0605201707478595539583..jpg'),
(94, 'PNEU MICHELIN 225/50 R 17 98V PILOT PRIMACY 3', '0605201707481744654389..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22550-r-17-98v-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 50, 98, 240, '0605201707485362919155..jpg'),
(95, 'PNEU MICHELIN 225/50 R 17 98W PILOT PRIMACY 3', '0605201707492713338144..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22550-r-17-98w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 50, 98, 270, '0605201707491612326941..jpg'),
(96, 'PNEU MICHELIN 225/50 R 17 94W PILOT PRIMACY 3', '0605201707508934969392..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22550-r-17-94w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 50, 94, 270, '0605201707504643490380..jpg'),
(97, 'PNEU MICHELIN 225/45 R 17 94W PILOT PRIMACY 3', '0605201707526374002202..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22545-r-17-94w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 45, 94, 270, '0605201707521652029773..jpg'),
(98, 'PNEU MICHELIN 225/45 R 17 91W PILOT PRIMACY 3', '0605201707533735322376..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22545-r-17-91w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 45, 91, 270, '0605201707536780247617..jpg'),
(99, 'PNEU MICHELIN 235/55 R 17 99V PILOT PRIMACY 3', '0605201707537538674395..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23555-r-17-99v-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 55, 99, 240, '0605201707532684655251..jpg'),
(100, 'PNEU MICHELIN 235/50 R 17 96W PILOT PRIMACY 3', '0605201707545315997641..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23550-r-17-96w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 50, 96, 270, '0605201707541649308494..jpg'),
(101, 'PNEU MICHELIN 235/45 R 17 97W PILOT PRIMACY 3', '0605201707559703345667..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23545-r-17-97w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 45, 97, 270, '0605201707557398030658..jpg'),
(102, 'PNEU MICHELIN 245/45 R 17 95Y PILOT PRIMACY 3', '0605201707564556171099..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24545-r-17-95y-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 45, 95, 300, '0605201707562980959171..jpg'),
(103, 'PNEU MICHELIN 215/55 R 18 99V PILOT PRIMACY 3', '0605201707576584955648..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21555-r-18-99v-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 55, 99, 240, '0605201707576579756844..jpg'),
(104, 'PNEU MICHELIN 225/55 R 18 98V PILOT PRIMACY 3', '0605201707581495017885..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22555-r-18-98v-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 55, 98, 240, '0605201707589765604415..jpg'),
(105, 'PNEU MICHELIN 225/45 R 18 91W PILOT PRIMACY 3', '0605201708009082750231..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22545-r-18-91w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 45, 91, 270, '0605201708005395026033..jpg'),
(106, 'PNEU MICHELIN 225/45 R 18 95Y  PILOT PRIMACY 3', '0605201708005881764449..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22545-r-18-95y--pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 45, 95, 300, '0605201708001921993527..jpg'),
(107, 'PNEU MICHELIN 235/50 R 18 101Y PILOT PRIMACY 3', '0605201708016824179001..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23550-r-18-101y-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 50, 101, 300, '0605201708015939855625..jpg'),
(108, 'PNEU MICHELIN 235/45 R 18 98W PILOT PRIMACY 3', '0605201708027132997872..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23545-r-18-98w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 45, 98, 270, '0605201708021846138342..jpg'),
(109, 'PNEU MICHELIN 235/45 R 18 98Y  PILOT PRIMACY 3', '0605201708038627272438..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23545-r-18-98y--pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 45, 98, 300, '0605201708037257808289..jpg'),
(110, 'PNEU MICHELIN 245/50 R 18 100Y  PILOT PRIMACY 3', '0605201708045507632100..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24550-r-18-100y--pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 50, 100, 300, '0605201708049856566819..jpg'),
(111, 'PNEU MICHELIN 245/45 R 18 100W PILOT PRIMACY 3', '0605201708057064402529..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24545-r-18-100w-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 45, 100, 270, '0605201708058507347943..jpg'),
(112, 'PNEU MICHELIN 245/45 R 18 100Y PILOT PRIMACY 3', '0605201708067029931288..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24545-r-18-100y-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 45, 100, 300, '0605201708063027922134..jpg'),
(113, 'PNEU MICHELIN 245/40 R 18 97Y PILOT PRIMACY 3', '0605201708074034125179..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24540-r-18-97y-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 40, 97, 300, '0605201708078856438779..jpg'),
(114, 'PNEU MICHELIN 275/40 ZR 18 99Y PILOT PRIMACY 3', '0605201708084806163911..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-27540-zr-18-99y-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 275, NULL, '5 ANOS', 11, 0, 0, 40, 99, 300, '0605201708087857501659..jpg'),
(115, 'PNEU MICHELIN 245/45 R 19 98Y PILOT PRIMACY 3', '0605201708099035339053..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24545-r-19-98y-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 45, 98, 300, '0605201708092476600354..jpg'),
(116, 'PNEU MICHELIN 275/40 R 19 101Y PILOT PRIMACY 3', '0605201708101143108722..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY 3</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pneu Michelin Primacy 3 &eacute; a op&ccedil;&atilde;o ideal para que o seu carro tenha o melhor desempenho de rodagem e frenagem.Seu bloco de borracha com desenho em chanfro, faz com que a press&atilde;o da frenagem seja distribu&iacute;da de maneira uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-27540-r-19-101y-pilot-primacy-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/kjq6P8fsj84', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 275, NULL, '5 ANOS', 11, 0, 0, 40, 101, 300, '0605201708101753690379..jpg'),
(117, 'PNEU MICHELIN 205/55 R 17 95V PILOT PRIMACY HP', '0705201712526868098448..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY HP</strong></p>\r\n<p>\r\n	<strong>SEGURAN&Ccedil;A EXCEPCIONAL COM MAIOR DURABILIDADE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	O MICHELIN Primacy HP freia mais r&aacute;pido em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior(1): o equivalente a quase uma faixa de pedestre.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior estabilidade em qualquer condi&ccedil;&atilde;o</strong></p>\r\n<p>\r\n	Em pistas molhadas e em curvas, o MICHELIN Primacy HP responde mais r&aacute;pido(2), oferecendo um controle maior do ve&iacute;culo ao motorista, mesmo em condi&ccedil;&otilde;es perigosas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	MICHELIN Primacy HP oferece uma durabilidade 20% maior que a m&eacute;dia dos concorrentes.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20555-r-17-95v-pilot-primacy-hp', NULL, NULL, NULL, 'https://www.youtube.com/embed/8RD_wmpZC_A', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 55, 95, 240, ''),
(118, 'PNEU MICHELIN 225/50 R 17 98V PILOT PRIMACY HP', '0705201712539629432579..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY HP</strong></p>\r\n<p>\r\n	<strong>SEGURAN&Ccedil;A EXCEPCIONAL COM MAIOR DURABILIDADE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	O MICHELIN Primacy HP freia mais r&aacute;pido em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior(1): o equivalente a quase uma faixa de pedestre.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior estabilidade em qualquer condi&ccedil;&atilde;o</strong></p>\r\n<p>\r\n	Em pistas molhadas e em curvas, o MICHELIN Primacy HP responde mais r&aacute;pido(2), oferecendo um controle maior do ve&iacute;culo ao motorista, mesmo em condi&ccedil;&otilde;es perigosas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	MICHELIN Primacy HP oferece uma durabilidade 20% maior que a m&eacute;dia dos concorrentes.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22550-r-17-98v-pilot-primacy-hp', NULL, NULL, NULL, 'https://www.youtube.com/embed/8RD_wmpZC_A', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 50, 98, 240, ''),
(119, 'PNEU MICHELIN 225/45 R 17 91W PILOT PRIMACY HP', '0705201712555630022266..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY HP</strong></p>\r\n<p>\r\n	<strong>SEGURAN&Ccedil;A EXCEPCIONAL COM MAIOR DURABILIDADE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	O MICHELIN Primacy HP freia mais r&aacute;pido em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior(1): o equivalente a quase uma faixa de pedestre.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior estabilidade em qualquer condi&ccedil;&atilde;o</strong></p>\r\n<p>\r\n	Em pistas molhadas e em curvas, o MICHELIN Primacy HP responde mais r&aacute;pido(2), oferecendo um controle maior do ve&iacute;culo ao motorista, mesmo em condi&ccedil;&otilde;es perigosas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	MICHELIN Primacy HP oferece uma durabilidade 20% maior que a m&eacute;dia dos concorrentes.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22545-r-17-91w-pilot-primacy-hp', NULL, NULL, NULL, 'https://www.youtube.com/embed/8RD_wmpZC_A', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 45, 91, 270, ''),
(120, 'PNEU MICHELIN 225/45 R 17 91Y  PILOT PRIMACY HP', '0705201712585216235824..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY HP</strong></p>\r\n<p>\r\n	<strong>SEGURAN&Ccedil;A EXCEPCIONAL COM MAIOR DURABILIDADE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	O MICHELIN Primacy HP freia mais r&aacute;pido em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior(1): o equivalente a quase uma faixa de pedestre.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior estabilidade em qualquer condi&ccedil;&atilde;o</strong></p>\r\n<p>\r\n	Em pistas molhadas e em curvas, o MICHELIN Primacy HP responde mais r&aacute;pido(2), oferecendo um controle maior do ve&iacute;culo ao motorista, mesmo em condi&ccedil;&otilde;es perigosas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	MICHELIN Primacy HP oferece uma durabilidade 20% maior que a m&eacute;dia dos concorrentes.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22545-r-17-91y--pilot-primacy-hp', NULL, NULL, NULL, 'https://www.youtube.com/embed/8RD_wmpZC_A', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 45, 91, 300, ''),
(121, 'PNEU MICHELIN 235/45 R 17 94W PILOT PRIMACY HP', '0705201712596912381011..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY HP</strong></p>\r\n<p>\r\n	<strong>SEGURAN&Ccedil;A EXCEPCIONAL COM MAIOR DURABILIDADE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	O MICHELIN Primacy HP freia mais r&aacute;pido em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior(1): o equivalente a quase uma faixa de pedestre.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior estabilidade em qualquer condi&ccedil;&atilde;o</strong></p>\r\n<p>\r\n	Em pistas molhadas e em curvas, o MICHELIN Primacy HP responde mais r&aacute;pido(2), oferecendo um controle maior do ve&iacute;culo ao motorista, mesmo em condi&ccedil;&otilde;es perigosas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	MICHELIN Primacy HP oferece uma durabilidade 20% maior que a m&eacute;dia dos concorrentes.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23545-r-17-94w-pilot-primacy-hp', NULL, NULL, NULL, 'https://www.youtube.com/embed/8RD_wmpZC_A', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 45, 94, 94, ''),
(122, 'PNEU MICHELIN 245/40 R 17 91Y PILOT PRIMACY HP', '0705201701107578454162..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY HP</strong></p>\r\n<p>\r\n	<strong>SEGURAN&Ccedil;A EXCEPCIONAL COM MAIOR DURABILIDADE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	O MICHELIN Primacy HP freia mais r&aacute;pido em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior(1): o equivalente a quase uma faixa de pedestre.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior estabilidade em qualquer condi&ccedil;&atilde;o</strong></p>\r\n<p>\r\n	Em pistas molhadas e em curvas, o MICHELIN Primacy HP responde mais r&aacute;pido(2), oferecendo um controle maior do ve&iacute;culo ao motorista, mesmo em condi&ccedil;&otilde;es perigosas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	MICHELIN Primacy HP oferece uma durabilidade 20% maior que a m&eacute;dia dos concorrentes.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24540-r-17-91y-pilot-primacy-hp', NULL, NULL, NULL, 'https://www.youtube.com/embed/8RD_wmpZC_A', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 40, 91, 300, ''),
(123, 'PNEU MICHELIN 255/40 ZR 17 94W PRIMACY HP', '0705201701118155136700..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY HP</strong></p>\r\n<p>\r\n	<strong>SEGURAN&Ccedil;A EXCEPCIONAL COM MAIOR DURABILIDADE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	O MICHELIN Primacy HP freia mais r&aacute;pido em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior(1): o equivalente a quase uma faixa de pedestre.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior estabilidade em qualquer condi&ccedil;&atilde;o</strong></p>\r\n<p>\r\n	Em pistas molhadas e em curvas, o MICHELIN Primacy HP responde mais r&aacute;pido(2), oferecendo um controle maior do ve&iacute;culo ao motorista, mesmo em condi&ccedil;&otilde;es perigosas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	MICHELIN Primacy HP oferece uma durabilidade 20% maior que a m&eacute;dia dos concorrentes.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25540-zr-17-94w-primacy-hp', NULL, NULL, NULL, 'https://www.youtube.com/embed/8RD_wmpZC_A', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 40, 94, 270, ''),
(124, 'PNEU MICHELIN 255/45 R 18 99Y PILOT PRIMACY HP', '0705201701123347842871..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY HP</strong></p>\r\n<p>\r\n	<strong>SEGURAN&Ccedil;A EXCEPCIONAL COM MAIOR DURABILIDADE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	O MICHELIN Primacy HP freia mais r&aacute;pido em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior(1): o equivalente a quase uma faixa de pedestre.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior estabilidade em qualquer condi&ccedil;&atilde;o</strong></p>\r\n<p>\r\n	Em pistas molhadas e em curvas, o MICHELIN Primacy HP responde mais r&aacute;pido(2), oferecendo um controle maior do ve&iacute;culo ao motorista, mesmo em condi&ccedil;&otilde;es perigosas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	MICHELIN Primacy HP oferece uma durabilidade 20% maior que a m&eacute;dia dos concorrentes.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25545-r-18-99y-pilot-primacy-hp', NULL, NULL, NULL, 'https://www.youtube.com/embed/8RD_wmpZC_A', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 45, 99, 300, ''),
(125, 'PNEU MICHELIN 275/45 R 18 103Y PILOT PRIMACY HP', '0705201701144136654684..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY HP</strong></p>\r\n<p>\r\n	<strong>SEGURAN&Ccedil;A EXCEPCIONAL COM MAIOR DURABILIDADE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	O MICHELIN Primacy HP freia mais r&aacute;pido em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior(1): o equivalente a quase uma faixa de pedestre.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior estabilidade em qualquer condi&ccedil;&atilde;o</strong></p>\r\n<p>\r\n	Em pistas molhadas e em curvas, o MICHELIN Primacy HP responde mais r&aacute;pido(2), oferecendo um controle maior do ve&iacute;culo ao motorista, mesmo em condi&ccedil;&otilde;es perigosas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	MICHELIN Primacy HP oferece uma durabilidade 20% maior que a m&eacute;dia dos concorrentes.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-27545-r-18-103y-pilot-primacy-hp', NULL, NULL, NULL, 'https://www.youtube.com/embed/8RD_wmpZC_A', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 275, NULL, '5 ANOS', 11, 0, 0, 45, 103, 300, ''),
(126, 'PNEU MICHELIN 275/35 R 19 96Y PILOT PRIMACY HP', '0705201701165985032125..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY HP</strong></p>\r\n<p>\r\n	<strong>SEGURAN&Ccedil;A EXCEPCIONAL COM MAIOR DURABILIDADE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	O MICHELIN Primacy HP freia mais r&aacute;pido em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior(1): o equivalente a quase uma faixa de pedestre.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior estabilidade em qualquer condi&ccedil;&atilde;o</strong></p>\r\n<p>\r\n	Em pistas molhadas e em curvas, o MICHELIN Primacy HP responde mais r&aacute;pido(2), oferecendo um controle maior do ve&iacute;culo ao motorista, mesmo em condi&ccedil;&otilde;es perigosas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	MICHELIN Primacy HP oferece uma durabilidade 20% maior que a m&eacute;dia dos concorrentes.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-27535-r-19-96y-pilot-primacy-hp', NULL, NULL, NULL, 'https://www.youtube.com/embed/8RD_wmpZC_A', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 275, NULL, '5 ANOS', 11, 0, 0, 35, 96, 300, ''),
(127, 'PNEU MICHELIN 205/55 ZR 16 94W PILOT SPORT 3', '0705201701267271709980..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 3</strong></p>\r\n<p>\r\n	<strong>M&Aacute;XIMO CONTROLE E SEGURAN&Ccedil;A PARA MAIOR PRAZER EM DIRIGIR</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>M&aacute;ximo controle</strong></p>\r\n<p>\r\n	Excelente estabilidade em curvas, derivada da experi&ecirc;ncia em competi&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Muito mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	Freia at&eacute; 3 metros antes em piso molhado, em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20555-zr-16-94w-pilot-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/MvmGSBItEQg', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 55, 94, 270, ''),
(128, 'PNEU MICHELIN 215/45 R 16 90V PILOT SPORT 3', '0705201701263031979633..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 3</strong></p>\r\n<p>\r\n	<strong>M&Aacute;XIMO CONTROLE E SEGURAN&Ccedil;A PARA MAIOR PRAZER EM DIRIGIR</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>M&aacute;ximo controle</strong></p>\r\n<p>\r\n	Excelente estabilidade em curvas, derivada da experi&ecirc;ncia em competi&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Muito mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	Freia at&eacute; 3 metros antes em piso molhado, em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21545-r-16-90v-pilot-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/MvmGSBItEQg', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 45, 90, 240, ''),
(129, 'PNEU MICHELIN 205/50 ZR 17 93W PILOT SPORT 3', '0705201701274734260924..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 3</strong></p>\r\n<p>\r\n	<strong>M&Aacute;XIMO CONTROLE E SEGURAN&Ccedil;A PARA MAIOR PRAZER EM DIRIGIR</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>M&aacute;ximo controle</strong></p>\r\n<p>\r\n	Excelente estabilidade em curvas, derivada da experi&ecirc;ncia em competi&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Muito mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	Freia at&eacute; 3 metros antes em piso molhado, em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20550-zr-17-93w-pilot-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/MvmGSBItEQg', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 50, 93, 270, ''),
(130, 'PNEU MICHELIN 205/40 ZR 17 84W PILOT SPORT 3', '0705201701287087000488..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 3</strong></p>\r\n<p>\r\n	<strong>M&Aacute;XIMO CONTROLE E SEGURAN&Ccedil;A PARA MAIOR PRAZER EM DIRIGIR</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>M&aacute;ximo controle</strong></p>\r\n<p>\r\n	Excelente estabilidade em curvas, derivada da experi&ecirc;ncia em competi&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Muito mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	Freia at&eacute; 3 metros antes em piso molhado, em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20540-zr-17-84w-pilot-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/MvmGSBItEQg', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 40, 84, 270, ''),
(131, 'PNEU MICHELIN 215/45 ZR 17 91W PILOT SPORT 3', '0705201701294339248889..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 3</strong></p>\r\n<p>\r\n	<strong>M&Aacute;XIMO CONTROLE E SEGURAN&Ccedil;A PARA MAIOR PRAZER EM DIRIGIR</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>M&aacute;ximo controle</strong></p>\r\n<p>\r\n	Excelente estabilidade em curvas, derivada da experi&ecirc;ncia em competi&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Muito mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	Freia at&eacute; 3 metros antes em piso molhado, em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21545-zr-17-91w-pilot-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/MvmGSBItEQg', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 45, 91, 270, ''),
(132, 'PNEU MICHELIN 215/40 ZR 17 87W PILOT SPORT 3', '0705201701304416803986..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 3</strong></p>\r\n<p>\r\n	<strong>M&Aacute;XIMO CONTROLE E SEGURAN&Ccedil;A PARA MAIOR PRAZER EM DIRIGIR</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>M&aacute;ximo controle</strong></p>\r\n<p>\r\n	Excelente estabilidade em curvas, derivada da experi&ecirc;ncia em competi&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Muito mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	Freia at&eacute; 3 metros antes em piso molhado, em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21540-zr-17-87w-pilot-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/MvmGSBItEQg', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 40, 87, 270, ''),
(133, 'PNEU MICHELIN 215/45 ZR 18 93W PILOT SPORT 3', '0705201701312519397776..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 3</strong></p>\r\n<p>\r\n	<strong>M&Aacute;XIMO CONTROLE E SEGURAN&Ccedil;A PARA MAIOR PRAZER EM DIRIGIR</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>M&aacute;ximo controle</strong></p>\r\n<p>\r\n	Excelente estabilidade em curvas, derivada da experi&ecirc;ncia em competi&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Muito mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	Freia at&eacute; 3 metros antes em piso molhado, em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21545-zr-18-93w-pilot-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/MvmGSBItEQg', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 45, 93, 270, ''),
(134, 'PNEU MICHELIN 235/45 ZR 18 98Y PILOT SPORT 3', '0705201701318358706082..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 3</strong></p>\r\n<p>\r\n	<strong>M&Aacute;XIMO CONTROLE E SEGURAN&Ccedil;A PARA MAIOR PRAZER EM DIRIGIR</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>M&aacute;ximo controle</strong></p>\r\n<p>\r\n	Excelente estabilidade em curvas, derivada da experi&ecirc;ncia em competi&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Muito mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	Freia at&eacute; 3 metros antes em piso molhado, em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23545-zr-18-98y-pilot-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/MvmGSBItEQg', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 45, 98, 300, ''),
(135, 'PNEU MICHELIN 235/40 ZR 18 95Y PILOT SPORT 3', '0705201701326138575254..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 3</strong></p>\r\n<p>\r\n	<strong>M&Aacute;XIMO CONTROLE E SEGURAN&Ccedil;A PARA MAIOR PRAZER EM DIRIGIR</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>M&aacute;ximo controle</strong></p>\r\n<p>\r\n	Excelente estabilidade em curvas, derivada da experi&ecirc;ncia em competi&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Muito mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	Freia at&eacute; 3 metros antes em piso molhado, em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23540-zr-18-95y-pilot-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/MvmGSBItEQg', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 40, 95, 300, ''),
(136, 'PNEU MICHELIN 255/40 ZR 18 99Y PILOT SPORT 3', '0705201701337085220652..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 3</strong></p>\r\n<p>\r\n	<strong>M&Aacute;XIMO CONTROLE E SEGURAN&Ccedil;A PARA MAIOR PRAZER EM DIRIGIR</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>M&aacute;ximo controle</strong></p>\r\n<p>\r\n	Excelente estabilidade em curvas, derivada da experi&ecirc;ncia em competi&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Muito mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	Freia at&eacute; 3 metros antes em piso molhado, em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25540-zr-18-99y-pilot-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/MvmGSBItEQg', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 40, 99, 300, ''),
(137, 'PNEU MICHELIN 285/35 ZR 18 101Y PILOT SPORT 3', '0705201701345530795653..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 3</strong></p>\r\n<p>\r\n	<strong>M&Aacute;XIMO CONTROLE E SEGURAN&Ccedil;A PARA MAIOR PRAZER EM DIRIGIR</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>M&aacute;ximo controle</strong></p>\r\n<p>\r\n	Excelente estabilidade em curvas, derivada da experi&ecirc;ncia em competi&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Muito mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	Freia at&eacute; 3 metros antes em piso molhado, em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-28535-zr-18-101y-pilot-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/MvmGSBItEQg', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 285, NULL, '5 ANOS', 11, 0, 0, 35, 101, 300, ''),
(138, 'PNEU MICHELIN 245/40 ZR 19 98Y PILOT SPORT 3', '0705201701351737758070..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 3</strong></p>\r\n<p>\r\n	<strong>M&Aacute;XIMO CONTROLE E SEGURAN&Ccedil;A PARA MAIOR PRAZER EM DIRIGIR</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>M&aacute;ximo controle</strong></p>\r\n<p>\r\n	Excelente estabilidade em curvas, derivada da experi&ecirc;ncia em competi&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Muito mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	Freia at&eacute; 3 metros antes em piso molhado, em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24540-zr-19-98y-pilot-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/MvmGSBItEQg', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 40, 98, 300, ''),
(139, 'PNEU MICHELIN 285/35 ZR 20 104Y PILOT SPORT 3', '0705201701362259794270..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 3</strong></p>\r\n<p>\r\n	<strong>M&Aacute;XIMO CONTROLE E SEGURAN&Ccedil;A PARA MAIOR PRAZER EM DIRIGIR</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>M&aacute;ximo controle</strong></p>\r\n<p>\r\n	Excelente estabilidade em curvas, derivada da experi&ecirc;ncia em competi&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Muito mais seguran&ccedil;a</strong></p>\r\n<p>\r\n	Freia at&eacute; 3 metros antes em piso molhado, em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-28535-zr-20-104y-pilot-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/MvmGSBItEQg', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 285, NULL, '5 ANOS', 11, 0, 0, 35, 104, 300, ''),
(140, 'PNEU MICHELIN 205/45 R 17 88Y PILOT SPORT 4', '0705201701525630787205..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	<strong>CONHE&Ccedil;A O NOVO PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Descri&ccedil;&atilde;o:</strong></p>\r\n<p>\r\n	Melhor escolha do segmento esportivo, o pneu MICHELIN Pilot Sport 4 permite que voc&ecirc; freie antes em piso seco e molhado e proporciona maior durabilidade. Com ele, voc&ecirc; desfruta da melhor performance com muito mais controle e seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MAIOR FRENAGEM EM PISO SECO E MOLHADO</strong></p>\r\n<p>\r\n	Excelente controle e reatividade para quem busca uma condu&ccedil;&atilde;o mais esportiva, sem abrir m&atilde;o da seguran&ccedil;a: Freia at&eacute; 2m antes em piso molhado e at&eacute; 1m antes em piso seco*.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MUITO MAIS DUR&Aacute;VEL</strong></p>\r\n<p>\r\n	De acordo com os testes realizados pelo instituto independente DEKRA, o pneu MICHELIN Pilot Sport 4 dura 15% a mais que a m&eacute;dia dos concorrentes. S&atilde;o 3.700 km&sup1; rodados a mais que a m&eacute;dia dos concorrentes ou at&eacute; 6 meses&sup2; mais de uso.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20545-r-17-88y-pilot-sport-4', NULL, NULL, NULL, 'https://www.youtube.com/embed/OAUSR0zxok0', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 45, 88, 300, '0705201701507156520220..jpg'),
(141, 'PNEU MICHELIN 215/45 ZR 17 91Y PILOT SPORT 4', '0705201701546869926390..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	<strong>CONHE&Ccedil;A O NOVO PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Descri&ccedil;&atilde;o:</strong></p>\r\n<p>\r\n	Melhor escolha do segmento esportivo, o pneu MICHELIN Pilot Sport 4 permite que voc&ecirc; freie antes em piso seco e molhado e proporciona maior durabilidade. Com ele, voc&ecirc; desfruta da melhor performance com muito mais controle e seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MAIOR FRENAGEM EM PISO SECO E MOLHADO</strong></p>\r\n<p>\r\n	Excelente controle e reatividade para quem busca uma condu&ccedil;&atilde;o mais esportiva, sem abrir m&atilde;o da seguran&ccedil;a: Freia at&eacute; 2m antes em piso molhado e at&eacute; 1m antes em piso seco*.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MUITO MAIS DUR&Aacute;VEL</strong></p>\r\n<p>\r\n	De acordo com os testes realizados pelo instituto independente DEKRA, o pneu MICHELIN Pilot Sport 4 dura 15% a mais que a m&eacute;dia dos concorrentes. S&atilde;o 3.700 km&sup1; rodados a mais que a m&eacute;dia dos concorrentes ou at&eacute; 6 meses&sup2; mais de uso.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21545-zr-17-91y-pilot-sport-4', NULL, NULL, NULL, 'https://www.youtube.com/embed/OAUSR0zxok0', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 45, 91, 300, '0705201701548241422950..jpg'),
(142, 'PNEU MICHELIN 225/45 ZR 17 94Y PILOT SPORT 4', '0705201701552433812924..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	<strong>CONHE&Ccedil;A O NOVO PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Descri&ccedil;&atilde;o:</strong></p>\r\n<p>\r\n	Melhor escolha do segmento esportivo, o pneu MICHELIN Pilot Sport 4 permite que voc&ecirc; freie antes em piso seco e molhado e proporciona maior durabilidade. Com ele, voc&ecirc; desfruta da melhor performance com muito mais controle e seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MAIOR FRENAGEM EM PISO SECO E MOLHADO</strong></p>\r\n<p>\r\n	Excelente controle e reatividade para quem busca uma condu&ccedil;&atilde;o mais esportiva, sem abrir m&atilde;o da seguran&ccedil;a: Freia at&eacute; 2m antes em piso molhado e at&eacute; 1m antes em piso seco*.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MUITO MAIS DUR&Aacute;VEL</strong></p>\r\n<p>\r\n	De acordo com os testes realizados pelo instituto independente DEKRA, o pneu MICHELIN Pilot Sport 4 dura 15% a mais que a m&eacute;dia dos concorrentes. S&atilde;o 3.700 km&sup1; rodados a mais que a m&eacute;dia dos concorrentes ou at&eacute; 6 meses&sup2; mais de uso.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22545-zr-17-94y-pilot-sport-4', NULL, NULL, NULL, 'https://www.youtube.com/embed/OAUSR0zxok0', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 45, 94, 300, '0705201701556919108877..jpg');
INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `id_categoriaproduto`, `id_subcategoriaproduto`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `modelo`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `codigo_produto`, `exibir_home`, `modelo_produto`, `aro`, `medida`, `indice`, `carga`, `largura`, `perfil`, `garantia`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `altura`, `indice_carga`, `indice_velocidade`, `imagem_flayer`) VALUES
(143, 'PNEU MICHELIN 235/45 ZR 17 97Y  PILOT SPORT 4', '0705201701563647910364..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	<strong>CONHE&Ccedil;A O NOVO PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Descri&ccedil;&atilde;o:</strong></p>\r\n<p>\r\n	Melhor escolha do segmento esportivo, o pneu MICHELIN Pilot Sport 4 permite que voc&ecirc; freie antes em piso seco e molhado e proporciona maior durabilidade. Com ele, voc&ecirc; desfruta da melhor performance com muito mais controle e seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MAIOR FRENAGEM EM PISO SECO E MOLHADO</strong></p>\r\n<p>\r\n	Excelente controle e reatividade para quem busca uma condu&ccedil;&atilde;o mais esportiva, sem abrir m&atilde;o da seguran&ccedil;a: Freia at&eacute; 2m antes em piso molhado e at&eacute; 1m antes em piso seco*.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MUITO MAIS DUR&Aacute;VEL</strong></p>\r\n<p>\r\n	De acordo com os testes realizados pelo instituto independente DEKRA, o pneu MICHELIN Pilot Sport 4 dura 15% a mais que a m&eacute;dia dos concorrentes. S&atilde;o 3.700 km&sup1; rodados a mais que a m&eacute;dia dos concorrentes ou at&eacute; 6 meses&sup2; mais de uso.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23545-zr-17-97y--pilot-sport-4', NULL, NULL, NULL, 'https://www.youtube.com/embed/OAUSR0zxok0', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 45, 97, 300, '0705201701563808210020..jpg'),
(144, 'PNEU MICHELIN 245/40 ZR 17 95Y PILOT SPORT 4', '0705201702186584582188..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	<strong>CONHE&Ccedil;A O NOVO PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Descri&ccedil;&atilde;o:</strong></p>\r\n<p>\r\n	Melhor escolha do segmento esportivo, o pneu MICHELIN Pilot Sport 4 permite que voc&ecirc; freie antes em piso seco e molhado e proporciona maior durabilidade. Com ele, voc&ecirc; desfruta da melhor performance com muito mais controle e seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MAIOR FRENAGEM EM PISO SECO E MOLHADO</strong></p>\r\n<p>\r\n	Excelente controle e reatividade para quem busca uma condu&ccedil;&atilde;o mais esportiva, sem abrir m&atilde;o da seguran&ccedil;a: Freia at&eacute; 2m antes em piso molhado e at&eacute; 1m antes em piso seco*.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MUITO MAIS DUR&Aacute;VEL</strong></p>\r\n<p>\r\n	De acordo com os testes realizados pelo instituto independente DEKRA, o pneu MICHELIN Pilot Sport 4 dura 15% a mais que a m&eacute;dia dos concorrentes. S&atilde;o 3.700 km&sup1; rodados a mais que a m&eacute;dia dos concorrentes ou at&eacute; 6 meses&sup2; mais de uso.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24540-zr-17-95y-pilot-sport-4', NULL, NULL, NULL, 'https://www.youtube.com/embed/OAUSR0zxok0', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 40, 95, 300, '0705201702183848644159..jpg'),
(145, 'PNEU MICHELIN 215/40 ZR 18 89Y PILOT SPORT 4', '0705201702207374365582..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	<strong>CONHE&Ccedil;A O NOVO PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Descri&ccedil;&atilde;o:</strong></p>\r\n<p>\r\n	Melhor escolha do segmento esportivo, o pneu MICHELIN Pilot Sport 4 permite que voc&ecirc; freie antes em piso seco e molhado e proporciona maior durabilidade. Com ele, voc&ecirc; desfruta da melhor performance com muito mais controle e seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MAIOR FRENAGEM EM PISO SECO E MOLHADO</strong></p>\r\n<p>\r\n	Excelente controle e reatividade para quem busca uma condu&ccedil;&atilde;o mais esportiva, sem abrir m&atilde;o da seguran&ccedil;a: Freia at&eacute; 2m antes em piso molhado e at&eacute; 1m antes em piso seco*.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MUITO MAIS DUR&Aacute;VEL</strong></p>\r\n<p>\r\n	De acordo com os testes realizados pelo instituto independente DEKRA, o pneu MICHELIN Pilot Sport 4 dura 15% a mais que a m&eacute;dia dos concorrentes. S&atilde;o 3.700 km&sup1; rodados a mais que a m&eacute;dia dos concorrentes ou at&eacute; 6 meses&sup2; mais de uso.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21540-zr-18-89y-pilot-sport-4', NULL, NULL, NULL, 'https://www.youtube.com/embed/OAUSR0zxok0', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 40, 89, 300, '0705201702209791198989..jpg'),
(146, 'PNEU MICHELIN 225/45 ZR 18 95Y PILOT SPORT 4', '0705201702209248700940..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	<strong>CONHE&Ccedil;A O NOVO PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Descri&ccedil;&atilde;o:</strong></p>\r\n<p>\r\n	Melhor escolha do segmento esportivo, o pneu MICHELIN Pilot Sport 4 permite que voc&ecirc; freie antes em piso seco e molhado e proporciona maior durabilidade. Com ele, voc&ecirc; desfruta da melhor performance com muito mais controle e seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MAIOR FRENAGEM EM PISO SECO E MOLHADO</strong></p>\r\n<p>\r\n	Excelente controle e reatividade para quem busca uma condu&ccedil;&atilde;o mais esportiva, sem abrir m&atilde;o da seguran&ccedil;a: Freia at&eacute; 2m antes em piso molhado e at&eacute; 1m antes em piso seco*.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MUITO MAIS DUR&Aacute;VEL</strong></p>\r\n<p>\r\n	De acordo com os testes realizados pelo instituto independente DEKRA, o pneu MICHELIN Pilot Sport 4 dura 15% a mais que a m&eacute;dia dos concorrentes. S&atilde;o 3.700 km&sup1; rodados a mais que a m&eacute;dia dos concorrentes ou at&eacute; 6 meses&sup2; mais de uso.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22545-zr-18-95y-pilot-sport-4', NULL, NULL, NULL, 'https://www.youtube.com/embed/OAUSR0zxok0', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 45, 95, 300, '0705201702209041583844..jpg'),
(147, 'PNEU MICHELIN 225/40 ZR 18 92Y PILOT SPORT 4', '0705201702212424729855..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	<strong>CONHE&Ccedil;A O NOVO PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Descri&ccedil;&atilde;o:</strong></p>\r\n<p>\r\n	Melhor escolha do segmento esportivo, o pneu MICHELIN Pilot Sport 4 permite que voc&ecirc; freie antes em piso seco e molhado e proporciona maior durabilidade. Com ele, voc&ecirc; desfruta da melhor performance com muito mais controle e seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MAIOR FRENAGEM EM PISO SECO E MOLHADO</strong></p>\r\n<p>\r\n	Excelente controle e reatividade para quem busca uma condu&ccedil;&atilde;o mais esportiva, sem abrir m&atilde;o da seguran&ccedil;a: Freia at&eacute; 2m antes em piso molhado e at&eacute; 1m antes em piso seco*.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MUITO MAIS DUR&Aacute;VEL</strong></p>\r\n<p>\r\n	De acordo com os testes realizados pelo instituto independente DEKRA, o pneu MICHELIN Pilot Sport 4 dura 15% a mais que a m&eacute;dia dos concorrentes. S&atilde;o 3.700 km&sup1; rodados a mais que a m&eacute;dia dos concorrentes ou at&eacute; 6 meses&sup2; mais de uso.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22540-zr-18-92y-pilot-sport-4', NULL, NULL, NULL, 'https://www.youtube.com/embed/OAUSR0zxok0', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 40, 92, 300, '0705201702213601464182..jpg'),
(148, 'PNEU MICHELIN 235/40 ZR 18 95Y PILOT SPORT 4', '0705201702235517281637..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	<strong>CONHE&Ccedil;A O NOVO PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Descri&ccedil;&atilde;o:</strong></p>\r\n<p>\r\n	Melhor escolha do segmento esportivo, o pneu MICHELIN Pilot Sport 4 permite que voc&ecirc; freie antes em piso seco e molhado e proporciona maior durabilidade. Com ele, voc&ecirc; desfruta da melhor performance com muito mais controle e seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MAIOR FRENAGEM EM PISO SECO E MOLHADO</strong></p>\r\n<p>\r\n	Excelente controle e reatividade para quem busca uma condu&ccedil;&atilde;o mais esportiva, sem abrir m&atilde;o da seguran&ccedil;a: Freia at&eacute; 2m antes em piso molhado e at&eacute; 1m antes em piso seco*.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MUITO MAIS DUR&Aacute;VEL</strong></p>\r\n<p>\r\n	De acordo com os testes realizados pelo instituto independente DEKRA, o pneu MICHELIN Pilot Sport 4 dura 15% a mais que a m&eacute;dia dos concorrentes. S&atilde;o 3.700 km&sup1; rodados a mais que a m&eacute;dia dos concorrentes ou at&eacute; 6 meses&sup2; mais de uso.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23540-zr-18-95y-pilot-sport-4', NULL, NULL, NULL, 'https://www.youtube.com/embed/OAUSR0zxok0', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 40, 95, 300, '0705201702234102254447..jpg'),
(149, 'PNEU MICHELIN 245/45 R 18 100Y PILOT SPORT 4', '0705201702237596798853..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	<strong>CONHE&Ccedil;A O NOVO PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Descri&ccedil;&atilde;o:</strong></p>\r\n<p>\r\n	Melhor escolha do segmento esportivo, o pneu MICHELIN Pilot Sport 4 permite que voc&ecirc; freie antes em piso seco e molhado e proporciona maior durabilidade. Com ele, voc&ecirc; desfruta da melhor performance com muito mais controle e seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MAIOR FRENAGEM EM PISO SECO E MOLHADO</strong></p>\r\n<p>\r\n	Excelente controle e reatividade para quem busca uma condu&ccedil;&atilde;o mais esportiva, sem abrir m&atilde;o da seguran&ccedil;a: Freia at&eacute; 2m antes em piso molhado e at&eacute; 1m antes em piso seco*.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MUITO MAIS DUR&Aacute;VEL</strong></p>\r\n<p>\r\n	De acordo com os testes realizados pelo instituto independente DEKRA, o pneu MICHELIN Pilot Sport 4 dura 15% a mais que a m&eacute;dia dos concorrentes. S&atilde;o 3.700 km&sup1; rodados a mais que a m&eacute;dia dos concorrentes ou at&eacute; 6 meses&sup2; mais de uso.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24545-r-18-100y-pilot-sport-4', NULL, NULL, NULL, 'https://www.youtube.com/embed/OAUSR0zxok0', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 45, 100, 300, '0705201702239205600085..jpg'),
(150, 'PNEU MICHELIN 245/40 ZR 18 97Y PILOT SPORT 4', '0705201702248944854210..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	<strong>CONHE&Ccedil;A O NOVO PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Descri&ccedil;&atilde;o:</strong></p>\r\n<p>\r\n	Melhor escolha do segmento esportivo, o pneu MICHELIN Pilot Sport 4 permite que voc&ecirc; freie antes em piso seco e molhado e proporciona maior durabilidade. Com ele, voc&ecirc; desfruta da melhor performance com muito mais controle e seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MAIOR FRENAGEM EM PISO SECO E MOLHADO</strong></p>\r\n<p>\r\n	Excelente controle e reatividade para quem busca uma condu&ccedil;&atilde;o mais esportiva, sem abrir m&atilde;o da seguran&ccedil;a: Freia at&eacute; 2m antes em piso molhado e at&eacute; 1m antes em piso seco*.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MUITO MAIS DUR&Aacute;VEL</strong></p>\r\n<p>\r\n	De acordo com os testes realizados pelo instituto independente DEKRA, o pneu MICHELIN Pilot Sport 4 dura 15% a mais que a m&eacute;dia dos concorrentes. S&atilde;o 3.700 km&sup1; rodados a mais que a m&eacute;dia dos concorrentes ou at&eacute; 6 meses&sup2; mais de uso.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24540-zr-18-97y-pilot-sport-4', NULL, NULL, NULL, 'https://www.youtube.com/embed/OAUSR0zxok0', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 40, 97, 300, '0705201702243573413408..jpg'),
(151, 'PNEU MICHELIN 255/35 ZR 18 94Y PILOT SPORT 4', '0705201702256519086211..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	<strong>CONHE&Ccedil;A O NOVO PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Descri&ccedil;&atilde;o:</strong></p>\r\n<p>\r\n	Melhor escolha do segmento esportivo, o pneu MICHELIN Pilot Sport 4 permite que voc&ecirc; freie antes em piso seco e molhado e proporciona maior durabilidade. Com ele, voc&ecirc; desfruta da melhor performance com muito mais controle e seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MAIOR FRENAGEM EM PISO SECO E MOLHADO</strong></p>\r\n<p>\r\n	Excelente controle e reatividade para quem busca uma condu&ccedil;&atilde;o mais esportiva, sem abrir m&atilde;o da seguran&ccedil;a: Freia at&eacute; 2m antes em piso molhado e at&eacute; 1m antes em piso seco*.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MUITO MAIS DUR&Aacute;VEL</strong></p>\r\n<p>\r\n	De acordo com os testes realizados pelo instituto independente DEKRA, o pneu MICHELIN Pilot Sport 4 dura 15% a mais que a m&eacute;dia dos concorrentes. S&atilde;o 3.700 km&sup1; rodados a mais que a m&eacute;dia dos concorrentes ou at&eacute; 6 meses&sup2; mais de uso.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25535-zr-18-94y-pilot-sport-4', NULL, NULL, NULL, 'https://www.youtube.com/embed/OAUSR0zxok0', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 35, 94, 300, '0705201702253197223382..jpg'),
(152, 'PNEU MICHELIN 265/35 ZR 18 97Y PILOT SPORT 4', '0705201702267577875103..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	<strong>CONHE&Ccedil;A O NOVO PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Descri&ccedil;&atilde;o:</strong></p>\r\n<p>\r\n	Melhor escolha do segmento esportivo, o pneu MICHELIN Pilot Sport 4 permite que voc&ecirc; freie antes em piso seco e molhado e proporciona maior durabilidade. Com ele, voc&ecirc; desfruta da melhor performance com muito mais controle e seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MAIOR FRENAGEM EM PISO SECO E MOLHADO</strong></p>\r\n<p>\r\n	Excelente controle e reatividade para quem busca uma condu&ccedil;&atilde;o mais esportiva, sem abrir m&atilde;o da seguran&ccedil;a: Freia at&eacute; 2m antes em piso molhado e at&eacute; 1m antes em piso seco*.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MUITO MAIS DUR&Aacute;VEL</strong></p>\r\n<p>\r\n	De acordo com os testes realizados pelo instituto independente DEKRA, o pneu MICHELIN Pilot Sport 4 dura 15% a mais que a m&eacute;dia dos concorrentes. S&atilde;o 3.700 km&sup1; rodados a mais que a m&eacute;dia dos concorrentes ou at&eacute; 6 meses&sup2; mais de uso.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26535-zr-18-97y-pilot-sport-4', NULL, NULL, NULL, 'https://www.youtube.com/embed/OAUSR0zxok0', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 35, 97, 300, '0705201702263387171832..jpg'),
(153, 'PNEU MICHELIN 275/35 ZR 18 99Y PILOT SPORT 4', '0705201702278462791546..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	<strong>CONHE&Ccedil;A O NOVO PNEU MICHELIN PILOT SPORT 4</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Descri&ccedil;&atilde;o:</strong></p>\r\n<p>\r\n	Melhor escolha do segmento esportivo, o pneu MICHELIN Pilot Sport 4 permite que voc&ecirc; freie antes em piso seco e molhado e proporciona maior durabilidade. Com ele, voc&ecirc; desfruta da melhor performance com muito mais controle e seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MAIOR FRENAGEM EM PISO SECO E MOLHADO</strong></p>\r\n<p>\r\n	Excelente controle e reatividade para quem busca uma condu&ccedil;&atilde;o mais esportiva, sem abrir m&atilde;o da seguran&ccedil;a: Freia at&eacute; 2m antes em piso molhado e at&eacute; 1m antes em piso seco*.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MUITO MAIS DUR&Aacute;VEL</strong></p>\r\n<p>\r\n	De acordo com os testes realizados pelo instituto independente DEKRA, o pneu MICHELIN Pilot Sport 4 dura 15% a mais que a m&eacute;dia dos concorrentes. S&atilde;o 3.700 km&sup1; rodados a mais que a m&eacute;dia dos concorrentes ou at&eacute; 6 meses&sup2; mais de uso.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-27535-zr-18-99y-pilot-sport-4', NULL, NULL, NULL, 'https://www.youtube.com/embed/OAUSR0zxok0', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 275, NULL, '5 ANOS', 11, 0, 0, 35, 99, 300, '0705201702277616645888..jpg'),
(154, 'PNEU MICHELIN 225/45 ZR 18 95Y PILOT SUPER SPORT', '0705201702482963282196..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22545-zr-18-95y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 45, 95, 300, '0705201702467415787275..jpg'),
(155, 'PNEU MICHELIN 225/40 ZR 18 88Y PILOT SUPER SPORT', '0705201708023117365294..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22540-zr-18-88y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 40, 88, 300, '0705201708022778785837..jpg'),
(156, 'PNEU MICHELIN 235/40 ZR 18 95Y PILOT SUPER SPORT', '0705201708032058392994..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23540-zr-18-95y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 40, 95, 300, '0705201708033342508251..jpg'),
(157, 'PNEU MICHELIN 245/45 R 18 100Y PILOT SUPER SPORT', '0705201708047704455028..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24545-r-18-100y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 45, 100, 300, '0705201708043626807807..jpg'),
(158, 'PNEU MICHELIN 245/40 ZR 18 97Y PILOT SUPER SPORT', '0705201708053781684338..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24540-zr-18-97y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 40, 97, 300, '0705201708055500151966..jpg'),
(159, 'PNEU MICHELIN 245/35 ZR 18 92Y PILOT SUPER SPORT', '0705201708069356256015..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24535-zr-18-92y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 35, 92, 300, '0705201708069899834932..jpg'),
(160, 'PNEU MICHELIN 255/40 ZR 18 95Y PILOT SUPER SPORT', '0705201708078454640267..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25540-zr-18-95y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 40, 95, 300, '0705201708077646584626..jpg'),
(161, 'PNEU MICHELIN 265/40 ZR 18 101Y PILOT SUPER SPORT', '0705201708082210212941..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26540-zr-18-101y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 40, 101, 300, '0705201708082990529255..jpg'),
(162, 'PNEU MICHELIN 275/40 ZR 18 99Y PILOT SUPER SPORT', '0705201708092801123170..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-27540-zr-18-99y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 275, NULL, '5 ANOS', 11, 0, 0, 40, 99, 300, '0705201708099143333521..jpg'),
(163, 'PNEU MICHELIN 225/40 ZR 19 93Y PILOT SUPER SPORT', '0705201708101468138322..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22540-zr-19-93y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 40, 93, 300, '0705201708109793748288..jpg'),
(164, 'PNEU MICHELIN 225/35 ZR 19 88Y PILOT SUPER SPORT', '0705201708116873091310..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22535-zr-19-88y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 35, 88, 300, '0705201708114601509869..jpg'),
(165, 'PNEU MICHELIN 235/35 ZR 19 91Y PILOT SUPER SPORT', '0705201708115501729535..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23535-zr-19-91y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 35, 91, 300, '0705201708116735594293..jpg'),
(166, 'PNEU MICHELIN 245/35 ZR 19 93Y PILOT SUPER SPORT', '0705201708134554782861..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24535-zr-19-93y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 35, 93, 300, '0705201708131589200132..jpg'),
(167, 'PNEU MICHELIN 245/35 ZR 19 89Y PILOT SUPER SPORT', '0705201708142790765845..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24535-zr-19-89y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 35, 89, 300, '0705201708145017464628..jpg'),
(168, 'PNEU MICHELIN 255/45 ZR 19 100Y PILOT SUPER SPORT', '0705201708153759839381..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25545-zr-19-100y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 45, 100, 300, '0705201708159975539334..jpg'),
(169, 'PNEU MICHELIN 255/40 ZR 19 100Y PILOT SUPER SPORT', '0705201708166385718658..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25540-zr-19-100y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 40, 100, 300, '0705201708168699415094..jpg'),
(170, 'PNEU MICHELIN 255/35 ZR 19 92Y PILOT SUPER SPORT', '0705201708179794296183..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25535-zr-19-92y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 35, 92, 300, '0705201708179898719870..jpg'),
(171, 'PNEU MICHELIN 265/40 ZR 19 102Y PILOT SUPER SPORT', '0705201708177643212469..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26540-zr-19-102y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 40, 102, 300, '0705201708175070084937..jpg'),
(172, 'PNEU MICHELIN 265/35 ZR 19 98Y PILOT SUPER SPORT', '0705201708186215719129..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26535-zr-19-98y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 35, 98, 300, '0705201708182237312400..jpg'),
(173, 'PNEU MICHELIN 275/40 ZR 19 105Y PILOT SUPER SPORT', '0705201708191709938007..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-27540-zr-19-105y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 275, NULL, '5 ANOS', 11, 0, 0, 40, 105, 300, '0705201708195524476149..jpg'),
(174, 'PNEU MICHELIN 275/35 ZR 19 100Y PILOT SUPER SPORT', '0705201708217415856971..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-27535-zr-19-100y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 275, NULL, '5 ANOS', 11, 0, 0, 35, 100, 300, '0705201708217318701081..jpg'),
(175, 'PNEU MICHELIN 285/40 ZR 19 103Y PILOT SUPER SPORT', '0705201708219336697450..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-28540-zr-19-103y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 285, NULL, '5 ANOS', 11, 0, 0, 40, 103, 300, '0705201708219698084911..jpg'),
(176, 'PNEU MICHELIN 285/35 ZR 19 103Y PILOT SUPER SPORT', '0705201708227725612521..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-28535-zr-19-103y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 285, NULL, '5 ANOS', 11, 0, 0, 35, 103, 300, '0705201708222575768004..jpg'),
(177, 'PNEU MICHELIN 295/35 ZR 19 104Y PILOT SUPER SPORT', '0705201708233311268885..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-29535-zr-19-104y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 295, NULL, '5 ANOS', 11, 0, 0, 35, 104, 300, '0705201708233178437645..jpg'),
(178, 'PNEU MICHELIN 295/30 ZR 19 100Y PILOT SUPER SPORT', '0705201708249982086763..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-29530-zr-19-100y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 295, NULL, '5 ANOS', 11, 0, 0, 30, 100, 300, '0705201708245109527226..jpg'),
(179, 'PNEU MICHELIN 305/30 ZR 19 102Y PILOT SUPER SPORT', '0705201708254367019368..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-30530-zr-19-102y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 305, NULL, '5 ANOS', 11, 0, 0, 30, 102, 300, '0705201708254516126070..jpg'),
(180, 'PNEU MICHELIN 225/35 ZR 20 90Y PILOT SUPER SPORT', '0705201708269619466082..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22535-zr-20-90y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 35, 90, 300, '0705201708269367316009..jpg'),
(181, 'PNEU MICHELIN 235/35 ZR 20 92Y PILOT SUPER SPORT', '0705201708275874829226..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23535-zr-20-92y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 35, 92, 300, '0705201708271391318382..jpg'),
(182, 'PNEU MICHELIN 245/40 ZR 20 99Y  PILOT SUPER SPORT', '0705201708279947628221..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24540-zr-20-99y--pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 40, 99, 300, '0705201708275720051278..jpg'),
(183, 'PNEU MICHELIN 245/35 ZR 20 95Y PILOT SUPER SPORT', '0705201708288272179634..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24535-zr-20-95y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 35, 95, 300, '0705201708288524949761..jpg'),
(184, 'PNEU MICHELIN 255/40 ZR 20 101Y PILOT SUPER SPORT', '0705201708299832866307..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25540-zr-20-101y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 40, 101, 300, '0705201708291494877905..jpg'),
(185, 'PNEU MICHELIN 255/35 ZR 20 97Y PILOT SUPER SPORT', '0705201708307328537011..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25535-zr-20-97y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 35, 97, 300, '0705201708301650801785..jpg'),
(186, 'PNEU MICHELIN 265/35 R 20 99Y PILOT SUPER SPORT', '0705201708313815393389..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26535-r-20-99y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 35, 99, 300, '0705201708315096082612..jpg'),
(187, 'PNEU MICHELIN 275/35 R 20 102Y PILOT SUPER SPORT', '0705201708326274680110..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-27535-r-20-102y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 29, NULL, NULL, NULL, 275, NULL, '5 ANOS', 11, 0, 0, 35, 102, 300, '0705201708327058572446..jpg'),
(188, 'PNEU MICHELIN 285/30 ZR 20 95Y PILOT SUPER SPORT ZP', '0705201708334209986080..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-28530-zr-20-95y-pilot-super-sport-zp', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 285, NULL, '5 ANOS', 11, 0, 0, 30, 95, 300, '0705201708339531339547..jpg');
INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `id_categoriaproduto`, `id_subcategoriaproduto`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `modelo`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `codigo_produto`, `exibir_home`, `modelo_produto`, `aro`, `medida`, `indice`, `carga`, `largura`, `perfil`, `garantia`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `altura`, `indice_carga`, `indice_velocidade`, `imagem_flayer`) VALUES
(189, 'PNEU MICHELIN 295/35 ZR 20 105Y PILOT SUPER SPORT', '0705201708343512731173..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-29535-zr-20-105y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 295, NULL, '5 ANOS', 11, 0, 0, 35, 105, 300, '0705201708349078112778..jpg'),
(190, 'PNEU MICHELIN 295/30 ZR 20 101Y PILOT SUPER SPORT', '0705201708348141181700..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-29530-zr-20-101y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 295, NULL, '5 ANOS', 11, 0, 0, 30, 101, 300, '0705201708348594578595..jpg'),
(191, 'PNEU MICHELIN 305/30 ZR 20 103Y PILOT SUPER SPORT', '0705201708365415941882..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-30530-zr-20-103y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 305, NULL, '5 ANOS', 11, 0, 0, 30, 103, 300, '0705201708367010257463..jpg'),
(192, 'PNEU MICHELIN 285/35 ZR 21 105Y PILOT SUPER SPORT', '0705201708368158974672..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-28535-zr-21-105y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 21, NULL, NULL, NULL, 285, NULL, '5 ANOS', 11, 0, 0, 35, 105, 300, '0705201708368308928550..jpg'),
(193, 'PNEU MICHELIN 325/30 R 21 108Y PILOT SUPER SPORT', '0705201708372170875618..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SUPER SPORT</strong></p>\r\n<p>\r\n	<strong>ESPECIALMENTE PROJETADO PARA CARROS SUPER ESPORTIVOS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nascido das pistas de corrida para oferecer mais seguran&ccedil;a e prazer em dirigir.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Benef&iacute;cios do produto</strong></p>\r\n<p>\r\n	- Seguran&ccedil;a m&aacute;xima mesmo em condi&ccedil;&otilde;es extremas</p>\r\n<p>\r\n	- Desenvolvido em parceria com as montadoras mais exigentes para oferecer m&aacute;ximo prazer ao dirigir.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-32530-r-21-108y-pilot-super-sport', NULL, NULL, NULL, 'https://www.youtube.com/embed/GNJnn6sHiQM', NULL, NULL, 'SIM', NULL, 21, NULL, NULL, NULL, 325, NULL, '5 ANOS', 11, 0, 0, 30, 108, 300, '0705201708371391984120..jpg'),
(194, 'PNEU MICHELIN 245/35 ZR 19 93Y PILOT SPORT CUP 2', '0705201710225685296836..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT CUP 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais r&aacute;pido em uma pista seca. -1,8 segundos por volta.</strong></p>\r\n<p>\r\n	A Bi-Compound Technology utiliza um composto de corrida de resist&ecirc;ncia no piso externo e um elast&ocirc;mero r&iacute;gido no piso interno.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais r&aacute;pido em uma pista molhada. -1,2 segundos por volta.</strong></p>\r\n<p>\r\n	+ 20% de profundidade mais profunda do piso.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pilot Sport Cup 2 &eacute; o pneu Streetable Track &amp; Competition da Michelin, desenvolvido originalmente para o uso de equipamento original na Ferrari 458 Speciale, no Mercedes SLS AMG Coupe Black Series e no Porsche 918 Spyder. Projetado para proporcionar tempos de circuito mais r&aacute;pidos e desempenho consistente, volta ap&oacute;s volta, o Pilot Sport Cup 2 permite que esses supercarros alcancem todo o seu potencial em condi&ccedil;&otilde;es secas. Como todos os pneus de ver&atilde;o, n&atilde;o se destinam a ser conduzidos em temperaturas de quase-congelamento, atrav&eacute;s de lama, neve ou gelo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O desenho do Pilot Sport Cup 2 molda a tecnologia Bi-Compound da Michelin num design assim&eacute;trico. A tecnologia Bi-Compound apresenta diferentes compostos de borracha nas regi&otilde;es externa e exterior do piso. Massivo ombros exterior apresentam uma borracha de piso cuja dureza foi especialmente concebido para proporcionar a m&aacute;xima ader&ecirc;ncia e excelente ader&ecirc;ncia seca em curvas, especialmente em cantos apertados. As costelas de centro entalhadas e um ombro interno usam a borracha do passo com um elast&ocirc;mero mais r&iacute;gido para assegurar a dire&ccedil;&atilde;o de precis&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os pneus Pilot Sport Cup 2 apresentam o patch de contato vari&aacute;vel Michelin 3.0, uma inova&ccedil;&atilde;o desenvolvida a partir da tecnologia Patch 2.0 de contato vari&aacute;vel integrada nos pneus Piloto Super Sport e Pilot Sport A / S 3. Variable Contact Patch 3.0 maximiza o tamanho do patch de contato e otimiza a press&atilde;o da pegada para que mais borracha permane&ccedil;a em contato com a pista ao encurralar, garantindo assim uma ader&ecirc;ncia superior em curvas e cantos apertados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A estrutura interna do pneu possui duas correias de a&ccedil;o refor&ccedil;adas por um fio Twaron enrolado em espiral. Twaron &eacute; um cabo de poliamida que oferece um refor&ccedil;o leve, de alta resist&ecirc;ncia acima das correias de a&ccedil;o para melhorar a durabilidade, manuseio e desgaste de alta velocidade. A tecnologia Michelin FAZ (Filament At Zero degrees) enrola o cord&atilde;o Twaron em torno da circunfer&ecirc;ncia do pneu, da mesma forma que a linha de pesca &eacute; enrolada em carret&eacute;is de pesca, para permitir que os engenheiros da Michelin sintonizem a tens&atilde;o e a for&ccedil;a. O Twaron &eacute; aplicado com tens&atilde;o vari&aacute;vel entre os ombros eo centro do pneu, e &eacute; apertado com tanta firmeza que literalmente trava a estrutura do pneu em uma correia praticamente inel&aacute;stica. Como resultado, a pegada do pneu permanece constante e a for&ccedil;a centr&iacute;fuga &eacute; efetivamente controlada, mesmo em velocidades muito altas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Pilot Sport Cup 2 da Michelin apresenta uma caixa de poli&eacute;ster que combina conforto de condu&ccedil;&atilde;o com um manuseamento com resposta. Sua regi&atilde;o do gr&acirc;nulo &eacute; 10% mais larga do que aquela dos pneus desportivos tradicionais e &eacute; refor&ccedil;ada com um composto elevado da borracha do m&oacute;dulo que aumenta a rigidez lateral do pneu e entrega a responsividade desportiva, assim como a dire&ccedil;&atilde;o da precis&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Um processo especial de moldagem de ferramentas levou ao desenvolvimento da tecnologia Michelin Premium Touch, um processo que tornou poss&iacute;vel criar mais contraste em sec&ccedil;&otilde;es da parede lateral externa utilizando t&eacute;cnicas de micro-geometria que absorvem a luz. A geometria da textura permite contraste vari&aacute;vel para criar tons de preto que t&ecirc;m a sensa&ccedil;&atilde;o de veludo.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24535-zr-19-93y-pilot-sport-cup-2', NULL, NULL, NULL, 'https://www.youtube.com/embed/XVQlAkIQaJ4', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 35, 93, 300, ''),
(195, 'PNEU MICHELIN 325/30 ZR 19 105Y PILOT SPORT CUP 2', '0705201710244003015578..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT CUP 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais r&aacute;pido em uma pista seca. -1,8 segundos por volta.</strong></p>\r\n<p>\r\n	A Bi-Compound Technology utiliza um composto de corrida de resist&ecirc;ncia no piso externo e um elast&ocirc;mero r&iacute;gido no piso interno.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais r&aacute;pido em uma pista molhada. -1,2 segundos por volta.</strong></p>\r\n<p>\r\n	+ 20% de profundidade mais profunda do piso.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pilot Sport Cup 2 &eacute; o pneu Streetable Track &amp; Competition da Michelin, desenvolvido originalmente para o uso de equipamento original na Ferrari 458 Speciale, no Mercedes SLS AMG Coupe Black Series e no Porsche 918 Spyder. Projetado para proporcionar tempos de circuito mais r&aacute;pidos e desempenho consistente, volta ap&oacute;s volta, o Pilot Sport Cup 2 permite que esses supercarros alcancem todo o seu potencial em condi&ccedil;&otilde;es secas. Como todos os pneus de ver&atilde;o, n&atilde;o se destinam a ser conduzidos em temperaturas de quase-congelamento, atrav&eacute;s de lama, neve ou gelo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O desenho do Pilot Sport Cup 2 molda a tecnologia Bi-Compound da Michelin num design assim&eacute;trico. A tecnologia Bi-Compound apresenta diferentes compostos de borracha nas regi&otilde;es externa e exterior do piso. Massivo ombros exterior apresentam uma borracha de piso cuja dureza foi especialmente concebido para proporcionar a m&aacute;xima ader&ecirc;ncia e excelente ader&ecirc;ncia seca em curvas, especialmente em cantos apertados. As costelas de centro entalhadas e um ombro interno usam a borracha do passo com um elast&ocirc;mero mais r&iacute;gido para assegurar a dire&ccedil;&atilde;o de precis&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os pneus Pilot Sport Cup 2 apresentam o patch de contato vari&aacute;vel Michelin 3.0, uma inova&ccedil;&atilde;o desenvolvida a partir da tecnologia Patch 2.0 de contato vari&aacute;vel integrada nos pneus Piloto Super Sport e Pilot Sport A / S 3. Variable Contact Patch 3.0 maximiza o tamanho do patch de contato e otimiza a press&atilde;o da pegada para que mais borracha permane&ccedil;a em contato com a pista ao encurralar, garantindo assim uma ader&ecirc;ncia superior em curvas e cantos apertados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A estrutura interna do pneu possui duas correias de a&ccedil;o refor&ccedil;adas por um fio Twaron enrolado em espiral. Twaron &eacute; um cabo de poliamida que oferece um refor&ccedil;o leve, de alta resist&ecirc;ncia acima das correias de a&ccedil;o para melhorar a durabilidade, manuseio e desgaste de alta velocidade. A tecnologia Michelin FAZ (Filament At Zero degrees) enrola o cord&atilde;o Twaron em torno da circunfer&ecirc;ncia do pneu, da mesma forma que a linha de pesca &eacute; enrolada em carret&eacute;is de pesca, para permitir que os engenheiros da Michelin sintonizem a tens&atilde;o e a for&ccedil;a. O Twaron &eacute; aplicado com tens&atilde;o vari&aacute;vel entre os ombros eo centro do pneu, e &eacute; apertado com tanta firmeza que literalmente trava a estrutura do pneu em uma correia praticamente inel&aacute;stica. Como resultado, a pegada do pneu permanece constante e a for&ccedil;a centr&iacute;fuga &eacute; efetivamente controlada, mesmo em velocidades muito altas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Pilot Sport Cup 2 da Michelin apresenta uma caixa de poli&eacute;ster que combina conforto de condu&ccedil;&atilde;o com um manuseamento com resposta. Sua regi&atilde;o do gr&acirc;nulo &eacute; 10% mais larga do que aquela dos pneus desportivos tradicionais e &eacute; refor&ccedil;ada com um composto elevado da borracha do m&oacute;dulo que aumenta a rigidez lateral do pneu e entrega a responsividade desportiva, assim como a dire&ccedil;&atilde;o da precis&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Um processo especial de moldagem de ferramentas levou ao desenvolvimento da tecnologia Michelin Premium Touch, um processo que tornou poss&iacute;vel criar mais contraste em sec&ccedil;&otilde;es da parede lateral externa utilizando t&eacute;cnicas de micro-geometria que absorvem a luz. A geometria da textura permite contraste vari&aacute;vel para criar tons de preto que t&ecirc;m a sensa&ccedil;&atilde;o de veludo.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-32530-zr-19-105y-pilot-sport-cup-2', NULL, NULL, NULL, 'https://www.youtube.com/embed/XVQlAkIQaJ4', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 325, NULL, '5 ANOS', 11, 0, 0, 30, 105, 300, ''),
(196, 'PNEU MICHELIN 245/35 ZR 20 91Y PILOT SPORT CUP 2', '0705201710251804640032..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT CUP 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais r&aacute;pido em uma pista seca. -1,8 segundos por volta.</strong></p>\r\n<p>\r\n	A Bi-Compound Technology utiliza um composto de corrida de resist&ecirc;ncia no piso externo e um elast&ocirc;mero r&iacute;gido no piso interno.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais r&aacute;pido em uma pista molhada. -1,2 segundos por volta.</strong></p>\r\n<p>\r\n	+ 20% de profundidade mais profunda do piso.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pilot Sport Cup 2 &eacute; o pneu Streetable Track &amp; Competition da Michelin, desenvolvido originalmente para o uso de equipamento original na Ferrari 458 Speciale, no Mercedes SLS AMG Coupe Black Series e no Porsche 918 Spyder. Projetado para proporcionar tempos de circuito mais r&aacute;pidos e desempenho consistente, volta ap&oacute;s volta, o Pilot Sport Cup 2 permite que esses supercarros alcancem todo o seu potencial em condi&ccedil;&otilde;es secas. Como todos os pneus de ver&atilde;o, n&atilde;o se destinam a ser conduzidos em temperaturas de quase-congelamento, atrav&eacute;s de lama, neve ou gelo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O desenho do Pilot Sport Cup 2 molda a tecnologia Bi-Compound da Michelin num design assim&eacute;trico. A tecnologia Bi-Compound apresenta diferentes compostos de borracha nas regi&otilde;es externa e exterior do piso. Massivo ombros exterior apresentam uma borracha de piso cuja dureza foi especialmente concebido para proporcionar a m&aacute;xima ader&ecirc;ncia e excelente ader&ecirc;ncia seca em curvas, especialmente em cantos apertados. As costelas de centro entalhadas e um ombro interno usam a borracha do passo com um elast&ocirc;mero mais r&iacute;gido para assegurar a dire&ccedil;&atilde;o de precis&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os pneus Pilot Sport Cup 2 apresentam o patch de contato vari&aacute;vel Michelin 3.0, uma inova&ccedil;&atilde;o desenvolvida a partir da tecnologia Patch 2.0 de contato vari&aacute;vel integrada nos pneus Piloto Super Sport e Pilot Sport A / S 3. Variable Contact Patch 3.0 maximiza o tamanho do patch de contato e otimiza a press&atilde;o da pegada para que mais borracha permane&ccedil;a em contato com a pista ao encurralar, garantindo assim uma ader&ecirc;ncia superior em curvas e cantos apertados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A estrutura interna do pneu possui duas correias de a&ccedil;o refor&ccedil;adas por um fio Twaron enrolado em espiral. Twaron &eacute; um cabo de poliamida que oferece um refor&ccedil;o leve, de alta resist&ecirc;ncia acima das correias de a&ccedil;o para melhorar a durabilidade, manuseio e desgaste de alta velocidade. A tecnologia Michelin FAZ (Filament At Zero degrees) enrola o cord&atilde;o Twaron em torno da circunfer&ecirc;ncia do pneu, da mesma forma que a linha de pesca &eacute; enrolada em carret&eacute;is de pesca, para permitir que os engenheiros da Michelin sintonizem a tens&atilde;o e a for&ccedil;a. O Twaron &eacute; aplicado com tens&atilde;o vari&aacute;vel entre os ombros eo centro do pneu, e &eacute; apertado com tanta firmeza que literalmente trava a estrutura do pneu em uma correia praticamente inel&aacute;stica. Como resultado, a pegada do pneu permanece constante e a for&ccedil;a centr&iacute;fuga &eacute; efetivamente controlada, mesmo em velocidades muito altas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Pilot Sport Cup 2 da Michelin apresenta uma caixa de poli&eacute;ster que combina conforto de condu&ccedil;&atilde;o com um manuseamento com resposta. Sua regi&atilde;o do gr&acirc;nulo &eacute; 10% mais larga do que aquela dos pneus desportivos tradicionais e &eacute; refor&ccedil;ada com um composto elevado da borracha do m&oacute;dulo que aumenta a rigidez lateral do pneu e entrega a responsividade desportiva, assim como a dire&ccedil;&atilde;o da precis&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Um processo especial de moldagem de ferramentas levou ao desenvolvimento da tecnologia Michelin Premium Touch, um processo que tornou poss&iacute;vel criar mais contraste em sec&ccedil;&otilde;es da parede lateral externa utilizando t&eacute;cnicas de micro-geometria que absorvem a luz. A geometria da textura permite contraste vari&aacute;vel para criar tons de preto que t&ecirc;m a sensa&ccedil;&atilde;o de veludo.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24535-zr-20-91y-pilot-sport-cup-2', NULL, NULL, NULL, 'https://www.youtube.com/embed/XVQlAkIQaJ4', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 35, 91, 300, ''),
(197, 'PNEU MICHELIN 265/35 ZR 20 95Y PILOT SPORT CUP 2', '0705201710262129384063..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT CUP 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais r&aacute;pido em uma pista seca. -1,8 segundos por volta.</strong></p>\r\n<p>\r\n	A Bi-Compound Technology utiliza um composto de corrida de resist&ecirc;ncia no piso externo e um elast&ocirc;mero r&iacute;gido no piso interno.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais r&aacute;pido em uma pista molhada. -1,2 segundos por volta.</strong></p>\r\n<p>\r\n	+ 20% de profundidade mais profunda do piso.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pilot Sport Cup 2 &eacute; o pneu Streetable Track &amp; Competition da Michelin, desenvolvido originalmente para o uso de equipamento original na Ferrari 458 Speciale, no Mercedes SLS AMG Coupe Black Series e no Porsche 918 Spyder. Projetado para proporcionar tempos de circuito mais r&aacute;pidos e desempenho consistente, volta ap&oacute;s volta, o Pilot Sport Cup 2 permite que esses supercarros alcancem todo o seu potencial em condi&ccedil;&otilde;es secas. Como todos os pneus de ver&atilde;o, n&atilde;o se destinam a ser conduzidos em temperaturas de quase-congelamento, atrav&eacute;s de lama, neve ou gelo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O desenho do Pilot Sport Cup 2 molda a tecnologia Bi-Compound da Michelin num design assim&eacute;trico. A tecnologia Bi-Compound apresenta diferentes compostos de borracha nas regi&otilde;es externa e exterior do piso. Massivo ombros exterior apresentam uma borracha de piso cuja dureza foi especialmente concebido para proporcionar a m&aacute;xima ader&ecirc;ncia e excelente ader&ecirc;ncia seca em curvas, especialmente em cantos apertados. As costelas de centro entalhadas e um ombro interno usam a borracha do passo com um elast&ocirc;mero mais r&iacute;gido para assegurar a dire&ccedil;&atilde;o de precis&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os pneus Pilot Sport Cup 2 apresentam o patch de contato vari&aacute;vel Michelin 3.0, uma inova&ccedil;&atilde;o desenvolvida a partir da tecnologia Patch 2.0 de contato vari&aacute;vel integrada nos pneus Piloto Super Sport e Pilot Sport A / S 3. Variable Contact Patch 3.0 maximiza o tamanho do patch de contato e otimiza a press&atilde;o da pegada para que mais borracha permane&ccedil;a em contato com a pista ao encurralar, garantindo assim uma ader&ecirc;ncia superior em curvas e cantos apertados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A estrutura interna do pneu possui duas correias de a&ccedil;o refor&ccedil;adas por um fio Twaron enrolado em espiral. Twaron &eacute; um cabo de poliamida que oferece um refor&ccedil;o leve, de alta resist&ecirc;ncia acima das correias de a&ccedil;o para melhorar a durabilidade, manuseio e desgaste de alta velocidade. A tecnologia Michelin FAZ (Filament At Zero degrees) enrola o cord&atilde;o Twaron em torno da circunfer&ecirc;ncia do pneu, da mesma forma que a linha de pesca &eacute; enrolada em carret&eacute;is de pesca, para permitir que os engenheiros da Michelin sintonizem a tens&atilde;o e a for&ccedil;a. O Twaron &eacute; aplicado com tens&atilde;o vari&aacute;vel entre os ombros eo centro do pneu, e &eacute; apertado com tanta firmeza que literalmente trava a estrutura do pneu em uma correia praticamente inel&aacute;stica. Como resultado, a pegada do pneu permanece constante e a for&ccedil;a centr&iacute;fuga &eacute; efetivamente controlada, mesmo em velocidades muito altas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Pilot Sport Cup 2 da Michelin apresenta uma caixa de poli&eacute;ster que combina conforto de condu&ccedil;&atilde;o com um manuseamento com resposta. Sua regi&atilde;o do gr&acirc;nulo &eacute; 10% mais larga do que aquela dos pneus desportivos tradicionais e &eacute; refor&ccedil;ada com um composto elevado da borracha do m&oacute;dulo que aumenta a rigidez lateral do pneu e entrega a responsividade desportiva, assim como a dire&ccedil;&atilde;o da precis&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Um processo especial de moldagem de ferramentas levou ao desenvolvimento da tecnologia Michelin Premium Touch, um processo que tornou poss&iacute;vel criar mais contraste em sec&ccedil;&otilde;es da parede lateral externa utilizando t&eacute;cnicas de micro-geometria que absorvem a luz. A geometria da textura permite contraste vari&aacute;vel para criar tons de preto que t&ecirc;m a sensa&ccedil;&atilde;o de veludo.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26535-zr-20-95y-pilot-sport-cup-2', NULL, NULL, NULL, 'https://www.youtube.com/embed/XVQlAkIQaJ4', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 35, 95, 300, ''),
(198, 'PNEU MICHELIN 295/30 ZR 20 101Y PILOT  SPORT CUP 2', '0705201710266289331519..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT CUP 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais r&aacute;pido em uma pista seca. -1,8 segundos por volta.</strong></p>\r\n<p>\r\n	A Bi-Compound Technology utiliza um composto de corrida de resist&ecirc;ncia no piso externo e um elast&ocirc;mero r&iacute;gido no piso interno.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais r&aacute;pido em uma pista molhada. -1,2 segundos por volta.</strong></p>\r\n<p>\r\n	+ 20% de profundidade mais profunda do piso.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pilot Sport Cup 2 &eacute; o pneu Streetable Track &amp; Competition da Michelin, desenvolvido originalmente para o uso de equipamento original na Ferrari 458 Speciale, no Mercedes SLS AMG Coupe Black Series e no Porsche 918 Spyder. Projetado para proporcionar tempos de circuito mais r&aacute;pidos e desempenho consistente, volta ap&oacute;s volta, o Pilot Sport Cup 2 permite que esses supercarros alcancem todo o seu potencial em condi&ccedil;&otilde;es secas. Como todos os pneus de ver&atilde;o, n&atilde;o se destinam a ser conduzidos em temperaturas de quase-congelamento, atrav&eacute;s de lama, neve ou gelo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O desenho do Pilot Sport Cup 2 molda a tecnologia Bi-Compound da Michelin num design assim&eacute;trico. A tecnologia Bi-Compound apresenta diferentes compostos de borracha nas regi&otilde;es externa e exterior do piso. Massivo ombros exterior apresentam uma borracha de piso cuja dureza foi especialmente concebido para proporcionar a m&aacute;xima ader&ecirc;ncia e excelente ader&ecirc;ncia seca em curvas, especialmente em cantos apertados. As costelas de centro entalhadas e um ombro interno usam a borracha do passo com um elast&ocirc;mero mais r&iacute;gido para assegurar a dire&ccedil;&atilde;o de precis&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os pneus Pilot Sport Cup 2 apresentam o patch de contato vari&aacute;vel Michelin 3.0, uma inova&ccedil;&atilde;o desenvolvida a partir da tecnologia Patch 2.0 de contato vari&aacute;vel integrada nos pneus Piloto Super Sport e Pilot Sport A / S 3. Variable Contact Patch 3.0 maximiza o tamanho do patch de contato e otimiza a press&atilde;o da pegada para que mais borracha permane&ccedil;a em contato com a pista ao encurralar, garantindo assim uma ader&ecirc;ncia superior em curvas e cantos apertados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A estrutura interna do pneu possui duas correias de a&ccedil;o refor&ccedil;adas por um fio Twaron enrolado em espiral. Twaron &eacute; um cabo de poliamida que oferece um refor&ccedil;o leve, de alta resist&ecirc;ncia acima das correias de a&ccedil;o para melhorar a durabilidade, manuseio e desgaste de alta velocidade. A tecnologia Michelin FAZ (Filament At Zero degrees) enrola o cord&atilde;o Twaron em torno da circunfer&ecirc;ncia do pneu, da mesma forma que a linha de pesca &eacute; enrolada em carret&eacute;is de pesca, para permitir que os engenheiros da Michelin sintonizem a tens&atilde;o e a for&ccedil;a. O Twaron &eacute; aplicado com tens&atilde;o vari&aacute;vel entre os ombros eo centro do pneu, e &eacute; apertado com tanta firmeza que literalmente trava a estrutura do pneu em uma correia praticamente inel&aacute;stica. Como resultado, a pegada do pneu permanece constante e a for&ccedil;a centr&iacute;fuga &eacute; efetivamente controlada, mesmo em velocidades muito altas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Pilot Sport Cup 2 da Michelin apresenta uma caixa de poli&eacute;ster que combina conforto de condu&ccedil;&atilde;o com um manuseamento com resposta. Sua regi&atilde;o do gr&acirc;nulo &eacute; 10% mais larga do que aquela dos pneus desportivos tradicionais e &eacute; refor&ccedil;ada com um composto elevado da borracha do m&oacute;dulo que aumenta a rigidez lateral do pneu e entrega a responsividade desportiva, assim como a dire&ccedil;&atilde;o da precis&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Um processo especial de moldagem de ferramentas levou ao desenvolvimento da tecnologia Michelin Premium Touch, um processo que tornou poss&iacute;vel criar mais contraste em sec&ccedil;&otilde;es da parede lateral externa utilizando t&eacute;cnicas de micro-geometria que absorvem a luz. A geometria da textura permite contraste vari&aacute;vel para criar tons de preto que t&ecirc;m a sensa&ccedil;&atilde;o de veludo.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-29530-zr-20-101y-pilot--sport-cup-2', NULL, NULL, NULL, 'https://www.youtube.com/embed/XVQlAkIQaJ4', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 295, NULL, '5 ANOS', 11, 0, 0, 30, 101, 300, ''),
(199, 'PNEU MICHELIN 305/30 ZR 20 103Y PILOT SPORT CUP 2', '0705201710279443919509..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT CUP 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais r&aacute;pido em uma pista seca. -1,8 segundos por volta.</strong></p>\r\n<p>\r\n	A Bi-Compound Technology utiliza um composto de corrida de resist&ecirc;ncia no piso externo e um elast&ocirc;mero r&iacute;gido no piso interno.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais r&aacute;pido em uma pista molhada. -1,2 segundos por volta.</strong></p>\r\n<p>\r\n	+ 20% de profundidade mais profunda do piso.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pilot Sport Cup 2 &eacute; o pneu Streetable Track &amp; Competition da Michelin, desenvolvido originalmente para o uso de equipamento original na Ferrari 458 Speciale, no Mercedes SLS AMG Coupe Black Series e no Porsche 918 Spyder. Projetado para proporcionar tempos de circuito mais r&aacute;pidos e desempenho consistente, volta ap&oacute;s volta, o Pilot Sport Cup 2 permite que esses supercarros alcancem todo o seu potencial em condi&ccedil;&otilde;es secas. Como todos os pneus de ver&atilde;o, n&atilde;o se destinam a ser conduzidos em temperaturas de quase-congelamento, atrav&eacute;s de lama, neve ou gelo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O desenho do Pilot Sport Cup 2 molda a tecnologia Bi-Compound da Michelin num design assim&eacute;trico. A tecnologia Bi-Compound apresenta diferentes compostos de borracha nas regi&otilde;es externa e exterior do piso. Massivo ombros exterior apresentam uma borracha de piso cuja dureza foi especialmente concebido para proporcionar a m&aacute;xima ader&ecirc;ncia e excelente ader&ecirc;ncia seca em curvas, especialmente em cantos apertados. As costelas de centro entalhadas e um ombro interno usam a borracha do passo com um elast&ocirc;mero mais r&iacute;gido para assegurar a dire&ccedil;&atilde;o de precis&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os pneus Pilot Sport Cup 2 apresentam o patch de contato vari&aacute;vel Michelin 3.0, uma inova&ccedil;&atilde;o desenvolvida a partir da tecnologia Patch 2.0 de contato vari&aacute;vel integrada nos pneus Piloto Super Sport e Pilot Sport A / S 3. Variable Contact Patch 3.0 maximiza o tamanho do patch de contato e otimiza a press&atilde;o da pegada para que mais borracha permane&ccedil;a em contato com a pista ao encurralar, garantindo assim uma ader&ecirc;ncia superior em curvas e cantos apertados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A estrutura interna do pneu possui duas correias de a&ccedil;o refor&ccedil;adas por um fio Twaron enrolado em espiral. Twaron &eacute; um cabo de poliamida que oferece um refor&ccedil;o leve, de alta resist&ecirc;ncia acima das correias de a&ccedil;o para melhorar a durabilidade, manuseio e desgaste de alta velocidade. A tecnologia Michelin FAZ (Filament At Zero degrees) enrola o cord&atilde;o Twaron em torno da circunfer&ecirc;ncia do pneu, da mesma forma que a linha de pesca &eacute; enrolada em carret&eacute;is de pesca, para permitir que os engenheiros da Michelin sintonizem a tens&atilde;o e a for&ccedil;a. O Twaron &eacute; aplicado com tens&atilde;o vari&aacute;vel entre os ombros eo centro do pneu, e &eacute; apertado com tanta firmeza que literalmente trava a estrutura do pneu em uma correia praticamente inel&aacute;stica. Como resultado, a pegada do pneu permanece constante e a for&ccedil;a centr&iacute;fuga &eacute; efetivamente controlada, mesmo em velocidades muito altas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Pilot Sport Cup 2 da Michelin apresenta uma caixa de poli&eacute;ster que combina conforto de condu&ccedil;&atilde;o com um manuseamento com resposta. Sua regi&atilde;o do gr&acirc;nulo &eacute; 10% mais larga do que aquela dos pneus desportivos tradicionais e &eacute; refor&ccedil;ada com um composto elevado da borracha do m&oacute;dulo que aumenta a rigidez lateral do pneu e entrega a responsividade desportiva, assim como a dire&ccedil;&atilde;o da precis&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Um processo especial de moldagem de ferramentas levou ao desenvolvimento da tecnologia Michelin Premium Touch, um processo que tornou poss&iacute;vel criar mais contraste em sec&ccedil;&otilde;es da parede lateral externa utilizando t&eacute;cnicas de micro-geometria que absorvem a luz. A geometria da textura permite contraste vari&aacute;vel para criar tons de preto que t&ecirc;m a sensa&ccedil;&atilde;o de veludo.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-30530-zr-20-103y-pilot-sport-cup-2', NULL, NULL, NULL, 'https://www.youtube.com/embed/XVQlAkIQaJ4', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 305, NULL, '5 ANOS', 11, 0, 0, 30, 103, 300, ''),
(200, 'PNEU MICHELIN 325/30 R 21 104Y PILOT SPORT CUP 2', '0705201710285894799017..jpg', '<p>\r\n	<strong>PNEU MICHELIN PILOT SPORT CUP 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais r&aacute;pido em uma pista seca. -1,8 segundos por volta.</strong></p>\r\n<p>\r\n	A Bi-Compound Technology utiliza um composto de corrida de resist&ecirc;ncia no piso externo e um elast&ocirc;mero r&iacute;gido no piso interno.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais r&aacute;pido em uma pista molhada. -1,2 segundos por volta.</strong></p>\r\n<p>\r\n	+ 20% de profundidade mais profunda do piso.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Pilot Sport Cup 2 &eacute; o pneu Streetable Track &amp; Competition da Michelin, desenvolvido originalmente para o uso de equipamento original na Ferrari 458 Speciale, no Mercedes SLS AMG Coupe Black Series e no Porsche 918 Spyder. Projetado para proporcionar tempos de circuito mais r&aacute;pidos e desempenho consistente, volta ap&oacute;s volta, o Pilot Sport Cup 2 permite que esses supercarros alcancem todo o seu potencial em condi&ccedil;&otilde;es secas. Como todos os pneus de ver&atilde;o, n&atilde;o se destinam a ser conduzidos em temperaturas de quase-congelamento, atrav&eacute;s de lama, neve ou gelo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O desenho do Pilot Sport Cup 2 molda a tecnologia Bi-Compound da Michelin num design assim&eacute;trico. A tecnologia Bi-Compound apresenta diferentes compostos de borracha nas regi&otilde;es externa e exterior do piso. Massivo ombros exterior apresentam uma borracha de piso cuja dureza foi especialmente concebido para proporcionar a m&aacute;xima ader&ecirc;ncia e excelente ader&ecirc;ncia seca em curvas, especialmente em cantos apertados. As costelas de centro entalhadas e um ombro interno usam a borracha do passo com um elast&ocirc;mero mais r&iacute;gido para assegurar a dire&ccedil;&atilde;o de precis&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os pneus Pilot Sport Cup 2 apresentam o patch de contato vari&aacute;vel Michelin 3.0, uma inova&ccedil;&atilde;o desenvolvida a partir da tecnologia Patch 2.0 de contato vari&aacute;vel integrada nos pneus Piloto Super Sport e Pilot Sport A / S 3. Variable Contact Patch 3.0 maximiza o tamanho do patch de contato e otimiza a press&atilde;o da pegada para que mais borracha permane&ccedil;a em contato com a pista ao encurralar, garantindo assim uma ader&ecirc;ncia superior em curvas e cantos apertados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A estrutura interna do pneu possui duas correias de a&ccedil;o refor&ccedil;adas por um fio Twaron enrolado em espiral. Twaron &eacute; um cabo de poliamida que oferece um refor&ccedil;o leve, de alta resist&ecirc;ncia acima das correias de a&ccedil;o para melhorar a durabilidade, manuseio e desgaste de alta velocidade. A tecnologia Michelin FAZ (Filament At Zero degrees) enrola o cord&atilde;o Twaron em torno da circunfer&ecirc;ncia do pneu, da mesma forma que a linha de pesca &eacute; enrolada em carret&eacute;is de pesca, para permitir que os engenheiros da Michelin sintonizem a tens&atilde;o e a for&ccedil;a. O Twaron &eacute; aplicado com tens&atilde;o vari&aacute;vel entre os ombros eo centro do pneu, e &eacute; apertado com tanta firmeza que literalmente trava a estrutura do pneu em uma correia praticamente inel&aacute;stica. Como resultado, a pegada do pneu permanece constante e a for&ccedil;a centr&iacute;fuga &eacute; efetivamente controlada, mesmo em velocidades muito altas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Pilot Sport Cup 2 da Michelin apresenta uma caixa de poli&eacute;ster que combina conforto de condu&ccedil;&atilde;o com um manuseamento com resposta. Sua regi&atilde;o do gr&acirc;nulo &eacute; 10% mais larga do que aquela dos pneus desportivos tradicionais e &eacute; refor&ccedil;ada com um composto elevado da borracha do m&oacute;dulo que aumenta a rigidez lateral do pneu e entrega a responsividade desportiva, assim como a dire&ccedil;&atilde;o da precis&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Um processo especial de moldagem de ferramentas levou ao desenvolvimento da tecnologia Michelin Premium Touch, um processo que tornou poss&iacute;vel criar mais contraste em sec&ccedil;&otilde;es da parede lateral externa utilizando t&eacute;cnicas de micro-geometria que absorvem a luz. A geometria da textura permite contraste vari&aacute;vel para criar tons de preto que t&ecirc;m a sensa&ccedil;&atilde;o de veludo.</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-32530-r-21-104y-pilot-sport-cup-2', NULL, NULL, NULL, 'https://www.youtube.com/embed/XVQlAkIQaJ4', NULL, NULL, 'SIM', NULL, 21, NULL, NULL, NULL, 325, NULL, '5 ANOS', 11, 0, 0, 30, 104, 300, ''),
(201, 'PNEU MICHELIN 185 R 14C 102R NOVO AGILIS', '0705201710415974191936..jpg', '<p>\r\n	<strong>PNEU MICHELIN AGILIS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais economia para o seu neg&oacute;cio. Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menos imprevistos no seu dia a dia</strong></p>\r\n<p>\r\n	Freia at&eacute; 7 metros antes em piso molhado (1)</p>\r\n<p>\r\n	Mais resistente &agrave;s perfura&ccedil;&otilde;es</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Laterais refor&ccedil;adas para maior robustez</strong></p>\r\n<p>\r\n	8 escudos laterais que aumentam a prote&ccedil;&atilde;o do pneu</p>\r\n<p>\r\n	Mistura de borracha de pneu de caminh&atilde;o que &eacute; muito mais resistente&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Melhor custo por quil&ocirc;metro do mercado</strong></p>\r\n<p>\r\n	40% mais dur&aacute;vel(2)</p>\r\n<p>\r\n	Economiza at&eacute; 2% de combust&iacute;vel(3)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Testes realizados em 2014 pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental), na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	2. Testes realizados em 2014, pelo instituto DEKRA com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	3. C&aacute;lculo baseado nos testes realizados em 2014, pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong> em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-185-r-14c-102r-novo-agilis', NULL, NULL, NULL, 'https://www.youtube.com/embed/RXi6oJf0HW4', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 185, NULL, '5 ANOS', 11, 0, 0, 80, 102, 170, '0705201710466200472538..jpg'),
(202, 'PNEU MICHELIN 205/75 R 14C 109Q AGILIS', '0705201710494453887173..jpg', '<p>\r\n	<strong>PNEU MICHELIN AGILIS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais economia para o seu neg&oacute;cio. Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menos imprevistos no seu dia a dia</strong></p>\r\n<p>\r\n	Freia at&eacute; 7 metros antes em piso molhado (1)</p>\r\n<p>\r\n	Mais resistente &agrave;s perfura&ccedil;&otilde;es</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Laterais refor&ccedil;adas para maior robustez</strong></p>\r\n<p>\r\n	8 escudos laterais que aumentam a prote&ccedil;&atilde;o do pneu</p>\r\n<p>\r\n	Mistura de borracha de pneu de caminh&atilde;o que &eacute; muito mais resistente&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Melhor custo por quil&ocirc;metro do mercado</strong></p>\r\n<p>\r\n	40% mais dur&aacute;vel(2)</p>\r\n<p>\r\n	Economiza at&eacute; 2% de combust&iacute;vel(3)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Testes realizados em 2014 pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental), na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	2. Testes realizados em 2014, pelo instituto DEKRA com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	3. C&aacute;lculo baseado nos testes realizados em 2014, pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20575-r-14c-109q-agilis', NULL, NULL, NULL, 'https://www.youtube.com/embed/RXi6oJf0HW4', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 75, 109, 160, '0705201710499433609897..jpg'),
(203, 'PNEU MICHELIN 195/70 R 15C 104R AGILIS GRNX', '0705201710505911847539..jpg', '<p>\r\n	<strong>PNEU MICHELIN AGILIS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais economia para o seu neg&oacute;cio. Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menos imprevistos no seu dia a dia</strong></p>\r\n<p>\r\n	Freia at&eacute; 7 metros antes em piso molhado (1)</p>\r\n<p>\r\n	Mais resistente &agrave;s perfura&ccedil;&otilde;es</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Laterais refor&ccedil;adas para maior robustez</strong></p>\r\n<p>\r\n	8 escudos laterais que aumentam a prote&ccedil;&atilde;o do pneu</p>\r\n<p>\r\n	Mistura de borracha de pneu de caminh&atilde;o que &eacute; muito mais resistente&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Melhor custo por quil&ocirc;metro do mercado</strong></p>\r\n<p>\r\n	40% mais dur&aacute;vel(2)</p>\r\n<p>\r\n	Economiza at&eacute; 2% de combust&iacute;vel(3)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Testes realizados em 2014 pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental), na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	2. Testes realizados em 2014, pelo instituto DEKRA com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	3. C&aacute;lculo baseado nos testes realizados em 2014, pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-19570-r-15c-104r-agilis-grnx', NULL, NULL, NULL, 'https://www.youtube.com/embed/RXi6oJf0HW4', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 70, 104, 170, '0705201710508603358521..jpg'),
(204, 'PNEU MICHELIN 205/70 R 15C 106R NOVO AGILIS', '0705201710513691957054..jpg', '<p>\r\n	<strong>PNEU MICHELIN AGILIS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais economia para o seu neg&oacute;cio. Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menos imprevistos no seu dia a dia</strong></p>\r\n<p>\r\n	Freia at&eacute; 7 metros antes em piso molhado (1)</p>\r\n<p>\r\n	Mais resistente &agrave;s perfura&ccedil;&otilde;es</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Laterais refor&ccedil;adas para maior robustez</strong></p>\r\n<p>\r\n	8 escudos laterais que aumentam a prote&ccedil;&atilde;o do pneu</p>\r\n<p>\r\n	Mistura de borracha de pneu de caminh&atilde;o que &eacute; muito mais resistente&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Melhor custo por quil&ocirc;metro do mercado</strong></p>\r\n<p>\r\n	40% mais dur&aacute;vel(2)</p>\r\n<p>\r\n	Economiza at&eacute; 2% de combust&iacute;vel(3)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Testes realizados em 2014 pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental), na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	2. Testes realizados em 2014, pelo instituto DEKRA com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	3. C&aacute;lculo baseado nos testes realizados em 2014, pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20570-r-15c-106r-novo-agilis', NULL, NULL, NULL, 'https://www.youtube.com/embed/RXi6oJf0HW4', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 70, 106, 170, '0705201710517375186954..jpg'),
(205, 'PNEU MICHELIN 215/70 R 15 109S AGILIS', '0705201710526958638305..jpg', '<p>\r\n	<strong>PNEU MICHELIN AGILIS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais economia para o seu neg&oacute;cio. Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menos imprevistos no seu dia a dia</strong></p>\r\n<p>\r\n	Freia at&eacute; 7 metros antes em piso molhado (1)</p>\r\n<p>\r\n	Mais resistente &agrave;s perfura&ccedil;&otilde;es</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Laterais refor&ccedil;adas para maior robustez</strong></p>\r\n<p>\r\n	8 escudos laterais que aumentam a prote&ccedil;&atilde;o do pneu</p>\r\n<p>\r\n	Mistura de borracha de pneu de caminh&atilde;o que &eacute; muito mais resistente&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Melhor custo por quil&ocirc;metro do mercado</strong></p>\r\n<p>\r\n	40% mais dur&aacute;vel(2)</p>\r\n<p>\r\n	Economiza at&eacute; 2% de combust&iacute;vel(3)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Testes realizados em 2014 pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental), na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	2. Testes realizados em 2014, pelo instituto DEKRA com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	3. C&aacute;lculo baseado nos testes realizados em 2014, pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21570-r-15-109s-agilis', NULL, NULL, NULL, 'https://www.youtube.com/embed/RXi6oJf0HW4', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 70, 109, 180, '0805201701171882694922..jpg');
INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `id_categoriaproduto`, `id_subcategoriaproduto`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `modelo`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `codigo_produto`, `exibir_home`, `modelo_produto`, `aro`, `medida`, `indice`, `carga`, `largura`, `perfil`, `garantia`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `altura`, `indice_carga`, `indice_velocidade`, `imagem_flayer`) VALUES
(206, 'PNEU MICHELIN 225/70 R 15C 112R NOVO AGILIS', '0705201710535921897201..jpg', '<p>\r\n	<strong>PNEU MICHELIN AGILIS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais economia para o seu neg&oacute;cio. Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menos imprevistos no seu dia a dia</strong></p>\r\n<p>\r\n	Freia at&eacute; 7 metros antes em piso molhado (1)</p>\r\n<p>\r\n	Mais resistente &agrave;s perfura&ccedil;&otilde;es</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Laterais refor&ccedil;adas para maior robustez</strong></p>\r\n<p>\r\n	8 escudos laterais que aumentam a prote&ccedil;&atilde;o do pneu</p>\r\n<p>\r\n	Mistura de borracha de pneu de caminh&atilde;o que &eacute; muito mais resistente&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Melhor custo por quil&ocirc;metro do mercado</strong></p>\r\n<p>\r\n	40% mais dur&aacute;vel(2)</p>\r\n<p>\r\n	Economiza at&eacute; 2% de combust&iacute;vel(3)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Testes realizados em 2014 pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental), na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	2. Testes realizados em 2014, pelo instituto DEKRA com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	3. C&aacute;lculo baseado nos testes realizados em 2014, pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22570-r-15c-112r-novo-agilis', NULL, NULL, NULL, 'https://www.youtube.com/embed/RXi6oJf0HW4', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 70, 112, 170, '0805201701178725027334..jpg'),
(207, 'PNEU MICHELIN 195/75 R 16C 107R NOVO AGILIS', '0705201710541204260550..jpg', '<p>\r\n	<strong>PNEU MICHELIN AGILIS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais economia para o seu neg&oacute;cio. Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menos imprevistos no seu dia a dia</strong></p>\r\n<p>\r\n	Freia at&eacute; 7 metros antes em piso molhado (1)</p>\r\n<p>\r\n	Mais resistente &agrave;s perfura&ccedil;&otilde;es</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Laterais refor&ccedil;adas para maior robustez</strong></p>\r\n<p>\r\n	8 escudos laterais que aumentam a prote&ccedil;&atilde;o do pneu</p>\r\n<p>\r\n	Mistura de borracha de pneu de caminh&atilde;o que &eacute; muito mais resistente&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Melhor custo por quil&ocirc;metro do mercado</strong></p>\r\n<p>\r\n	40% mais dur&aacute;vel(2)</p>\r\n<p>\r\n	Economiza at&eacute; 2% de combust&iacute;vel(3)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Testes realizados em 2014 pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental), na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	2. Testes realizados em 2014, pelo instituto DEKRA com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	3. C&aacute;lculo baseado nos testes realizados em 2014, pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-19575-r-16c-107r-novo-agilis', NULL, NULL, NULL, 'https://www.youtube.com/embed/RXi6oJf0HW4', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 75, 107, 170, '0805201701182418180699..jpg'),
(208, 'PNEU MICHELIN 195/65 R 16C 104R NOVO AGILIS', '0705201710558547961529..jpg', '<p>\r\n	<strong>PNEU MICHELIN AGILIS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais economia para o seu neg&oacute;cio. Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menos imprevistos no seu dia a dia</strong></p>\r\n<p>\r\n	Freia at&eacute; 7 metros antes em piso molhado (1)</p>\r\n<p>\r\n	Mais resistente &agrave;s perfura&ccedil;&otilde;es</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Laterais refor&ccedil;adas para maior robustez</strong></p>\r\n<p>\r\n	8 escudos laterais que aumentam a prote&ccedil;&atilde;o do pneu</p>\r\n<p>\r\n	Mistura de borracha de pneu de caminh&atilde;o que &eacute; muito mais resistente&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Melhor custo por quil&ocirc;metro do mercado</strong></p>\r\n<p>\r\n	40% mais dur&aacute;vel(2)</p>\r\n<p>\r\n	Economiza at&eacute; 2% de combust&iacute;vel(3)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Testes realizados em 2014 pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental), na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	2. Testes realizados em 2014, pelo instituto DEKRA com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	3. C&aacute;lculo baseado nos testes realizados em 2014, pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-19565-r-16c-104r-novo-agilis', NULL, NULL, NULL, 'https://www.youtube.com/embed/RXi6oJf0HW4', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 65, 104, 170, '0805201701188436636514..jpg'),
(209, 'PNEU MICHELIN 205/75 R 16C 110R NOVO AGILIS', '0705201710565142041283..jpg', '<p>\r\n	<strong>PNEU MICHELIN AGILIS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais economia para o seu neg&oacute;cio. Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menos imprevistos no seu dia a dia</strong></p>\r\n<p>\r\n	Freia at&eacute; 7 metros antes em piso molhado (1)</p>\r\n<p>\r\n	Mais resistente &agrave;s perfura&ccedil;&otilde;es</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Laterais refor&ccedil;adas para maior robustez</strong></p>\r\n<p>\r\n	8 escudos laterais que aumentam a prote&ccedil;&atilde;o do pneu</p>\r\n<p>\r\n	Mistura de borracha de pneu de caminh&atilde;o que &eacute; muito mais resistente&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Melhor custo por quil&ocirc;metro do mercado</strong></p>\r\n<p>\r\n	40% mais dur&aacute;vel(2)</p>\r\n<p>\r\n	Economiza at&eacute; 2% de combust&iacute;vel(3)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Testes realizados em 2014 pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental), na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	2. Testes realizados em 2014, pelo instituto DEKRA com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	3. C&aacute;lculo baseado nos testes realizados em 2014, pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20575-r-16c-110r-novo-agilis', NULL, NULL, NULL, 'https://www.youtube.com/embed/RXi6oJf0HW4', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 75, 110, 170, '0805201701187821508198..jpg'),
(210, 'PNEU MICHELIN 205/65 R 16C 107T NOVO AGILIS', '0705201710577180222272..jpg', '<p>\r\n	<strong>PNEU MICHELIN AGILIS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais economia para o seu neg&oacute;cio. Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menos imprevistos no seu dia a dia</strong></p>\r\n<p>\r\n	Freia at&eacute; 7 metros antes em piso molhado (1)</p>\r\n<p>\r\n	Mais resistente &agrave;s perfura&ccedil;&otilde;es</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Laterais refor&ccedil;adas para maior robustez</strong></p>\r\n<p>\r\n	8 escudos laterais que aumentam a prote&ccedil;&atilde;o do pneu</p>\r\n<p>\r\n	Mistura de borracha de pneu de caminh&atilde;o que &eacute; muito mais resistente&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Melhor custo por quil&ocirc;metro do mercado</strong></p>\r\n<p>\r\n	40% mais dur&aacute;vel(2)</p>\r\n<p>\r\n	Economiza at&eacute; 2% de combust&iacute;vel(3)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Testes realizados em 2014 pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental), na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	2. Testes realizados em 2014, pelo instituto DEKRA com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	3. C&aacute;lculo baseado nos testes realizados em 2014, pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20565-r-16c-107t-novo-agilis', NULL, NULL, NULL, 'https://www.youtube.com/embed/RXi6oJf0HW4', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 65, 107, 190, '0805201701186163048653..jpg'),
(211, 'PNEU MICHELIN 215/75 R 16C 116R NOVO AGILIS', '0705201710585587527470..jpg', '<p>\r\n	<strong>PNEU MICHELIN AGILIS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais economia para o seu neg&oacute;cio. Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menos imprevistos no seu dia a dia</strong></p>\r\n<p>\r\n	Freia at&eacute; 7 metros antes em piso molhado (1)</p>\r\n<p>\r\n	Mais resistente &agrave;s perfura&ccedil;&otilde;es</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Laterais refor&ccedil;adas para maior robustez</strong></p>\r\n<p>\r\n	8 escudos laterais que aumentam a prote&ccedil;&atilde;o do pneu</p>\r\n<p>\r\n	Mistura de borracha de pneu de caminh&atilde;o que &eacute; muito mais resistente&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Melhor custo por quil&ocirc;metro do mercado</strong></p>\r\n<p>\r\n	40% mais dur&aacute;vel(2)</p>\r\n<p>\r\n	Economiza at&eacute; 2% de combust&iacute;vel(3)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Testes realizados em 2014 pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental), na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	2. Testes realizados em 2014, pelo instituto DEKRA com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	3. C&aacute;lculo baseado nos testes realizados em 2014, pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21575-r-16c-116r-novo-agilis', NULL, NULL, NULL, 'https://www.youtube.com/embed/RXi6oJf0HW4', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 75, 116, 170, '0805201701198854996980..jpg'),
(212, 'PNEU MICHELIN 225/75 R 16C 118R NOVO AGILIS', '0705201710596447489021..jpg', '<p>\r\n	<strong>PNEU MICHELIN AGILIS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais economia para o seu neg&oacute;cio. Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menos imprevistos no seu dia a dia</strong></p>\r\n<p>\r\n	Freia at&eacute; 7 metros antes em piso molhado (1)</p>\r\n<p>\r\n	Mais resistente &agrave;s perfura&ccedil;&otilde;es</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Laterais refor&ccedil;adas para maior robustez</strong></p>\r\n<p>\r\n	8 escudos laterais que aumentam a prote&ccedil;&atilde;o do pneu</p>\r\n<p>\r\n	Mistura de borracha de pneu de caminh&atilde;o que &eacute; muito mais resistente&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Melhor custo por quil&ocirc;metro do mercado</strong></p>\r\n<p>\r\n	40% mais dur&aacute;vel(2)</p>\r\n<p>\r\n	Economiza at&eacute; 2% de combust&iacute;vel(3)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Testes realizados em 2014 pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental), na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	2. Testes realizados em 2014, pelo instituto DEKRA com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	3. C&aacute;lculo baseado nos testes realizados em 2014, pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22575-r-16c-118r-novo-agilis', NULL, NULL, NULL, 'https://www.youtube.com/embed/RXi6oJf0HW4', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 75, 118, 170, '0805201701195898216068..jpg'),
(213, 'PNEU MICHELIN 225/65 R 16 112R NOVO AGILIS', '0705201711002752033008..jpg', '<p>\r\n	<strong>PNEU MICHELIN AGILIS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Mais economia para o seu neg&oacute;cio. Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menos imprevistos no seu dia a dia</strong></p>\r\n<p>\r\n	Freia at&eacute; 7 metros antes em piso molhado (1)</p>\r\n<p>\r\n	Mais resistente &agrave;s perfura&ccedil;&otilde;es</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Laterais refor&ccedil;adas para maior robustez</strong></p>\r\n<p>\r\n	8 escudos laterais que aumentam a prote&ccedil;&atilde;o do pneu</p>\r\n<p>\r\n	Mistura de borracha de pneu de caminh&atilde;o que &eacute; muito mais resistente&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Melhor custo por quil&ocirc;metro do mercado</strong></p>\r\n<p>\r\n	40% mais dur&aacute;vel(2)</p>\r\n<p>\r\n	Economiza at&eacute; 2% de combust&iacute;vel(3)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Testes realizados em 2014 pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental), na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	2. Testes realizados em 2014, pelo instituto DEKRA com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	3. C&aacute;lculo baseado nos testes realizados em 2014, pelo instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro(pirelli, goodyear, maxxis e continental) na dimen&ccedil;&atilde;o 195/75 R16C</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22565-r-16-112r-novo-agilis', NULL, NULL, NULL, 'https://www.youtube.com/embed/RXi6oJf0HW4', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 65, 112, 170, '0805201701197727576572..jpg'),
(214, 'PNEU MICHELIN 205/70 R 15 96T LTX FORCE', '0805201706294909116191..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong> em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20570-r-15-96t-ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 70, 96, 190, '0805201706333871544479..jpg'),
(215, 'PNEU MICHELIN 205/65 R 15 94T LTX FORCE', '0805201706355495669970..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20565-r-15-94t-ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 65, 94, 190, '0805201706352404622907..jpg'),
(216, 'PNEU MICHELIN 205/60 R 15 91H  LTX FORCE', '0805201706366482757483..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20560-r-15-91h--ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 60, 91, 210, '0805201706362416877568..jpg'),
(217, 'PNEU MICHELIN 225/75 R 15  108S  LTX FORCE', '0805201706377470224843..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22575-r-15--108s--ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 75, 108, 180, '0805201706377919710928..jpg'),
(218, 'PNEU MICHELIN 235/75 R 15 105T  LTX FORCE', '0805201706387442786668..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23575-r-15-105t--ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 75, 105, 105, '0805201706388049228344..jpg'),
(219, 'PNEU MICHELIN 31X10,50 R 15 109S  LTX FORCE', '0805201706395304932689..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-31x1050-r-15-109s--ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 31, NULL, '5 ANOS', 11, 0, 0, 10, 109, 180, '0805201706391903214537..jpg'),
(220, 'PNEU MICHELIN 205/60 R 16 92H  LTX FORCE', '0805201706404299200148..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20560-r-16-92h--ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 60, 92, 210, '0805201706408183358514..jpg'),
(221, 'PNEU MICHELIN 245/70 R 16 111T  LTX FORCE', '0805201706415773333058..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24570-r-16-111t--ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 70, 111, 190, '0805201706415045056918..jpg'),
(222, 'PNEU MICHELIN 265/75 R 16 123R  LTX FORCE', '0805201706426690779365..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26575-r-16-123r--ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 75, 123, 170, '0805201706428596857719..jpg'),
(223, 'PNEU MICHELIN 225/65 R 17 102H LTX FORCE', '0805201706436981068953..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22565-r-17-102h-ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 65, 102, 210, '0805201706438381937133..jpg'),
(224, 'PNEU MICHELIN 245/65 R 17 111T  LTX FORCE', '0805201706448598824598..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24565-r-17-111t--ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 65, 111, 190, '0805201706449141039401..jpg'),
(225, 'PNEU MICHELIN 265/60 R 18 110T  LTX FORCE', '0805201706458790367614..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26560-r-18-110t--ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 60, 110, 190, '0805201706459648194499..jpg'),
(226, 'PNEU MICHELIN 215/65 R 16 98T LTX FORCE', '0805201706495765267482..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21565-r-16-98t-ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 65, 98, 190, '0805201706495275549565..jpg'),
(227, 'PNEU MICHELIN 225/75 R 16 115S LTX FORCE', '0805201706511642548549..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22575-r-16-115s-ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 75, 115, 180, '0805201706516296691488..jpg'),
(228, 'PNEU MICHELIN 235/70 R 16 106T LTX FORCE', '0805201706522351825978..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23570-r-16-106t-ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 70, 106, 190, '0805201706526997556516..jpg'),
(229, 'PNEU MICHELIN 245/75 R 16 120S LTX FORCE', '0805201706527078196191..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24575-r-16-120s-ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 75, 120, 180, '0805201706523199165707..jpg'),
(230, 'PNEU MICHELIN 255/70 R 16 111T  LTX FORCE', '0805201706538875541993..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25570-r-16-111t--ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 70, 111, 190, '0805201706531361590578..jpg');
INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `id_categoriaproduto`, `id_subcategoriaproduto`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `modelo`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `codigo_produto`, `exibir_home`, `modelo_produto`, `aro`, `medida`, `indice`, `carga`, `largura`, `perfil`, `garantia`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `altura`, `indice_carga`, `indice_velocidade`, `imagem_flayer`) VALUES
(231, 'PNEU MICHELIN 265/65 R 17 112H  LTX FORCE', '0805201706549302283144..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26565-r-17-112h--ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 65, 112, 210, '0805201706548531651074..jpg'),
(232, 'PNEU MICHELIN 265/70 R 16 112T LTX FORCE', '0805201706568446722481..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX FORCE</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A melhor solu&ccedil;&atilde;o, n&atilde;o importa o seu caminho.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Durabilidade Insuper&aacute;vel</strong></p>\r\n<p>\r\n	Dura at&eacute; 35% a mais que os principais concorrentes da categoria.(1) Seguran&ccedil;a m&aacute;xima na estrada e fora da estrada</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Na Estrada</strong></p>\r\n<p>\r\n	Freia at&eacute; 2 metros antes em piso molhado(2)</p>\r\n<p>\r\n	Melhor controle em situa&ccedil;&atilde;o de aquaplanagem(2)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Fora da Estrada</strong></p>\r\n<p>\r\n	Tra&ccedil;&atilde;o e robustez comprovadas em uso extremo de rali.</p>\r\n<p>\r\n	Economiza at&eacute; 4% de combust&iacute;vel</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma combina&ccedil;&atilde;o perfeita entre durabilidade e seguran&ccedil;a no asfalto.&nbsp;</strong></p>\r\n<p>\r\n	Pneus traz letras brancas opcional. Excelente custo benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	(1)Testes realizados em 2014 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear) na dimens&atilde;o 265/70 R16 112 T.</p>\r\n<p>\r\n	(2)Testes de distancia de frenagem no molhado e aquaplanagem em curva realizados em 2014 pelo Instituto TUV SUD Alemanha, com pneus comprados no mercado brasileiro, (Pirelli, Goodyear), na dimens&atilde;o 265/70 R16 112T</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26570-r-16-112t-ltx-force', NULL, NULL, NULL, 'https://www.youtube.com/embed/Gnkr5G8netU', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 70, 112, 190, '0805201706561812209350..jpg'),
(233, 'PNEU MICHELIN 245/65 R 17 107S LTX  A /T 2', '0805201707095365794694..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX A/T2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>O PNEU IDEAL PARA TODOS OS TIPOS DE TERRENO: DURABILIDADE, CONFORTO E TRA&Ccedil;&Atilde;O</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	Menor desgaste da banda de rodagem e maior resist&ecirc;ncia a solos irregulares.</p>\r\n<p>\r\n	<strong>Maior Conforto</strong></p>\r\n<p>\r\n	Baixo n&iacute;vel de ru&iacute;dos e vibra&ccedil;&otilde;es.</p>\r\n<p>\r\n	<strong>Melhor capacidade de tra&ccedil;&atilde;o&nbsp;</strong></p>\r\n<p>\r\n	Excelente desempenho, garantindo maior seguran&ccedil;a, em todos os tipos de terreno.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MAX TOUCH CONSTRUCTION&trade;</strong></p>\r\n<p>\r\n	Maximiza o contato com o solo e melhora o n&iacute;vel de tra&ccedil;&atilde;o, distribuindo uniformemente as for&ccedil;as necess&aacute;rias para acelerar e frear. Essa arquitetura garante um desgaste uniforme da banda de rodagem, assegurando vida longa ao pneu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MICHELIN COMFORT CONTROL</strong></p>\r\n<p>\r\n	Tecnologia aplicada na fabrica&ccedil;&atilde;o e no desenho da escultura, que garantem uma condu&ccedil;&atilde;o mais silenciosa e agrad&aacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>MICHELIN BITING EDGES</strong></p>\r\n<p>\r\n	Blocos da banda de rodagem mais agressivos para cavar em qualquer superf&iacute;cie, proporcionando melhor tra&ccedil;&atilde;o off-road, em v&aacute;rios tipos de terrenos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong> em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24565-r-17-107s-ltx--a-t-2', NULL, NULL, NULL, 'https://www.youtube.com/embed/akGRIMSAxfs', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 65, 107, 180, ''),
(234, 'PNEU MICHELIN 215/75 R 15 100T LATITUDE CROSS', '0805201707241259965039..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE CROSS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior conforto</strong></p>\r\n<p>\r\n	Rodagem mais silenciosa e agrad&aacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	Resistente em qualquer tipo de superf&iacute;cie.</p>\r\n<p>\r\n	Tra&ccedil;&atilde;o off-road</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Maior ader&ecirc;ncia em superf&iacute;cies escorregadias (tra&ccedil;&atilde;o e freadas), garantindo maior seguran&ccedil;a.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Rodagem mais silenciosa e agrad&aacute;vel.</strong></p>\r\n<p>\r\n	O pneu Michelin Latitude Cross foi desenvolvido para utiliza&ccedil;&agrave;o em SUVs, crossovers e picapes e oferece bom comportamento em desempenho e seguran&ccedil;a, inclusive em terrenos acidentados.&nbsp;</p>\r\n<p>\r\n	A banda de rodagem assim&eacute;trica oferece bom apoio nas curvas em conjunto com a estrutura refor&ccedil;ada, enquanto os blocos independentes conseguem garantir boa tra&ccedil;&agrave;o e manuseio da dire&ccedil;&agrave;o no uso como pneu off-road.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong> em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21575-r-15-100t-latitude-cross', NULL, NULL, NULL, 'https://www.youtube.com/embed/pv1b3lXdCRg', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 75, 100, 190, '0805201707244585959393..jpg'),
(235, 'PNEU MICHELIN 255/70 R 15 108H LATITUDE CROSS', '0805201707299524747456..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE CROSS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior conforto</strong></p>\r\n<p>\r\n	Rodagem mais silenciosa e agrad&aacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	Resistente em qualquer tipo de superf&iacute;cie.</p>\r\n<p>\r\n	Tra&ccedil;&atilde;o off-road</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Maior ader&ecirc;ncia em superf&iacute;cies escorregadias (tra&ccedil;&atilde;o e freadas), garantindo maior seguran&ccedil;a.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Rodagem mais silenciosa e agrad&aacute;vel.</strong></p>\r\n<p>\r\n	O pneu Michelin Latitude Cross foi desenvolvido para utiliza&ccedil;&agrave;o em SUVs, crossovers e picapes e oferece bom comportamento em desempenho e seguran&ccedil;a, inclusive em terrenos acidentados.&nbsp;</p>\r\n<p>\r\n	A banda de rodagem assim&eacute;trica oferece bom apoio nas curvas em conjunto com a estrutura refor&ccedil;ada, enquanto os blocos independentes conseguem garantir boa tra&ccedil;&agrave;o e manuseio da dire&ccedil;&agrave;o no uso como pneu off-road.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25570-r-15-108h-latitude-cross', NULL, NULL, NULL, 'https://www.youtube.com/embed/pv1b3lXdCRg', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 70, 108, 210, '0805201707298045420220..jpg'),
(236, 'PNEU MICHELIN 205/80 R 16 104T LATITUDE CROSS', '0805201707302785303766..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE CROSS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior conforto</strong></p>\r\n<p>\r\n	Rodagem mais silenciosa e agrad&aacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	Resistente em qualquer tipo de superf&iacute;cie.</p>\r\n<p>\r\n	Tra&ccedil;&atilde;o off-road</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Maior ader&ecirc;ncia em superf&iacute;cies escorregadias (tra&ccedil;&atilde;o e freadas), garantindo maior seguran&ccedil;a.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Rodagem mais silenciosa e agrad&aacute;vel.</strong></p>\r\n<p>\r\n	O pneu Michelin Latitude Cross foi desenvolvido para utiliza&ccedil;&agrave;o em SUVs, crossovers e picapes e oferece bom comportamento em desempenho e seguran&ccedil;a, inclusive em terrenos acidentados.&nbsp;</p>\r\n<p>\r\n	A banda de rodagem assim&eacute;trica oferece bom apoio nas curvas em conjunto com a estrutura refor&ccedil;ada, enquanto os blocos independentes conseguem garantir boa tra&ccedil;&agrave;o e manuseio da dire&ccedil;&agrave;o no uso como pneu off-road.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-20580-r-16-104t-latitude-cross', NULL, NULL, NULL, 'https://www.youtube.com/embed/pv1b3lXdCRg', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 80, 104, 190, '0805201707309459235010..jpg'),
(237, 'PNEU MICHELIN 235/60 R 16 104H LATITUDE CROSS', '0805201707312799492018..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE CROSS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior conforto</strong></p>\r\n<p>\r\n	Rodagem mais silenciosa e agrad&aacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	Resistente em qualquer tipo de superf&iacute;cie.</p>\r\n<p>\r\n	Tra&ccedil;&atilde;o off-road</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Maior ader&ecirc;ncia em superf&iacute;cies escorregadias (tra&ccedil;&atilde;o e freadas), garantindo maior seguran&ccedil;a.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Rodagem mais silenciosa e agrad&aacute;vel.</strong></p>\r\n<p>\r\n	O pneu Michelin Latitude Cross foi desenvolvido para utiliza&ccedil;&agrave;o em SUVs, crossovers e picapes e oferece bom comportamento em desempenho e seguran&ccedil;a, inclusive em terrenos acidentados.&nbsp;</p>\r\n<p>\r\n	A banda de rodagem assim&eacute;trica oferece bom apoio nas curvas em conjunto com a estrutura refor&ccedil;ada, enquanto os blocos independentes conseguem garantir boa tra&ccedil;&agrave;o e manuseio da dire&ccedil;&agrave;o no uso como pneu off-road.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23560-r-16-104h-latitude-cross', NULL, NULL, NULL, 'https://www.youtube.com/embed/pv1b3lXdCRg', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 60, 104, 210, '0805201707311186392369..jpg'),
(238, 'PNEU MICHELIN 215/60 R 17 100H LATITUDE CROSS', '0805201707313666750722..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE CROSS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior conforto</strong></p>\r\n<p>\r\n	Rodagem mais silenciosa e agrad&aacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	Resistente em qualquer tipo de superf&iacute;cie.</p>\r\n<p>\r\n	Tra&ccedil;&atilde;o off-road</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Maior ader&ecirc;ncia em superf&iacute;cies escorregadias (tra&ccedil;&atilde;o e freadas), garantindo maior seguran&ccedil;a.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Rodagem mais silenciosa e agrad&aacute;vel.</strong></p>\r\n<p>\r\n	O pneu Michelin Latitude Cross foi desenvolvido para utiliza&ccedil;&agrave;o em SUVs, crossovers e picapes e oferece bom comportamento em desempenho e seguran&ccedil;a, inclusive em terrenos acidentados.&nbsp;</p>\r\n<p>\r\n	A banda de rodagem assim&eacute;trica oferece bom apoio nas curvas em conjunto com a estrutura refor&ccedil;ada, enquanto os blocos independentes conseguem garantir boa tra&ccedil;&agrave;o e manuseio da dire&ccedil;&agrave;o no uso como pneu off-road.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21560-r-17-100h-latitude-cross', NULL, NULL, NULL, 'https://www.youtube.com/embed/pv1b3lXdCRg', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 60, 100, 210, '0805201707311499215128..jpg'),
(239, 'PNEU MICHELIN 255/65 R 17 110T LATITUDE CROSS', '0805201707327664118152..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE CROSS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior conforto</strong></p>\r\n<p>\r\n	Rodagem mais silenciosa e agrad&aacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade</strong></p>\r\n<p>\r\n	Resistente em qualquer tipo de superf&iacute;cie.</p>\r\n<p>\r\n	Tra&ccedil;&atilde;o off-road</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Maior ader&ecirc;ncia em superf&iacute;cies escorregadias (tra&ccedil;&atilde;o e freadas), garantindo maior seguran&ccedil;a.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Rodagem mais silenciosa e agrad&aacute;vel.</strong></p>\r\n<p>\r\n	O pneu Michelin Latitude Cross foi desenvolvido para utiliza&ccedil;&agrave;o em SUVs, crossovers e picapes e oferece bom comportamento em desempenho e seguran&ccedil;a, inclusive em terrenos acidentados.&nbsp;</p>\r\n<p>\r\n	A banda de rodagem assim&eacute;trica oferece bom apoio nas curvas em conjunto com a estrutura refor&ccedil;ada, enquanto os blocos independentes conseguem garantir boa tra&ccedil;&agrave;o e manuseio da dire&ccedil;&agrave;o no uso como pneu off-road.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25565-r-17-110t-latitude-cross', NULL, NULL, NULL, 'https://www.youtube.com/embed/pv1b3lXdCRg', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 65, 110, 190, '0805201707328467611231..jpg'),
(240, 'PNEU MICHELIN 235/75 R15 109T  XLT A / S', '0905201710398322919449..jpg', '<p>\r\n	<strong>PNEU MICHELIN XLT A/S</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>50% mais dur&aacute;vel*&nbsp;</strong></p>\r\n<p>\r\n	&bull; Durabilidade Michelin X&reg; LT A/S: +50% vs. m&eacute;dia da concorr&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes realizados em 2016 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	Muito mas resistente &agrave; agress&otilde;es .**&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>DUPLA CARCA&Ccedil;A ROBUSTA:&nbsp;</strong></p>\r\n<p>\r\n	&bull; 2 napas carca&ccedil;as&nbsp;</p>\r\n<p>\r\n	&bull; Cabos mais grossos e densos&nbsp;</p>\r\n<p>\r\n	Com isso, o pneu MICHELIN X&reg; LT A/S atende &agrave;s necessidades de alto torque das caminhonetas modernas e permite encarar os trabalhos pesados.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	**Em rela&ccedil;&atilde;o &agrave; gama anterior Michelin LTX M/S 2</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>25% mais seguro nas aquaplanagens.***&nbsp;</strong></p>\r\n<p>\r\n	Maior seguran&ccedil;a: +25% mais controle em situa&ccedil;&atilde;o de aquaplanagem em curva</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	***Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes de aquaplanagem em curva realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong> em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23575-r15-109t--xlt-a--s', NULL, NULL, NULL, 'https://www.youtube.com/embed/HEjGNvZO6jA', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 75, 109, 190, ''),
(241, 'PNEU MICHELIN 215/85 R 16 115R  XLT A / S', '0905201710407951433699..jpg', '<p>\r\n	<strong>PNEU MICHELIN XLT A/S</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>50% mais dur&aacute;vel*&nbsp;</strong></p>\r\n<p>\r\n	&bull; Durabilidade Michelin X&reg; LT A/S: +50% vs. m&eacute;dia da concorr&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes realizados em 2016 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	Muito mas resistente &agrave; agress&otilde;es .**&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>DUPLA CARCA&Ccedil;A ROBUSTA:&nbsp;</strong></p>\r\n<p>\r\n	&bull; 2 napas carca&ccedil;as&nbsp;</p>\r\n<p>\r\n	&bull; Cabos mais grossos e densos&nbsp;</p>\r\n<p>\r\n	Com isso, o pneu MICHELIN X&reg; LT A/S atende &agrave;s necessidades de alto torque das caminhonetas modernas e permite encarar os trabalhos pesados.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	**Em rela&ccedil;&atilde;o &agrave; gama anterior Michelin LTX M/S 2</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>25% mais seguro nas aquaplanagens.***&nbsp;</strong></p>\r\n<p>\r\n	Maior seguran&ccedil;a: +25% mais controle em situa&ccedil;&atilde;o de aquaplanagem em curva</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	***Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes de aquaplanagem em curva realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21585-r-16-115r--xlt-a--s', NULL, NULL, NULL, 'https://www.youtube.com/embed/HEjGNvZO6jA', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 85, 115, 170, ''),
(242, 'PNEU MICHELIN 245/70 R 16 107T XLT A / S', '0905201710417247535598..jpg', '<p>\r\n	<strong>PNEU MICHELIN XLT A/S</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>50% mais dur&aacute;vel*&nbsp;</strong></p>\r\n<p>\r\n	&bull; Durabilidade Michelin X&reg; LT A/S: +50% vs. m&eacute;dia da concorr&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes realizados em 2016 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	Muito mas resistente &agrave; agress&otilde;es .**&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>DUPLA CARCA&Ccedil;A ROBUSTA:&nbsp;</strong></p>\r\n<p>\r\n	&bull; 2 napas carca&ccedil;as&nbsp;</p>\r\n<p>\r\n	&bull; Cabos mais grossos e densos&nbsp;</p>\r\n<p>\r\n	Com isso, o pneu MICHELIN X&reg; LT A/S atende &agrave;s necessidades de alto torque das caminhonetas modernas e permite encarar os trabalhos pesados.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	**Em rela&ccedil;&atilde;o &agrave; gama anterior Michelin LTX M/S 2</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>25% mais seguro nas aquaplanagens.***&nbsp;</strong></p>\r\n<p>\r\n	Maior seguran&ccedil;a: +25% mais controle em situa&ccedil;&atilde;o de aquaplanagem em curva</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	***Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes de aquaplanagem em curva realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24570-r-16-107t-xlt-a--s', NULL, NULL, NULL, 'https://www.youtube.com/embed/HEjGNvZO6jA', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 70, 107, 190, ''),
(243, 'PNEU MICHELIN 255/70 R 16 111T XLT A / S', '0905201710424692502079..jpg', '<p>\r\n	<strong>PNEU MICHELIN XLT A/S</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>50% mais dur&aacute;vel*&nbsp;</strong></p>\r\n<p>\r\n	&bull; Durabilidade Michelin X&reg; LT A/S: +50% vs. m&eacute;dia da concorr&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes realizados em 2016 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	Muito mas resistente &agrave; agress&otilde;es .**&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>DUPLA CARCA&Ccedil;A ROBUSTA:&nbsp;</strong></p>\r\n<p>\r\n	&bull; 2 napas carca&ccedil;as&nbsp;</p>\r\n<p>\r\n	&bull; Cabos mais grossos e densos&nbsp;</p>\r\n<p>\r\n	Com isso, o pneu MICHELIN X&reg; LT A/S atende &agrave;s necessidades de alto torque das caminhonetas modernas e permite encarar os trabalhos pesados.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	**Em rela&ccedil;&atilde;o &agrave; gama anterior Michelin LTX M/S 2</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>25% mais seguro nas aquaplanagens.***&nbsp;</strong></p>\r\n<p>\r\n	Maior seguran&ccedil;a: +25% mais controle em situa&ccedil;&atilde;o de aquaplanagem em curva</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	***Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes de aquaplanagem em curva realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25570-r-16-111t-xlt-a--s', NULL, NULL, NULL, 'https://www.youtube.com/embed/HEjGNvZO6jA', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 70, 111, 190, ''),
(244, 'PNEU MICHELIN 265/75 R 16 123R  XLT A / S', '0905201710434061609353..jpg', '<p>\r\n	<strong>PNEU MICHELIN XLT A/S</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>50% mais dur&aacute;vel*&nbsp;</strong></p>\r\n<p>\r\n	&bull; Durabilidade Michelin X&reg; LT A/S: +50% vs. m&eacute;dia da concorr&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes realizados em 2016 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	Muito mas resistente &agrave; agress&otilde;es .**&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>DUPLA CARCA&Ccedil;A ROBUSTA:&nbsp;</strong></p>\r\n<p>\r\n	&bull; 2 napas carca&ccedil;as&nbsp;</p>\r\n<p>\r\n	&bull; Cabos mais grossos e densos&nbsp;</p>\r\n<p>\r\n	Com isso, o pneu MICHELIN X&reg; LT A/S atende &agrave;s necessidades de alto torque das caminhonetas modernas e permite encarar os trabalhos pesados.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	**Em rela&ccedil;&atilde;o &agrave; gama anterior Michelin LTX M/S 2</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>25% mais seguro nas aquaplanagens.***&nbsp;</strong></p>\r\n<p>\r\n	Maior seguran&ccedil;a: +25% mais controle em situa&ccedil;&atilde;o de aquaplanagem em curva</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	***Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes de aquaplanagem em curva realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26575-r-16-123r--xlt-a--s', NULL, NULL, NULL, 'https://www.youtube.com/embed/HEjGNvZO6jA', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 75, 123, 170, ''),
(245, 'PNEU MICHELIN 245/65 R 17 109T  XLT A / S', '0905201710444322329925..jpg', '<p>\r\n	<strong>PNEU MICHELIN XLT A/S</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>50% mais dur&aacute;vel*&nbsp;</strong></p>\r\n<p>\r\n	&bull; Durabilidade Michelin X&reg; LT A/S: +50% vs. m&eacute;dia da concorr&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes realizados em 2016 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	Muito mas resistente &agrave; agress&otilde;es .**&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>DUPLA CARCA&Ccedil;A ROBUSTA:&nbsp;</strong></p>\r\n<p>\r\n	&bull; 2 napas carca&ccedil;as&nbsp;</p>\r\n<p>\r\n	&bull; Cabos mais grossos e densos&nbsp;</p>\r\n<p>\r\n	Com isso, o pneu MICHELIN X&reg; LT A/S atende &agrave;s necessidades de alto torque das caminhonetas modernas e permite encarar os trabalhos pesados.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	**Em rela&ccedil;&atilde;o &agrave; gama anterior Michelin LTX M/S 2</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>25% mais seguro nas aquaplanagens.***&nbsp;</strong></p>\r\n<p>\r\n	Maior seguran&ccedil;a: +25% mais controle em situa&ccedil;&atilde;o de aquaplanagem em curva</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	***Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes de aquaplanagem em curva realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24565-r-17-109t--xlt-a--s', NULL, NULL, NULL, 'https://www.youtube.com/embed/HEjGNvZO6jA', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 65, 109, 190, ''),
(246, 'PNEU MICHELIN 265/70 R 17 121R  XLT A / S', '0905201710459909226720..jpg', '<p>\r\n	<strong>PNEU MICHELIN XLT A/S</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>50% mais dur&aacute;vel*&nbsp;</strong></p>\r\n<p>\r\n	&bull; Durabilidade Michelin X&reg; LT A/S: +50% vs. m&eacute;dia da concorr&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes realizados em 2016 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	Muito mas resistente &agrave; agress&otilde;es .**&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>DUPLA CARCA&Ccedil;A ROBUSTA:&nbsp;</strong></p>\r\n<p>\r\n	&bull; 2 napas carca&ccedil;as&nbsp;</p>\r\n<p>\r\n	&bull; Cabos mais grossos e densos&nbsp;</p>\r\n<p>\r\n	Com isso, o pneu MICHELIN X&reg; LT A/S atende &agrave;s necessidades de alto torque das caminhonetas modernas e permite encarar os trabalhos pesados.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	**Em rela&ccedil;&atilde;o &agrave; gama anterior Michelin LTX M/S 2</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>25% mais seguro nas aquaplanagens.***&nbsp;</strong></p>\r\n<p>\r\n	Maior seguran&ccedil;a: +25% mais controle em situa&ccedil;&atilde;o de aquaplanagem em curva</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	***Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes de aquaplanagem em curva realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26570-r-17-121r--xlt-a--s', NULL, NULL, NULL, 'https://www.youtube.com/embed/HEjGNvZO6jA', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 70, 121, 170, ''),
(247, 'PNEU MICHELIN 265/65 R 17 112T  XLT A / S', '0905201710465243530310..jpg', '<p>\r\n	<strong>PNEU MICHELIN XLT A/S</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>50% mais dur&aacute;vel*&nbsp;</strong></p>\r\n<p>\r\n	&bull; Durabilidade Michelin X&reg; LT A/S: +50% vs. m&eacute;dia da concorr&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes realizados em 2016 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	Muito mas resistente &agrave; agress&otilde;es .**&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>DUPLA CARCA&Ccedil;A ROBUSTA:&nbsp;</strong></p>\r\n<p>\r\n	&bull; 2 napas carca&ccedil;as&nbsp;</p>\r\n<p>\r\n	&bull; Cabos mais grossos e densos&nbsp;</p>\r\n<p>\r\n	Com isso, o pneu MICHELIN X&reg; LT A/S atende &agrave;s necessidades de alto torque das caminhonetas modernas e permite encarar os trabalhos pesados.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	**Em rela&ccedil;&atilde;o &agrave; gama anterior Michelin LTX M/S 2</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>25% mais seguro nas aquaplanagens.***&nbsp;</strong></p>\r\n<p>\r\n	Maior seguran&ccedil;a: +25% mais controle em situa&ccedil;&atilde;o de aquaplanagem em curva</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	***Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes de aquaplanagem em curva realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26565-r-17-112t--xlt-a--s', NULL, NULL, NULL, 'https://www.youtube.com/embed/HEjGNvZO6jA', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 65, 112, 190, ''),
(248, 'PNEU MICHELIN 265/65 R 17 112T XLT A / S', '0905201710476232983962..jpg', '<p>\r\n	<strong>PNEU MICHELIN XLT A/S</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>50% mais dur&aacute;vel*&nbsp;</strong></p>\r\n<p>\r\n	&bull; Durabilidade Michelin X&reg; LT A/S: +50% vs. m&eacute;dia da concorr&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes realizados em 2016 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	Muito mas resistente &agrave; agress&otilde;es .**&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>DUPLA CARCA&Ccedil;A ROBUSTA:&nbsp;</strong></p>\r\n<p>\r\n	&bull; 2 napas carca&ccedil;as&nbsp;</p>\r\n<p>\r\n	&bull; Cabos mais grossos e densos&nbsp;</p>\r\n<p>\r\n	Com isso, o pneu MICHELIN X&reg; LT A/S atende &agrave;s necessidades de alto torque das caminhonetas modernas e permite encarar os trabalhos pesados.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	**Em rela&ccedil;&atilde;o &agrave; gama anterior Michelin LTX M/S 2</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>25% mais seguro nas aquaplanagens.***&nbsp;</strong></p>\r\n<p>\r\n	Maior seguran&ccedil;a: +25% mais controle em situa&ccedil;&atilde;o de aquaplanagem em curva</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	***Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes de aquaplanagem em curva realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26565-r-17-112t-xlt-a--s', NULL, NULL, NULL, 'https://www.youtube.com/embed/HEjGNvZO6jA', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 65, 112, 190, ''),
(249, 'PNEU MICHELIN 265/70 R 18 116T XLT A / S', '0905201710486071813512..jpg', '<p>\r\n	<strong>PNEU MICHELIN XLT A/S</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>50% mais dur&aacute;vel*&nbsp;</strong></p>\r\n<p>\r\n	&bull; Durabilidade Michelin X&reg; LT A/S: +50% vs. m&eacute;dia da concorr&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes realizados em 2016 certificados pelo Instituto Dekra, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	Muito mas resistente &agrave; agress&otilde;es .**&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>DUPLA CARCA&Ccedil;A ROBUSTA:&nbsp;</strong></p>\r\n<p>\r\n	&bull; 2 napas carca&ccedil;as&nbsp;</p>\r\n<p>\r\n	&bull; Cabos mais grossos e densos&nbsp;</p>\r\n<p>\r\n	Com isso, o pneu MICHELIN X&reg; LT A/S atende &agrave;s necessidades de alto torque das caminhonetas modernas e permite encarar os trabalhos pesados.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	**Em rela&ccedil;&atilde;o &agrave; gama anterior Michelin LTX M/S 2</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>25% mais seguro nas aquaplanagens.***&nbsp;</strong></p>\r\n<p>\r\n	Maior seguran&ccedil;a: +25% mais controle em situa&ccedil;&atilde;o de aquaplanagem em curva</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	***Em rela&ccedil;&atilde;o &agrave; m&eacute;dia dos principais concorrentes.. Testes de aquaplanagem em curva realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 265/70 R16.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26570-r-18-116t-xlt-a--s', NULL, NULL, NULL, 'https://www.youtube.com/embed/HEjGNvZO6jA', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 70, 116, 190, ''),
(250, 'PNEU MICHELIN 235/75 R 15 108T LTX  M/S 2', '0905201711027331436922..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX M/S 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>EXCEPCIONAL COMBINA&Ccedil;&Atilde;O DE DURABILIDADE E SEGURAN&Ccedil;A NO ASFALTO</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Seguran&ccedil;a garantida</p>\r\n<p>\r\n	Excelente frenagem em pisos molhados.</p>\r\n<p>\r\n	Maior durabilidade</p>\r\n<p>\r\n	Refer&ecirc;ncia em durabilidade no asfalto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	ESPECIAL DE BORRACHA e um melhor sistema de escoamento da &aacute;gua que evita aquaplanagem e permite uma redu&ccedil;&atilde;o de dist&acirc;ncia de frenagem.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Max Touch Construction&trade;&nbsp;</strong></p>\r\n<p>\r\n	Tecnologia aplicada a estrutura do pneu que maximiza o contato com o solo e melhora o n&iacute;vel de tra&ccedil;&atilde;o, distribuindo uniformemente as for&ccedil;as necess&aacute;rias para acelerar e frear. Essa arquitetura garante um desgaste uniforme da banda de rodagem, assegurando vida longa ao pneu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade: </strong>em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23575-r-15-108t-ltx--ms-2', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 235, NULL, '5', 11, 0, 0, 75, 108, 190, ''),
(251, 'PNEU MICHELIN 225/70 R 16 101T LTX  M/S 2', '0905201711047514440437..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX M/S 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>EXCEPCIONAL COMBINA&Ccedil;&Atilde;O DE DURABILIDADE E SEGURAN&Ccedil;A NO ASFALTO</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Seguran&ccedil;a garantida</p>\r\n<p>\r\n	Excelente frenagem em pisos molhados.</p>\r\n<p>\r\n	Maior durabilidade</p>\r\n<p>\r\n	Refer&ecirc;ncia em durabilidade no asfalto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	ESPECIAL DE BORRACHA e um melhor sistema de escoamento da &aacute;gua que evita aquaplanagem e permite uma redu&ccedil;&atilde;o de dist&acirc;ncia de frenagem.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Max Touch Construction&trade;&nbsp;</strong></p>\r\n<p>\r\n	Tecnologia aplicada a estrutura do pneu que maximiza o contato com o solo e melhora o n&iacute;vel de tra&ccedil;&atilde;o, distribuindo uniformemente as for&ccedil;as necess&aacute;rias para acelerar e frear. Essa arquitetura garante um desgaste uniforme da banda de rodagem, assegurando vida longa ao pneu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:&nbsp;</strong>em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22570-r-16-101t-ltx--ms-2', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 70, 101, 190, ''),
(252, 'PNEU MICHELIN 235/70 R 16 104T  LTX  M/S 2', '0905201711053372424600..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX M/S 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>EXCEPCIONAL COMBINA&Ccedil;&Atilde;O DE DURABILIDADE E SEGURAN&Ccedil;A NO ASFALTO</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Seguran&ccedil;a garantida</p>\r\n<p>\r\n	Excelente frenagem em pisos molhados.</p>\r\n<p>\r\n	Maior durabilidade</p>\r\n<p>\r\n	Refer&ecirc;ncia em durabilidade no asfalto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	ESPECIAL DE BORRACHA e um melhor sistema de escoamento da &aacute;gua que evita aquaplanagem e permite uma redu&ccedil;&atilde;o de dist&acirc;ncia de frenagem.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Max Touch Construction&trade;&nbsp;</strong></p>\r\n<p>\r\n	Tecnologia aplicada a estrutura do pneu que maximiza o contato com o solo e melhora o n&iacute;vel de tra&ccedil;&atilde;o, distribuindo uniformemente as for&ccedil;as necess&aacute;rias para acelerar e frear. Essa arquitetura garante um desgaste uniforme da banda de rodagem, assegurando vida longa ao pneu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:&nbsp;</strong>em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23570-r-16-104t--ltx--ms-2', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 70, 104, 190, ''),
(253, 'PNEU MICHELIN 245/75 R 16 120R  LTX  M/S 2', '0905201711052450021716..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX M/S 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>EXCEPCIONAL COMBINA&Ccedil;&Atilde;O DE DURABILIDADE E SEGURAN&Ccedil;A NO ASFALTO</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Seguran&ccedil;a garantida</p>\r\n<p>\r\n	Excelente frenagem em pisos molhados.</p>\r\n<p>\r\n	Maior durabilidade</p>\r\n<p>\r\n	Refer&ecirc;ncia em durabilidade no asfalto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	ESPECIAL DE BORRACHA e um melhor sistema de escoamento da &aacute;gua que evita aquaplanagem e permite uma redu&ccedil;&atilde;o de dist&acirc;ncia de frenagem.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Max Touch Construction&trade;&nbsp;</strong></p>\r\n<p>\r\n	Tecnologia aplicada a estrutura do pneu que maximiza o contato com o solo e melhora o n&iacute;vel de tra&ccedil;&atilde;o, distribuindo uniformemente as for&ccedil;as necess&aacute;rias para acelerar e frear. Essa arquitetura garante um desgaste uniforme da banda de rodagem, assegurando vida longa ao pneu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:&nbsp;</strong>em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24575-r-16-120r--ltx--ms-2', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 75, 120, 170, ''),
(254, 'PNEU MICHELIN 255/70 R 16 109T  LTX  M/S 2', '0905201711062241260686..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX M/S 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>EXCEPCIONAL COMBINA&Ccedil;&Atilde;O DE DURABILIDADE E SEGURAN&Ccedil;A NO ASFALTO</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Seguran&ccedil;a garantida</p>\r\n<p>\r\n	Excelente frenagem em pisos molhados.</p>\r\n<p>\r\n	Maior durabilidade</p>\r\n<p>\r\n	Refer&ecirc;ncia em durabilidade no asfalto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	ESPECIAL DE BORRACHA e um melhor sistema de escoamento da &aacute;gua que evita aquaplanagem e permite uma redu&ccedil;&atilde;o de dist&acirc;ncia de frenagem.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Max Touch Construction&trade;&nbsp;</strong></p>\r\n<p>\r\n	Tecnologia aplicada a estrutura do pneu que maximiza o contato com o solo e melhora o n&iacute;vel de tra&ccedil;&atilde;o, distribuindo uniformemente as for&ccedil;as necess&aacute;rias para acelerar e frear. Essa arquitetura garante um desgaste uniforme da banda de rodagem, assegurando vida longa ao pneu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:&nbsp;</strong>em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25570-r-16-109t--ltx--ms-2', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 70, 109, 190, ''),
(255, 'PNEU MICHELIN 255/65 R 16 106T LTX  M/S 2', '0905201711078482072899..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX M/S 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>EXCEPCIONAL COMBINA&Ccedil;&Atilde;O DE DURABILIDADE E SEGURAN&Ccedil;A NO ASFALTO</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Seguran&ccedil;a garantida</p>\r\n<p>\r\n	Excelente frenagem em pisos molhados.</p>\r\n<p>\r\n	Maior durabilidade</p>\r\n<p>\r\n	Refer&ecirc;ncia em durabilidade no asfalto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	ESPECIAL DE BORRACHA e um melhor sistema de escoamento da &aacute;gua que evita aquaplanagem e permite uma redu&ccedil;&atilde;o de dist&acirc;ncia de frenagem.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Max Touch Construction&trade;&nbsp;</strong></p>\r\n<p>\r\n	Tecnologia aplicada a estrutura do pneu que maximiza o contato com o solo e melhora o n&iacute;vel de tra&ccedil;&atilde;o, distribuindo uniformemente as for&ccedil;as necess&aacute;rias para acelerar e frear. Essa arquitetura garante um desgaste uniforme da banda de rodagem, assegurando vida longa ao pneu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:&nbsp;</strong>em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25565-r-16-106t-ltx--ms-2', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 65, 106, 190, ''),
(256, 'PNEU MICHELIN 245/65 R 17 105T LTX  M/S 2', '0905201711084293153790..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX M/S 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>EXCEPCIONAL COMBINA&Ccedil;&Atilde;O DE DURABILIDADE E SEGURAN&Ccedil;A NO ASFALTO</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Seguran&ccedil;a garantida</p>\r\n<p>\r\n	Excelente frenagem em pisos molhados.</p>\r\n<p>\r\n	Maior durabilidade</p>\r\n<p>\r\n	Refer&ecirc;ncia em durabilidade no asfalto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	ESPECIAL DE BORRACHA e um melhor sistema de escoamento da &aacute;gua que evita aquaplanagem e permite uma redu&ccedil;&atilde;o de dist&acirc;ncia de frenagem.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Max Touch Construction&trade;&nbsp;</strong></p>\r\n<p>\r\n	Tecnologia aplicada a estrutura do pneu que maximiza o contato com o solo e melhora o n&iacute;vel de tra&ccedil;&atilde;o, distribuindo uniformemente as for&ccedil;as necess&aacute;rias para acelerar e frear. Essa arquitetura garante um desgaste uniforme da banda de rodagem, assegurando vida longa ao pneu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:&nbsp;</strong>em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-24565-r-17-105t-ltx--ms-2', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 245, NULL, '5 ANOS', 11, 0, 0, 65, 105, 190, ''),
(257, 'PNEU MICHELIN 275/65 R 18 123R LTX  M/S 2', '0905201711095955595581..jpg', '<p>\r\n	<strong>PNEU MICHELIN LTX M/S 2</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>EXCEPCIONAL COMBINA&Ccedil;&Atilde;O DE DURABILIDADE E SEGURAN&Ccedil;A NO ASFALTO</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Seguran&ccedil;a garantida</p>\r\n<p>\r\n	Excelente frenagem em pisos molhados.</p>\r\n<p>\r\n	Maior durabilidade</p>\r\n<p>\r\n	Refer&ecirc;ncia em durabilidade no asfalto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	ESPECIAL DE BORRACHA e um melhor sistema de escoamento da &aacute;gua que evita aquaplanagem e permite uma redu&ccedil;&atilde;o de dist&acirc;ncia de frenagem.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Max Touch Construction&trade;&nbsp;</strong></p>\r\n<p>\r\n	Tecnologia aplicada a estrutura do pneu que maximiza o contato com o solo e melhora o n&iacute;vel de tra&ccedil;&atilde;o, distribuindo uniformemente as for&ccedil;as necess&aacute;rias para acelerar e frear. Essa arquitetura garante um desgaste uniforme da banda de rodagem, assegurando vida longa ao pneu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:&nbsp;</strong>em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-27565-r-18-123r-ltx--ms-2', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 275, NULL, '5 ANOS', 11, 0, 0, 65, 123, 170, '');
INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `id_categoriaproduto`, `id_subcategoriaproduto`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `modelo`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `codigo_produto`, `exibir_home`, `modelo_produto`, `aro`, `medida`, `indice`, `carga`, `largura`, `perfil`, `garantia`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `altura`, `indice_carga`, `indice_velocidade`, `imagem_flayer`) VALUES
(258, 'PNEU MICHELIN 215/65 R 16 98H PRIMACY SUV', '0905201711359603432582..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY SUV</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>3 VEZES MAIS SEGURO, AGORA PARA SUV. SEGURAN&Ccedil;A EM QUALQUER CONDI&Ccedil;&Atilde;O CLIM&Aacute;TICA. RODAGEM SILENCIOSA PARA MAIOR CONFORTO DENTRO DO SEU SUV.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Freia at&eacute; 7 metros antes em piso molhado*&nbsp;</strong></p>\r\n<p>\r\n	-Freia at&eacute; 3 metros antes no piso seco**&nbsp;</p>\r\n<p>\r\n	-12% mais em ader&ecirc;ncia nas curas***&nbsp;</p>\r\n<p>\r\n	-Sem abrir m&atilde;o da economia de combust&iacute;vel, do desgaste e do conforto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Teste de distancia de frenagem em piso molhado realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	** Teste de distancia de frenagem em piso seco realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	***Teste de ader&ecirc;ncia em curva sobre piso molhado realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA STABILIGRIP&nbsp;</strong></p>\r\n<p>\r\n	&bull; Para melhor ader&ecirc;ncia em solos molhados, em curvas e retas</p>\r\n<p>\r\n	&bull; Em caso de frenagem, o pneu recebe uma forte press&atilde;o, o que causa deforma&ccedil;&atilde;o nos blocos de borracha. O uso de um refor&ccedil;o entre os blocos garante menor deforma&ccedil;&atilde;o e maior &aacute;rea de contato com o solo, maximizando a ader&ecirc;ncia do pneu. As novas lamelas auto blocantes, Stabiligrip, com desenho arredondado, garantem maior rigidez e melhor escoamento da &aacute;gua, permitindo assim uma melhor frenagem em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA FLEXMAX</strong></p>\r\n<p>\r\n	&bull; Para melhor ader&ecirc;ncia em solos secos.</p>\r\n<p>\r\n	&bull; O composto de borracha do Primacy SUV se adapta &agrave;s menores irregularidades do piso e permite uma excelente performance de frenagem.</p>\r\n<p>\r\n	&bull; Os chanfros dos blocos de borracha da banda de rodagem impedem a deforma&ccedil;&atilde;o dos blocos, permitindo assim maior &aacute;rea de contato com o solo e maior ader&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>ENHANCED SILENCE COMFORT&nbsp;</strong></p>\r\n<p>\r\n	&bull; Quanto maior o n&uacute;mero de blocos na dire&ccedil;&atilde;o da circunfer&ecirc;ncia do pneu, maior a frequ&ecirc;ncia do som, o que contribui para uma melhor percep&ccedil;&atilde;o de ru&iacute;do no ve&iacute;culo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong> em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-21565-r-16-98h-primacy-suv', NULL, NULL, NULL, 'https://www.youtube.com/embed/OFYGiu_Xtp4', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 65, 98, 210, ''),
(259, 'PNEU MICHELIN 235/60 R 16 100H PRIMACY SUV', '0905201711386241108837..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY SUV</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>3 VEZES MAIS SEGURO, AGORA PARA SUV. SEGURAN&Ccedil;A EM QUALQUER CONDI&Ccedil;&Atilde;O CLIM&Aacute;TICA. RODAGEM SILENCIOSA PARA MAIOR CONFORTO DENTRO DO SEU SUV.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Freia at&eacute; 7 metros antes em piso molhado*&nbsp;</strong></p>\r\n<p>\r\n	-Freia at&eacute; 3 metros antes no piso seco**&nbsp;</p>\r\n<p>\r\n	-12% mais em ader&ecirc;ncia nas curas***&nbsp;</p>\r\n<p>\r\n	-Sem abrir m&atilde;o da economia de combust&iacute;vel, do desgaste e do conforto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Teste de distancia de frenagem em piso molhado realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	** Teste de distancia de frenagem em piso seco realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	***Teste de ader&ecirc;ncia em curva sobre piso molhado realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA STABILIGRIP&nbsp;</strong></p>\r\n<p>\r\n	&bull; Para melhor ader&ecirc;ncia em solos molhados, em curvas e retas</p>\r\n<p>\r\n	&bull; Em caso de frenagem, o pneu recebe uma forte press&atilde;o, o que causa deforma&ccedil;&atilde;o nos blocos de borracha. O uso de um refor&ccedil;o entre os blocos garante menor deforma&ccedil;&atilde;o e maior &aacute;rea de contato com o solo, maximizando a ader&ecirc;ncia do pneu. As novas lamelas auto blocantes, Stabiligrip, com desenho arredondado, garantem maior rigidez e melhor escoamento da &aacute;gua, permitindo assim uma melhor frenagem em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA FLEXMAX</strong></p>\r\n<p>\r\n	&bull; Para melhor ader&ecirc;ncia em solos secos.</p>\r\n<p>\r\n	&bull; O composto de borracha do Primacy SUV se adapta &agrave;s menores irregularidades do piso e permite uma excelente performance de frenagem.</p>\r\n<p>\r\n	&bull; Os chanfros dos blocos de borracha da banda de rodagem impedem a deforma&ccedil;&atilde;o dos blocos, permitindo assim maior &aacute;rea de contato com o solo e maior ader&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>ENHANCED SILENCE COMFORT&nbsp;</strong></p>\r\n<p>\r\n	&bull; Quanto maior o n&uacute;mero de blocos na dire&ccedil;&atilde;o da circunfer&ecirc;ncia do pneu, maior a frequ&ecirc;ncia do som, o que contribui para uma melhor percep&ccedil;&atilde;o de ru&iacute;do no ve&iacute;culo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23560-r-16-100h-primacy-suv', NULL, NULL, NULL, 'https://www.youtube.com/embed/OFYGiu_Xtp4', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 60, 100, 210, ''),
(260, 'PNEU MICHELIN 225/65 R 17 102H PRIMACY SUV', '0905201711393529805590..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY SUV</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>3 VEZES MAIS SEGURO, AGORA PARA SUV. SEGURAN&Ccedil;A EM QUALQUER CONDI&Ccedil;&Atilde;O CLIM&Aacute;TICA. RODAGEM SILENCIOSA PARA MAIOR CONFORTO DENTRO DO SEU SUV.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Freia at&eacute; 7 metros antes em piso molhado*&nbsp;</strong></p>\r\n<p>\r\n	-Freia at&eacute; 3 metros antes no piso seco**&nbsp;</p>\r\n<p>\r\n	-12% mais em ader&ecirc;ncia nas curas***&nbsp;</p>\r\n<p>\r\n	-Sem abrir m&atilde;o da economia de combust&iacute;vel, do desgaste e do conforto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Teste de distancia de frenagem em piso molhado realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	** Teste de distancia de frenagem em piso seco realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	***Teste de ader&ecirc;ncia em curva sobre piso molhado realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA STABILIGRIP&nbsp;</strong></p>\r\n<p>\r\n	&bull; Para melhor ader&ecirc;ncia em solos molhados, em curvas e retas</p>\r\n<p>\r\n	&bull; Em caso de frenagem, o pneu recebe uma forte press&atilde;o, o que causa deforma&ccedil;&atilde;o nos blocos de borracha. O uso de um refor&ccedil;o entre os blocos garante menor deforma&ccedil;&atilde;o e maior &aacute;rea de contato com o solo, maximizando a ader&ecirc;ncia do pneu. As novas lamelas auto blocantes, Stabiligrip, com desenho arredondado, garantem maior rigidez e melhor escoamento da &aacute;gua, permitindo assim uma melhor frenagem em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA FLEXMAX</strong></p>\r\n<p>\r\n	&bull; Para melhor ader&ecirc;ncia em solos secos.</p>\r\n<p>\r\n	&bull; O composto de borracha do Primacy SUV se adapta &agrave;s menores irregularidades do piso e permite uma excelente performance de frenagem.</p>\r\n<p>\r\n	&bull; Os chanfros dos blocos de borracha da banda de rodagem impedem a deforma&ccedil;&atilde;o dos blocos, permitindo assim maior &aacute;rea de contato com o solo e maior ader&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>ENHANCED SILENCE COMFORT&nbsp;</strong></p>\r\n<p>\r\n	&bull; Quanto maior o n&uacute;mero de blocos na dire&ccedil;&atilde;o da circunfer&ecirc;ncia do pneu, maior a frequ&ecirc;ncia do som, o que contribui para uma melhor percep&ccedil;&atilde;o de ru&iacute;do no ve&iacute;culo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-22565-r-17-102h-primacy-suv', NULL, NULL, NULL, 'https://www.youtube.com/embed/OFYGiu_Xtp4', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 65, 102, 170, ''),
(261, 'PNEU MICHELIN 235/65 R 17 108V PRIMACY SUV', '0905201711403137125881..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY SUV</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>3 VEZES MAIS SEGURO, AGORA PARA SUV. SEGURAN&Ccedil;A EM QUALQUER CONDI&Ccedil;&Atilde;O CLIM&Aacute;TICA. RODAGEM SILENCIOSA PARA MAIOR CONFORTO DENTRO DO SEU SUV.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Freia at&eacute; 7 metros antes em piso molhado*&nbsp;</strong></p>\r\n<p>\r\n	-Freia at&eacute; 3 metros antes no piso seco**&nbsp;</p>\r\n<p>\r\n	-12% mais em ader&ecirc;ncia nas curas***&nbsp;</p>\r\n<p>\r\n	-Sem abrir m&atilde;o da economia de combust&iacute;vel, do desgaste e do conforto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Teste de distancia de frenagem em piso molhado realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	** Teste de distancia de frenagem em piso seco realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	***Teste de ader&ecirc;ncia em curva sobre piso molhado realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA STABILIGRIP&nbsp;</strong></p>\r\n<p>\r\n	&bull; Para melhor ader&ecirc;ncia em solos molhados, em curvas e retas</p>\r\n<p>\r\n	&bull; Em caso de frenagem, o pneu recebe uma forte press&atilde;o, o que causa deforma&ccedil;&atilde;o nos blocos de borracha. O uso de um refor&ccedil;o entre os blocos garante menor deforma&ccedil;&atilde;o e maior &aacute;rea de contato com o solo, maximizando a ader&ecirc;ncia do pneu. As novas lamelas auto blocantes, Stabiligrip, com desenho arredondado, garantem maior rigidez e melhor escoamento da &aacute;gua, permitindo assim uma melhor frenagem em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA FLEXMAX</strong></p>\r\n<p>\r\n	&bull; Para melhor ader&ecirc;ncia em solos secos.</p>\r\n<p>\r\n	&bull; O composto de borracha do Primacy SUV se adapta &agrave;s menores irregularidades do piso e permite uma excelente performance de frenagem.</p>\r\n<p>\r\n	&bull; Os chanfros dos blocos de borracha da banda de rodagem impedem a deforma&ccedil;&atilde;o dos blocos, permitindo assim maior &aacute;rea de contato com o solo e maior ader&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>ENHANCED SILENCE COMFORT&nbsp;</strong></p>\r\n<p>\r\n	&bull; Quanto maior o n&uacute;mero de blocos na dire&ccedil;&atilde;o da circunfer&ecirc;ncia do pneu, maior a frequ&ecirc;ncia do som, o que contribui para uma melhor percep&ccedil;&atilde;o de ru&iacute;do no ve&iacute;culo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23565-r-17-108v-primacy-suv', NULL, NULL, NULL, 'https://www.youtube.com/embed/OFYGiu_Xtp4', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 65, 108, 240, ''),
(262, 'PNEU MICHELIN 255/60 R 18 112H PRIMACY SUV', '0905201711426410776716..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY SUV</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>3 VEZES MAIS SEGURO, AGORA PARA SUV. SEGURAN&Ccedil;A EM QUALQUER CONDI&Ccedil;&Atilde;O CLIM&Aacute;TICA. RODAGEM SILENCIOSA PARA MAIOR CONFORTO DENTRO DO SEU SUV.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Freia at&eacute; 7 metros antes em piso molhado*&nbsp;</strong></p>\r\n<p>\r\n	-Freia at&eacute; 3 metros antes no piso seco**&nbsp;</p>\r\n<p>\r\n	-12% mais em ader&ecirc;ncia nas curas***&nbsp;</p>\r\n<p>\r\n	-Sem abrir m&atilde;o da economia de combust&iacute;vel, do desgaste e do conforto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Teste de distancia de frenagem em piso molhado realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	** Teste de distancia de frenagem em piso seco realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	***Teste de ader&ecirc;ncia em curva sobre piso molhado realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA STABILIGRIP&nbsp;</strong></p>\r\n<p>\r\n	&bull; Para melhor ader&ecirc;ncia em solos molhados, em curvas e retas</p>\r\n<p>\r\n	&bull; Em caso de frenagem, o pneu recebe uma forte press&atilde;o, o que causa deforma&ccedil;&atilde;o nos blocos de borracha. O uso de um refor&ccedil;o entre os blocos garante menor deforma&ccedil;&atilde;o e maior &aacute;rea de contato com o solo, maximizando a ader&ecirc;ncia do pneu. As novas lamelas auto blocantes, Stabiligrip, com desenho arredondado, garantem maior rigidez e melhor escoamento da &aacute;gua, permitindo assim uma melhor frenagem em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA FLEXMAX</strong></p>\r\n<p>\r\n	&bull; Para melhor ader&ecirc;ncia em solos secos.</p>\r\n<p>\r\n	&bull; O composto de borracha do Primacy SUV se adapta &agrave;s menores irregularidades do piso e permite uma excelente performance de frenagem.</p>\r\n<p>\r\n	&bull; Os chanfros dos blocos de borracha da banda de rodagem impedem a deforma&ccedil;&atilde;o dos blocos, permitindo assim maior &aacute;rea de contato com o solo e maior ader&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>ENHANCED SILENCE COMFORT&nbsp;</strong></p>\r\n<p>\r\n	&bull; Quanto maior o n&uacute;mero de blocos na dire&ccedil;&atilde;o da circunfer&ecirc;ncia do pneu, maior a frequ&ecirc;ncia do som, o que contribui para uma melhor percep&ccedil;&atilde;o de ru&iacute;do no ve&iacute;culo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25560-r-18-112h-primacy-suv', NULL, NULL, NULL, 'https://www.youtube.com/embed/OFYGiu_Xtp4', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 60, 112, 170, ''),
(263, 'PNEU MICHELIN 265/60 R 18 110H PRIMACY SUV', '0905201711425698840216..jpg', '<p>\r\n	<strong>PNEU MICHELIN PRIMACY SUV</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>3 VEZES MAIS SEGURO, AGORA PARA SUV. SEGURAN&Ccedil;A EM QUALQUER CONDI&Ccedil;&Atilde;O CLIM&Aacute;TICA. RODAGEM SILENCIOSA PARA MAIOR CONFORTO DENTRO DO SEU SUV.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Freia at&eacute; 7 metros antes em piso molhado*&nbsp;</strong></p>\r\n<p>\r\n	-Freia at&eacute; 3 metros antes no piso seco**&nbsp;</p>\r\n<p>\r\n	-12% mais em ader&ecirc;ncia nas curas***&nbsp;</p>\r\n<p>\r\n	-Sem abrir m&atilde;o da economia de combust&iacute;vel, do desgaste e do conforto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Teste de distancia de frenagem em piso molhado realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	** Teste de distancia de frenagem em piso seco realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	***Teste de ader&ecirc;ncia em curva sobre piso molhado realizado em 2016 pelo Instituto T&Uuml;V S&Uuml;D Alemanha, com pneus comprados no mercado brasileiro (Pirelli, Goodyear e Bridgestone) na dimens&atilde;o 215/65 R16.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA STABILIGRIP&nbsp;</strong></p>\r\n<p>\r\n	&bull; Para melhor ader&ecirc;ncia em solos molhados, em curvas e retas</p>\r\n<p>\r\n	&bull; Em caso de frenagem, o pneu recebe uma forte press&atilde;o, o que causa deforma&ccedil;&atilde;o nos blocos de borracha. O uso de um refor&ccedil;o entre os blocos garante menor deforma&ccedil;&atilde;o e maior &aacute;rea de contato com o solo, maximizando a ader&ecirc;ncia do pneu. As novas lamelas auto blocantes, Stabiligrip, com desenho arredondado, garantem maior rigidez e melhor escoamento da &aacute;gua, permitindo assim uma melhor frenagem em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TECNOLOGIA FLEXMAX</strong></p>\r\n<p>\r\n	&bull; Para melhor ader&ecirc;ncia em solos secos.</p>\r\n<p>\r\n	&bull; O composto de borracha do Primacy SUV se adapta &agrave;s menores irregularidades do piso e permite uma excelente performance de frenagem.</p>\r\n<p>\r\n	&bull; Os chanfros dos blocos de borracha da banda de rodagem impedem a deforma&ccedil;&atilde;o dos blocos, permitindo assim maior &aacute;rea de contato com o solo e maior ader&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>ENHANCED SILENCE COMFORT&nbsp;</strong></p>\r\n<p>\r\n	&bull; Quanto maior o n&uacute;mero de blocos na dire&ccedil;&atilde;o da circunfer&ecirc;ncia do pneu, maior a frequ&ecirc;ncia do som, o que contribui para uma melhor percep&ccedil;&atilde;o de ru&iacute;do no ve&iacute;culo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26560-r-18-110h-primacy-suv', NULL, NULL, NULL, 'https://www.youtube.com/embed/OFYGiu_Xtp4', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 60, 110, 210, ''),
(264, 'PNEU MICHELIN 235/65 R 17 104H LATITUDE TOUR HP', '0905201701243495413702..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE TOUR HP</strong></p>\r\n<p>\r\n	<strong>EXCEL&Ecirc;NCIA EM EQUIL&Iacute;BRIO DE PERFORMANCES: DURABILIDADE, SEGURAN&Ccedil;A E CONFORTO NA MEDIDA CERTA</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade&nbsp;</strong></p>\r\n<p>\r\n	-20% mais dur&aacute;vel em reala&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Seguran&ccedil;a garantida</strong></p>\r\n<p>\r\n	-5% mais est&aacute;vel em pisos molhados em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.&nbsp;</p>\r\n<p>\r\n	-Bem-Estar e Conforto para os passageiros&nbsp;</p>\r\n<p>\r\n	-5% mais silencioso e 5% de redu&ccedil;&atilde;o das vibra&ccedil;&otilde;es em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TERRAIN PROOF</strong></p>\r\n<p>\r\n	Composto de borracha derivado dos pneus de minera&ccedil;&atilde;o que garante uma excelente capacidade de tra&ccedil;&atilde;o e redu&ccedil;&atilde;o do desgaste (por atrito), permitindo assim maior durabilidade e maior resist&ecirc;ncia em usos mais agressivos do pneu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>STABILIGRIP</strong></p>\r\n<p>\r\n	Consiste na aplica&ccedil;&atilde;o de dentes de borracha na escultura interna do pneu, assegurando melhor ader&ecirc;ncia no piso molhado e, consequentemente, maior estabilidade. Al&eacute;m disso, por estarem cuidadosamente posicionados, esses dentes garantem tamb&eacute;m um contato com o solo mais silencioso e confort&aacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong> em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23565-r-17-104h-latitude-tour-hp', NULL, NULL, NULL, 'https://www.youtube.com/embed/djGvqMFWrhw', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 65, 104, 210, '0905201701256802078220..jpg'),
(265, 'PNEU MICHELIN 235/65 R 18 104H LATITUDE TOUR HP', '0905201701278056031134..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE TOUR HP</strong></p>\r\n<p>\r\n	<strong>EXCEL&Ecirc;NCIA EM EQUIL&Iacute;BRIO DE PERFORMANCES: DURABILIDADE, SEGURAN&Ccedil;A E CONFORTO NA MEDIDA CERTA</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade&nbsp;</strong></p>\r\n<p>\r\n	-20% mais dur&aacute;vel em reala&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Seguran&ccedil;a garantida</strong></p>\r\n<p>\r\n	-5% mais est&aacute;vel em pisos molhados em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.&nbsp;</p>\r\n<p>\r\n	-Bem-Estar e Conforto para os passageiros&nbsp;</p>\r\n<p>\r\n	-5% mais silencioso e 5% de redu&ccedil;&atilde;o das vibra&ccedil;&otilde;es em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TERRAIN PROOF</strong></p>\r\n<p>\r\n	Composto de borracha derivado dos pneus de minera&ccedil;&atilde;o que garante uma excelente capacidade de tra&ccedil;&atilde;o e redu&ccedil;&atilde;o do desgaste (por atrito), permitindo assim maior durabilidade e maior resist&ecirc;ncia em usos mais agressivos do pneu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>STABILIGRIP</strong></p>\r\n<p>\r\n	Consiste na aplica&ccedil;&atilde;o de dentes de borracha na escultura interna do pneu, assegurando melhor ader&ecirc;ncia no piso molhado e, consequentemente, maior estabilidade. Al&eacute;m disso, por estarem cuidadosamente posicionados, esses dentes garantem tamb&eacute;m um contato com o solo mais silencioso e confort&aacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong> em estoque</p>\r\n<div>\r\n	&nbsp;</div>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23565-r-18-104h-latitude-tour-hp', NULL, NULL, NULL, 'https://www.youtube.com/embed/djGvqMFWrhw', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 65, 104, 210, '0905201701272566018915..jpg'),
(266, 'PNEU MICHELIN 255/55 R 18 105V LATITUDE TOUR HP', '0905201701281376867881..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE TOUR HP</strong></p>\r\n<p>\r\n	<strong>EXCEL&Ecirc;NCIA EM EQUIL&Iacute;BRIO DE PERFORMANCES: DURABILIDADE, SEGURAN&Ccedil;A E CONFORTO NA MEDIDA CERTA</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade&nbsp;</strong></p>\r\n<p>\r\n	-20% mais dur&aacute;vel em reala&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Seguran&ccedil;a garantida</strong></p>\r\n<p>\r\n	-5% mais est&aacute;vel em pisos molhados em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.&nbsp;</p>\r\n<p>\r\n	-Bem-Estar e Conforto para os passageiros&nbsp;</p>\r\n<p>\r\n	-5% mais silencioso e 5% de redu&ccedil;&atilde;o das vibra&ccedil;&otilde;es em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TERRAIN PROOF</strong></p>\r\n<p>\r\n	Composto de borracha derivado dos pneus de minera&ccedil;&atilde;o que garante uma excelente capacidade de tra&ccedil;&atilde;o e redu&ccedil;&atilde;o do desgaste (por atrito), permitindo assim maior durabilidade e maior resist&ecirc;ncia em usos mais agressivos do pneu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>STABILIGRIP</strong></p>\r\n<p>\r\n	Consiste na aplica&ccedil;&atilde;o de dentes de borracha na escultura interna do pneu, assegurando melhor ader&ecirc;ncia no piso molhado e, consequentemente, maior estabilidade. Al&eacute;m disso, por estarem cuidadosamente posicionados, esses dentes garantem tamb&eacute;m um contato com o solo mais silencioso e confort&aacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25555-r-18-105v-latitude-tour-hp', NULL, NULL, NULL, 'https://www.youtube.com/embed/djGvqMFWrhw', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 55, 105, 240, '0905201701287364563333..jpg'),
(267, 'PNEU MICHELIN 265/50 R 19 110V LATITUDE TOUR HP', '0905201701296615674106..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE TOUR HP</strong></p>\r\n<p>\r\n	<strong>EXCEL&Ecirc;NCIA EM EQUIL&Iacute;BRIO DE PERFORMANCES: DURABILIDADE, SEGURAN&Ccedil;A E CONFORTO NA MEDIDA CERTA</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade&nbsp;</strong></p>\r\n<p>\r\n	-20% mais dur&aacute;vel em reala&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Seguran&ccedil;a garantida</strong></p>\r\n<p>\r\n	-5% mais est&aacute;vel em pisos molhados em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.&nbsp;</p>\r\n<p>\r\n	-Bem-Estar e Conforto para os passageiros&nbsp;</p>\r\n<p>\r\n	-5% mais silencioso e 5% de redu&ccedil;&atilde;o das vibra&ccedil;&otilde;es em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TERRAIN PROOF</strong></p>\r\n<p>\r\n	Composto de borracha derivado dos pneus de minera&ccedil;&atilde;o que garante uma excelente capacidade de tra&ccedil;&atilde;o e redu&ccedil;&atilde;o do desgaste (por atrito), permitindo assim maior durabilidade e maior resist&ecirc;ncia em usos mais agressivos do pneu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>STABILIGRIP</strong></p>\r\n<p>\r\n	Consiste na aplica&ccedil;&atilde;o de dentes de borracha na escultura interna do pneu, assegurando melhor ader&ecirc;ncia no piso molhado e, consequentemente, maior estabilidade. Al&eacute;m disso, por estarem cuidadosamente posicionados, esses dentes garantem tamb&eacute;m um contato com o solo mais silencioso e confort&aacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26550-r-19-110v-latitude-tour-hp', NULL, NULL, NULL, 'https://www.youtube.com/embed/djGvqMFWrhw', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 50, 110, 240, '0905201701296502214512..jpg'),
(268, 'PNEU MICHELIN 295/40 R 20 106V LATITUDE TOUR HP', '0905201701302980073044..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE TOUR HP</strong></p>\r\n<p>\r\n	<strong>EXCEL&Ecirc;NCIA EM EQUIL&Iacute;BRIO DE PERFORMANCES: DURABILIDADE, SEGURAN&Ccedil;A E CONFORTO NA MEDIDA CERTA</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Maior durabilidade&nbsp;</strong></p>\r\n<p>\r\n	-20% mais dur&aacute;vel em reala&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Seguran&ccedil;a garantida</strong></p>\r\n<p>\r\n	-5% mais est&aacute;vel em pisos molhados em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.&nbsp;</p>\r\n<p>\r\n	-Bem-Estar e Conforto para os passageiros&nbsp;</p>\r\n<p>\r\n	-5% mais silencioso e 5% de redu&ccedil;&atilde;o das vibra&ccedil;&otilde;es em rela&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>TERRAIN PROOF</strong></p>\r\n<p>\r\n	Composto de borracha derivado dos pneus de minera&ccedil;&atilde;o que garante uma excelente capacidade de tra&ccedil;&atilde;o e redu&ccedil;&atilde;o do desgaste (por atrito), permitindo assim maior durabilidade e maior resist&ecirc;ncia em usos mais agressivos do pneu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>STABILIGRIP</strong></p>\r\n<p>\r\n	Consiste na aplica&ccedil;&atilde;o de dentes de borracha na escultura interna do pneu, assegurando melhor ader&ecirc;ncia no piso molhado e, consequentemente, maior estabilidade. Al&eacute;m disso, por estarem cuidadosamente posicionados, esses dentes garantem tamb&eacute;m um contato com o solo mais silencioso e confort&aacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-29540-r-20-106v-latitude-tour-hp', NULL, NULL, NULL, 'https://www.youtube.com/embed/djGvqMFWrhw', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 295, NULL, '5 ANOS', 11, 0, 0, 40, 106, 240, '0905201701305444224377..jpg'),
(269, 'PNEU MICHELIN 235/65 R 17 104V LATITUDE SPORT 3', '0905201702008116699726..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong> em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23565-r-17-104v-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 65, 104, 240, '0905201702032563291518..jpg'),
(270, 'PNEU MICHELIN 235/60 R 17 102V LATITUDE SPORT 3', '0905201703374807195514..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23560-r-17-102v-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 60, 102, 240, '0905201703373408333195..jpg'),
(271, 'PNEU MICHELIN 255/60 R 17 106V LATITUDE SPORT 3', '0905201703397699209406..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25560-r-17-106v-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 60, 106, 240, '0905201703393166949848..jpg'),
(272, 'PNEU MICHELIN 275/55 R 17 109V LATITUDE SPORT 3', '0905201703404319843467..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-27555-r-17-109v-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 275, NULL, '5 ANOS', 11, 0, 0, 55, 109, 240, '0905201703402665191532..jpg'),
(273, 'PNEU MICHELIN 235/60 R 18 103V LATITUDE SPORT 3', '0905201703417818131686..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23560-r-18-103v-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 60, 103, 240, '0905201703413566270073..jpg'),
(274, 'PNEU MICHELIN 235/60 R 18 103W LATITUDE SPORT 3', '0905201703428229999140..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23560-r-18-103w-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 60, 103, 270, '0905201703423338353153..jpg'),
(275, 'PNEU MICHELIN 235/55 R 18 100V  LATITUDE SPORT 3', '0905201703431866997198..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23555-r-18-100v--latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 55, 100, 240, '0905201703433636736021..jpg'),
(276, 'PNEU MICHELIN 255/60 R 18 112V LATITUDE SPORT 3', '0905201703441936644534..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25560-r-18-112v-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 60, 112, 240, '0905201703449070452633..jpg'),
(277, 'PNEU MICHELIN 255/55 R 18 105W LATITUDE SPORT 3', '0905201703459954107258..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25555-r-18-105w-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 55, 105, 270, '0905201703459918394937..jpg'),
(278, 'PNEU MICHELIN 255/55 R 18 109V LATITUDE SPORT 3 ZP', '0905201703468597364794..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25555-r-18-109v-latitude-sport-3-zp', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 55, 109, 240, '0905201703469491427031..jpg'),
(279, 'PNEU MICHELIN 235/55 R 19 101Y LATITUDE SPORT 3', '0905201703477920331458..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23555-r-19-101y-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 55, 101, 300, '0905201703474063280986..jpg'),
(280, 'PNEU MICHELIN 235/55 R 19 105V LATITUDE SPORT 3', '0905201703481271261270..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-23555-r-19-105v-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 235, NULL, '5 ANOS', 11, 0, 0, 55, 105, 240, '0905201703489884033563..jpg'),
(281, 'PNEU MICHELIN 255/50 R 19 103Y LATITUDE SPORT 3', '0905201703498632074313..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25550-r-19-103y-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 50, 103, 300, '0905201703491228008294..jpg'),
(282, 'PNEU MICHELIN 265/50 R 19 110Y LATITUDE SPORT 3', '0905201703492234233841..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26550-r-19-110y-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 50, 110, 300, '0905201703498781051437..jpg');
INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `id_categoriaproduto`, `id_subcategoriaproduto`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `modelo`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `codigo_produto`, `exibir_home`, `modelo_produto`, `aro`, `medida`, `indice`, `carga`, `largura`, `perfil`, `garantia`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `altura`, `indice_carga`, `indice_velocidade`, `imagem_flayer`) VALUES
(283, 'PNEU MICHELIN 285/45 R 19 111W LATITUDE SPORT 3', '0905201703506357556316..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-28545-r-19-111w-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 19, NULL, NULL, NULL, 285, NULL, '5 ANOS', 11, 0, 0, 45, 111, 270, '0905201703503130257646..jpg'),
(284, 'PNEU MICHELIN 255/50 R 20 109Y LATITUDE SPORT 3', '0905201703512702801620..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25550-r-20-109y-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 50, 109, 300, '0905201703517322382674..jpg'),
(285, 'PNEU MICHELIN 255/45 R 20 101W LATITUDE SPORT 3', '0905201703523764008540..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-25545-r-20-101w-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 255, NULL, '5 ANOS', 11, 0, 0, 45, 101, 270, '0905201703525550573244..jpg'),
(286, 'PNEU MICHELIN 265/50 R 20 107V LATITUDE SPORT 3', '0905201703538445793874..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26550-r-20-107v-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 50, 107, 240, '0905201703534595011857..jpg'),
(287, 'PNEU MICHELIN 265/45 R 20 104Y LATITUDE SPORT 3', '0905201703545401476766..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26545-r-20-104y-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 45, 104, 300, '0905201703547696365701..jpg'),
(288, 'PNEU MICHELIN 275/45 R 20 110Y LATITUDE SPORT 3', '0905201703552002060384..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-27545-r-20-110y-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 275, NULL, '5 ANOS', 11, 0, 0, 45, 110, 300, '0905201703559917660574..jpg'),
(289, 'PNEU MICHELIN 275/40 R 20 106Y LATITUDE SPORT 3', '0905201703551189381579..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-27540-r-20-106y-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 275, NULL, '5 ANOS', 11, 0, 0, 40, 106, 300, '0905201703557193809747..jpg'),
(290, 'PNEU MICHELIN 295/40 R 20 106Y LATITUDE SPORT 3', '0905201703563519902026..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-29540-r-20-106y-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 295, NULL, '5 ANOS', 11, 0, 0, 40, 106, 300, '0905201703566454397146..jpg'),
(291, 'PNEU MICHELIN 315/35 R 20 110W LATITUDE SPORT 3', '0905201703579590774455..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-31535-r-20-110w-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 20, NULL, NULL, NULL, 315, NULL, '5 ANOS', 11, 0, 0, 35, 110, 270, '0905201703575695425416..jpg'),
(292, 'PNEU MICHELIN 265/40 R 21 101Y LATITUDE SPORT 3', '0905201703587867708210..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-26540-r-21-101y-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 21, NULL, NULL, NULL, 265, NULL, '5 ANOS', 11, 0, 0, 40, 101, 300, '0905201703582752599774..jpg'),
(293, 'PNEU MICHELIN 295/35 R 21 103Y LATITUDE SPORT 3', '0905201703595823491683..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-29535-r-21-103y-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 21, NULL, NULL, NULL, 295, NULL, '5 ANOS', 11, 0, 0, 35, 103, 300, '0905201703598895522678..jpg'),
(294, 'PNEU MICHELIN 295/35 R 21 107Y LATITUDE SPORT 3', '0905201703599232643954..jpg', '<p>\r\n	<strong>PNEU MICHELIN LATITUDE SPORT 3</strong></p>\r\n<p>\r\n	<strong>O PNEU PARA SUVS DE ALTA PERFORMANCE, RECONHECIDO POR SUA EXCEL&Ecirc;NCIA EM DIRIGIBILIDADE E SEGURAN&Ccedil;A.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Menor dist&acirc;ncia de frenagem em compara&ccedil;&atilde;o &agrave; gera&ccedil;&atilde;o anterior.</strong></p>\r\n<p>\r\n	Nova escultura da banda de rodagem com um maior n&uacute;mero de sulcos transversais e longitudinais que funcionam como canaletas para drenagem da &aacute;gua, garantindo assim uma excepcional performance em solos molhados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>&Uacute;ltima gera&ccedil;&atilde;o de pneus homologados para os SUVs mais prestigiados.</strong></p>\r\n<p>\r\n	Performances reconhecidas pelas montadoras de maior prest&iacute;gio como Volvo, Porsche e BMW que equipam os ve&iacute;culos mais importantes de seu portf&oacute;lio com o novo MICHELIN Latitude Sport 3.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-michelin-29535-r-21-107y-latitude-sport-3', NULL, NULL, NULL, 'https://www.youtube.com/embed/1I66O2LRAic', NULL, NULL, 'SIM', NULL, 21, NULL, NULL, NULL, 295, NULL, '5 ANOS', 11, 0, 0, 35, 107, 300, '0905201703593726020740..jpg'),
(295, 'PNEU BFGOODRICH 175/70 R 14 84T G-GRIP', '0905201707189216518596..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong> em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-17570-r-14-84t-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 175, NULL, '5 ANOS', 11, 0, 0, 70, 84, 190, '0905201707309709769955..jpg'),
(296, 'PNEU BFGOODRICH 175/65 R 14 82T G-GRIP', '0905201707322019120082..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-17565-r-14-82t-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 175, NULL, '5 ANOS', 11, 0, 0, 65, 82, 190, '0905201707329665435920..jpg'),
(297, 'PNEU BFGOODRICH 185/70 R 14 88T G-GRIP', '0905201707336544119007..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-18570-r-14-88t-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 185, NULL, '5 ANOS', 11, 0, 0, 70, 88, 190, '0905201707335981299181..jpg'),
(298, 'PNEU BFGOODRICH 185/65 R 14 86H G-GRIP', '0905201707343121679325..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-18565-r-14-86h-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 185, NULL, '5 ANOS', 11, 0, 0, 65, 86, 210, '0905201707348797201907..jpg'),
(299, 'PNEU BFGOODRICH 185/55 R 14 80H G-GRIP', '0905201707357597635007..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-18555-r-14-80h-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 14, NULL, NULL, NULL, 185, NULL, '5 ANOS', 11, 0, 0, 55, 80, 210, '0905201707353462704643..jpg'),
(300, 'PNEU BFGOODRICH 195/55 R 15 85V G-GRIP', '0905201707358930365302..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-19555-r-15-85v-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 55, 85, 240, '0905201707354869032253..jpg'),
(301, 'PNEU BFGOODRICH 195/50 R 15 82V G-GRIP', '0905201707363134422430..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-19550-r-15-82v-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 50, 82, 240, '0905201707364851028410..jpg'),
(302, 'PNEU BFGOODRICH 195/45 R 15 78V G-GRIP', '0905201707375913391266..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-19545-r-15-78v-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 15, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 45, 78, 240, '0905201707375050251177..jpg'),
(303, 'PNEU BFGOODRICH 195/50 R 16 88V G-GRIP', '0905201707382348007079..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-19550-r-16-88v-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 50, 88, 240, '0905201707388419398793..jpg'),
(304, 'PNEU BFGOODRICH 195/45 R 16 84V G-GRIP', '0905201707395722323349..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-19545-r-16-84v-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 195, NULL, '5 ANOS', 11, 0, 0, 45, 84, 240, '0905201707394145858474..jpg'),
(305, 'PNEU BFGOODRICH 205/55 R 16 91W G-GRIP', '0905201707404752729294..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-20555-r-16-91w-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 55, 91, 270, '0905201707403948494514..jpg'),
(306, 'PNEU BFGOODRICH 205/50 R 16 87W G-GRIP', '0905201707411659528261..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-20550-r-16-87w-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 16, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 50, 87, 270, '0905201707415897427656..jpg'),
(307, 'PNEU BFGOODRICH 205/45 R 17 88W G-GRIP', '0905201707412781300424..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-20545-r-17-88w-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 45, 88, 270, '0905201707417161892693..jpg'),
(308, 'PNEU BFGOODRICH 205/40 R 17 84W G-GRIP', '0905201707423600032515..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-20540-r-17-84w-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 205, NULL, '5 ANOS', 11, 0, 0, 40, 84, 270, '0905201707429325817090..jpg'),
(309, 'PNEU BFGOODRICH 215/45 R 17 91 W G-GRIP', '0905201707436684408312..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-21545-r-17-91-w-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 215, NULL, '5 ANOS', 11, 0, 0, 45, 91, 270, '0905201707431611733900..jpg'),
(310, 'PNEU BFGOODRICH 225/45 R 17 91Y G-GRIP', '0905201707445298342282..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-22545-r-17-91y-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 17, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 45, 91, 300, '0905201707445764984492..jpg'),
(311, 'PNEU BFGOODRICH 225/40 R 18 92Y G-GRIP', '0905201707459539606169..jpg', '<p>\r\n	<strong>BFGoodrich g-Grip</strong></p>\r\n<p>\r\n	<strong>O controle nas tuas m&atilde;os</strong></p>\r\n<p>\r\n	<strong>Para uma vasta gama de ve&iacute;culos, desde o citadino ao de grande cilindrada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Um prazer incompar&aacute;vel em solo seco</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Quer em retas, quer em curvas, os seus grandes ombros e o seu &ldquo;rib&rdquo; central cont&iacute;nuo oferecem uma resposta imediata a todas as situa&ccedil;&otilde;es e um perfeito controlo das trajet&oacute;rias.</p>\r\n<p>\r\n	As lamelas &ldquo;chanfro&rdquo; melhoram a estabilidade dos tacos de borracha r&iacute;gidos para otimizar a travagem em solo seco.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Uma seguran&ccedil;a &oacute;tima em estrada molhada</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma forma direcional e um rasto com tamanho &uacute;nico que permite um escoamento &oacute;timo da &aacute;gua em linha reta.</p>\r\n<p>\r\n	Um rasto &ldquo;Vortex&rdquo; que permite que a &aacute;gua escoe mais facilmente atrav&eacute;s de uma travagem e estabilidade em curvas &oacute;timos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Disponibilidade:</strong>&nbsp;em estoque</p>', 0, 0, '', '', '', 'SIM', 0, 'pneu-bfgoodrich-22540-r-18-92y-ggrip', NULL, NULL, NULL, '', NULL, NULL, 'SIM', NULL, 18, NULL, NULL, NULL, 225, NULL, '5 ANOS', 11, 0, 0, 40, 92, 300, '0905201707455358057800..jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos_aplicacoes`
--

CREATE TABLE `tb_produtos_aplicacoes` (
  `idprodutoaplicacao` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `titulo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos_categorias`
--

CREATE TABLE `tb_produtos_categorias` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `id_categoriaproduto` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(11) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_produtos_categorias`
--

INSERT INTO `tb_produtos_categorias` (`id`, `id_produto`, `id_categoriaproduto`, `ativo`, `ordem`, `url_amigavel`) VALUES
(14, 1, 75, 'SIM', 0, ''),
(15, 1, 76, 'SIM', 0, ''),
(16, 1, 77, 'SIM', 0, ''),
(17, 82, 81, 'SIM', 0, ''),
(18, 82, 78, 'SIM', 0, ''),
(19, 83, 75, 'SIM', 0, ''),
(20, 83, 80, 'SIM', 0, ''),
(21, 84, 74, 'SIM', 0, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos_modelos_aceitos`
--

CREATE TABLE `tb_produtos_modelos_aceitos` (
  `idprodutosmodeloaceito` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `id_tipoveiculo` int(11) NOT NULL,
  `id_marcaveiculo` int(11) NOT NULL,
  `id_modeloveiculo` int(11) NOT NULL,
  `ano_inicial` int(11) NOT NULL,
  `ano_final` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(11) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_produtos_modelos_aceitos`
--

INSERT INTO `tb_produtos_modelos_aceitos` (`idprodutosmodeloaceito`, `id_produto`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `ano_inicial`, `ano_final`, `ativo`, `ordem`, `url_amigavel`) VALUES
(106, 45, 11, 123, 3, 0, 0, 'SIM', 0, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_promocoes`
--

CREATE TABLE `tb_promocoes` (
  `idpromocao` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `imagem_principal` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_promocoes`
--

INSERT INTO `tb_promocoes` (`idpromocao`, `titulo`, `descricao`, `imagem`, `imagem_principal`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`, `url`) VALUES
(1, 'promoção em destaque', NULL, '2004201707391397310949..jpg', '2004201707391216111874.jpg', 'SIM', NULL, 'promocao-em-destaque', '', '', '', NULL, '/produto/hjhjhjhjh'),
(2, 'promoção 01', '<div>\r\n	teste 01 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</div>\r\n<div>\r\n	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</div>\r\n<div>\r\n	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</div>\r\n<div>\r\n	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</div>\r\n<div>\r\n	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</div>\r\n<div>\r\n	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>', '2004201707401356301440..jpg', NULL, 'SIM', NULL, 'promocao-01', '', '', '', NULL, ''),
(46, 'promoção 02', NULL, '2104201701261234340349..jpg', NULL, 'SIM', NULL, 'promocao-02', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_seo`
--

CREATE TABLE `tb_seo` (
  `idseo` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_seo`
--

INSERT INTO `tb_seo` (`idseo`, `titulo`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Index', 'Index', NULL, NULL, 'SIM', NULL, NULL),
(2, 'Empresa', 'Empresa', NULL, NULL, 'SIM', NULL, NULL),
(3, 'Promoções', 'Promoções', NULL, NULL, 'SIM', NULL, NULL),
(5, 'Orçamento', 'Orçamento', NULL, NULL, 'SIM', NULL, NULL),
(6, 'Lojas', 'Lojas', NULL, NULL, 'SIM', NULL, NULL),
(7, 'Serviços', 'Serviços', NULL, NULL, 'SIM', NULL, NULL),
(8, 'Produtos', 'Produtos', NULL, NULL, 'SIM', NULL, NULL),
(9, 'Trabalhe Conosco', 'Trabalhe Conosco', NULL, NULL, 'SIM', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_servicos`
--

CREATE TABLE `tb_servicos` (
  `idservico` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `imagem_icone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem_principal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) UNSIGNED NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `imagem_icone`, `imagem_principal`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`) VALUES
(5, 'SUSPENSÃO', '<p>\r\n	Fundada em 1990 com a filosofia de sempre contribuir para evolu&ccedil;&atilde;o tecnol&oacute;gica,nos especializamos a cada dia para propor<br />\r\n	cionar trabalhos de alta qualidade para nossa clientela.&nbsp;</p>', '2702201701321337852680..jpg', '2304201712261250287984.png', '2508201602594049189843.jpg', 'SIM', NULL, 'suspensÃo', '', '', '', 0, '', ''),
(57, 'BALANCEAMENTO', '<p>\r\n	Redu&ccedil;&atilde;o de custos com manuten&ccedil;&atilde;o: ACM protege a parede externa da polui&ccedil;&atilde;o e do clima.</p>\r\n<p>\r\n	Mant&eacute;m a modernidade da fachada por muito tempo.</p>', '2702201701333778186899..jpg', '2304201712271152020262.png', '', 'SIM', NULL, 'balanceamento', '', '', '', 0, '', ''),
(58, 'REPARO ADEQUADO', '<div>\r\n	REPARO Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>\r\n<div>\r\n	&nbsp;</div>', 'imagem_nao_disponivel.jpg', '2304201712321329314538.png', '', 'SIM', NULL, 'reparo-adequado', NULL, NULL, NULL, 0, '', ''),
(59, 'CALIBRAGEM', '<div>\r\n	CALIBRAGEM Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>\r\n<div>\r\n	&nbsp;</div>', 'imagem_nao_disponivel.jpg', '2304201712321217690215.png', '', 'SIM', NULL, 'calibragem', NULL, NULL, NULL, 0, '', ''),
(60, 'RODÍZIO DE PNEUS', '<div>\r\n	RODIZIO Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>\r\n<div>\r\n	&nbsp;</div>', 'imagem_nao_disponivel.jpg', '2304201712331396517216.png', '', 'SIM', NULL, 'rodÍzio-de-pneus', NULL, NULL, NULL, 0, '', ''),
(61, 'BALANCEAMENTO', '<div>\r\n	BALANCEAMENTO Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>\r\n<div>\r\n	&nbsp;</div>', 'imagem_nao_disponivel.jpg', '2304201712341265113446.png', '', 'SIM', NULL, 'balanceamento', NULL, NULL, NULL, 0, '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_subcategorias_produtos`
--

CREATE TABLE `tb_subcategorias_produtos` (
  `idsubcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `id_categoriaproduto` int(11) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_subcategorias_produtos`
--

INSERT INTO `tb_subcategorias_produtos` (`idsubcategoriaproduto`, `titulo`, `id_categoriaproduto`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_subcategorias_unidades`
--

CREATE TABLE `tb_subcategorias_unidades` (
  `idsubcategoriaunidade` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `id_categoriaunidade` int(11) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_sugestoes_mensagens`
--

CREATE TABLE `tb_sugestoes_mensagens` (
  `idsugestaomensagem` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL,
  `id_categoriamensagem` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_sugestoes_mensagens`
--

INSERT INTO `tb_sugestoes_mensagens` (`idsugestaomensagem`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`, `id_categoriamensagem`) VALUES
(1, 'Podem existir mil obstáculos', '<p>\r\n	Podem existir mil obst&aacute;culos, mas nada far&aacute; com que meu amor por ti morra. Atravessarei at&eacute; os maiores mares, mas n&atilde;o existir&aacute; &aacute;gua suficiente que afogue o amor que sinto por voc&ecirc;.</p>', 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'podem-existir-mil-obstaculos', 'Podem existir mil obstáculos', '', '', NULL, 4),
(2, 'Subirei até a montanha', 'Subirei até a montanha mais alta do mundo, só para te ver, e de lá gritarei seu nome para ver se me ouve, e se me ouvires, direi uma só frase: Eu te amo.', 'imagem_nao_disponivel.jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, NULL, 4),
(3, 'E quando o vento passar', 'E quando o vento passar, levará consigo o que eu disse, e quando ele soprar em seu ouvido, escutarás junto ao vento: Eu te amo.', 'imagem_nao_disponivel.jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, NULL, 4),
(4, 'E toda vez que o vento soprar', 'E toda vez que o vento soprar em seu ouvido, não será só apenas o vento, mas eu dizendo que te amo.', 'imagem_nao_disponivel.jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, NULL, 4),
(5, 'Desejo a você', '<p>\r\n	Um anivers&aacute;rio cheio de paz...<br />\r\n	Que os sentimentos mais puros<br />\r\n	se concretizem em gestos de bondade,<br />\r\n	e o amor encontre abertas as portas<br />\r\n	do seu cora&ccedil;&atilde;o.<br />\r\n	Que voc&ecirc; possa guardar deste anivers&aacute;rio<br />\r\n	as melhores lembran&ccedil;as.<br />\r\n	E que tudo contribua para sua felicidade.</p>\r\n<p>\r\n	Abra&ccedil;os e Feliz Anivers&aacute;rio!</p>', 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'desejo-a-voce', '', '', '', NULL, 1),
(6, 'Seja Muito Feliz, Hoje e Sempre', 'Feliz aniversário! Que hoje todos os sorrisos se abram para você, que seu coração seja inundado pela alegria e gratidão pela vida e que as homenagens abundem, pois você merece viver um dia muito especial.', 'imagem_nao_disponivel.jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, NULL, 1),
(7, 'Duradouro e feliz', '<p>\r\n	A vida &eacute; feita de momentos bons e ruins. Aproveitem intensamente os bons e estejam unidos para superar os ruins. Assim se constr&oacute;i um casamento duradouro e feliz!</p>', 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'duradouro-e-feliz', '', '', '', NULL, 2),
(8, 'O amor de um homem', '<p>\r\n	O amor de um homem por uma mulher &eacute; muito mais profundo e verdadeiro quando Deus acontece primeiro na vida do casal.</p>', 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'o-amor-de-um-homem', 'O amor de um homem', 'O amor de um homem', 'O amor de um homem', NULL, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_tipos_servicos`
--

CREATE TABLE `tb_tipos_servicos` (
  `idtiposervico` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_tipos_servicos`
--

INSERT INTO `tb_tipos_servicos` (`idtiposervico`, `titulo`, `descricao`, `imagem`, `ordem`, `ativo`, `url_amigavel`) VALUES
(1, 'BALANCEAMENTO', '<p>\r\n	<strong>De Olho no Balanceamento</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Conhe&ccedil;a os sinais que indicam pneus desbalanceados e quando fazer a manuten&ccedil;&atilde;o</strong></p>\r\n<p>\r\n	Balanceamento &eacute; o processo de compensa&ccedil;&atilde;o feito para equilibrar o conjunto de pneu e rodas do ve&iacute;culo. Ele &eacute; importante para evitar o desgaste prematuro dos pneus e dos componentes da suspens&atilde;o e da dire&ccedil;&atilde;o.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Para que serve o balanceamento dos pneus?</strong></p>\r\n<p>\r\n	O balanceamento &eacute; necess&aacute;rio para suprimir trepida&ccedil;&otilde;es que possam ocorrer no volante do carro. Al&eacute;m disto, quando h&aacute; desbalanceamento, a dire&ccedil;&atilde;o torna-se inst&aacute;vel e ocorre um desgaste irregular dos pneus.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Sinais que indicam pneus desbalanceados</strong></p>\r\n<p>\r\n	A maneira mais comum de perceber um ve&iacute;culo com pneus desbalanceados &eacute; quando o motorista sente trepida&ccedil;&atilde;o no volante - ou &agrave;s vezes vibra&ccedil;&otilde;es no ve&iacute;culo todo &ndash; ao alcan&ccedil;ar velocidade m&eacute;dia de 60km/h.</p>\r\n<p>\r\n	Por&eacute;m, quando os pneus do eixo trativo e dos eixos auxiliares est&aacute; desbalanceado, &eacute; mais dif&iacute;cil perceber as vibra&ccedil;&otilde;es. Por este motivo &eacute; necess&aacute;rio balancear todas as rodas com frequ&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Quando o balanceamento deve ser feito?</strong></p>\r\n<p>\r\n	Sempre que houver substitui&ccedil;&atilde;o de pneus;</p>\r\n<p>\r\n	Sempre que for efetuado conserto em pneus ou c&acirc;maras;</p>\r\n<p>\r\n	Por ocasi&atilde;o de vibra&ccedil;&otilde;es no volante ou guid&atilde;o;</p>\r\n<p>\r\n	Sempre que houver substitui&ccedil;&atilde;o de elementos do conjunto rodante &ndash; por exemplo: pastilhas de freios, rolamento da roda, pe&ccedil;as da suspens&atilde;o, etc;</p>\r\n<p>\r\n	Para conjuntos de bicicletas ou motocicletas que tenham rodas com raios. Neste caso, verifique periodicamente a tens&atilde;o e o estados deles;</p>\r\n<p>\r\n	A cada 10 mil quil&ocirc;metros rodados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Aproveite que agora voc&ecirc; sabe mais sobre balanceamento e leve seu carro ou moto para a manuten&ccedil;&atilde;o!</strong></p>', '1404201711471240525988.png', 0, 'SIM', 'balanceamento'),
(14, 'CALIBRAGEM', '<p>\r\n	<strong>Manter seu pneu calibrado</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Motorista sob press&atilde;o: Sua aten&ccedil;&atilde;o &eacute; fundamental quando o assunto &eacute; calibragem.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Pode parecer um simples detalhe, daqueles que a gente s&oacute; lembra quando notamos alguma coisa errada. Mas a press&atilde;o dos pneus &eacute; importante n&atilde;o s&oacute; para evitar acidentes, como tamb&eacute;m para garantir mais economia para o seu bolso.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	E o segredo para evitar muitos problemas est&aacute; na aten&ccedil;&atilde;o do motorista, que deve tornar o cuidado com os pneus um h&aacute;bito mais frequente.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma recente pesquisa realizada pela Michelin em 2013, revelou n&uacute;meros que chamam a aten&ccedil;&atilde;o. Ao avaliar carros e caminhonetes em 5 cidades brasileiras, apenas 69% dos ve&iacute;culos tinham a press&atilde;o correta. E em 9% deles, a falta de calibragem adequada representava alto risco de acidentes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para se ter uma ideia, levando essa propor&ccedil;&atilde;o em considera&ccedil;&atilde;o, &eacute; poss&iacute;vel afirmar que o Brasil tem aproximadamente 3,3 milh&otilde;es de ve&iacute;culos com alto potencial de provocar acidentes. Com consequ&ecirc;ncias, inclusive, para outros ve&iacute;culos.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	N&atilde;o &eacute; &agrave; toa que nosso pa&iacute;s ocupa a triste marca de 4&ordm; lugar em todo o mundo em acidentes graves no tr&acirc;nsito.</p>\r\n<div>\r\n	&nbsp;</div>', '1404201711481128883418.png', 0, 'SIM', 'calibragem'),
(15, 'RODÍZIO DE PNEUS', '<p>\r\n	<strong>Como fazer o rod&iacute;zio dos pneus?</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para obter o comportamento ideal do ve&iacute;culo em curvas, nas freadas, nas pistas molhadas e nas situa&ccedil;&otilde;es de emerg&ecirc;ncia &eacute; importante que os quatro pneus em uso tenham um desgaste semelhante entre eles. Por&eacute;m, cada um dos pneus que est&aacute; rodando em um ve&iacute;culo ser&aacute; submetido a diferentes esfor&ccedil;os e, por consequ&ecirc;ncia, ter&aacute; desgastes diferentes. Por exemplo, o eixo de tra&ccedil;&atilde;o vai exigir mais do pneu que o outro eixo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ao longo do tempo essas diferen&ccedil;as de desgaste se acentuar&atilde;o e poder&atilde;o comprometer o equil&iacute;brio do ve&iacute;culo. Por isso, &eacute; importante realizar rod&iacute;zios peri&oacute;dicos, alternando a posi&ccedil;&atilde;o dos pneus no ve&iacute;culo e para se obter semelhantes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O rod&iacute;zio b&aacute;sico consiste em montar os pneus que estavam no eixo dianteiro na parte traseira, conservando o mesmo lado do ve&iacute;culo, independente do tipo do pneu, se ele &eacute; direcional, assim&eacute;trico, caminhonete, etc.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Por&eacute;m, em algumas situa&ccedil;&otilde;es espec&iacute;ficas, pode ser realizado um rod&iacute;zio em X, passando o pneu dianteiro direito para a posi&ccedil;&atilde;o traseira esquerda e vice-versa. Um exemplo da necessidade deste tipo de rod&iacute;zio &eacute; um ve&iacute;culo que fa&ccedil;a mais curvas para um lado que para outro em sua utiliza&ccedil;&atilde;o di&aacute;ria. Neste caso, os pneus de um lado se desgastar&atilde;o mais que o outro lado, e o rod&iacute;zio b&aacute;sico n&atilde;o ir&aacute; conseguir equilibrar os desgastes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&Eacute; necess&aacute;rio verificar o tipo de desgaste dos pneus para decidir o melhor rod&iacute;zio a fazer. O especialista em pneus presente nas lojas MICHELIN poder&aacute; orientar melhor sobre essas op&ccedil;&otilde;es de rod&iacute;zio.</p>', '1404201711481274437521.png', 0, 'SIM', 'rodÍzio-de-pneus'),
(16, 'SUSPENSÃO', '<p>\r\n	<strong>Suspens&atilde;o</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O sistema de suspens&atilde;o &eacute; respons&aacute;vel pela estabilidade do autom&oacute;vel e tem a fun&ccedil;&atilde;o de absorver todas as irregularidades do solo e evitar que trancos e solavancos incomodem os usu&aacute;rios. O sistema compreende o conjunto de todos os componentes mec&acirc;nicos que se articulam e unem as rodas do ve&iacute;culo &agrave; carroceira.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>H&aacute; cinco componentes essenciais no sistema de suspens&atilde;o, s&atilde;o eles:</strong></p>\r\n<p>\r\n	Molas</p>\r\n<p>\r\n	Amortecedores</p>\r\n<p>\r\n	Barras estabilizadoras</p>\r\n<p>\r\n	Pinos esf&eacute;ricos (piv&ocirc;s)</p>\r\n<p>\r\n	Bandejas de suspens&atilde;o</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Recomenda-se que a manuten&ccedil;&atilde;o da suspens&atilde;o seja feita regularmente ou quando surgir algum problema entre os intervalos de revis&atilde;o. Geralmente, esse intervalo acontece a cada 7.000 km, quando &eacute; sugerida uma avalia&ccedil;&atilde;o no balanceamento das rodas. Aproveite e fa&ccedil;a uma revis&atilde;o na suspens&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Um ve&iacute;culo sem molas e amortecedores &eacute; desconfort&aacute;vel para seus usu&aacute;rios, com vibra&ccedil;&otilde;es no carro e ru&iacute;dos no painel, principalmente em pisos irregulares. Fortes impactos podem ocasionar danos e ocasionar trincas na estrutura do autom&oacute;vel, comprometendo sua vida &uacute;til. Os amortecedores limitam oscila&ccedil;&otilde;es no ve&iacute;culo, tornado a dirigibilidade muito mais segura e est&aacute;vel. Contudo, a continuidade do trabalho provoca desgaste no amortecedor. Portanto, fique atento. A vida &uacute;til do amortecedor &eacute; longa, mas aos 40.000 km fa&ccedil;a uma revis&atilde;o preventiva. Caso necess&aacute;rio, providencia rapidamente sua substitui&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Al&eacute;m dos amortecedores, certifique-se que os seus acess&oacute;rios e componentes (coifas de prote&ccedil;&atilde;o da haste, batentes e os coxins) estejam em ordem, pois eles tamb&eacute;m sofrem desgastes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>O sistema de suspens&atilde;o influi diretamente no comportamento do ve&iacute;culo e pode provocar:</strong></p>\r\n<p>\r\n	Desgaste irregular dos pneus</p>\r\n<p>\r\n	Ru&iacute;dos indesej&aacute;veis</p>\r\n<p>\r\n	Descontrole do ve&iacute;culo</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Todo o cuidado &eacute; pouco! Dirija atenciosamente, preventivamente, e mantenha o seu ve&iacute;culo em boas condi&ccedil;&otilde;es de uso com Griffe Pneus, garantindo, assim, a sua seguran&ccedil;a e sua fam&iacute;lia</strong>.</p>', '1904201712361284610069.png', 0, 'SIM', 'suspensÃo'),
(17, 'REPARO ADEQUADO', '<p>\r\n	<strong>Recomenda&ccedil;&otilde;es na hora de reparar o pneu</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Qual a tecnologia e o material recomendado para a repara&ccedil;&atilde;o?</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Na hora de fazer o conserto dos pneus do seu carro, opte sempre pela tecnologia oferecida nas revendas MICHELIN, a pe&ccedil;a de repara&ccedil;&atilde;o dos pneus (PRP), tamb&eacute;m conhecida como o plug de repara&ccedil;&atilde;o, destinado aos reparos de danos de 3 a 6 mm em pneus com c&acirc;mara.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	No processo de repara&ccedil;&atilde;o adotado pela MICHELIN, o pneu &eacute; examinado em toda a sua estrutura, principalmente na parte interna, a fim de detectar danos que possam prejudicar a mobilidade do ve&iacute;culo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Essa tecnologia aplicada corretamente garante a veda&ccedil;&atilde;o total do furo, evitando a perda de ar durante a rodagem e garantindo a m&aacute;xima seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A qualidade e a efic&aacute;cia da repara&ccedil;&atilde;o &eacute; de inteira e exclusiva responsabilidade da empresa que a realizou.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A garantia do pneu reparado passa a ser responsabilidade da empresa que efetuou a repara&ccedil;&atilde;o.</p>', '1904201712371146856439.png', 0, 'SIM', 'reparo-adequado'),
(18, 'ALINHAMENTO', '<p>\r\n	<strong>A import&acirc;ncia do alinhamento</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Saiba quando e onde fazer o alinhamento dos pneus do carro</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Um aspecto muito importante que voc&ecirc; deve cuidar sempre na manuten&ccedil;&atilde;o do seu carro &eacute; o alinhamento. Ve&iacute;culos com &acirc;ngulos da suspens&atilde;o e dire&ccedil;&atilde;o incorretos podem apresentar problemas de comportamento e de seguran&ccedil;a.</p>\r\n<p>\r\n	Al&eacute;m disto, um alinhamento irregular causa um maior desgaste ao pneu e pode causar danos &agrave; suspens&atilde;o, dire&ccedil;&atilde;o e rodagem.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>O que &eacute; o alinhamento?</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Alinhamento &eacute; o processo de regulagem dos &acirc;ngulos da dire&ccedil;&atilde;o e suspens&atilde;o do ve&iacute;culo. Basicamente s&atilde;o tr&ecirc;s &acirc;ngulos a ser verificados no alinhamento: converg&ecirc;ncia/diverg&ecirc;ncia, c&acirc;mber e c&aacute;ster.</p>\r\n<p>\r\n	3 boas raz&otilde;es para fazer o alinhamento dos pneus constantemente</p>\r\n<p>\r\n	Voc&ecirc; economiza dinheiro, j&aacute; que seus pneus v&atilde;o durar mais;</p>\r\n<p>\r\n	A dire&ccedil;&atilde;o ficar&aacute; mais macia, pois haver&aacute; menor resist&ecirc;ncia de rolamento;</p>\r\n<p>\r\n	Voc&ecirc; ter&aacute; maior controle de dire&ccedil;&atilde;o, aumentando a seguran&ccedil;a do ve&iacute;culo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Quando fazer o alinhamento dos pneus?</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Em todas as revis&otilde;es peri&oacute;dicas estipuladas pelo fabricante do ve&iacute;culo ou pelo menos a cada 7000 quil&ocirc;metros;</p>\r\n<p>\r\n	Sempre ap&oacute;s um impacto forte contra buracos, pedras, guias ou outros objetos;</p>\r\n<p>\r\n	Sempre que houver a substitui&ccedil;&atilde;o de algum elemento da suspens&atilde;o ou da dire&ccedil;&atilde;o;</p>\r\n<p>\r\n	Toda vez que notar algum comportamento estranho no ve&iacute;culo, tendendo a ir mais para um lado ou com dificuldade de se manter na trajet&oacute;ria;</p>\r\n<p>\r\n	Quando forem verificados desgastes irregulares nos pneus;</p>\r\n<p>\r\n	Sempre que houver substitui&ccedil;&atilde;o de pneus.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Quanto tempo demora para fazer o alinhamento dos pneus?</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O processo &eacute; r&aacute;pido: a maioria dos alinhamentos na frente e atr&aacute;s demoram apenas 30 minutos!</p>', '1904201712381392393457.png', 0, 'SIM', 'alinhamento'),
(19, 'reparo galeria', '', '0905201704265474095071.jpg', 0, 'NAO', 'reparo-galeria');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_tipos_veiculos`
--

CREATE TABLE `tb_tipos_veiculos` (
  `idtipoveiculo` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descricao` longtext,
  `imagem` varchar(255) NOT NULL,
  `ordem` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_tipos_veiculos`
--

INSERT INTO `tb_tipos_veiculos` (`idtipoveiculo`, `titulo`, `descricao`, `imagem`, `ordem`, `ativo`, `url_amigavel`) VALUES
(11, 'CARRO', '', '1404201711551385879377.png', 0, 'SIM', 'carro'),
(12, 'MOTO', '', '1404201711561142586706.png', 0, 'SIM', 'moto'),
(13, 'CAMINHONETE', '', '1404201711571169892926.png', 0, 'SIM', 'caminhonete'),
(14, 'CAMINHÃO', '', '1404201711571114687468.png', 0, 'SIM', 'caminhao'),
(15, 'TRATORES', '', '1404201711581277238393.png', 0, 'SIM', 'tratores');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_unidades`
--

CREATE TABLE `tb_unidades` (
  `idunidade` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_place` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaunidade` int(11) NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `ddd1` varchar(10) CHARACTER SET latin1 NOT NULL,
  `ddd2` varchar(10) CHARACTER SET latin1 NOT NULL,
  `ddd3` varchar(10) CHARACTER SET latin1 NOT NULL,
  `ddd4` varchar(10) CHARACTER SET latin1 NOT NULL,
  `telefone1` varchar(255) CHARACTER SET latin1 NOT NULL,
  `telefone2` varchar(255) CHARACTER SET latin1 NOT NULL,
  `telefone3` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `telefone4` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `id_unidade` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_unidades`
--

INSERT INTO `tb_unidades` (`idunidade`, `titulo`, `endereco`, `src_place`, `id_categoriaunidade`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `ddd1`, `ddd2`, `ddd3`, `ddd4`, `telefone1`, `telefone2`, `telefone3`, `telefone4`, `id_unidade`) VALUES
(2, 'GOIÂNIA', 'RUA C-69 Nº 99 - JARDIM GOIÁS', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3821.378911958309!2d-49.236650785571996!3d-16.707935288489164!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef1abc70916d7%3A0x116e733ee7fce10a!2sGriffe+Pneus+Michellin+Jardim+Goi%C3%A1s+-+Flam', 103, 'SIM', NULL, 'goiÂnia', NULL, NULL, NULL, '(62)', '(62)', '', '', '3515-1111', '99646-7175', '', '', 0),
(45, 'GOIÂNIA', 'AV 85 Nº 3376 - SETOR BUENO', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30571.031415499987!2d-49.2519717537712!3d-16.70793454026747!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef1220d555557%3A0xfce1794c20456c2a!2sGriffe+Pneus+Michellin+Av.+85!5e0!3m2!1spt-BR!2sb', 103, 'SIM', NULL, 'goiÂnia', NULL, NULL, NULL, '(62)', '(62)', '', '', '3281-9111', '99902-1458', '', '', 0),
(46, 'GOIÂNIA', 'RUA 7 QUADRA F-5 LT.63 - SETOR OESTE', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30571.03165147966!2d-49.25197176543303!3d-16.707933066846977!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef15d5366d807%3A0xfa8e71558e087d65!2sGriffe+Pneus+Michellin+Setor+Oeste+-+Pra%C3%A7a+', 103, 'SIM', NULL, 'goiÂnia', NULL, NULL, NULL, '(62)', '(62)', '', '', '3093-3111', '99928-7973', '', '', 0),
(47, 'GOIÂNIA', 'AV. INDEPENDÊNCIA Nº 3778 - CENTRO', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30578.165051630618!2d-49.27616696044919!3d-16.663335399999994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef3c365cb79b5%3A0x1b37bfc2148998b7!2sGriffe+Pneus+Michellin+Av.+Independ%C3%AAncia!5', 103, 'SIM', NULL, 'goiÂnia', NULL, NULL, NULL, '(62)', '(62)', '', '', '3945-1111', '99826-4582', '', '', 0),
(48, 'GOIÂNIA', 'PÇ VALTER SANTOS Nº 242 - COIMBRA', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30571.032123438985!2d-49.25197178875669!3d-16.707930120006324!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef4008d273a37%3A0xb4fd1a4955d263fb!2sGriffe+Pneus+Michellin+St.+Coimbra+-+Pra%C3%A7a', 103, 'SIM', NULL, 'goiÂnia', NULL, NULL, NULL, '(62)', '(62)', '', '', '3086-3111', '99827-4026', '', '', 0),
(49, 'ANÁPOLIS', 'AV. BRASIL SUL Nº. 45 - JUNDIAÍ', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3828.8721810626525!2d-48.95215768557793!3d-16.32947398872055!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ea46357bfc721%3A0xaae7be11f01a9bf9!2sGriffe+Pneus+Michellin+An%C3%A1polis!5e0!3m2!1sp', 104, 'SIM', NULL, 'anÁpolis', NULL, NULL, NULL, '(62)', '(62)', '', '', '3329-0250', '99838-2680', '', '', 0),
(50, 'BRASÍLIA', 'SCRS Q.514 BL .B LJ. 69 TÉRREO - ASA SUL', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d122842.74407747782!2d-48.03605245765739!3d-15.812612275887385!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a3aaa689bac13%3A0x2014d8bb390cf0af!2sGriffe+Pneus+Michellin+514+Asa+Sul!5e0!3m2!1spt', 105, 'SIM', NULL, 'brasÍlia', NULL, NULL, NULL, '(61)', '(61)', '', '', '3245-6111', '99656-3872', '', '', 0),
(51, 'BRASÍLIA', 'QSD 23 LT 8 PISTÃO SUL - TAGUATINGA', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d122842.75770806175!2d-48.03605264512358!3d-15.81258982774235!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a328899ed8105%3A0xb67e7e8c867d5113!2sGriffe+Pneus+Michellin+Taguatinga!5e0!3m2!1spt-B', 105, 'SIM', NULL, 'brasÍlia', NULL, NULL, NULL, '(61)', '(61)', '', '', '3562-2222', '99656-5825', '', '', 0),
(52, 'BRASÍLIA', 'SIA TRECHO 2 LT. 770 LOJA  1 - GUARÁ', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d122842.77133860944!2d-48.03605283258983!3d-15.812567379625873!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a3052f42d790f%3A0x4a035a5dd673867b!2sGriffe+Pneus+Michellin+SIA!5e0!3m2!1spt-BR!2sbr', 105, 'SIM', NULL, 'brasÍlia', NULL, NULL, NULL, '(61)', '(61)', '', '', '3361-3111', '99938-8634', '', '', 0),
(53, 'BRASÍLIA', 'SHC/S CR QD 506 BL. A LJ. 54 - ASA SUL', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d122842.78496912096!2d-48.03605302005607!3d-15.81254493153797!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a3ac76fbb26df%3A0xb33a537aa71c7511!2sGriffe+Pneus+Michellin+506+Asa+Sul!5e0!3m2!1spt-', 105, 'SIM', NULL, 'brasÍlia', NULL, NULL, NULL, '(61)', '(61)', '', '', '3346-8111', '99682-0922', '', '', 0),
(54, 'BRASÍLIA', 'SEP NORTE Q.503 CJ. A BL. B - ASA NORTE', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d122842.79859959625!2d-48.036053207522265!3d-15.812522483478629!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a3aaa6700864b%3A0x3be37111561079e5!2sGriffe+Pneus+Michellin+503+Asa+Norte!5e0!3m2!1', 105, 'SIM', NULL, 'brasÍlia', NULL, NULL, NULL, '(61)', '(61)', '', '', '3425-3444', '99633-9365', '', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_unidades_categorias`
--

CREATE TABLE `tb_unidades_categorias` (
  `id` int(11) NOT NULL,
  `id_unidade` int(11) NOT NULL,
  `id_categoriaunidade` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(11) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_uniformes`
--

CREATE TABLE `tb_uniformes` (
  `iduniforme` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `video_youtube` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_uniformes`
--

INSERT INTO `tb_uniformes` (`iduniforme`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `video_youtube`) VALUES
(1, 'Uniformes de Saúdes', '2409201605381218447454.png', '<p>\r\n	Port&atilde;o autom&aacute;tico basculante articulado&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lider Portão automático basculante articulado', 'keywords Portão automático basculante articulado', 'description Portão automático basculante articulado', 'SIM', NULL, 'uniformes-de-saudes', 'https://www.youtube.com/embed/BbagKCkzrWs'),
(8, 'Uniformes de Limpeza geral', '2409201605411332434728.png', NULL, '', '', '', 'SIM', NULL, 'uniformes-de-limpeza-geral', NULL),
(9, 'Uniformes Cozinheiros', '2409201605401358107866.png', NULL, '', '', '', 'SIM', NULL, 'uniformes-cozinheiros', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_usuarios`
--

CREATE TABLE `tb_usuarios` (
  `idusuario` int(10) UNSIGNED NOT NULL,
  `nome` varchar(245) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `cpf` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `rg` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `numero` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `complemento` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `bairro` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `uf` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `tel_celular` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `tel_residencial` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_usuarios`
--

INSERT INTO `tb_usuarios` (`idusuario`, `nome`, `email`, `senha`, `cpf`, `rg`, `endereco`, `numero`, `complemento`, `bairro`, `cidade`, `uf`, `tel_celular`, `tel_residencial`, `ativo`, `ordem`, `url_amigavel`) VALUES
(7, 'Marcio André da Silva', 'marcio@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '917.639.161-20', '1905364', 'Quadra 204 Conjunto 17 Lote', '08', 'Casa 2', 'Recanto das Emas', 'Brasília', 'DF', '(61) 8606-6484', '(61) 3456-7864', 'SIM', 0, 'marcio-andre-da-silva'),
(8, 'Marcio', 'marciomas@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '917.639.161-20', '8272916', 'Quadra 204 conjunto 20 lote', '1', '', 'Recanto das Emas', 'Brasília', 'DF', '(61) 8539-8898', '(61) 3334-0987', 'SIM', 0, 'marcio'),
(9, 'MOBILE', '', 'cb7e7bca423a4b5a591a2c5bf49c688b', '697834785', '7548675', 'MOBILE', '8', 'MOBILE', 'MOBILE', 'MOBILE', 'MO', '9765341867', '4581236714', 'SIM', 0, 'mobile'),
(10, 'Joana Dark', 'joana@masmidia.com.br', 'd41d8cd98f00b204e9800998ecf8427e', '917.639.161-20', '1905361', 'Quadra 204 conjunto 17 lote', '8', 'casa 2', 'Recanto das Emas', 'Brasília', 'DF', '(61) 8604-9092', '(61) 3847-4939', 'SIM', 0, 'joana-dark'),
(11, 'Fabiana', 'fabiana@masmidia.com.br', 'd41d8cd98f00b204e9800998ecf8427e', '917.639.161-20', '1896788', 'Quadra 200 lote', '8', 'apt 101', 'recanto das emas', 'brasilia', 'DF', '(61) 9837-8787', '(61) 8308-9221', 'SIM', 0, 'fabiana'),
(12, 'fabio', 'fabio@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '917.639.161-20', '9128937981', 'quadra 200 lote', '4', 'apt 5', 'recanto das emas', 'brasíliad', 'DF', '(61) 9373-8287', '(51) 8189-3637', 'SIM', 0, 'fabio'),
(13, 'mobile', 'mobile@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '576576576', '576576', 'quadra 204', '8', 'casa 1', 'recanto das emas', 'brasília', 'df', '7861872358761', '675126785673', 'SIM', 0, 'mobile'),
(14, 'User14', 'user14@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'Quadra 200 conjunto 20', '5', 'casa 1', 'Recanto das Emas', 'Brasília', 'DF', '8765-0987', '3332-0987', 'SIM', 0, 'user14'),
(15, 'Homeweb', 'homewebbrasil@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'Quadra 100', '8', 'casa 3', 'asa sul', 'brasília', 'df', '8765-0989', '3437-9876', 'SIM', 0, 'homeweb'),
(16, 'Terezinha Aparecida', 'terezinha@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'Quadra 200 lote', '10', 'casa 5', 'Recanto das Emas', 'Brasília', 'DF', '91069282', '3423-0987', 'SIM', 0, 'terezinha-aparecida'),
(17, 'Miguel José', 'miguel@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'Quadra 106 conjunto 20 lote', '10', 'apt 202', 'Recanto das Emas', 'Brasília', 'DF', '8876-0988', '3561-0987', 'SIM', 0, 'miguel-jose'),
(18, 'JOANA SILVA', 'joanasilva@masmidia.com.br', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'QUADRA 100 LOTE', '34', 'CASA 2', 'RECANTO DAS EMAS', 'BRASÍLIA', 'DF', '8765-0998', '3456-0098', 'SIM', 0, 'joana-silva'),
(19, 'Bianca Barros', 'biancabarros1@hotmail.com', '729987adc7a9936a7ae9ba744291e35d', '', '', 'RUA SACRAMENTO', '0', 'QD 35, LT 20', 'CARDOSO', 'APARECIDA DE  GOIANIA', '74', '96871100', '62 35186845', 'SIM', 0, 'bianca-barros'),
(20, 'HOMEWEB TESTE', 'atendimento.sites@homewebbrasil.com.br', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'ENDEREÇO DO CONTATO', 'TESTE', 'COMPLEMENTO', 'TESTE', 'TESTE', 'GO', '(22) 2222-2222', '(22) 2222-2222', 'SIM', 0, 'homeweb-teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_vendas`
--

CREATE TABLE `tb_vendas` (
  `idvenda` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `mensagem_cartao` longtext COLLATE utf8_unicode_ci,
  `id_frete` int(11) DEFAULT NULL,
  `endereco_entrega` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numero_entrega` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `complemento_entrega` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nome_contato` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone_contato` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_venda` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'AGUARDANDO PAGAMENTO',
  `ponto_referencia` longtext CHARACTER SET utf8,
  `observacoes` longtext COLLATE utf8_unicode_ci,
  `horario_entrega` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `msg_email` longtext COLLATE utf8_unicode_ci NOT NULL,
  `msg_deposito` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bairro_entrega` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade_entrega` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cep_entrega` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_pagamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor_frete` double NOT NULL,
  `total` double NOT NULL,
  `data_pagamento` date NOT NULL,
  `data_separacao_entrega` date NOT NULL,
  `data_entrega` date NOT NULL,
  `data_entrega_cliente` date NOT NULL,
  `celular_contato` varchar(80) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_vendas_produtos`
--

CREATE TABLE `tb_vendas_produtos` (
  `id_venda` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `valor` double DEFAULT NULL,
  `qtd` int(11) DEFAULT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_atuacoes`
--
ALTER TABLE `tb_atuacoes`
  ADD PRIMARY KEY (`idatuacao`);

--
-- Indexes for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  ADD PRIMARY KEY (`idavaliacaoproduto`);

--
-- Indexes for table `tb_banners`
--
ALTER TABLE `tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- Indexes for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  ADD PRIMARY KEY (`idbannerinterna`);

--
-- Indexes for table `tb_categorias_mensagens`
--
ALTER TABLE `tb_categorias_mensagens`
  ADD PRIMARY KEY (`idcategoriamensagem`);

--
-- Indexes for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  ADD PRIMARY KEY (`idcategoriaproduto`);

--
-- Indexes for table `tb_categorias_unidades`
--
ALTER TABLE `tb_categorias_unidades`
  ADD PRIMARY KEY (`idcategoriaunidade`);

--
-- Indexes for table `tb_clientes`
--
ALTER TABLE `tb_clientes`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indexes for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  ADD PRIMARY KEY (`idcomentariodica`);

--
-- Indexes for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  ADD PRIMARY KEY (`idcomentarioproduto`);

--
-- Indexes for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  ADD PRIMARY KEY (`idconfiguracao`);

--
-- Indexes for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  ADD PRIMARY KEY (`iddepoimento`);

--
-- Indexes for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  ADD PRIMARY KEY (`iddica`);

--
-- Indexes for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indexes for table `tb_equipamentos`
--
ALTER TABLE `tb_equipamentos`
  ADD PRIMARY KEY (`idequipamento`);

--
-- Indexes for table `tb_equipes`
--
ALTER TABLE `tb_equipes`
  ADD PRIMARY KEY (`idequipe`);

--
-- Indexes for table `tb_especificacoes`
--
ALTER TABLE `tb_especificacoes`
  ADD PRIMARY KEY (`idespecificacao`);

--
-- Indexes for table `tb_facebook`
--
ALTER TABLE `tb_facebook`
  ADD PRIMARY KEY (`idface`);

--
-- Indexes for table `tb_fretes`
--
ALTER TABLE `tb_fretes`
  ADD PRIMARY KEY (`idfrete`);

--
-- Indexes for table `tb_galerias`
--
ALTER TABLE `tb_galerias`
  ADD PRIMARY KEY (`idgaleria`);

--
-- Indexes for table `tb_galerias_equipamentos`
--
ALTER TABLE `tb_galerias_equipamentos`
  ADD PRIMARY KEY (`id_galeriaequipamento`);

--
-- Indexes for table `tb_galerias_geral`
--
ALTER TABLE `tb_galerias_geral`
  ADD PRIMARY KEY (`id_galeriageral`);

--
-- Indexes for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  ADD PRIMARY KEY (`idgaleriaportifolio`);

--
-- Indexes for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  ADD PRIMARY KEY (`id_galeriaproduto`);

--
-- Indexes for table `tb_galerias_servicos`
--
ALTER TABLE `tb_galerias_servicos`
  ADD PRIMARY KEY (`id_galeriaservico`);

--
-- Indexes for table `tb_galerias_uniformes`
--
ALTER TABLE `tb_galerias_uniformes`
  ADD PRIMARY KEY (`id_galeriauniformes`);

--
-- Indexes for table `tb_galeria_empresa`
--
ALTER TABLE `tb_galeria_empresa`
  ADD PRIMARY KEY (`idgaleriaempresa`);

--
-- Indexes for table `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`idlogin`);

--
-- Indexes for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  ADD PRIMARY KEY (`idloglogin`);

--
-- Indexes for table `tb_lojas`
--
ALTER TABLE `tb_lojas`
  ADD PRIMARY KEY (`idloja`);

--
-- Indexes for table `tb_marcas_veiculos`
--
ALTER TABLE `tb_marcas_veiculos`
  ADD PRIMARY KEY (`idmarcaveiculo`);

--
-- Indexes for table `tb_modelos_veiculos`
--
ALTER TABLE `tb_modelos_veiculos`
  ADD PRIMARY KEY (`idmodeloveiculo`);

--
-- Indexes for table `tb_noticias`
--
ALTER TABLE `tb_noticias`
  ADD PRIMARY KEY (`idnoticia`);

--
-- Indexes for table `tb_parceiros`
--
ALTER TABLE `tb_parceiros`
  ADD PRIMARY KEY (`idparceiro`);

--
-- Indexes for table `tb_portfolios`
--
ALTER TABLE `tb_portfolios`
  ADD PRIMARY KEY (`idportfolio`);

--
-- Indexes for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`idproduto`);

--
-- Indexes for table `tb_produtos_aplicacoes`
--
ALTER TABLE `tb_produtos_aplicacoes`
  ADD PRIMARY KEY (`idprodutoaplicacao`);

--
-- Indexes for table `tb_produtos_categorias`
--
ALTER TABLE `tb_produtos_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_produtos_modelos_aceitos`
--
ALTER TABLE `tb_produtos_modelos_aceitos`
  ADD PRIMARY KEY (`idprodutosmodeloaceito`);

--
-- Indexes for table `tb_promocoes`
--
ALTER TABLE `tb_promocoes`
  ADD PRIMARY KEY (`idpromocao`);

--
-- Indexes for table `tb_seo`
--
ALTER TABLE `tb_seo`
  ADD PRIMARY KEY (`idseo`);

--
-- Indexes for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  ADD PRIMARY KEY (`idservico`);

--
-- Indexes for table `tb_subcategorias_produtos`
--
ALTER TABLE `tb_subcategorias_produtos`
  ADD PRIMARY KEY (`idsubcategoriaproduto`);

--
-- Indexes for table `tb_subcategorias_unidades`
--
ALTER TABLE `tb_subcategorias_unidades`
  ADD PRIMARY KEY (`idsubcategoriaunidade`);

--
-- Indexes for table `tb_sugestoes_mensagens`
--
ALTER TABLE `tb_sugestoes_mensagens`
  ADD PRIMARY KEY (`idsugestaomensagem`);

--
-- Indexes for table `tb_tipos_servicos`
--
ALTER TABLE `tb_tipos_servicos`
  ADD PRIMARY KEY (`idtiposervico`);

--
-- Indexes for table `tb_tipos_veiculos`
--
ALTER TABLE `tb_tipos_veiculos`
  ADD PRIMARY KEY (`idtipoveiculo`);

--
-- Indexes for table `tb_unidades`
--
ALTER TABLE `tb_unidades`
  ADD PRIMARY KEY (`idunidade`);

--
-- Indexes for table `tb_unidades_categorias`
--
ALTER TABLE `tb_unidades_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_uniformes`
--
ALTER TABLE `tb_uniformes`
  ADD PRIMARY KEY (`iduniforme`);

--
-- Indexes for table `tb_usuarios`
--
ALTER TABLE `tb_usuarios`
  ADD PRIMARY KEY (`idusuario`);

--
-- Indexes for table `tb_vendas`
--
ALTER TABLE `tb_vendas`
  ADD PRIMARY KEY (`idvenda`);

--
-- Indexes for table `tb_vendas_produtos`
--
ALTER TABLE `tb_vendas_produtos`
  ADD PRIMARY KEY (`id_venda`,`id_produto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_atuacoes`
--
ALTER TABLE `tb_atuacoes`
  MODIFY `idatuacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;
--
-- AUTO_INCREMENT for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  MODIFY `idavaliacaoproduto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_banners`
--
ALTER TABLE `tb_banners`
  MODIFY `idbanner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  MODIFY `idbannerinterna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tb_categorias_mensagens`
--
ALTER TABLE `tb_categorias_mensagens`
  MODIFY `idcategoriamensagem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  MODIFY `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `tb_categorias_unidades`
--
ALTER TABLE `tb_categorias_unidades`
  MODIFY `idcategoriaunidade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `tb_clientes`
--
ALTER TABLE `tb_clientes`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  MODIFY `idcomentariodica` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  MODIFY `idcomentarioproduto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  MODIFY `idconfiguracao` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  MODIFY `iddepoimento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  MODIFY `iddica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  MODIFY `idempresa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_equipamentos`
--
ALTER TABLE `tb_equipamentos`
  MODIFY `idequipamento` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tb_equipes`
--
ALTER TABLE `tb_equipes`
  MODIFY `idequipe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_especificacoes`
--
ALTER TABLE `tb_especificacoes`
  MODIFY `idespecificacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_facebook`
--
ALTER TABLE `tb_facebook`
  MODIFY `idface` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `tb_fretes`
--
ALTER TABLE `tb_fretes`
  MODIFY `idfrete` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_galerias`
--
ALTER TABLE `tb_galerias`
  MODIFY `idgaleria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `tb_galerias_equipamentos`
--
ALTER TABLE `tb_galerias_equipamentos`
  MODIFY `id_galeriaequipamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=326;
--
-- AUTO_INCREMENT for table `tb_galerias_geral`
--
ALTER TABLE `tb_galerias_geral`
  MODIFY `id_galeriageral` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;
--
-- AUTO_INCREMENT for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  MODIFY `idgaleriaportifolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  MODIFY `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=412;
--
-- AUTO_INCREMENT for table `tb_galerias_servicos`
--
ALTER TABLE `tb_galerias_servicos`
  MODIFY `id_galeriaservico` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_galerias_uniformes`
--
ALTER TABLE `tb_galerias_uniformes`
  MODIFY `id_galeriauniformes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `tb_galeria_empresa`
--
ALTER TABLE `tb_galeria_empresa`
  MODIFY `idgaleriaempresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  MODIFY `idloglogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1869;
--
-- AUTO_INCREMENT for table `tb_lojas`
--
ALTER TABLE `tb_lojas`
  MODIFY `idloja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_marcas_veiculos`
--
ALTER TABLE `tb_marcas_veiculos`
  MODIFY `idmarcaveiculo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;
--
-- AUTO_INCREMENT for table `tb_modelos_veiculos`
--
ALTER TABLE `tb_modelos_veiculos`
  MODIFY `idmodeloveiculo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_noticias`
--
ALTER TABLE `tb_noticias`
  MODIFY `idnoticia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `tb_parceiros`
--
ALTER TABLE `tb_parceiros`
  MODIFY `idparceiro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_portfolios`
--
ALTER TABLE `tb_portfolios`
  MODIFY `idportfolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `idproduto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=312;
--
-- AUTO_INCREMENT for table `tb_produtos_aplicacoes`
--
ALTER TABLE `tb_produtos_aplicacoes`
  MODIFY `idprodutoaplicacao` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_produtos_categorias`
--
ALTER TABLE `tb_produtos_categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tb_produtos_modelos_aceitos`
--
ALTER TABLE `tb_produtos_modelos_aceitos`
  MODIFY `idprodutosmodeloaceito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT for table `tb_promocoes`
--
ALTER TABLE `tb_promocoes`
  MODIFY `idpromocao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `tb_seo`
--
ALTER TABLE `tb_seo`
  MODIFY `idseo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  MODIFY `idservico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `tb_subcategorias_produtos`
--
ALTER TABLE `tb_subcategorias_produtos`
  MODIFY `idsubcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_subcategorias_unidades`
--
ALTER TABLE `tb_subcategorias_unidades`
  MODIFY `idsubcategoriaunidade` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_sugestoes_mensagens`
--
ALTER TABLE `tb_sugestoes_mensagens`
  MODIFY `idsugestaomensagem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_tipos_servicos`
--
ALTER TABLE `tb_tipos_servicos`
  MODIFY `idtiposervico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tb_tipos_veiculos`
--
ALTER TABLE `tb_tipos_veiculos`
  MODIFY `idtipoveiculo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tb_unidades`
--
ALTER TABLE `tb_unidades`
  MODIFY `idunidade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `tb_unidades_categorias`
--
ALTER TABLE `tb_unidades_categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_uniformes`
--
ALTER TABLE `tb_uniformes`
  MODIFY `iduniforme` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_usuarios`
--
ALTER TABLE `tb_usuarios`
  MODIFY `idusuario` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tb_vendas`
--
ALTER TABLE `tb_vendas`
  MODIFY `idvenda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tb_vendas_produtos`
--
ALTER TABLE `tb_vendas_produtos`
  MODIFY `id_venda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
